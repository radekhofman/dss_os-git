# DSS Cherrypy App Template

## Features
- Configured for OpenShift or standalone local deployment
- Working static files configuration
- Configured user session support
- Configured mako template engine
- Various util functions providing abstraction over current deployment

## Installation

### Prerequisites

- OpenShift gear with Python 2.6 or 2.7 cartridge
- Installed cherrypy,mako

		ssh <your connection string to OpenShift gear>
		pip install cherrypy
		pip install mako

### First Time Deploy

	cd projectapplication/directory
	git init
	git add .
	git commit . -m "Initial deploy commit"
	# Replace git repo url with your own application repo
	git git push ssh://52d8d8cd4382ec344a000082@python-krablak.rhcloud.com/~/git/python.git/ --force
	
### Repeated Deploy
Just push your changes to application git repo in usual way.

### Standalone/Development Run
This mode is intended to be used mainly for **local development**. For production use the application configuration needs significant changes according to cherrypy official documentation. 

- Install all required libraries
- Run *application* file like python executable

		cd projectapplication/directory
		python apllication
	

	
