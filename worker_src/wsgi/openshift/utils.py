#!/usr/bin/env python
# -*- encoding: UTF-8 -*-
"""
Provides various utility functions related to OpenShift environment.
"""
import tempfile
import os

import openshift.variables as os_vars


def deployed_on_openshift():
    """
    Returns true in case that application is deployed and running on Openshift.
    """
    return os_vars.OPENSHIFT_APP_DNS and os_vars.OPENSHIFT_APP_UUID


def get_tempdir():
    """
    Returns path to temporary directory in respect to current deployment.
    """
    return os_vars.OPENSHIFT_TMP_DIR if deployed_on_openshift() else  tempfile.gettempdir()

def get_templates_dir(templates_dir_name=u"html_templates"):
    """
    Returns path to directory with respect to current deployment.
    """
    root_dir_path = None
    if deployed_on_openshift():
        root_dir_path = os.path.join(os_vars.OPENSHIFT_REPO_DIR,"wsgi")
    else:
        root_dir_path = os.path.abspath(".")
    return os.path.join(root_dir_path,templates_dir_name)