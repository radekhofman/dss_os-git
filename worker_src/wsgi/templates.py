#!/usr/bin/env python
# -*- encoding: UTF-8 -*-
"""
Provides various functions related to mako templating.
"""
import os
from mako.lookup import TemplateLookup

import openshift.variables as os_vars
import openshift.utils as os_utils
HTML_TEMPLATES_DIR = u"html_templates"
HTML_TEMPLATES_PATH = os.path.join(os_vars.OPENSHIFT_REPO_DIR,"wsgi",HTML_TEMPLATES_DIR) if os_utils.deployed_on_openshift() else HTML_TEMPLATES_DIR
lookup = TemplateLookup(directories=[HTML_TEMPLATES_PATH])

def get_template(template_name):
    """
    Returns template for given name..
    """
    return lookup.get_template(template_name)
