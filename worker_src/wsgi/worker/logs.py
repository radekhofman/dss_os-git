# -*- encoding: UTF-8 -*-
import time
import webservices


new_logs = [] #we only send new logs which are concatenated to the already existing

def print_task(task):
    print "\n=============================="
    print "Solving task:", task["username"], task["description"]  
    print "=============================="

def log(line, t="info"):
    """
    adds a log line to the array new_logs
    t - log type
    """
    global new_logs
    new_logs.append(time.strftime("[%d-%m-%Y %H:%M:%S]", time.localtime(time.time()))+" ["+t+"]"+" - "+line)
    
def plog(line): #log and pring
    """
    adds a log line to the array new_logs and print in to console
    """
    
    log(line)
    print line

def worker_log(username, md5_pass, worker_dict, url, worker_state, worker_resources, worker_task_percentage):
    """
    logs to file and to db on server
    lines in new_logs will be metged with those already logged either on the server and on the local machime
    """
    global new_logs
    #logging to file on local machine
    #here we log only loglines
    #TO BE DONE
    ####+++++++++++++++++++++++++++++++++++++++++++
    
    #logging to workers log on the server
    #here we save everything - resources etc...
    worker_dict["worker_log"] = new_logs
    worker_dict["worker_resources"] = worker_resources
    worker_dict["worker_state"] = worker_state
    worker_dict["worker_task_percentage"] = worker_task_percentage
    
    try:
        webservices.update_worker_status(username, md5_pass, url, worker_dict)
        #cleaning the log
        new_logs = []
    except Exception, e:
        print "Worker update failed!!!", e
    #returning modified - updated - worker_dict
    return worker_dict
    
    #clean temporal global variable new_logs
    new_logs = []