# -*- encoding: UTF-8 -*-
import urllib
import cPickle
import simplejson as sj

WORKER_TASK_CONNECTOR_NAME = "get_task"
WORKER_STATUS_UPDATE_CONNECTOR_NAME = "update_worker_status"
WORKER_TASK_RESULTS_CONNECTOR_NAME = "insert_results"
WORKER_TASK_UPDATE_CONNECTOR_NAME = "update_task_status"

#call webservice for free task
def get_free_task(username, md5_pass, worker_dict, url):
    """
    fetches free task from server according to the worker_type and its dedication
    """
    ws_url = url+WORKER_TASK_CONNECTOR_NAME #handlers of methods...
    worker_dict_pckl = cPickle.dumps(worker_dict)
    worker_dict_json = sj.dumps({"worker_dict_pckl": worker_dict_pckl})
    params = urllib.urlencode({'username': username,
                               'md5_pass': md5_pass,
                               'worker_dict': worker_dict_json
                               })
    
    res = urllib.urlopen(ws_url, params).read()
    return res

#calls webservice for update of workers status
def update_worker_status(username, md5_pass, url, worker_dict):
    """
    updates status of the worker on the server in db_workers
    worker is identified with its unique username
    worker_data = {"new_logs": new_logs,
                   "worker_resources": worker_resources,
                   "worker_status": worker_status,
                   "worker_task_percentage": worker_task_percentage
                   }
    
    """
    ws_url = url+WORKER_STATUS_UPDATE_CONNECTOR_NAME
    #worker_data_pckl = cPickle.dumps(worker_data)
    #worker_data_json = sj.dumps({"worker_dict_pckl": worker_data_pckl})
    worker_dict_pckl = cPickle.dumps(worker_dict)
    worker_dict_json = sj.dumps({"worker_dict_pckl": worker_dict_pckl})
    params = urllib.urlencode({'username': username,
                               'md5_pass': md5_pass,
                               #'worker_data': worker_data_json,
                               'worker_dict': worker_dict_json
                               })
    
    urllib.urlopen(ws_url, params)   
    #returns nothing
    
#calls webservice consuming output of the worker for particular task
def insert_task_results(username, md5_pass, task, url):
    """
    inserts outpu of the task into DB
    """
    ws_url = url+WORKER_TASK_RESULTS_CONNECTOR_NAME #handlers of methods...
    task_pckl = cPickle.dumps(task)
    task_json = sj.dumps({"task_pckl": task_pckl})
    params = urllib.urlencode({   'username': username,
                                  'md5_pass': md5_pass,
                                 'task_json': task_json
                               })
     
    return urllib.urlopen(ws_url, params).read()  

def update_task_status(url):
    """
    updates status of a task in tasks' DB - percentage and/or task's log
    """      
    ws_url = url+WORKER_TASK_UPDATE_CONNECTOR_NAME #handlers of methods...

