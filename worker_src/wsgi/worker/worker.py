#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import simplejson as sj

import cPickle
from importlib import import_module
import md5
import platform
import socket
import sys
import threading
import time
import puff.run_puff

import ip
from logs import plog, print_task, worker_log
import webservices

import puff.ppe
import puff.puff_tools

#CONSTANTS
#urls to webservices...
URL_DEVEL = 'http://localhost:8080/services/'
URL_PROD = 'http://dss-pyjunkies.rhcloud.com/services/' #dss.utia.cas.cz
DEFAULT_PING_INTERVAL = 1 #in seconds

#global variables, should be done better
worker_resources = {}
worker_task_percentage = 0.

def create_worker_dictionary(id0, username, worker_type, worker_dedicated_to):
    """
    returns dictionary of worker's properties accordint to model from db_models in db_models.py on server side     
    """
    info = socket.gethostbyname_ex(socket.gethostname())

    d = {"worker_username": username,
         "worker_id": id0, #not used, unique should be worker_username identifiing particulate worker
         "worker_ip": ip.get_lan_ip(),
         "worker_machine_name": info[0], #domanin name of the workers physical machine, multiple workers can share one maschime
         "worker_creation": time.time(), #time of worker's operation start
         "worker_type": worker_type, #type of the worker, 2 - task type = Puff
         "worker_last_ping": None, #set on the server - the time of last worker's query on a free task 
         "worker_ping_interval": None, #set on the server using "worker_last_ping"
         "worker_DEFAULT_PING_INTERVAL": DEFAULT_PING_INTERVAL, #real ping interval set in the worker
         "worker_state": 0, #state of a worker: 0 - idle, 1 - computing, 2 - ??down?? - whatever
         "worker_dedicated": worker_dedicated_to, #is worker dedicatd to a particular array of users? None if not, usergroup otehrwise
         #following proprties are related to platform
         "worker_architecture": platform.architecture(), #workers platform.architecture()
         "worker_node": platform.node(), 
         "worker_platform": platform.platform(),
         "worker_machine": platform.machine(),
         "worker_processor": platform.processor(),
         "worker_system": platform.system(),
         "worker_version": platform.version(),
         "worker_log": [], #array of lines
         "worker_resources": {}, #dictionary of workers resources
         "worker_task_percentage": 0
        }
    return d


#loop updating worker's state on the server, fetching and computing tasks    
def run_worker(id0, DEFAULT_PING_INTERVAL, username, md5_pass, worker_dict, url):
    """
    runs the basic loop
    """
    plog( "Starting worker %s" % username)

    print "starting worker"
    #runs in a loop
    worker_state = 0 #state of a worker: 0 - idle, 1 - computing, 2 - ??down?? - whatever
    task_thread = None

    global worker_task_percentage
    global worker_resources

    while True: #just for the loop
        time.sleep(DEFAULT_PING_INTERVAL)

        if (worker_state == 1): #worker is busy, waiting for idle state...
            #testing whether the task is done
            if (task_thread.isAlive()):
                print "Worker busy, waiting for current task to finish"
                #fetch percentage and task logs???
            else:
                worker_task_percentage = 100.
                worker_state = 0
                plog( "Task is done, setting worker to idle...")

        elif (worker_state == 0): #worker is idle, trying to fetch new task
            print "Fetching free tasks..."
            task = None

            
            try:
            
                #fetching new taks from a webservice
                #unJSONing and uncPickling to dictionary
                task_json = sj.loads(webservices.get_free_task(username, md5_pass, worker_dict, url))
                task_pckl = task_json["task_pckl"]
                task = cPickle.loads(task_pckl)
                
                
    
                if task: 
                    plog( "Task fetched") 
                    #task not None - there is a free task to solve, from db_tools already marked as "under solution"  
                    worker_state = 1 #setting worker state to busy 
                    worker_task_percentage = 0. #zeriong worker's task percentage
    
                    task_thread = threading.Thread(name='task_thread', target=compute_task, args=(task, worker_dict, username, md5_pass, url))
                    task_thread.start()
                    #compute_task(task, worker_dict)
    
            
            except Exception, e:
                import traceback
                traceback.print_exc()
                msg0 = "Task fetch failed!!!" + str(e)  
                plog(msg0)
            
        #we update worker's log
        #new line in new_logsm resources and task percentage
        #up to now, resources and task percentages ae not used
        worker_dict = worker_log(username, md5_pass, worker_dict, url, worker_state, worker_resources, worker_task_percentage)               



def compute_task(task, worker_dict, username, md5_pass, url):
    """
    computation of the task
    """
    print_task(task)
    task_input = task["task_input"]
    
    try:
        
        output = puff.run_puff.run_puff(task_input)
        plog( "Saving outputs....")

        #we update task with data regarding computatino and with the output of the computations
        task["task_output"] = output #settin output of task
        task["task_status"] = 2 #setting task status to "successfully solved" state
        task["end_of_computation"] = time.time()
        task["computation_time"] = task["end_of_computation"] - task["start_of_computation"]

        plog( "Size of output: %f" % sys.getsizeof(output))
        plog( "Size of whole task: %f" % sys.getsizeof(task))

    
    except Exception, e:
        print "ERROR", e
        task["task_status"] = 3 #setting task status to "computational error" state

    finally:
        #finally we send result to the server
        if (sj.loads(webservices.insert_task_results(username, md5_pass, task, url))["res"] == 1):
            plog( "OK, task results inserted to DB")
        else:
            plog( "Task results not inserted to DB!!!")
    
def main():
    """
    main method...
    """
    if (len(sys.argv) == 5):  # first arg is script filename itself
        id0 = int(sys.argv[1])
        username = sys.argv[2]
        password = sys.argv[3]
        typer = sys.argv[4] #devel or prod...
        #md5 hash of password boradcasted over internet
        md5_pass = md5.md5(password).hexdigest()

        try:
            urls = {'devel': URL_DEVEL, 'prod': URL_PROD}
            url = urls[typer]  # raises descriptive TODO
        except KeyError:
            raise ValueError("'{0}' not understood for deployment type, use one of {1}".format(
                typer, urls.keys()))

        worker_type = 2 #PUFF WORKER
        worker_dedicated_to = None #usergroup - the worker is dedicated to users belonging to this usergroup 

        worker_dict = create_worker_dictionary(id0, username, worker_type, worker_dedicated_to)

        run_worker(id0, DEFAULT_PING_INTERVAL, username, md5_pass, worker_dict, url)

    else:
        print "Usage: {0} <id> <username> <password> <task> <deployment>".format(sys.argv[0])
        print
        print "  id            identify with this numerical id when talking to server"
        print "  username      use this login name when speaking to server"
        print "  password      authenticate with this password when speaking to server"
        print "  deployment    'devel' or 'prod', for development and production respectively"
        sys.exit(1)  # error return code


if __name__ == "__main__":
    main()
