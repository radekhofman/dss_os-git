import propagate_puff as pp
import numpy
from puff_tools import Puff

"""
    #0 - id,
    #1 - Q,
    #2 - wind_a,
    #3 - wind_b,
    #4 - wind_c,
    #5 - pcategory #deprecated
    #6 - x0,
    #7 - y0,
    #8 - H,
    #9 - tot_d,
    #10 - tot_t,
    #11 - dd_total,
    #12 - rd_total,
    #13 - sigmaxy,
    #14 - sigmaz
    #15 - lambda_decay
    #16 - vG
"""

#tady jeste neni nogil, takze muzu pouzivat pythoni metody
#
def parallel_puff(j, total_time_global, models, receptors,
    doseA, doseB, doseMJU, doseMJUaE, doseCb, dose_integration_time,
    dose_sub_step_no, n, METEO_FORECAST, obs_meteo_flag=False,
    METEO_OBS_WS=None, METEO_OBS_WD=None):
    
    #print "models: ", models.shape
    
    pN = models.shape[1]
    mC = models.shape[0]
    rC = receptors.shape[0]

    tt = int(total_time_global / dose_integration_time)
    t_meteo = int(total_time_global / 60.0 / 60.0) #v jake jsme hodine


    #udelat double[:,:,:] DISP_MODELS, int[:] STABS,
    STABS = numpy.ones((mC,pN), dtype=numpy.int32)
    DISP_MODELS = numpy.zeros((mC, pN, rC+17)) #, dtype=numpy.double)
    #udelame z models velky pole
    
    for i in range(mC):
        for j in range(pN):
            puff = models[i,j]
            DISP_MODELS[i,j,0] = 0 #puff.id
            DISP_MODELS[i,j,1] = puff.Q
            DISP_MODELS[i,j,2] = puff.wind_a
            DISP_MODELS[i,j,3] = puff.wind_b
            DISP_MODELS[i,j,4] = 0 #puff.Pcategory #deprecated
            DISP_MODELS[i,j,5] = puff.wind_c 
            DISP_MODELS[i,j,6] = puff.x0
            DISP_MODELS[i,j,7] = puff.y0
            DISP_MODELS[i,j,8] = puff.H
            DISP_MODELS[i,j,9] = puff.tot_d
            DISP_MODELS[i,j,10] = puff.tot_t
            DISP_MODELS[i,j,11] = puff.dd_total
            DISP_MODELS[i,j,12] = puff.rd_total
            DISP_MODELS[i,j,13] = puff.sigmaxy
            DISP_MODELS[i,j,14] = puff.sigmaz
            DISP_MODELS[i,j,15] = puff.lambda_decay
            DISP_MODELS[i,j,16] = puff.vG
            
            STABS[i,j] = puff.Pcategory

    #sigma koeficienty a vysky smes. vrstev, posledni radek jsou smes. vysky
    sig_coe_mx_lay_he = numpy.array([[1.503, 0.876, 0.659, 0.640, 0.801, 1.294], #sigmaxyA
                                     [0.833, 0.823, 0.807, 0.784, 0.754, 0.718], #sigmaxyB
                                     [0.151, 0.127, 0.165, 0.215, 0.264, 0.241], #sigmazA
                                     [1.219, 1.108, 0.996, 0.885, 0.774, 0.662], #sigmazB
                                     [1300.,  920.,  840.,  500.,  400.,  150.]]) #, dtype=numpy.double)#mxt heights

    RESULTS = numpy.zeros((DISP_MODELS.shape[0], DISP_MODELS.shape[1], DISP_MODELS.shape[2]), dtype=numpy.double)
    
    
    for i in range(mC):
        RESULTS[i,:,:] = pp.propagate_puff_model_external_Cython(i, tt, t_meteo, total_time_global, DISP_MODELS[i,:,:], STABS[i,:], receptors, doseA, doseB, doseMJU, doseMJUaE, doseCb, dose_integration_time, dose_sub_step_no, n, METEO_FORECAST, obs_meteo_flag, METEO_OBS_WS, METEO_OBS_WD, sig_coe_mx_lay_he)
       
       
    ci_of_models = numpy.zeros((mC, rC, pN))
    dose_of_models = numpy.zeros((mC, rC, pN))

    
    models_ret = numpy.zeros((mC, pN), dtype=Puff)
    for i in range(mC):
        for j in range(pN):
            puff = Puff(id=0,
                Q=RESULTS[i,j,1], wind_a=RESULTS[i,j,2], wind_b=RESULTS[i,j,3], Pcategory=0,
                wind_c=RESULTS[i,j,5], # odhadovane parametry
                x0=RESULTS[i,j,6], y0=RESULTS[i,j,7], H=RESULTS[i,j,8], tot_d=RESULTS[i,j,9], tot_t=RESULTS[i,j,10],
                dd_total=RESULTS[i,j,11], rd_total=RESULTS[i,j,12], sigmaxy=RESULTS[i,j,13],
                sigmaz=RESULTS[i,j,14], lambda_decay=RESULTS[i,j,15], vG=RESULTS[i,j,16])
            
            puff.Pcategory = STABS[i,j]
            
            models_ret[i,j] = puff
            
            ci_of_models[i, :, j] = RESULTS[i, j, 17:]
            
            for k in range(rC):
                dose_of_models[i, k, j] = RESULTS[i, j, 17+k] * puff.Q
                #print puff.Q            
            
    #predelani results na normalni format
    #ci = get_dose_at_receptor(tt, PUFFS[i, :], xrec, yrec, zrec, doseA, doseB, doseMJU, doseMJUaE, doseCb, mix_layer_height, partial_time, n)
    #PUFFS[i+pN, rec_no] += ci #je tu += protoze jedu pres sub_stepy
    #Qi = PUFFS[id, i, 1]
    #Qi_inventories[i] = Qi
    #ci_of_receptors[rec_no, i] += ci
    #dose_at_receptors[rec_no, i] += Qi * ci
    return (models_ret, ci_of_models, dose_of_models)
    