'''
Created on Jun 11, 2012

@author: jezisek
'''
import numpy
import math

from cython.parallel import prange

##################################################################################
class Drone:
    "class of an autonomous drone"

    def __init__(self, id, x0, y0, spd, dit):
        self.id = id
        self.x = x0           #x - x coord
        self.y = y0           #y - y coord
        self.spd = spd         #spd - speed [m/s]
        self.dose_integration_time = dit

    def generate_candidates_grid(self, conf):
        #generation using a tuple...
        #fracts = (8, (0.33, 1., 2., 5.))
        "generates candidates on sampling locations on a polar grid"
        d = conf[0] #self.dose_integration_time * self.spd
        #print "dron id %d candidates:" % self.id
        #conf = (d, 8, (0.33, 1., 2., 5.))
        nangle = conf[1]
        dists = conf[2]
        self.cand = numpy.zeros((int(nangle * len(dists) + 1), 3)) # +1 - center point, no move
        self.cand[0, :] = numpy.array([self.x, self.y, 0.0])
        index = 1 #current drone loc at the beginning
        for i in range(int(nangle)):
            for j in range(len(dists)):
                self.cand[index,:] = numpy.array([self.x + math.cos(2 * math.pi / nangle * i) * d * dists[j], self.y + math.sin(2 * math.pi / nangle * i) * d * dists[j], 0.])
                index += 1

        #print self.cand
        return self.cand
    
    def move_to_candidate(self, c):
        #setting new dron's coordinates to selected candidate
        self.x = self.cand[c, 0]
        self.y = self.cand[c, 1]
        print "FROM DRONE: moved to [%5.1f, %5.1f]" % (self.x, self.y)


#########################ORIG ENTROPY FROM CDC
def eval_entropy_orig(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY):
    ENTROPY = numpy.zeros((dCC, dCC))
    for i1 in range(dCC):
        for i2 in range(dCC):
            rec_d1 = i1        #index of candidate location of the firt drone
            rec_d2 = dCC + i2  #index of candidate location of the second drone
            #vector of particle values in rec_d1 and rec_d2
            MU = numpy.array(numpy.sum(dose_of_models_aux[:, t, [rec_d1, rec_d2], :], axis=2) + BACKGROUND_DOSE)
            #print "MU", MU
            #print "MU shape",  MU.shape
            SIGMA = gamm_y * MU
            #print "SIGMA", SIGMA
            ENTROPY[i1, i2] = eval_entropy_2D_orig(MU, SIGMA, VAHY)
            #print i1, i2, ENTROPY[i1, i2]

    ent_ext = numpy.argmin(ENTROPY)
    print "entropy extrem:", ent_ext
    best_loc = (int(ent_ext / (dCC)), ent_ext % (dCC))
    return best_loc, ENTROPY


###############################################################################
def eval_entropy_2D_orig(MU, SIGMA, W):
    #calculates entropy using fully-Gauss of two spatial locations
    ret = 0.
    mC = MU.shape[0]
    mu_z = 0.
    log_gam_mu = 0.
    COV2_z = numpy.zeros([2, 2])
    
    for k in range(mC): #pres particly
        mu = MU[k,:]
        sigma = SIGMA[k,:]
        w = W[k]
        mu_z += w * mu
        log_gam_mu += w * numpy.sum(numpy.log(mu))
        COV2_z += w * (numpy.diag(sigma ** 2) + numpy.outer(mu, mu))
        #print "COV2_z", COV2_z

    COV_z = COV2_z - numpy.outer(mu_z, mu_z)
    dCOV_z = numpy.linalg.det(COV_z)
    
    #print "COV_z", COV_z

    if (dCOV_z > 0):
        ret = log_gam_mu -  0.5 * math.log(dCOV_z)
    else:
        ret = -1. * numpy.inf
    
    return ret        
        
##################################################################################
def eval_entropy_2D(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY):
    ENTROPY = numpy.zeros((dCC, dCC))
    for i1 in range(dCC):
        #print i1, "of", dCC
        for i2 in range(dCC):
            rec_d1 = i1        #index of candidate location of the firt drone
            rec_d2 = dCC + i2  #index of candidate location of the second drone
            #vector of particle values in rec_d1 and rec_d2
            aux_mat = numpy.zeros((dose_of_models_aux.shape[0], 2, dose_of_models_aux.shape[3]))
            aux_mat[:,0,:] = dose_of_models_aux[:, t, rec_d1, :]
            aux_mat[:,1,:] = dose_of_models_aux[:, t, rec_d2, :]
            aux = numpy.sum(aux_mat, axis=2)
            MU_Ent = numpy.add(BACKGROUND_DOSE, aux)
            #print "MU", MU_Ent
            #print "MU shape",  MU.shape
            SIGMA_Ent = numpy.multiply(gamm_y, MU_Ent)
            #print "SIGMA", SIGMA_Ent
            ENTROPY[i1, i2] = eval_entropy_2D_one_part(MU_Ent, SIGMA_Ent, VAHY)
            #print i1, i2, ENTROPY[i1, i2]

    ent_ext = numpy.argmin(ENTROPY)
    print "entropy extrem:", ent_ext
    return (int(ent_ext / (dCC)), ent_ext % (dCC)), ENTROPY


#################################################################################
def eval_entropy_2D_one_part(MU, SIGMA, W):
    #calculates entropy using fully-Gauss of two spatial locations
    #weights are after resampling - 1./mC

    ret = 0.
    mC = MU.shape[0]
    mu_z = numpy.zeros(2)
    log_gam_mu = 0.
    COV2_z = numpy.zeros((2, 2))
    
    for k in range(mC): #pres particly
        mu = MU[k,:]
        sigma = SIGMA[k,:]
        w = W[k]
        mu_z += numpy.multiply(w, mu)
        log_gam_mu += w * numpy.sum(numpy.log(mu))
        COV2_z += numpy.multiply(w, (numpy.diag(numpy.power(sigma, 2)) + numpy.outer(mu, mu)))
        #print "COV2_z", COV2_z

    COV_z = COV2_z - numpy.outer(mu_z, mu_z)
    dCOV_z = numpy.linalg.det(COV_z)

    #print "COV_z", COV_z
    if (dCOV_z > 0):
        ret = log_gam_mu -  0.5 * math.log(dCOV_z)
    else:
        ret = -1. * numpy.inf
    
    return ret

##################################################################################
def eval_MUTUAL_entropy_2D_WITH_GIL(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY):
    ENTROPY = numpy.zeros((dCC, dCC), dtype=numpy.double)
    MU_ent = numpy.zeros((dose_of_models_aux.shape[0], 2), dtype=numpy.double)
    SIGMA_ent = numpy.zeros((dose_of_models_aux.shape[0], 2), dtype=numpy.double)
    DOM =  numpy.sum(dose_of_models_aux[:,t,:,:], axis=2) + BACKGROUND_DOSE
    gammy_DOM = numpy.multiply(gamm_y, DOM)
    ENTROPY_col = numpy.zeros((dCC), dtype=numpy.double)
    
    #print "DOM.shape", DOM.shape
    #print gammy_DOM.shape
    #print VAHY.shape

    for i1 in range(dCC): #, nogil=True):
        #printf("start %d", i1)
        ENTROPY[i1,:] = eval_entropy_2D_inner_NOGIL(i1, MU_ent, SIGMA_ent, ENTROPY_col, DOM, gammy_DOM, dCC, gamm_y, VAHY)

        #printf("end %d", i1)

    show_entropy(ENTROPY)

    for i in range(ENTROPY.shape[0]):
        s = "%d " % i
        for j in range(ENTROPY.shape[0]):
            s += "%2.1E " %   ENTROPY[i,j]
        s += "\n"
        print s
    
    """
    print "Postprocessing of entropy - adding penalization for movement of drones"
    penal = 1.0
    ENTROPY[1:, 1:] = numpy.add(penal, ENTROPY[1:, 1:])           
    """    
    ent_ext = numpy.argmin(ENTROPY)
    print "entropy extrem:", ent_ext
    return (int(ent_ext / (dCC)), ent_ext % (dCC))

#################################################################################
def eval_entropy_2D_inner_NOGIL(rec_d1, MU_ent, SIGMA_ent, ENTROPY_col, DOM, gammy_DOM, dCC, gamm_y, VAHY):
    #for i1 in prange(dCC):
        #print i1, "of", dCC

    for i2 in range(dCC):
        #rec_d1 = i1        #index of candidate location of the firt drone
        rec_d2 = dCC + i2  #index of candidate location of the second drone
        #vector of particle values in rec_d1 and rec_d2
        
        MU_ent[:,0] = DOM[:, rec_d1]
        MU_ent[:,1] = DOM[:, rec_d2]
        #print "MU", MU_ent
        #aux = numpy.sum(aux_mat, axis=2)
        #MU_Ent = numpy.add(BACKGROUND_DOSE, aux)
        #print "MU shape",  MU.shape
        SIGMA_ent[:,0] = gammy_DOM[:, rec_d1]
        SIGMA_ent[:,1] = gammy_DOM[:, rec_d2]
        #print "SIGMA", SIGMA_ent
        
        ENTROPY_col[i2] = eval_entropy_2D_one_part_NOGIL(MU_ent, SIGMA_ent, VAHY)
        #print i1, i2, ENTROPY[i1, i2]

    return ENTROPY_col


#################################################################################
def eval_entropy_2D_one_part_NOGIL(MU, SIGMA, W):
    #calculates entropy using fully-Gauss of two spatial locations
    #weights are after resampling - 1./mC

    ret = 0.
    mC = MU.shape[0]
    mu_z0 = 0. #
    mu_z1 = 0. #numpy.zeros(2)
    log_gam_mu = 0.
    
    COV2_z00 = 0. #numpy.zeros((2, 2))
    COV2_z10 = 0. #
    COV2_z01 = 0. #
    COV2_z11 = 0. #
    
    COV_z00 = 0. #numpy.zeros((2, 2))
    COV_z01 = 0.
    COV_z10 = 0.
    COV_z11 = 0.
    
    for k in range(mC): #pres particly
        mu0 = MU[k,0]
        mu1 = MU[k,1]
        
        sigma0 = SIGMA[k,0]
        sigma1 = SIGMA[k,1]
        
        w = W[k]
        
        mu_z0 += w * mu0 #numpy.multiply(w, mu)
        mu_z1 += w * mu1
        
        log_gam_mu += w * (math.log(mu0) + math.log(mu1))
        #COV2_z += numpy.multiply(w, (numpy.diag(numpy.power(sigma, 2)) + numpy.outer(mu, mu)))
        COV2_z00 += w * (sigma0**2 + mu0**2)
        COV2_z11 += w * (sigma1**2 + mu1**2)
        COV2_z10 += w * mu0*mu1
        COV2_z01 += w * mu0*mu1
        #print "COV2_z", COV2_z00, COV2_z01, COV2_z10, COV2_z11

    #COV_z = COV2_z - numpy.outer(mu_z, mu_z)
    
    COV_z00 = COV2_z00 - mu_z0**2
    COV_z01 = COV2_z01 - mu_z0*mu_z1
    COV_z10 = COV2_z10 - mu_z0*mu_z1
    COV_z11 = COV2_z11 - mu_z1**2
    
    dCOV_z = COV_z00*COV_z11 - COV_z10*COV_z01 #numpy.linalg.det(COV_z)

    #print "COV_z", COV_z00, COV_z01, COV_z10, COV_z11

    if (dCOV_z > 0):
        ret = log_gam_mu -  0.5 * math.log(dCOV_z)
    else:
        ret = -1. * 1.0E500
    
    return ret

###########################################################################################################################
###########################################################################################################################
###########################################################################################################################
###########################################################################################################################
###########################################################################################################################
####### MISCLASSIFICATION LOSS ENTROPY

def show_entropy(ENTROPY0):
    ENTROPY = ENTROPY0 - numpy.max(ENTROPY0)
    s = ""
    d = ENTROPY.shape[0]
    for i in range(d):
        for j in range(d):
            if (ENTROPY[i,j] == 0):
                s += "."
            else:
                s += "*"
        s += "\n"
    
    print "LOSS after NORMALIZATION, Legend: . = 0, * > 0:"    
    print s


def loss_opti_vizu(MU_poi, POIsC, thr, dose_of_models_vizu, t, hypo_obs_count, wei, mC, gamm_y, BACKGROUND_DOSE, alpha, beta, receptors_vizu_count):
    #LOSS VIZU
    CURR_LOSS_VIZU = numpy.zeros(receptors_vizu_count)
    for i2 in range(receptors_vizu_count):
        DOMV = numpy.sum(dose_of_models_vizu[:,t,i2,:], axis=1) + BACKGROUND_DOSE

        LOSS_FP =  numpy.zeros(mC) #loss false positive
        LOSS_FN =  numpy.zeros(mC) #loss false negative

        for j in range(mC):
            hypo_obs_vizu = numpy.random.normal(DOMV[j], gamm_y * DOMV[j], hypo_obs_count) #rvc = receptors_vizu_count

            for k in range(hypo_obs_count):
                ll = eval_norm_log_likelihood_vec(DOMV, gamm_y * DOMV, hypo_obs_vizu[k])

                vahy = numpy.exp(ll - ll.max()) * wei
                vahy = vahy / vahy.sum()

                #print "weights Loss optim", vahy
                nKR = numpy.kron(numpy.ones((1,POIsC)), numpy.reshape(vahy, (mC, 1)) )
                C_hat = numpy.sum( (nKR * MU_poi ), axis=0)

                LOSS_FP[j] += numpy.sum(( C_hat > thr) * (MU_poi[j,:] < thr)   )
                LOSS_FN[j] += numpy.sum(( C_hat < thr) * (MU_poi[j,:] > thr)   )

        CURR_LOSS_VIZU[i2] = alpha * numpy.sum(wei * LOSS_FP) + beta * numpy.sum(wei * LOSS_FN)
    return CURR_LOSS_VIZU

#############################################################################################################################################

def eval_norm_log_likelihood_vec(mu, sigma, y):
    "z vektoru mu a sigma vrati vektor likelihoodu"
    #print "mu", mu.shape
    #print "sigma", sigma.shape
    #print "y", y.shape

    ll = numpy.add(numpy.multiply(-1.0, numpy.log(sigma)), numpy.multiply(-0.5, numpy.divide( numpy.power(numpy.subtract(mu,y),2.), numpy.power(sigma,2.))))# * numpy.substract(mu,y)

    #ll = -1.0 * numpy.log(sigma) * -0.5 * (mu-y)/sigma**2. *(mu-y)
    return ll

################################################################################################################################
def eval_norm_log_likelihood_vec_2D(mu, sigma, y):
    "z vektoru mu a sigma vrati vektor likelihoodu"
    #print "mu", mu.shape
    #print "sigma", sigma.shape
    #print "y", y.shape
    
    ll = numpy.add(numpy.multiply(-1.0, numpy.log(sigma)), numpy.multiply(-0.5, numpy.divide( numpy.power(numpy.subtract(mu,y),2.), numpy.power(sigma,2.))))# * numpy.substract(mu,y)
    #ll = -1.0 * numpy.log(sigma) * -0.5 * (mu-y)/sigma**2. *(mu-y)
    return ll

################################################################################################################################
def loss_opti_2D(MU_poi, POIsC, thr, MU_z, DOMS, hypo_obs_count, wei, mC, dC, gamm_y, alpha, beta, receptors_count):
    """kkk"""
    SIGMA_z = numpy.multiply(gamm_y, MU_z)

    LOSS_FP =  numpy.zeros(mC) #loss false positive
    LOSS_FN =  numpy.zeros(mC) #loss false negative

    #print "MU_z[j,:]", MU_z[0,:].shape
    #print "SIGMA_z[j,:]", SIGMA_z[0,:].shape
    #print "hypo_obs_count", hypo_obs_count, dC


    #LOSS DRONES AND OBSERVATIONS
    for j in range(mC):
        hypo_obs_cand = numpy.random.normal(MU_z[j,:], SIGMA_z[j,:], (hypo_obs_count,dC))
        hypo_obs_stab = numpy.random.normal(DOMS[j,:], numpy.multiply(gamm_y, DOMS[j,:]), (hypo_obs_count,receptors_count))

        
        for k in range(hypo_obs_count):
            ll = numpy.sum(eval_norm_log_likelihood_vec_2D(MU_z, SIGMA_z, hypo_obs_cand[k,:]), axis=1) #secteme pro vsechna mereni
            #ll += numpy.sum(eval_norm_log_likelihood_vec(DOMS, gamm_y * DOMS, hypo_obs_stab[k,:]), axis=1)

            vahy = numpy.multiply(numpy.exp(numpy.subtract(ll, numpy.max(ll) )  ), wei)
            vahy = vahy / numpy.sum(vahy)

            nKR = numpy.kron(numpy.ones((1,POIsC)), numpy.reshape(vahy, (mC, 1)) )
            C_hat = numpy.sum( numpy.multiply(nKR, MU_poi ), axis=0)

            on =  numpy.ones(POIsC) #ones, if condition is True
            ze =  numpy.zeros(POIsC) #zeros if condition is False

            thr_v = numpy.multiply(thr, numpy.ones(POIsC), dtype=numpy.double)
            LOSS_FP[j] += numpy.sum( numpy.multiply( numpy.where(C_hat > thr_v, on, ze), numpy.where(MU_poi[j,:] < thr_v, on, ze) )  )
            LOSS_FN[j] += numpy.sum( numpy.multiply( numpy.where(C_hat < thr_v, on, ze), numpy.where(MU_poi[j,:] > thr_v, on, ze) )  )

            #LOSS_FP[j] += numpy.sum(( C_hat > thr) * (MU_poi[j,:] < thr)   )
            #LOSS_FN[j] += numpy.sum(( C_hat < thr) * (MU_poi[j,:] > thr)   )

    #print "======"
    #print numpy.min(LOSS_FP), numpy.max(LOSS_FP)
    #print numpy.min(LOSS_FN), numpy.max(LOSS_FN)
    #print wei[0]
    #print alpha * numpy.sum(numpy.multiply(wei, LOSS_FP)) + beta * numpy.sum(numpy.multiply(wei, LOSS_FN))
    
    return alpha * numpy.sum(numpy.multiply(wei, LOSS_FP)) + beta * numpy.sum(numpy.multiply(wei, LOSS_FN))


################################################################################################################################

def eval_LOSS_entropy_2D_WITH_GIL(alpha, beta, mC, dC, dCC, t, MU_poi, thr, POIsC, dose_of_models_aux, dose_of_models_stab, BACKGROUND_DOSE, hypo_obs_count, receptors_count, receptors_vizu_count, gamm_y ):
    LOSS = numpy.zeros((dCC, dCC))
             
    wei = numpy.ones(mC)*1. / mC
    
    DOM_aux = numpy.sum(dose_of_models_aux[:,t,:,:], axis=2)
    DOM_stab = numpy.sum(dose_of_models_stab[:,t,:,:], axis=2)

    DOMS = numpy.add(DOM_stab, BACKGROUND_DOSE)
     
    print "waiting to finish all LOSS OPTIMIZATION jobs..."
    for i1 in range(dCC): #cyklus pres kandidaty
        for i2 in range(dCC):
            MU_z = numpy.add(DOM_aux.take((i1, dCC + i2), axis=1), BACKGROUND_DOSE) #suma pres vsechny puffy v puff modelu
            #print MU_z[0][0] 5.0E-8        
            LOSS[i1, i2] = loss_opti_2D(MU_poi, POIsC, thr, MU_z, DOMS, hypo_obs_count, wei, mC, dC, gamm_y, alpha, beta, receptors_count)
                                      #(MU_poi, POIsC, thr, MU_z, DOMS, hypo_obs_count, wei, mC, dC, gamm_y, alpha, beta, receptors_count)
            #print i1, i2, LOSS[i1, i2]

    show_entropy(LOSS)
    print "FINISHED!"
    
    #############searching for optimal location
    print LOSS
    print "LOSS MIN", numpy.min(LOSS)
    print "LOSS MAX", numpy.max(LOSS)
     
    ent_ext = numpy.argmin(LOSS)
    print "Loss extrem:", ent_ext
    best_loc = (int(ent_ext / (dCC)), ent_ext % (dCC))
    print "BEST LOC"
    print best_loc
    
    return best_loc
    

if __name__ == '__main__':
    "test of entropy"
    dC = 2
    dCC = 13
    mC = 100
    VAHY = numpy.ones(mC) * 1./mC
    BACKGROUND_DOSE = 5.0E-08
    gamm_y = 0.2
    t = 0
    
    pN = 6
    receptors_count = 10
    receptors_vizu_count = 30
    
    dose_of_models_aux = numpy.abs(numpy.random.normal(0., 1., (mC, 1, dC*dCC, pN)))

    dose_of_models_stab = numpy.abs(numpy.random.normal(0., 1., (mC, 1, receptors_count, pN)))
    dose_of_models_vizu_opti = numpy.abs(numpy.random.normal(0., 1., (mC, 1, receptors_vizu_count, pN))) #hodnoty na gridu pro vizualizaci pred optimalizaci dronu
    dose_of_models_vizu_apost = numpy.abs(numpy.random.normal(0., 1., (mC, 1, receptors_vizu_count, pN)))
    
    #mutual information entropy
    """
    b0 = eval_entropy_2D_WITH_GIL(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY)
    print "1"
    b1 = eval_entropy_2D(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY)
    print "2"
    b2 = eval_entropy_orig(dCC, dose_of_models_aux, t, BACKGROUND_DOSE, gamm_y, VAHY)
    print "3"
    
    print b0, b1, b2
    """
    
    alpha = 0.5
    beta = 0.5
    hypo_obs_count = 1
    POIs = numpy.array([2,5,8,9,12,29]) #POIs must be aligned with vizu grid - indexes od the vizu grid, oringetation from left-bottom to top-right
    POIsC = POIs.shape[0]
    
    ######predicted values in locastions of POI using naive transition proposals
    MU_poi = numpy.sum(dose_of_models_vizu_opti[:,t,:,:], axis=2).take(POIs, axis=1) + BACKGROUND_DOSE
    ####threshold for missclassification loss function
    thr_mult = 1.0
    threshold = numpy.mean(numpy.mean(MU_poi)) * thr_mult
    print "threshold", threshold

    
    #poi = numpy.array([21*1+3, 21*3+5, 21*4+7, 21*5+9, 11*21+10, 12*21+10, 11*21+11, 12*21+11]) 3 v ose sireni + ctverec nekde 
    
    best_loc = eval_LOSS_entropy_2D_WITH_GIL(alpha, beta, mC, dC, dCC, t, MU_poi, threshold, POIsC, dose_of_models_aux, dose_of_models_stab, BACKGROUND_DOSE, hypo_obs_count, receptors_count, receptors_vizu_count, gamm_y)
    print best_loc
    
    print "Calculating loss vizu..."
    LOSS_VIZU = loss_opti_vizu(MU_poi, POIsC, threshold, dose_of_models_vizu_opti, t, hypo_obs_count, VAHY, mC, gamm_y, BACKGROUND_DOSE, alpha, beta, receptors_vizu_count)
    print LOSS_VIZU
    
    
    