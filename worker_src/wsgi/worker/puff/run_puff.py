'''
Created on Jan 28, 2014

@author: jezisek
'''
from puff_tools import Puff
from nuclideDB import NuclideDB
import cPickle
import numpy
import ppe

######DATA TYPES - seialization for mongodb
def ma2mo(matrix): #matrix - numpy nd.array to mongo, matrix is serialized using cPickle
    return cPickle.dumps(matrix)

def mo2ma(mongo_obj): #mongo to numpy nd.array
    return cPickle.loads(str(mongo_obj))

def run_puff(task_input):
    
    #getting task inputs
    activities_ch = mo2ma(task_input["activities"])[:,0] #Bq
    rel_heights_ch = mo2ma(task_input["rel_heights"])[:,0] #m
    
    #conversion from charaaray to array
    activities = numpy.zeros(activities_ch.shape)
    rel_heights = numpy.zeros(rel_heights_ch.shape)
    for i in range(activities.shape[0]):
        activities[i] = float(activities_ch[i])
        rel_heights[i] = float(rel_heights[i])
        
    nucl = "AR-41" #task_input["nuclides"][0] #we have just one nuclide..
    lon = task_input["lon"] #deg
    lat = task_input["lat"] #deg
    inner_time_step = task_input["inner_time_step"]
    duration_of_release = task_input["duration_of_release"] #hours
    
    PASQUILL = ["A", "B", "C", "D", "E", "F"]
    
    meteo_ch = mo2ma(task_input["meteo"])
    meteo = numpy.zeros(meteo_ch.shape)
    for i in range(meteo.shape[0]):
        for j in range(meteo.shape[1]):
            if j != 2:
                meteo[i,j] = float(meteo_ch[i,j])
            else:
                meteo[i,j] = PASQUILL.index(meteo_ch[i,2])
    
    simulaton_length = task_input["simulation_length"] #hours
    output_grid_step = task_input["output_grid_step"] #km
    output_domain_size_x = int(task_input["output_domain_size_x"]/output_grid_step) #km
    output_domain_size_y = int(task_input["output_domain_size_y"]/output_grid_step) #km
    
    puff_sampling_step = task_input["puff_sampling_step"] #min

    
    ###########radiological parameters regarding the realsed radionuclide
    propagation_time = simulaton_length * 3600 #s
    rel_time = duration_of_release * 3600 #s
    dose_integration_time = puff_sampling_step * 60.
    dose_sub_step_no = int(puff_sampling_step / inner_time_step) #the number of integration steps used in step-wise integration of dose with dose_integration_time
    total_steps = propagation_time / dose_integration_time #the total number of time steps
    pN = int(rel_time / dose_integration_time) #number of puffs in a puff model calculated from dose_integration_time and the duration of release
    
#######################################################################
    mC = 1 #number of puff models to release - number of particles in a particle filter...
    n = 5 #number of abscissas in Gaussian quadratures
    
    nDB = NuclideDB()
    
    doseA     = nDB.getDoseA(nucl)
    doseB     = nDB.getDoseB(nucl)
    doseMJU   = nDB.getDoseMJU(nucl)
    doseMJUaE = nDB.getDoseMJUaE(nucl)
    doseCb    = nDB.getDoseCb(nucl)
    
#####################################################################
    
    
    models = numpy.zeros((mC, pN), dtype=Puff) #initialization of variables in an array representing a puff model containing pN puffs

    for i in range(mC):
        for j in range(pN):
            H = rel_heights[i]
            vG = nDB.getDepoVG(nucl)
            lambda_decay = nDB.getLambda(nucl)
            puff = Puff(id=j,
                Q=0., wind_a=0., wind_b=0., Pcategory=0, wind_c=0., 
                x0=0., y0=0., H=H, tot_d=1., tot_t=1., dd_total=1., rd_total=1., sigmaxy=0.,
                sigmaz=0., lambda_decay=lambda_decay, vG=vG)
            models[i,j] = puff
    
    
    receptors = []
    #coordinates of the source - usually located at the center of the computational grid
    sourceX = (output_domain_size_x-1)*output_grid_step / 2.0 * 1000
    sourceY = (output_domain_size_y-1)*output_grid_step / 2.0 * 1000
    
    #grid for vizualization purposes
    for k in range(int(output_domain_size_x)):
        for l in range(int(output_domain_size_y)):
                xxx = sourceX - (k*1000*output_grid_step)
                yyy = (l*1000*output_grid_step)-sourceY
                receptors.append((xxx, yyy, 0))
    
    receptors = numpy.array(receptors)       
    
    dose_of_models = numpy.zeros((mC, int(propagation_time/dose_integration_time), len(receptors)))
    
    
    
    for t in range(int(total_steps)): #cyklus pres casove kroky
        total_time_global = t * dose_integration_time
        print "TIME STEP %d, time = %3.2fmin" % (t, total_time_global/60.0) 
        
        for j in range(mC): #cycle over puff models - for mC=1 just one puff model
            for i in range(len(models[j])): #cycle over puff in the puff model
                p = models[j][i] # puff
                print i, "of", len(models[j])
                print activities[i]
                print meteo[i,2]
                (p.Q, p.wind_a, p.wind_b, p.Pcategory, p.wind_c) = (activities[i], 1., 0., meteo[i,2], 0.0)
                
        (models, ci_of_models, dose_of_puffs) = ppe.parallel_puff(j, total_time_global, models, receptors, doseA, doseB, doseMJU, doseMJUaE, doseCb, dose_integration_time, dose_sub_step_no, n, meteo, False, [], [])     
        #getting results..., summing over all puffs in puff model
        dose_of_models[0, t, :] = numpy.sum(numpy.array(dose_of_puffs[0,:,:]), axis=1 ) #sumace pres prispevky puffu
                        
        
    print "Output shape:", dose_of_models.shape
    #raw_input("dose of models")
    return {"dose": ma2mo(dose_of_models)}


    
if __name__ == '__main__':
    pass