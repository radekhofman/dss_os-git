#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Puff(object):
    """Kontejner pro parametry jednoho puffu"""

    def __init__(self, id, Q, wind_a, wind_b, Pcategory, wind_c, x0, y0, H, tot_d, tot_t,
                 dd_total, rd_total, sigmaxy, sigmaz, lambda_decay, vG):
        self.id = id

        self.Q = Q # odhadovany parametr
        self.wind_a = wind_a  # odhadovany parametr
        self.wind_b = wind_b  # odhadovany parametr
        self.Pcategory = Pcategory  # odhadovany parametr; TODO: opravdu?
        self.wind_c = wind_c  # odhadovany parametr

        self.x0 = x0
        self.y0 = y0
        self.H = H
        self.tot_d = tot_d
        self.tot_t = tot_t
        self.dd_total = dd_total
        self.rd_total = rd_total
        self.sigmaxy = sigmaxy
        self.sigmaz = sigmaz
        self.lambda_decay = lambda_decay
        self.vG = vG

