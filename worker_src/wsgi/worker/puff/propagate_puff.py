#! /usr/bin/python
# -*- coding: utf-8 -*-

# direkrtivy pro Cython, ktere zrychluji kod (za cenu tezsiho debugovani):
#cython: boundscheck=False
#cython: cdivision=True
#cython: infer_types=True

try:  # hack pro Cython verzi: importujme math pouze v pure Pythonu, v Cythonu uz je nadefinovan
    math.log
except NameError:
    import math
import numpy


#modification of former method to be in compliance with nogil statement
#dose_at_receptors = numpy.zeros((len(receptors), len(PUFFS)))
#Qi_inventories    = numpy.zeros(len(PUFFS))
#ci_of_receptors   = numpy.zeros((len(receptors), len(PUFFS)))

######
#vstupuje tam PUFF model tj numpy.array pN x pocet_parametru, tj. treba 6 x 17
#podtim musej v jedinem python super array i ty vystupy, abychom vraceli jeden parametr, nevim, jak bez GILu
#predat neco slozitejsiho vypocet dose of receptors se provede az o uroven vyse bez GILu
#tj. PUFFS maji dimenzi: (2*6) x (max{pocet_parametru_puffu=16, rec_no}) 3*6 - pro kazdy puff jeho params, pak Ci
def propagate_puff_model_external_Cython2(id):
    return 10.0

def propagate_puff_model_external_Cython(id, tt, t_meteo, total_time_global, PUFFS, STABS, receptors, doseA, doseB, doseMJU, doseMJUaE, doseCb, dose_integration_time, dose_sub_step_no, n, METEO_FORECAST, obs_meteo_flag, METEO_OBS_WS, METEO_OBS_WD, sig_coe_mx_lay_he):
    #print "PUFFS shape:", PUFFS.shape    
    partial_time = dose_integration_time / dose_sub_step_no
    grid_step = 3.0 #krok metea

    pN = PUFFS.shape[0]#puffnumber, musi byt externe kvuli nogilu

    for t in range(dose_sub_step_no):
        for i in range(pN):
            if (i <= tt):
                wd = 0.0
                ws = 0.0
                wb = PUFFS[i, 5]  #wind bias

                if (obs_meteo_flag):
                    ws = METEO_OBS_WS[tt]
                    wd = METEO_OBS_WD[tt]
                else:
                    WD = get_meteo_wd(METEO_FORECAST, grid_step, t_meteo, total_time_global,
                                       PUFFS[i, 6]/1000.0, PUFFS[i, 7]/1000.0)
                    WS = get_meteo_ws(METEO_FORECAST, grid_step, t_meteo, total_time_global,
                                       PUFFS[i, 6]/1000.0, PUFFS[i, 7]/1000.0)
                    
                    wd = WD + PUFFS[i, 3] 
                    ws = PUFFS[i, 2] * WS 

                
                wd += wb* math.sqrt((PUFFS[i, 6] /1000.)**2 + (PUFFS[i, 7]/1000.)**2 ) #wb * d**2
                
                if (wd > 2.0*math.pi):
                    wd -= 2*math.pi
                elif (wd < 0.0):
                    wd += 2*math.pi# - wd


                mix_layer_height = sig_coe_mx_lay_he[4,STABS[i]]#getMixingLayerHeight(STABS[i], sig_coe_mx_lay_he)

                dx = (math.sin(wd)*ws) * partial_time #vx = (math.sin(wd)*ws)
                dy = (math.cos(wd)*ws) * partial_time #vy = (math.cos(wd)*ws)

                PUFFS[i, 6] = PUFFS[i, 6] + dx #puff.x0
                PUFFS[i, 7] = PUFFS[i, 7] + dy #puff.y0

                #delta_distance = math.sqrt(dx**2 + dy**2)

                PUFFS[i, 9] = PUFFS[i, 9] + math.sqrt(dx**2 + dy**2) #puff.tot_d
                PUFFS[i, 10] = PUFFS[i, 10] + partial_time   #puff.tot_t

                #ochuzovani puffu rozpadem a suchou depozici
                PUFFS[i, 11] = PUFFS[i, 11] * getDryDepo(partial_time, PUFFS[i, 14], PUFFS[i, 16], PUFFS[i, 8]) #puff.dd_total
                PUFFS[i, 12] = PUFFS[i, 12] * math.exp(-PUFFS[i, 15]*partial_time) #puff.rd_total

                #disperze
                PUFFS[i, 13] = get_dispersion_coefs_sXY(PUFFS[i, 9], STABS[i], sig_coe_mx_lay_he) #puff.sigmaxy
                PUFFS[i, 14] =  get_dispersion_coefs_sZ(PUFFS[i, 9], STABS[i], sig_coe_mx_lay_he) #puff.sigmaz

                #print "time ", tt, "puff", i, ":"
                #print "x=", PUFFS[i, 6], "y=", PUFFS[i, 7], "sigxy", PUFFS[i, 13]
        #ulozeni stavu posunute puffy jsou v jejich puvodnim poli, co se nam vrati, jen se tma jeste prilepi jejich ci


        for j in range(receptors.shape[0]):
            # následující přiřazení by šlo napsat v Pythonu hezčeji, ale to by Cython neurychlil:
            (xrec, yrec, zrec) = (receptors[j, 0], receptors[j, 1], receptors[j, 2])

            for i in range(pN):
                if (i <= (tt)):
                    ci = get_dose_at_receptor(tt, PUFFS[i, :], xrec, yrec, zrec, doseA, doseB, doseMJU, doseMJUaE, doseCb, mix_layer_height, partial_time, n)
                    PUFFS[i, 17 + j] += ci #je tu += protoze jedu pres sub_stepy



    return PUFFS


########################################################################################
    #0 - id,
    #1 - Q,
    #2 - wind_a,
    #3 - wind_b,
    #4 - wind_c,
    #5 - pcategory #deprecated
    #6 - x0,
    #7 - y0,
    #8 - H,
    #9 - tot_d,
    #10 - tot_t,
    #11 - dd_total,
    #12 - rd_total,
    #13 - sigmaxy,
    #14 - sigmaz
    #15 - lambda_decay
    #16 - vG

####################################################################################################################
def get_dose_at_receptor(tt, puff, xrec, yrec, zrec, doseA, doseB, doseMJU, doseMJUaE, doseCb, mix_layer_height, partial_time, n):
    "pocita davku na receptoru pomoci m/mju"

    d = math.sqrt( (puff[6]-xrec)**2 + (puff[7]-yrec)**2 )
    m = 15. #m/mu

    ret_1 = 0.0
    if (d < (4.*puff[13]+ m/doseMJU) ):
        gamma_flux_rate = Gauss_Legendre_quadrature_3D(tt, puff, xrec, yrec, zrec, doseA, doseB, doseMJU, mix_layer_height, n)
        ret_1 = gamma_flux_rate * doseMJUaE * doseCb / 1.225 * partial_time

    return ret_1

####################################################################################################################

GLq_weights_5 = numpy.array([0.236926885056, 0.478628670499, 0.568888888889, 0.478628670499, 0.236926885056])
GLq_absicas_5 = numpy.array([-0.906179845939, -0.538469310106, 0.0, 0.538469310106, 0.906179845939])

GLq_absicas_12 = numpy.array([-0.9815606342,-0.9041172564,-0.7699026742,-0.5873179543,-0.367831499,-0.1252334085,0.1252334085,0.367831499,0.5873179543,0.7699026742,0.9041172564,0.9815606342])
GLq_weights_12 = numpy.array([0.0471753364,0.106939326,0.1600783285,0.2031674267,0.2334925365,0.2491470458,0.2491470458,0.2334925365,0.2031674267,0.1600783285,0.106939326,0.0471753364])

GLq_absicas_20 = numpy.array([-0.9931285992,-0.9639719273,-0.9122344283,-0.8391169718,-0.7463319065,-0.6360536807,-0.510867002,-0.3737060887,-0.2277858511,-0.0765265211,0.0765265211,0.2277858511,0.3737060887,0.510867002,0.6360536807,0.7463319065,0.8391169718,0.9122344283,0.9639719273,0.9931285992])
GLq_weights_20 = numpy.array([0.0176140071,0.0406014299,0.0626720483,0.0832767416,0.1019301198,0.118194532,0.1316886385,0.1420961093,0.1491729865,0.1527533871,0.1527533871,0.1491729865,0.1420961093,0.1316886385,0.118194532,0.1019301198,0.0832767416,0.0626720483,0.0406014299,0.0176140071])

####################################################################################################################
def Gauss_Legendre_quadrature_3D(tt, puff, xrec, yrec, zrec, doseA, doseB, doseMJU, mix_layer_height, n):
    #n - number of abscissas
    ret1 = 0.0
    ret2 = 0.0
    ret3 = 0.0

    a1 = 0.0
    b1 = 5.0 / doseMJU #average_range_5
    a2 = 0.0
    b2 = math.pi / 2.0
    a3 = 0.0
    b3 = 2*math.pi

    if (n == 5):
        GLq_absicas = GLq_absicas_5
        GLq_weights = GLq_weights_5
    if (n == 12):
        GLq_absicas = GLq_absicas_12
        GLq_weights = GLq_weights_12
    if (n == 20):
        GLq_absicas = GLq_absicas_20
        GLq_weights = GLq_weights_20

    for i in range(n):
        x = (b1-a1)/2.0 * GLq_absicas[i] + (b1+a1)/2.0
        ret2 = 0
        for j in range(n):
            y = (b2-a2)/2.0 * GLq_absicas[j] + (b2+a2)/2.0
            ret3 = 0
            for k in range(n):
                z = (b3-a3)/2.0 * GLq_absicas[k] + (b3+a3)/2.0
                ret3 += GLq_weights[k]*get_gamma_flux_rate_argument(tt, puff, x, y, z, xrec, yrec, zrec, doseA, doseB, doseMJU, mix_layer_height)
            ret2 += GLq_weights[j] * ret3
        ret1 += GLq_weights[i] * ret2

    return (b1-a1) * (b2-a2) * (b3-a3) / 8.0 * ret1

####################################################################################################################
def get_gamma_flux_rate_argument(tt, puff, r, th, phi, xrec, yrec, zrec, doseA, doseB, doseMJU, mix_layer_height):
    "integrand do toku gamma zareni, sfericke souradnice"
    x = xrec + r * math.sin(th) * math.cos(phi)
    y = yrec + r * math.sin(th) * math.sin(phi)
    z = r * math.cos(th)

    d = math.sqrt( (x-xrec)**2 + (y-yrec)**2 + (z-zrec)**2 )

    #pocitame pouze pro jeden Bacquerel!!! Q=1.0
    C = get_conc_at_location(1.0, puff[6], puff[7], puff[8], puff[11], puff[12],
        puff[13], puff[14], x, y, z, mix_layer_height) #x,y,z jsou body ve 3D, pres ktery se integruje

    B = 1.0 + doseA * doseMJU * math.exp(doseB * doseMJU * d) #nelinearni koeficient nakupeni
    return C * B * math.exp(-1.0 * doseMJU * d) / (4.0 * math.pi * d**2) *  r**2 * math.sin(th) # na konci jakobian


####################################################################################################################

def get_conc_at_location(Q, x0, y0, H, dd_total, rd_total, sigmaxy, sigmaz, xrec, yrec, zrec, mix_layer_height):
    "pocita koncentraci na souradnicich xrec,yrec,zrec=0 podle koncentrace ve sttedu puffu"
    (xToPuff, yToPuff) = (math.fabs(x0-xrec), math.fabs(y0-yrec))
    factor0 = math.pow(2*math.pi, 1.5)*sigmaz*sigmaxy**2
    factor1 = math.exp( -( (xToPuff**2)/(2.0*sigmaxy**2) ) )
    factor2 = math.exp( -( (yToPuff**2)/(2.0*sigmaxy**2) ) )
    factor3 = getReflectionAtTheTopOfMixingLayerZ(mix_layer_height, zrec, sigmaz, H) + vertFactReflGroundZ(zrec, sigmaz, H)

    return Q * rd_total * dd_total / factor0 * factor1 * factor2 * factor3

####################################################################################################################

def getReflectionAtTheTopOfMixingLayerZ(M, z, sz, H):
    ret = math.exp(-(2*M- z + H)**2/(2*sz**2)) + math.exp(-(2*M+ z+ H)**2/(2*sz**2)) + math.exp(-(2*M- z - H)**2/(2*sz**2)) + math.exp(-(2*M+ z - H)**2/(2*sz**2))
    return ret

####################################################################################################################

#exponecialni zeslabeni po vysce a odraz od zeme
def vertFactReflGroundZ(z, sz, H):
    ret =  math.exp(-(z + H)**2/(2*sz**2)) + math.exp(-(z - H)**2/(2*sz**2))
    return ret

######################################################################################################################
#workaroundy kvuli cythonovani, nefunugje s nogilem:((
#sigmaxyA = numpy.array([1.503, 0.876, 0.659, 0.640, 0.801, 1.294])
#sigmaxyB = numpy.array([0.833, 0.823, 0.807, 0.784, 0.754, 0.718])
#sigmazA  = numpy.array([0.151, 0.127, 0.165, 0.215, 0.264, 0.241])
#sigmazB  = numpy.array([1.219, 1.108, 0.996, 0.885, 0.774, 0.662])
#pripravim si dopredu vystupni nogilovy pole
#disp_coefs_ret = numpy.array([0.,0.])

def get_dispersion_coefs_sXY(totDist, Pcategory, data): #, multSigXY, multSigZ):
    s1 = 0
    if (totDist > 10000.):
        s1 = math.pow(data[0,Pcategory]*10000., data[1,Pcategory] - 0.5)*(totDist**(0.5))
    else:
        s1 = math.pow(data[0,Pcategory]*totDist, data[1,Pcategory])
    return s1 

####################################################################################################################
def get_dispersion_coefs_sZ(totDist, Pcategory, data):
    return math.pow((data[2,Pcategory]*totDist), data[3,Pcategory])

####################################################################################################################
def getDryDepo(delta_time, deltaSigmaz, vG, H): #dd1 = koeficient
    if (deltaSigmaz > 0):
        ret = 1.0 - (math.sqrt(2/math.pi)*(vG/(deltaSigmaz))*(delta_time)*math.exp(-(H**2)/(2.0*deltaSigmaz**2)))
    else:
        ret = 1.0
    return ret

####################################################################################################################
def bilinear_interp(q11, q12, q21, q22, x1, y1, x2, y2, x, y):
    return q11*(x2-x)*(y2-y)/(x2-x1)/(y2-y1)+q21*(x-x1)*(y2-y)/(x2-x1)/(y2-y1)+q12*(x2-x)*(y-y1)/(x2-x1)/(y2-y1)+q22*(x-x1)*(y-y1)/(x2-x1)/(y2-y1)

####################################################################################################################
def get_meteo_ws(METEO, grid_step, t0, total_time_global, X, Y):
    #t0 = int(t / 60.0 / 60.0)
    """
    x = X % grid_step
    y = Y % grid_step

    ws_0 = bilinear_interp(METEO[t0,1,0,0], METEO[t0,1,0,0], METEO[t0,1,0,0], METEO[t0,1,0,0], 0, 0, grid_step, grid_step, x, y)
    ws_1 = bilinear_interp(METEO[t0+1,1,0,0], METEO[t0+1,1,0,0], METEO[t0+1,1,0,0], METEO[t0+1,1,0,0], 0, 0, grid_step, grid_step, x, y)

    return (total_time_global-t0*60*60)/(60*60*(t0+1 - t0))*(ws_1-ws_0) + ws_0
    """
    return METEO[t0, 1]
####################################################################################################################
def get_meteo_wd(METEO, grid_step, t0, total_time_global, X, Y):
    #t0 = int(t / 60.0 / 60.0)
    
    """
    x = X % grid_step
    y = Y % grid_step

    wd_0 = bilinear_interp(METEO[t0,0,0,0], METEO[t0,0,0,0], METEO[t0,0,0,0], METEO[t0,0,0,0], 0, 0, grid_step, grid_step, x, y)
    wd_1 = bilinear_interp(METEO[t0+1,0,0,0], METEO[t0+1,0,0,0], METEO[t0+1,0,0,0], METEO[t0+1,0,0,0], 0, 0, grid_step, grid_step, x, y)
    
    return (total_time_global-t0*60*60)/(60*60*(t0+1 - t0))*(wd_1-wd_0) + wd_0
    """
    return METEO[t0, 0]
    
####################################################################################################################
"""
def get_meteo(METEO, grid_step, t, X, Y):
    "vraci meteo podle casu a polohy, nastaveno pro MEDARDA"
    "INTERPOLACE I V CASE!!!"
    #tt = hodinovy index predpovedi
    #t = sklutecny cas predpovedi, ve kterem se interpoluje s tech hodinovych
    t0 = int(t / 60.0 / 60.0)
    wd = 0
    ws = 0

    #xgrid = math.floor(X/grid_step) + (METEO.shape[2]-1)/2.0
    x = X % grid_step
    #ygrid = math.floor(Y/grid_step) + (METEO.shape[3]-1)/2.0
    y = Y % grid_step

    #print xgrid, ygrid, ":", x, y
    wd11_0 = METEO[t0, 0, 0, 0]
    wd12_0 = METEO[t0, 0, 0, 0]
    wd21_0 = METEO[t0, 0, 0, 0]
    wd22_0 = METEO[t0, 0, 0, 0]

    ws11_0 = METEO[t0, 1, 0, 0]
    ws12_0 = METEO[t0, 1, 0, 0]
    ws21_0 = METEO[t0, 1, 0, 0]
    ws22_0 = METEO[t0, 1, 0, 0]

    wd11_1 = METEO[t0+1, 0, 0, 0]
    wd12_1 = METEO[t0+1, 0, 0, 0]
    wd21_1 = METEO[t0+1, 0, 0, 0]
    wd22_1 = METEO[t0+1, 0, 0, 0]

    ws11_1 = METEO[t0+1, 1, 0, 0]
    ws12_1 = METEO[t0+1, 1, 0, 0]
    ws21_1 = METEO[t0+1, 1, 0, 0]
    ws22_1 = METEO[t0+1, 1, 0, 0]

    wd_0 = bilinear_interp(wd11_0, wd12_0, wd21_0, wd22_0, 0, 0, grid_step, grid_step, x, y)
    ws_0 = bilinear_interp(ws11_0, ws12_0, ws21_0, ws22_0, 0, 0, grid_step, grid_step, x, y)

    wd_1 = bilinear_interp(wd11_1, wd12_1, wd21_1, wd22_1, 0, 0, grid_step, grid_step, x, y)
    ws_1 = bilinear_interp(ws11_1, ws12_1, ws21_1, ws22_1, 0, 0, grid_step, grid_step, x, y)

    wd = (t-t0*60*60)/(60*60*(t0+1 - t0))*(wd_1-wd_0) + wd_0
    ws = (t-t0*60*60)/(60*60*(t0+1 - t0))*(ws_1-ws_0) + ws_0
    return (wd, ws)
"""
if __name__ == "__main__":
    print "Hello World";
