import sys
import numpy


class NuclideDB:
    "tridaobsahujici DB nuklidu a prislune parametry pro jejich fyzikalni popis"
    nuclides     = []
    halfTimes    = []
    physicalType = []

    doseA = []
    doseB = []
    doseMJU = []
    doseE = []
    doseMJUaE = []
    doseCb = []

    def __init__(self):
        "konstruktor"
        self.initializeDB()
        #print "Nuclide DB ready"

    def getIndexOfNuclide(self, nuclide):
        nuclide = nuclide.upper()
        index = -1
        for i in range(len(self.nuclides)):
            if (nuclide == self.nuclides[i]):
                #print self.nuclides[i]
                index = i
        if (index < 0 ):
            print "Nuclide %s not found in nuclide DB" % nuclide
            print "Exiting"
            sys.exit(0)

        return index
    
    def getDepoVG(self, nuclide):
        "vrati hodnotu depozice pro prislusny nuklid"
        index = self.getIndexOfNuclide(nuclide)
        type = self.physicalType[index]

        v = 0.0
        
        if (type == "noble"):
            v = 0.0
        elif (type == "aero"):
            v = 0.003
        elif (type == "eliod"):
            v = 0.015

        return v

    def getLambda(self, nuclide):
        "vraci hodnotu konstanty rozpadu lambda pro dany nuklid"
        index = self.getIndexOfNuclide(nuclide)
        lamb = 0.693/self.halfTimes[index]
        #print nuclide + ": index = "+ str(index)+"; t1/2 = " + str(self.halfTimes[index]) + "; lambda = " + str(lamb)
        return lamb

    def getDoseA(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseA[index]

    def getDoseB(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseB[index]

    def getDoseMJU(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseMJU[index]


    def getDoseE(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseE[index]

    def getDoseMJUaE(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseMJUaE[index]

    def getDoseCb(self, nuclide):
        index = self.getIndexOfNuclide(nuclide)
        return self.doseCb[index]

    def initializeDB(self):
        self.nuclides     = ["KR-88", "XE-135", "XE-135M",  "XE-138",  "AR-41",  "CS-137",  "I-131"]
        self.halfTimes    = [  10224,    32400,       900,     847.8,   6560.0,  9.52e+08, 6.95e+05] #s
        self.physicalType = ["noble",  "noble",   "noble",   "noble",  "noble",    "aero",  "eliod"]
        self.doseA        = [    0.0,      0.0,       0.0,       0.0,    1.040,       0.0,      0.0] #
        self.doseB        = [    0.0,      0.0,       0.0,       0.0,   0.0338,       0.0,      0.0] #
        self.doseMJU      = [    0.0,      0.0,       0.0,       0.0,  0.00668,       0.0,      0.0] #m-1
        self.doseE        = [    0.0,      0.0,       0.0,       0.0,  1293000,       0.0,      0.0] #eV
        self.doseMJUaE    = [    0.0,      0.0,       0.0,       0.0, 6.12E-16,       0.0,      0.0] #Gy m2
        self.doseCb       = [    0.0,      0.0,       0.0,       0.0,    0.765,       0.0,      0.0] #Sv/Gy

