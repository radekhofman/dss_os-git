#!/usr/bin/env python
# -*- encoding: UTF-8 -*-
'''
Module provides application  pages.

'''
import cherrypy
import templates

class MainPage:

    @cherrypy.expose
    def index(self):
        template = templates.get_template("welcome.html")
        return template.render()

