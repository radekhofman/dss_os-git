from setuptools import setup

setup(name='DSS OpenShift',
      version='0.1',
      description='DSS customized for deployment on OpenShift platform',
      author='@krablak',
      author_email='krablak@gmail.com',
      url='https://bitbucket.org/radekhofman/dss_openshift',
      install_requires=['CherryPy=3.2.2','pymongo','mako','numpy','PIL','matplotlib>=1.3.1'],
     )
