#!/usr/bin/env python
# -*- encoding: UTF-8 -*-

import cherrypy
from mako.lookup import TemplateLookup
import sys

import config as cf

import web_services as ws
import account_page
import task_page_puff
import results_page
import contact_page
import workers_page
import tools_page
import nuclides_page
import main_page
import guest_user_page


import schedulers.guest_deleting

SESSION_KEY = cf.SESSION_KEY
HTML_TEMP_DIR = cf.HTML_TEMP_DIR
SCRIPTS_DIR = cf.SCRIPTS_DIR


lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
#config same for all the parts
cp_conf_common = cf.cp_conf_common



####MAIN METHOD
def main():
    #sys.args will tell us which configuration we use, wether productino or development run on localhost
    #tree structure of the app
    msg = "Usage: python main_app.py ['devel' - development configuration on localhost, 'prod' - prodcution on dss.utia.cas.cz]"

    if (len(sys.argv) >= 2):
        if (sys.argv[1] in ["devel", "prod", "ssl"]):

            type  = sys.argv[1] #devel or prod
            cherrypy.root = main_page.MainApp() #Main

            if (type == "devel"):
                print "using config", cf.global_conf_devel
                cherrypy.config.update(cf.global_conf_devel)
                url = "http://localhost:8080/"
                cherrypy.root.services = ws.WebServices() #webservices for task workers

            elif (type == "prod"):
                print "using config", cf.global_conf_prod
                cherrypy.config.update(cf.global_conf_prod)
                url = "http://dss.utia.cas.cz/"
                cherrypy.root.services = ws.WebServices() #webservices for task workers

            elif (type == "ssl"):
                print "using config", cf.global_conf_prod_ssl
                cherrypy.config.update(cf.global_conf_prod_ssl)
                url = "https://dss.utia.cas.cz/"
                #web_services are mounted in redirecter on http

            # Start guest user deleting job
            schedulers.guest_deleting.start_guest_deleting_job()

            cherrypy.root.account = account_page.Account() #Account
            cherrypy.root.task = task_page_puff.Task() #Simulation
            cherrypy.root.results = results_page.Results(url) #DA
            cherrypy.root.nuclides = nuclides_page.Nuclides() #Nuclides managment
            cherrypy.root.workers = workers_page.Workers() #workers
            cherrypy.root.contact = contact_page.Contact() #contact
            cherrypy.root.tools = tools_page.Tools() #contact
            cherrypy.root.guest = guest_user_page.GuestUserPage() # Guest user login handler
            cherrypy.tree.mount(cherrypy.root, '/', config=cf.config)
            cherrypy.engine.start()
            cherrypy.engine.block()  # make it possible to stop server by Ctrl+C etc.
        else:
            print msg
    else:
        print msg

if __name__ == "__main__":
    main()
