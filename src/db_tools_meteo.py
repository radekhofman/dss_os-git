# -*- encoding: UTF-8 -*-
import admin_tools as at
import cPickle
import locking
import time
import pymongo
import bson
from tools import mo2ma, ma2mo
#getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']
db_meteo = at.dbs['db_meteo']


def get_list_of_meteos(username):
    """
    return list of workers
    """
    meteos = db_meteo.find({"username": username})
    
    return meteos

    
def get_meteos_count(username):
    """
    returns count of registered workers
    """
    return get_list_of_meteos(username).count()


def save_meteo(meteo):
    """
    saves source term of a user into source terms collection
    """
    meteo["meteo"] = ma2mo(meteo["meteo"])
        
    res = ""
    
    try:
        db_meteo.insert(meteo)
        res = "Meteo successfully saved!"
    except Exception, e:
        print "Error inserting meteo", e
        res = "Meteo not inserted", e
    finally:
        return res

def get_meteo_of_id(_id):
    """
    return source term DB entry of given _id
    """
    ret = db_meteo.find_one({"_id": bson.objectid.ObjectId(_id)})
    print "meteo"
    print ret
    ret["meteo"] = mo2ma(ret["meteo"])    
    return ret
    

def delete_meteo(_id):
    """
    deletes source_term of a given _id from collection
    """
    #st = db_source_terms.find_one({"_id": _id})
    print "deleting meteo..."
    db_meteo.remove({"_id": bson.objectid.ObjectId(_id)})
    
    