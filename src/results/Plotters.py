from pylab import *
import copy
import matplotlib.pyplot as plt


###########################################################################################################3
def contourAStanice(M, img_savepath, Xmea, Ymea, names, dimX, dimY, deltaStep, title1, rady):
    X1 = math.floor(dimX/2.)
    X2 = math.ceil(dimX/2.)
    Y1 = math.floor(dimY/2.)
    Y2 = math.ceil(dimY/2.)
    x = arange(-X1, X2, deltaStep)
    y = arange(-Y1, Y2, deltaStep)
    
    
    print X1
    print X2
    X, Y = meshgrid(x, y)
    pocetRaduOdMaxima = rady
    logMax = math.log(M.max(), 10.0)
    radMaxima = math.floor(logMax)

    
    levels = logspace(radMaxima - pocetRaduOdMaxima, radMaxima+1, num = pocetRaduOdMaxima+2)
        
    print radMaxima
    print M.min()
    print M.max()
    print "levels:"
    print levels
    fig = plt.figure(figsize=(15,15), dpi=400)
               
    M1 = copy.deepcopy(M)
    for i in range(len(M)):
        for j in range(len(M)):
            M1[i][j] = M[len(M)-1-j][i]

    ax = fig.add_axes([0., 0., 1., 1.])
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_frame_on(False)

    
    cs1 = ax.contourf(X, Y, M.transpose(),  levels, alpha=1.0, cmap=cm.jet, norm=matplotlib.colors.LogNorm(), )
    cset2 = ax.contour(X, Y, M.transpose(), levels, colors = 'k', hold='on', linewidths = (1.5,), )

    for c in cset2.collections:
        c.set_linestyle('solid')
    ax.set_title(title1)
    
    ax.set_aspect(1.)
    ax.set_xlim(-X1, X2)
    ax.set_ylim(-Y1, Y2)
    
    #white color is transparent here:)
    savefig(img_savepath, transparent=True)

    fig = plt.figure(figsize=(1.3, 6))
    
    ####ploting colorbar
    #this should turn off all axes and frame box
    ax1 = fig.add_axes([0.0, 0.0, 0.000001, 1.0])
    ax1.set_frame_on(False)
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    ax1.set_xticks([])
    ax1.set_yticks([])
    cax = axes([0.0, 0.1, 0.15, 0.8])
    cb = colorbar(cs1, orientation='vertical', cax=cax)
    #cb.set_ticks(levels)
    #cb.set_ticklabels([("%2.1E" % (tick*1000.)) for tick in levels[:-1]])
    cb.set_label("[mSv]")
    c_fname = img_savepath[:-4]+"_colorbar.svg" #remove extension and add a new one:)
    plt.savefig(c_fname)

