# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools_users

from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_DIR],default_filters=['decode.utf8'], input_encoding='utf-8', output_encoding='utf-8')

########################################################################################
#### FOOTER
########################################################################################
def get_footer():
    return  lookup.get_template("footer.html").render().decode('utf-8')
########################################################################################
#### NAVIBAR
########################################################################################
def navibar(active=0):
    """
    twitter bootstrap navibar
    """
    ret = ""


    ret += """
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/"><div class="row-fluid">DSSOS</div></a>


    """

    # user menu: logout...
    ret += drop_down_logout()
    # buttons
    ret += navbar_buttons(active)

    ret += """
        </div>
      </div>
    </div>
    """

    return ret

########################################################################################
def get_nb_active_array(butt_no, active):
    cl = [''] * butt_no
    if (len(cl) >= active):
        cl[active - 1] = "class='active'"
    return tuple(cl)

def navbar_buttons(active):
    """
    gets navivar buttons
    """
    ret = ""
    username = cherrypy.session.get(cf.SESSION_KEY)

    if (username != None):  # user is logged in
        # 3 buttons...
        ret += """
          <div class="nav-collapse">
            <ul class="nav">
              <li %s><a id="homeLnk" href="/">Home</a></li>
              <li %s><a id="accountLnk" href="/account">Account</a></li>
              <li %s><a id="newTaskLnk" href="/task">New task</a></li>
              <li %s><a id="resultsLnk" href="/results">View results</a></li>
              <li %s><a id="workersLnk" href="/workers">Workers</a></li>
              <li %s><a id="contactLnk" href="/contact">About</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        """ % get_nb_active_array(6, active)

    else:  # user is not logged in:
        # just 3 buttons
        ret += """
          <div class="nav-collapse">
            <ul class="nav">
              <li %s><a id="homeLnk" href="/">Home</a></li>
              <li %s><a id="loginLnk" href="/login">Login</a></li>
              <li %s><a id="workersLnk" href="/workers">Workers</a></li>
              <li %s><a id="contactLnk" href="/contact">About</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        """ % get_nb_active_array(4, active)

    return ret

########################################################################################
def drop_down_logout():
    """
    returns drop down menu for logged users, None otherwise
    """
    username = cherrypy.session.get(cf.SESSION_KEY)

    ret = ""
    if (username != None):  # user is logged in
        user = db_tools_users.find_user(cherrypy.session.get(cf.SESSION_KEY))
        user_displayname = "unknown"
        if user:
            user_displayname = "%s %s" % (user['first_name'],user['surname'])   
        ret += """
           <div class="btn-group pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="">
              <i class="icon-user"></i> %s
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="/account">Account details</a></li>
              <li class="divider"></li>

          """ % user_displayname
        if db_tools_users.is_admin(username):
            ret += """
                 <li><a id="adminLnk" href="/admin">Admin</a></li>
                 <li class="divider"></li>
            """
        ret += """
                <li><a href="/logout">Sign Out</a></li>
            </ul>
          </div>
          """
    else:
        ret += ""

    return ret

def head():
    """
    gets html of the head of the main page
    """

    ret = """
    <style type="text/css">
    body {
    position: relative;
    background-position: 0 40px;
    padding-top: 110px;
    //background-image: url(/images/teme_s.png);
    //background-repeat: no-repeat;
    //-webkit-background-size: cover;
    //-moz-background-size: cover;
    //-o-background-size: cover;
    //background-size: cover;
    }
    </style>
    """

    """
    background: url(/images/teme_s.png) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    """

    return ret

def head_about():
    """
    gets html of head for page contacts
    """

    ret = """
    <link href="/css_styles/docs.css" rel="stylesheet">
    <style type="text/css">
    body {
    data-spy: scroll;
    data-target: .subnav;
    data-offset: 50;
    }
    </style>

    """

    return ret


