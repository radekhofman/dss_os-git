#coding=utf-8

def make_string(text, length, align): #align: 0 - left; 1 - right
    """z textu udela text zarovnany doleva nebo doprava o dane delce"""
    l1 = len(text.strip())
    pocet_mezer = length - l1
    mezery = " "*pocet_mezer

    ret = ""

    if (not align):
        ret = text.strip() + mezery
    else:
        ret = mezery + text.strip()

    return ret

def exponential_formatter(a):
    #premodi exponecialni cislo na dvomistny exponent
    str = a
    l = len(a)
    if (not a[l-4] == "E" or not a[l-4] == "e" ):
        str = a[:l-3]+a[l-2:]
    return str


def julday(date):
    "ze stringovyho datumu DD.MM. udela Julian day"
    dny = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    #print date.strip().split(".")
    D = date.strip().split(".")[0]
    M = date.strip().split(".")[1]
    ret = int(D)
    
    for i in range(int(M)-1):
        ret += dny[i]

    return ret


def calday(jday):
    dny = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    index = 0
    m = 0
    d = 0
    for i in range(len(dny)):
        jday = jday - dny[i]
        if (jday <= 0):
            m = i
            d = jday + dny[i]
            break

    return m+1, d



def calday1(jday): # Julian period day.
    """Arguments:  jday is a float as returned from julday()
    Return value:  month, day, year and day of week as integers, in the Gregorian
    proleptic calendar only."""
    jn=jday

    jalpha=int(((jn-1867216)-0.25)/36524.25)
    ja=int(jn+1+jalpha-(int((0.25*jalpha))))
    jb=int(ja+1524)
    jc=int(6680.0+((jb-2439870.0)-122.1)/365.25)
    jd=int(365.0*jc+(0.25*jc))
    je=int((jb-jd)/30.6001)

    mymday=(jb-jd-(int(30.6001*je)))
    while mymday<0:
        mymday=mymday+7
    mymon=je-1
    if(mymon>12):
        mymon=mymon-12
    while mymon<0:
        mymon=mymon+12
    myyear=jc-4715
    if(mymon>2):
        myyear=myyear-1
    mywday=(jn)%7
    mymon=mymon-1
    return mymon,mymday,myyear,mywday

if __name__ == "__main__":
    print "Hello";