# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import db_tools_source_terms as dtst
import db_tools_meteo as dtm
import tools
from mako.lookup import TemplateLookup
import config as cf
import numpy
import types

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
status_labels = ["border-color: green; border-width: 2px;" ,  "border-color: red; border-width: 2px;"]
##########################################################################################################################
### TASK BODY
##########################################################################################################################


def head():
    ret = """
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script src="/scripts/thirdparty/vendor/styledmarker.js"></script>
    <script src="/scripts/app/receptors/map.tools.js"></script>
    <script src="/scripts/app/receptors/main.js"></script>
    <script src="/scripts/app/receptors/map.js"></script>
    <script src="/scripts/app/receptors/map.modal.js"></script>
    <script src="/scripts/app/receptors/map.table.js"></script>
    <script src="/scripts/app/harp_assim.js"></script>

    """
    return ret


def task_create_body_0(**values):   #HTML page with forms for runnig of new taskof type 1
    #dict values could contains all possible parameters of the release which could be loaded into forms...
    #notepad with tabs with form for initialization of a new task
    ret = ""
    
    if (values.has_key("msg")):
            ret += """<div id="msg" class="alert alert-error"><center><h3> %s </h3></center></div>""" % values["msg"]
    
    tab1 = release_scenario(**values)
    tab2 = meteo(**values)
    tab3 = receptors(**values)
    tab4 = measurements(**values)
    
   
    tmpl = lookup.get_template("new_task_HARP_asim_base.html")
    ret += tmpl.render(tab1=tab1, tab2=tab2, tab3=tab3, tab4=tab4)
    
    #load receptors modal form
    tmpl = lookup.get_template("loadConfigurationModal.html")
    ret += tmpl.render()
    
    """
    #now we append a modal form window with configuration of nuclides 
    nucl_table = generate_nucl_table(**values)
    segments_combo = generate_segment_combo(**values)
    tmpl = lookup.get_template("new_task_modal_nucl_table.html")
    
    ret += tmpl.render(nucl_table=nucl_table, segments_combo=segments_combo)
    
    
    #now we append a modal window with map of source location
    tmpl = lookup.get_template("new_task_modal_source_location_map.html")
    ret += tmpl.render()
   
    #now we add modal windows with setting of meteotable
    tmpl = lookup.get_template("new_task_modal_meteo_settings.html")
    ret += tmpl.render()
    
    #now we add modal windows with form for loading of source terms
    
    #placeholder of the load for ajax update - otherwise saved STs woun't be shown without refresh
    st_table = get_source_term_table(values["username"])
    tmpl = lookup.get_template("new_task_modal_load_st.html")
    ret += tmpl.render(st_table=st_table)
    
    #now we add modal windows with form for saving of source terms
    tmpl = lookup.get_template("new_task_modal_save_st.html")
    ret += tmpl.render()

    #placeholder of the load for ajax update - otherwise saved meteos will not be shown without refresh
    meteo_table = get_saved_meteo_table(values["username"])#get_st_load_table(values["username"])
    tmpl = lookup.get_template("new_task_modal_load_meteo.html")
    ret += tmpl.render(meteo_table=meteo_table)
    
    #now we add modal windows with form for saving of source terms
    tmpl = lookup.get_template("new_task_modal_save_meteo.html")
    ret += tmpl.render()    
    """
    
    return ret





################################
#### TABS
################################

def release_scenario(**values): #values - eventual values of the form fields in case of Load task case
    """
    reurn HTML for the firt tab - release scenario
    """
    ret = ""
    return ret


def meteo(**values): #values - eventual values of the form fields in case of Load task case
    """
    reurn HTML for the second tab - meteo
    """
    ret = ""
    return ret


def receptors(**values): #values - eventual values of the form fields in case of Load task case
    """
    reurn HTML for the third tab - RMN configuration
    """
    ret = ""
    
    tmpl = lookup.get_template("harp_assim_tab3.html")
    ret += tmpl.render()
        
    return ret


def measurements(**values): #values - eventual values of the form fields in case of Load task case
    """
    reurn HTML for the fourth tab - measurements/twin model ocnfiguration
    """
    ret = ""
    return ret
