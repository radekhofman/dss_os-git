#!/usr/bin/env python
# -*- encoding: UTF-8 -*-
'''
Module providing Openshift environment variables.

see https://www.openshift.com/page/openshift-environment-variables
'''

import os

# Informational

# The fully-qualified domain name for your application
OPENSHIFT_APP_DNS = os.getenv('OPENSHIFT_APP_DNS',"")
# Your application's name
OPENSHIFT_APP_NAME = os.getenv('OPENSHIFT_APP_NAME',"")
# The UUID your application runs as (32 hex characters)
OPENSHIFT_APP_UUID = os.getenv('OPENSHIFT_APP_UUID',"")

# Directories

# A persistent directory for your data
OPENSHIFT_DATA_DIR = os.getenv('OPENSHIFT_DATA_DIR',"")
# Repository containing the currently deployed version (of the application)
OPENSHIFT_REPO_DIR = os.getenv('OPENSHIFT_REPO_DIR',"")
# A temporary directory you can use. Even though the path is just `/tmp`, the magic of SELinux protects your data from other users
OPENSHIFT_TMP_DIR = os.getenv('OPENSHIFT_TMP_DIR',"")

# Gear Information

# The fully-qualified domain name for this gear
OPENSHIFT_GEAR_DNS = os.getenv('OPENSHIFT_GEAR_DNS',"")
# This gear's name
OPENSHIFT_GEAR_NAME = os.getenv('OPENSHIFT_GEAR_NAME',"")
# The UUID for this gear
OPENSHIFT_GEAR_UUID = os.getenv('OPENSHIFT_GEAR_UUID',"")
