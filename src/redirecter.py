import cherrypy
import os
import config as cf
import web_services as ws

class RootServer:
    @cherrypy.expose
    def index(self, **keywords):
        raise cherrypy.HTTPRedirect("https://dss.utia.cas.cz")

if __name__ == '__main__':


    cherrypy.root = RootServer()
    #redirecter handles also communication with workers
    cherrypy.root.services = ws.WebServices() #webservices for task workers

    #without ssl, but immediatelly redirects to ssl page
    cherrypy.config.update(cf.global_conf_prod)  
    cherrypy.tree.mount(cherrypy.root, '/')
    cherrypy.engine.start()