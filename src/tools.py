# -*- encoding: UTF-8 -*-
import time
import cPickle

def get_kwarg(kwargs, key, default_value=None):
    """
    returs value of a demanded key if present or None otherwise
    """

    if kwargs.has_key(key):
        return kwargs[key]
    else:
        return default_value

def get_nucl_list():
    """
    generates html table with nuclides
    """
    nucl_list = ["H3",      "BE7",     "C14",     "F18",     "NA22",    "NA24",    
                "CL38",    "AR41",    "K42",     "CR51",    "MN54",    "MN56",    
                "FE55",    "FE59",    "CO58",    "CO60",   "NI63",    "CU64",    
                "ZN65",    "AS76",    "KR85M",   "KR85",    "KR87",    "KR88",    
                "KR89",    "KR90",    "RB86",    "RB88",    "RB89",   "SR89",    
                "SR90",    "SR91",    "SR92",    "Y88",     "Y90",     "Y91M",    
                "Y91",     "ZR95",    "ZR97",    "NB95",    "NB97",    "MO99",    
                "TC99M",   "TC99",    "RU103",   "RU105",   "RU106",   "RH105",   
                "RH106M",  "AG110M",  "SB122",   "SB124",   "SB125",   "SB127",   
                "SB129",   "TE125M",  "TE127M",  "TE127",   "TE129M",  "TE129",   
                "TE131M",  "TE131",   "TE132",   "TE133M",  "TE133",   "TE134",   
                "I129O",   "I129",    "I129A",   "I131O",   "I131",    "I131A",   
                "I132O",   "I132",    "I132A",  "I133O",   "I133",    "I133A",   
                "I134O",   "I134",   "I134A",   "I135O",   "I135",    "I135A",   
                "XE131M",  "XE133M",  "XE133",   "XE135M",  "XE135",   "XE137",   
                "XE138",   "CS134",   "CS136",   "CS137",   "CS138",   "BA139",   
                "BA140",   "LA140",   "CE139",   "CE141",   "CE143",   "CE144",   
                "PR143",   "PR144",   "ND147",   "PM147",   "SM153",   "EU154",   
                "W187",    "U235",    "U238",    "NP239",   "PU238",   "PU239",   
                "PU240",   "PU241",   "AM241",   "CM242",   "CM244",   "HTO",     
                "H3O",     "CO2",     "CO"] #total 123 nuclides
    
    return nucl_list

def sort_nuclides(keys): #keys je pole nuklidu....
    """sorts nuclides according to their atomic numbers"""
    nuc = [""] * 123 #number of nuclides is 123
    
    nucl_list = get_nucl_list()
    
    for nucl in keys:
        if (nucl.strip() in nucl_list):
            index = nucl_list.index(nucl.strip())
            nuc[index] = nucl

    nuclides_sort = []

    for nucl in nuc:
        if nucl != "":
            nuclides_sort.append(nucl)

    return nuclides_sort

def mnl(nuclide):
    """
    wrapper for make_nuclide_label(nuclide): with a short name
    """
    ret = make_nuclide_label(nuclide)
    return ret
    
def make_nuclide_label(nuclide):
    """
    from nuclide format in HARP, e.g. CS137, should make ^{137}Cs:)
    """

    text = []
    digit = ""
    for i in range(len(nuclide)):
        s = nuclide[i]
        if not s.isdigit():
            text.append(s.lower())
        else:
            digit = nuclide[i:].lower()
            break #break of for cycle
    """    
    for s in nuclide:
        print s
        if s.isdigit():
            digit += s
        else:
            text.append(s.lower()) 
    """             
    text[0] = text[0].upper()
                
    return "<sup>%s</sup>%s" % (digit, "".join(text))


def convert_epoch_to_time(epoch):
    """
    converts time in epoch to something readable
    """
    return time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(epoch)) 

def wrap_error(error):
    """
    wraps error messages with appropriate HTML...
    """
    return """<center><h3><div class="alert alert-error">%s</div></h3></center>""" % error

######DATA TYPES - seialization for mongodb
def ma2mo(matrix): #matrix - numpy nd.array to mongo, matrix is serialized using cPickle
    return cPickle.dumps(matrix)

def mo2ma(mongo_obj): #mongo to numpy nd.array
    return cPickle.loads(str(mongo_obj))

