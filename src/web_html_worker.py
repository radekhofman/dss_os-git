# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
from mako.lookup import TemplateLookup
import time

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])


def head():
    """
    return HTML of head of the worker page
    """
    
    ret = """
          <script type="text/javascript" src="/scripts/app/workers.js"></script>     
    """
    
    return ret

def get_body():
    """
    returns HTML of body of the workers page
    """
    
    first = 0
    size = 10
    controls = ""
    label = ""
    task_details = ""

    tmpl = lookup.get_template("workers_base.html")
    ret = tmpl.render(first=first, size=size, controls=controls)#, label=label, task_details=task_details)
        
    return ret
    
def worker_table(workers, fisrt, size):
    """
    returns HTML of workers table - workers is a slice of all worker given by first and size
    """  
    ret = """
        <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No.</th>
                <th>Username</th>
                <th>Machine</th>
                <th>IP address</th>   
                <th>System</th>              
                <th>Worker's state</th>
                <th>Activity</th>
              </tr>
            </thead>
            <tbody>      
    """
    
    for i in range(workers.count()):
        worker = workers[i]
        ping_latency = abs((time.time() - worker["worker_last_ping"]) - worker["worker_DEFAULT_PING_INTERVAL"] )
        ping_latency_ratio = ping_latency/worker["worker_DEFAULT_PING_INTERVAL"]
        if (ping_latency_ratio > 2.):
            ping_latency_str = "N/A" 
        else:
            ping_latency_str = "OK" #int(ping_latency)
            
        ret += """
        <tr>
            <td>%d</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>                                                   
        </tr>
        """ % (i+1, 
               worker["worker_username"], 
               worker["worker_machine_name"],
               str(worker["worker_ip"]), 
               worker["worker_system"],
               get_worker_state_badge(worker["worker_state"], ping_latency_ratio), 
               ping_latency_str
               )
    
    ret += """
            </tbody>  
        </table>    
    """
    
    return ret
    
    
def get_worker_state_badge(state, ping_latency_ratio):
    """
    returns graphical badge indicating state of the worker
    """    
    if ping_latency_ratio > 2.:
        state = 2
        
    values = ["""<span class="label label-success">IDLE</span>""",
              """<span class="label label-warning">BUSY</span>""",
              """<span class="label label-error">DOWN</span>"""]
    return values[state]
    
    
    