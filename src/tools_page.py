import cherrypy
import copy
import config as cf
import db_models
import db_tools_receptors
import web_html_receptors
import simplejson as sj

#standard config of all exposed classes in my app
cp_conf_common = cf.cp_conf_common

SESSION_KEY = cf.SESSION_KEY
HTML_TEMP_DIR = cf.HTML_TEMP_DIR
SCRIPTS_DIR = cf.SCRIPTS_DIR

class Tools(object):
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    #workers_page is open to all users
    #cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    
    @cherrypy.expose
    def load_receptors(self, **kwargs): #same as in the case of task table.. firt worker to display and the number of displayed workers
        #calls methods for table generation
        _id = kwargs["_id"]
        cherrypy.response.headers['Content-Type'] = 'application/json'
        receptor_data = db_tools_receptors.get_receptors_of_id(_id)["data"]
        return sj.dumps({"receptor_data": receptor_data})
    
    
    @cherrypy.expose
    def save_receptors(self,  **kwargs):
        description  = kwargs["description"]
        data = kwargs["data"]
        username = cherrypy.session.get(SESSION_KEY, None)
        db_entry = db_models.receptors_DB_model(username, description, data)
        res = db_tools_receptors.save_receptors(db_entry)
        print description
        print data
    
        return res
   
   
    @cherrypy.expose
    def delete_receptors(self, **kwargs):
        _id = kwargs["_id"]
        res = db_tools_receptors.delete_receptors(_id)
        return res
        
    @cherrypy.expose    
    def update_receptors_load_form(self):
        """
        return HTML table with list of saved tasks
        """
        username = cherrypy.session.get(cf.SESSION_KEY) 
        receptors_table = web_html_receptors.get_saved_receptors_table(username)
        return receptors_table

        
        
        
        
        
        

