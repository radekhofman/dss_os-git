# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
########################################################################################
#### BODY_MAIN
########################################################################################

def main_body():
    """
    gets body of the main page
    """
    tmpl = lookup.get_template("main_page.html")
    is_user_logged = cherrypy.session.get(cf.SESSION_KEY) != None
    ret = tmpl.render(is_user_logged=is_user_logged)
    
    return ret

########################################################################################
def login_form(msg):
    """
    login form for non-logged users
    """
    
    ret = ""
    
    username = cherrypy.session.get(cf.SESSION_KEY)
    
    if (username == None):
        #aditional styles for login form
        ret += """
            <style type="text/css">
              /* Override some defaults */
              body {
                padding-top: 60px; 
                
                
              }
            </style>        
                
        """
        if (msg != None):
            ret += """<div class="alert alert-error"><center> %s </center></div>""" % msg
        
        ret += """

          <div class="row-fluid">
              <div class="span4 offset4">
                  <div class="login-form" style="margin-bottom: 200px;">
                      <h2>Login</h2>
                      <form action="/login" method="post">
                          <fieldset>
                              <div class="clearfix">
                                  <input type="text" name="username" placeholder="Username">
                              </div>
                              <div class="clearfix">
                                  <input type="password" name="password" placeholder="Password">
                              </div>
                              <button class="btn btn-primary" type="submit">Sign in</button>
                              or
                              <a href="/guest/login" class="btn btn-success">Login as guest</a>
                          </fieldset>
                      </form>
                  </div>
              </div>
          </div>
        """
    else: #when user is logged, login form is not ofeered in navibar anymore, user can only logout
        ret += ""
        
    return ret