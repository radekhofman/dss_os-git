# -*- encoding: UTF-8 -*-
import db_tools as dt
import admin_tools as at
from db_tools import ma2mo #ndarray serialization to string - can be inserted into mongo
import time
import numpy

if __name__ == "__main__":
    dt.clear_mongo()

    #some testing users
    dt.insert_user(username="panda", password="bambus", email="wombat@panda.cz", user_type="user" )
    dt.insert_user(username="wombat", password="bambus", email="wombat@panda.cz", user_type="admin" )
    dt.insert_user(username="a", password="a", email="wombat@panda.cz", user_type="admin" )
    
    
    #slots for workers
    dt.insert_user(username="worker_0", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_1", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_2", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_3", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_4", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_5", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_6", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_7", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_8", password="worker", email="N/A", user_type="worker" )
    dt.insert_user(username="worker_9", password="worker", email="N/A", user_type="worker" )

    #printing of reuslts
    at.print_users()
    at.print_tasks()
    
