# -*- encoding: UTF-8 -*-
'''
Implementation of Guest users deleting job.
'''
import sched 
import time
import thread

from app_loging import app_logger as logging

import db_tools_users

# Repeat interval of deleting job in seconds
REPEAT_INTERVAL_DELETING_JOB = 60*10

def start_guest_deleting_job():
    """
    Starts guest user deleting job.
    """
    logging.debug("Scheduling guest users deleting job")
    def scheduler_thread():
        scheduler = sched.scheduler(time.time, time.sleep)
        def guest_delete_job():
            logging.debug("Started guest users deleting job")
            try:
                db_tools_users.delete_expired_guest_users()
            except Exception, e:
                logging.debug("Job Execution end with error %s" % str(e))
                logging.exception(e)
            finally:
                scheduler.enter(REPEAT_INTERVAL_DELETING_JOB, 1, guest_delete_job, ())
                logging.debug("Finished guest users deleting job")
        scheduler.enter(REPEAT_INTERVAL_DELETING_JOB, 1, guest_delete_job, ())
        scheduler.run()
    thread.start_new_thread(scheduler_thread,())
