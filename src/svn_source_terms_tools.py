#coding=utf-8
"""
tools for insertion and export of source terms in SVN into the MongoDB

current source term DB model:

    d = {"username": kwargs["username"], #username
         "activities": kwargs["activities"], #numpy.array: must be pickeld before insertion to DB
         "segment_properties": kwargs["segment_properties"], #numpy.array: must be pickeld before insertion to DB
         "segments_count": kwargs["segments_count"],
         "nuclides": kwargs["nuclides"],
         "description": kwargs["description"],
         "modified": time.time()
         }
"""
import re
import files_tools
import numpy
import admin_tools
import os
import time
import db_tools_source_terms as dtst


def load_source_term(path, f_name, username="", MacWin=0):
    """ Opens a file with a source term"""
    
    handle = open(path+os.sep+f_name,"r")
    s = handle.readlines()
    handle.close()


    if (len(s) > 0): #je zadano jmeno nejakeho soubory
        #vytvorim novy objekt typu Source_term a naplnim ho
        
        line = re.sub('^[ ]+', '', s[3])
        doby = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin])[:-1])        
        
        line = re.sub('^[ ]+', '', s[6])
        tep_kap = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin])[:-1])
        
        line = re.sub('^[ ]+', '', s[9])
        rychlosti = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin])[:-1])
        
        line = re.sub('^[ ]+', '', s[12])
        vysky = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin])[:-1])
        
        segments_count = len(doby)
        
        print "Number of segments: ", segments_count
        nucl_no = len(s)-15
        
        activities = numpy.zeros((nucl_no, segments_count))
        counter = 0
        nuclides = []
        #ted pekne po segmentech roztridim nuklidy
        for line in s[15:]:
            nuclides.append(line[:12].rstrip())
            line0 = re.sub('^[ ]+', '', line[12:])
            
            field = re.split('[\r\n\t ]+',line0[0:len(line0)-MacWin])
            
            if (len(field) > segments_count): 
                print "panda"
                field = re.split('[\r\n\t ]+',line0[0:len(line0)])[:-1]
                print field
                      
            activities[counter, :] = map(float, field)[:]
            print nuclides[counter], activities[counter, :]
            counter += 1
       
        segments_properties = numpy.zeros((4, segments_count))

        for j in range(segments_count):
            segments_properties[0,j] = doby[j]
            segments_properties[1,j] = tep_kap[j]
            segments_properties[2,j] = rychlosti[j] 
            segments_properties[3,j] = vysky[j] 
                
        description = unicode( "<b>"+f_name+"</b><br>"+s[0]+"<br>"+s[1], errors='ignore')
        
        print "Summary"
        print activities
        print nuclides
        print segments_properties
        print segments_count
        print description
        
        d = {"username": username,
             "activities": activities,
             "nuclides": nuclides,
             "segments_properties": segments_properties,
             "segments_count": segments_count,
             "description": description,
             "modified": time.time(),
             #"H": vysky
             }
        
        return d



def OnSave_source_term(st, fname=''):
    """ Opens a file with a source term"""
    
    impl_path = ""
    if (fname):
        impl_path = fname


    #je vybrana nejaka cesta
    if (len(impl_path) > 0):
        try:
            handle = open(impl_path,"w+")
            out = make_source_term_write_string(st)
            print out
            handle.write(out)
            handle.close()
        except IOError:
            print "Nelze otevrit soubor pro zapis zdrojoveho clenu"


def make_source_term_write_string(st):
    """udelam obsah souboru *.svn se zdrojovym clenem a vrati ho pro zapis"""
    s = ""

    s += st.text1 + "\n"
    s += st.text2 + "\n"
    s += "Trvani fazi uniku (hodiny)\n"
    for segment in st.segmenty:
        s += files_tools.make_string("%5.4f" % segment.doba_uniku, 10, 1)
    s += "\n\n"
    s += "Energie uniku ve fazich (kW)\n"
    for segment in st.segmenty:
        s += files_tools.make_string("%2.1f" % segment.tepelna_vydatnost, 10, 1)
    s += "\n\n"
    s += "Vertikalni rychlost vytoku (m/s)\n"
    for segment in st.segmenty:
        s += files_tools.make_string("%2.1f" % segment.vertikalni_rychlost, 10, 1)
    s += "\n\n"
    s += "Vyska uniku ve fazich (m/s)\n"
    for segment in st.segmenty:
        s += files_tools.make_string("%4.1f" % segment.vyska_zdroje, 10, 1)
    s += "\n\n"

    s += "Nuklidy a unikle aktivity: [Bq]"

    dict = st.get_nuclide_dictionary()

    nuclide_list = st.get_list_of_nuclides()

    for nuclide in nuclide_list:
        list = dict[nuclide]
        s += "\n"+files_tools.make_string(nuclide, 11, 0)
        for data in list:
            s += files_tools.make_string("%3.2E" % data, 10, 1)

    return s


if __name__ == "__main__":
    #path_to_sourceterms = "source_terms_svn"+os.sep+"batch_1"
    path_to_sourceterms = "source_terms_svn"+os.sep+"batch_2"
    # f_name = "ETE_vystreleni regul- svazku_2005.svn"
    # load_source_term(path_to_sourceterms, f_name)
    
    files_all = os.listdir(path_to_sourceterms)
    svn_files = []
    
    print "Insertion of source terms into DB"
    print "Following source terms will be inserted:"
    for file in files_all:
        if file.endswith("svn") and not file.startswith("."):
            print file
            svn_files.append(file)
            
    print "Available users:"
    admin_tools.print_users()
    username = raw_input("Enter username: ")
    
    for file in svn_files:
        st = load_source_term(path_to_sourceterms, file, username=username)
        dtst.insert_source_term(st)
       
    
    
    
    
    