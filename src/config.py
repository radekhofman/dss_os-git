# -*- encoding: UTF-8 -*-
import os
import openshift.variables as os_vars

SESSION_KEY = '_cp_username'

HTML_TEMP_D = os.path.join(os_vars.OPENSHIFT_REPO_DIR,"wsgi",u"html_templates") if os_vars.OPENSHIFT_REPO_DIR else u"html_templates"
HTML_TEMP_DIR = os.path.join(os.path.abspath("."), HTML_TEMP_D)

SCRIPTS_D = u"static/scripts"
SCRIPTS_DIR = os.path.join(os.path.abspath("."), SCRIPTS_D)

IMAGES_D = u"static/images"
IMAGES_DIR = os.path.join(os.path.abspath("."), IMAGES_D)

STYLES_D = u"static/css_styles"
STYLES_DIR = os.path.join(os.path.abspath("."), STYLES_D)

TEMP_D = u"overlays"
#TEMP_DIR = os.path.join(os.path.abspath("."), TEMP_D)
TEMP_DIR = os.path.join(os_vars.OPENSHIFT_REPO_DIR,"wsgi","static","overlays") if os_vars.OPENSHIFT_REPO_DIR else TEMP_D


DATAFILES_D = u"datafiles"
DATAFILES_DIR = os.path.join(os_vars.OPENSHIFT_TMP_DIR,DATAFILES_D) if os_vars.OPENSHIFT_REPO_DIR else DATAFILES_D
#DATAFILES_DIR = os.path.join(os.path.abspath("."), DATAFILES_D)


#config same for all the parts
cp_conf_common = {          
        'tools.sessions.on': True,
        'tools.auth.on': True
    }


global_conf_prod = {
       'global':    { 'server.environment': 'production',
                      'engine.autoreload_on': True,
                      'engine.autoreload_frequency': 5,
                      'server.socket_host': '147.231.16.143',
                      'server.socket_port': 80,
                      'tools.sessions.on': True,
                      'tools.auth.on': True,
                    }
              }

global_conf_openshift = {
       'global':    { 'server.environment': 'production',
                      'engine.autoreload_on': True,
                      'engine.autoreload_frequency': 5,
                      'tools.sessions.on': True,
                      'tools.auth.on': True,
                    }
              }

#different configs for different deployment cases
global_conf_prod_ssl = {
       'global':    { 'server.environment': 'production',
                      'engine.autoreload_on': True,
                      'engine.autoreload_frequency': 5,
                      'server.socket_host': '147.231.16.143',
                      #'server.socket_port': 80,
                      'tools.sessions.on': True,
                      'tools.auth.on': True,
                      'server.socket_port':443,
                      'server.ssl_module':'pyopenssl',
                      'server.ssl_certificate':'keys'+os.sep+'dss.utia.cas.cz-1357637803.pem',
                      'server.ssl_private_key':'keys'+os.sep+'dss-serverkey.pem',                     
                    }
                        
              }

global_conf_devel = {
                     
       'global':    { 'server.environment': 'production',
                      'engine.autoreload_on': True,
                      'engine.autoreload_frequency': 5,
                      'server.socket_host': '0.0.0.0',
                      'server.socket_port': 8080,
                      'tools.sessions.on': True,
                      'tools.auth.on': True,
                                            
                    }
              }


config = {
         '/scripts': #adresar s JS
                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': SCRIPTS_DIR,
                },
         '/images': #adresar s JS
                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': IMAGES_DIR,
                },
          '/css_styles': #adresar s JS
                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': STYLES_DIR,
                },
          '/overlays': #adresar s JS
                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': TEMP_DIR,
                },
          '/datafiles': #adresar s JS
                {'tools.staticdir.on': True,
                 'tools.staticdir.dir': DATAFILES_DIR,
                }          
                    
         }

