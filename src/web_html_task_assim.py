# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools

##########################################################################################################################
### TASK BODY
##########################################################################################################################

def task_body():
    """
    renders body of the new task page
    """
    ret = """
        <div class="page-header">
            <h1>New task <small>Create new task</small></h1>
        </div>    
              <div class="row">
                <div class="span3">
                  <h2>Atmosheric dispersion modeling</h2>
                  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                  <p><a class="btn btn-primary" href="/task/create?task_type=0">Create &raquo;</a></p>
                </div><!--/span-->
                <div class="span3">
                  <h2>Data assimilation Lab</h2>
                  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                  <p><a class="btn btn-primary" href="/task/create?task_type=1">Create &raquo;</a></p>
                </div><!--/span-->
                <div class="span3">
                  <h2>What-if analyses</h2>
                  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                  <p><a class="btn btn-primary" href="/task/create?task_type=2">Create &raquo;</a></p>
                </div><!--/span-->
              </div><!--/row-->
              <div class="row">
    """
    return ret

def task_create_body_0():   #HTML page with forms for runnig of new taskof type 0
    ret = """
          <div class="page-header">
              <h1>New dispersion modeling task <small>Create new task</small></h1>
          </div>      
    """
    
    return ret

def task_create_body_1():   #HTML page with forms for runnig of new taskof type 1
    
    tab1 = assim_scenario()
    
    ret = """
          <div class="page-header">
              <h1>New data assimilation task <small>Create new task</small></h1>
          </div>    
          <div class="tabbable" style="margin-bottom: 18px;">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab1" data-toggle="tab">Assimilation scenario</a></li>
              <li><a href="#tab2" data-toggle="tab">Source term</a></li>
              <li><a href="#tab3" data-toggle="tab">Meteorological data</a></li>
              <li><a href="#tab4" data-toggle="tab">Observations (twin model)</a></li>
            </ul>
            <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
              <div class="tab-pane active" id="tab1">
                <h2>Assimilation scenario</h2>
                %s
              </div>
              <div class="tab-pane" id="tab2">
                <h2>Source term</h2>
                <p>Howdy, I'm in Section 2.</p>
              </div>
              <div class="tab-pane" id="tab3">
                <h2>Meteorlogical data</h2>
                <p>What up girl, this is Section 3.</p>
              </div>
              <div class="tab-pane" id="tab4">
                <h2>Observations (twin model)<h2>
                <p>What up girl, this is Section 3.</p>
              </div>              
            </div>
          </div> <!-- /tabbable -->              
    """ % tab1
    
    return ret

def task_create_body_2():   #HTML page with forms for runnig of new taskof type 2
    ret = """
          <div class="page-header">
              <h1>New task <small>Create new task</small></h1>
          </div>       
    """
    
    return ret

################################
#### TABS
################################

def assim_scenario():
    """
    reurn HTML for the firt tab
    """
    ret = """
           <div class="row">
            <div class="span4">
              <h3>Prepend &amp; append inputs</h3>
              <p>Input groups&mdash;with appended or prepended text&mdash;provide an easy way to give more context for your inputs. Great examples include the @ sign for Twitter usernames or $ for finances.</p>
              <hr>
              <h3>Checkboxes and radios</h3>
              <p>Up to v1.4, Bootstrap required extra markup around checkboxes and radios to stack them. Now, it's a simple matter of repeating the <code>&lt;label class="checkbox"&gt;</code> that wraps the <code>&lt;input type="checkbox"&gt;</code>.</p>
              <p>Inline checkboxes and radios are also supported. Just add <code>.inline</code> to any <code>.checkbox</code> or <code>.radio</code> and you're done.</p>
              <hr>
              <h4>Inline forms and append/prepend</h4>
              <p>To use prepend or append inputs in an inline form, be sure to place the <code>.add-on</code> and <code>input</code> on the same line, without spaces.</p>
              <hr>
              <h4>Form help text</h4>
              <p>To add help text for your form inputs, include inline help text with <code>&lt;span class="help-inline"&gt;</code> or a help text block with <code>&lt;p class="help-block"&gt;</code> after the input element.</p>
            </div>
            <div class="span8">
              <form class="form-horizontal">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label">Form grid sizes</label>
                    <div class="controls docs-input-sizes">
                      <input class="span1" type="text" placeholder=".span1">
                      <input class="span2" type="text" placeholder=".span2">
                      <input class="span3" type="text" placeholder=".span3">
                      <select class="span1">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                      <select class="span2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                      <select class="span3">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                      <p class="help-block">Use the same <code>.span*</code> classes from the grid system for input sizes.</p>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Alternate sizes</label>
                    <div class="controls docs-input-sizes">
                      <input class="input-mini" type="text" placeholder=".input-mini">
                      <input class="input-small" type="text" placeholder=".input-small">
                      <input class="input-medium" type="text" placeholder=".input-medium">
                      <p class="help-block">You may also use static classes that don't map to the grid, adapt to the responsive CSS styles, or account for varying types of controls (e.g., <code>input</code> vs. <code>select</code>).</p>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="prependedInput">Prepended text</label>
                    <div class="controls">
                      <div class="input-prepend">
                        <span class="add-on">@</span><input class="span2" id="prependedInput" size="16" type="text">
                      </div>
                      <p class="help-block">Here's some help text</p>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="appendedInput">Appended text</label>
                    <div class="controls">
                      <div class="input-append">
                        <input class="span2" id="appendedInput" size="16" type="text"><span class="add-on">.00</span>
                      </div>
                      <span class="help-inline">Here's more help text</span>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="appendedPrependedInput">Append and prepend</label>
                    <div class="controls">
                      <div class="input-prepend input-append">
                        <span class="add-on">$</span><input class="span2" id="appendedPrependedInput" size="16" type="text"><span class="add-on">.00</span>
                      </div>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="appendedInputButton">Append with button</label>
                    <div class="controls">
                      <div class="input-append">
                        <input class="span2" id="appendedInputButton" size="16" type="text"><button class="btn" type="button">Go!</button>
                      </div>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="appendedInputButtons">Two-button append</label>
                    <div class="controls">
                      <div class="input-append">
                        <input class="span2" id="appendedInputButtons" size="16" type="text"><button class="btn" type="button">Search</button><button class="btn" type="button">Options</button>
                      </div>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="inlineCheckboxes">Inline checkboxes</label>
                    <div class="controls">
                      <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                      </label>
                      <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                      </label>
                      <label class="checkbox inline">
                        <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                      </label>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="optionsCheckboxList">Checkboxes</label>
                    <div class="controls">
                      <label class="checkbox">
                        <input type="checkbox" name="optionsCheckboxList1" value="option1">
                        Option one is this and that&mdash;be sure to include why it's great
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" name="optionsCheckboxList2" value="option2">
                        Option two can also be checked and included in form results
                      </label>
                      <label class="checkbox">
                        <input type="checkbox" name="optionsCheckboxList3" value="option3">
                        Option three can&mdash;yes, you guessed it&mdash;also be checked and included in form results
                      </label>
                      <p class="help-block"><strong>Note:</strong> Labels surround all the options for much larger click areas and a more usable form.</p>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Radio buttons</label>
                    <div class="controls">
                      <label class="radio">
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        Option one is this and that&mdash;be sure to include why it's great
                      </label>
                      <label class="radio">
                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                        Option two can be something else and selecting it will deselect option one
                      </label>
                    </div>
                  </div>
                  <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button class="btn">Cancel</button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div><!-- /row -->
      """
    return ret    
