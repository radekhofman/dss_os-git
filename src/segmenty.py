#coding=utf-8
import files_tools
import wx
import wx.grid
import wx.lib.buttons
import hin00dat_class
import time
import numpy
import os
import re
from dictionary import gl

[wxID_PANEL1, wxID_PANEL1ADD_BUTTON, wxID_PANEL1ALL_NUCL_LIST,
 wxID_PANEL1LOAD_SOURCE_BUTTON, wxID_PANEL1REMOVE_BUTTON,
 wxID_PANEL1SAVE_SOURCE_BUTTON, wxID_PANEL1SEG_NO_COMBO,
 wxID_PANEL1SEL_NUCL_LIST, wxID_PANEL1STATICTEXT1, wxID_PANEL1STATICTEXT2,
 wxID_PANEL1STATICTEXT3, wxID_PANEL1STATICTEXT4, wxID_PANEL1STATICTEXT5,
 wxID_PANEL1TEXTCTRL1, wxID_PANEL1TEXTCTRL2,
] = [wx.NewId() for _init_ctrls in range(15)]


class MyPage(wx.Panel):
    """
    each panel instance creates a notbook page
    with a similar design and mytext content
    """
    def __init__(self, parent, lang, MacWin):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        #self.SetBackgroundColour(mycolor)
        self.lang = lang
        self.MacWin = MacWin
      ###HORNI PULKA
        #nacte ze souboru dostupne nuklidy
        self.create_nucl_list()

        self.panel_top = wx.Panel(self, -1)

        self.all_nucl_listBox = wx.CheckListBox(choices=self.nucl_list,
              id=wxID_PANEL1ALL_NUCL_LIST, name=u'all_nucl_list', parent=self.panel_top,
              pos=wx.Point(24, 40), size=wx.Size(104, 200), style=0)

        self.sel_nucl_listBox = wx.CheckListBox(choices=self.sel_nucl_l,
              id=wxID_PANEL1SEL_NUCL_LIST, name=u'sel_nucl_list', parent=self.panel_top,
              pos=wx.Point(216, 40), size=wx.Size(104, 200), style=0)

        self.staticText1 = wx.StaticText(id=wxID_PANEL1STATICTEXT1,
              label=gl('DS1', -1, self.lang), name='staticText1', parent=self.panel_top,
              pos=wx.Point(24, 24), size=wx.Size(111, 16), style=0)
        self.staticText1.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD,
              False, u'Tahoma'))

        self.staticText2 = wx.StaticText(id=wxID_PANEL1STATICTEXT2,
              label=gl('DS2', -1, self.lang), name='staticText2', parent=self.panel_top,
              pos=wx.Point(216, 24), size=wx.Size(99, 14), style=0)
        self.staticText2.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))

        self.add_button = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.Bitmap(u'../maps/pics/next.png',
              wx.BITMAP_TYPE_PNG), id=wxID_PANEL1ADD_BUTTON, label=u'',
              name=u'add_button', parent=self.panel_top, pos=wx.Point(144, 64),
              size=wx.Size(56, 56), style=0)
        self.add_button.Bind(wx.EVT_BUTTON, self.OnAdd_buttonButton,
              id=wxID_PANEL1ADD_BUTTON)

        self.remove_button = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.Bitmap(u'../maps/pics/back.png',
              wx.BITMAP_TYPE_PNG), id=wxID_PANEL1REMOVE_BUTTON, label=u'',
              name=u'remove_button', parent=self.panel_top, pos=wx.Point(144, 144),
              size=wx.Size(56, 56), style=0)
        self.remove_button.Bind(wx.EVT_BUTTON, self.OnRemove_buttonButton,
              id=wxID_PANEL1REMOVE_BUTTON)
   
        self.textCtrl1 = wx.TextCtrl(id=wxID_PANEL1TEXTCTRL1, name='textCtrl1',
              parent=self.panel_top, pos=wx.Point(350, 48), size=wx.Size(450, 24),
              style=0, value='Zdrojovy clen')

        self.staticText3 = wx.StaticText(id=wxID_PANEL1STATICTEXT3,
              label=gl('DS3', -1, self.lang), name='staticText3', parent=self.panel_top,
              pos=wx.Point(350, 24), size=wx.Size(154, 16), style=0)
        self.staticText3.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))


        self.staticText4 = wx.StaticText(id=wxID_PANEL1STATICTEXT4,
              label=gl('DS4', -1, self.lang), name='staticText4', parent=self.panel_top,
              pos=wx.Point(350, 86), size=wx.Size(149, 16), style=0)
        self.staticText4.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))

        cas = time.strftime("%d.%m.%y %H:%M")
        self.textCtrl2 = wx.TextCtrl(id=wxID_PANEL1TEXTCTRL2, name='textCtrl2',
              parent=self.panel_top, pos=wx.Point(350, 110), size=wx.Size(450, 24),
              style=0, value=cas)


        self.staticText5 = wx.StaticText(id=wxID_PANEL1STATICTEXT5,
              label=gl('DS5', -1, self.lang), name='staticText5', parent=self.panel_top,
              pos=wx.Point(350, 158), size=wx.Size(110, 16), style=0)
        self.staticText5.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))
        self.seg_no_combo = wx.ComboBox(choices=[str(i) for i in range(1,25)], id=wxID_PANEL1SEG_NO_COMBO,
              name=u'seg_no_combo', parent=self.panel_top, pos=wx.Point(470, 156),
              size=wx.Size(50, 24), style=0, value='5')
        self.seg_no_combo.Bind(wx.EVT_COMBOBOX, self.OnSeg_no_comboCombobox,
              id=wxID_PANEL1SEG_NO_COMBO)
     
        self.load_source_button = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.Bitmap(u'../maps/pics/load_s.png',
              wx.BITMAP_TYPE_PNG),
              id=wxID_PANEL1LOAD_SOURCE_BUTTON,
              label=gl('DS6', -1, self.lang),
              name=u'load_source_button', parent=self.panel_top, pos=wx.Point(350, 200),
              size=wx.Size(160, 40), style=0)
        self.load_source_button.Bind(wx.EVT_BUTTON,
              self.OnLoad_source_buttonButton,
              id=wxID_PANEL1LOAD_SOURCE_BUTTON)

        self.save_source_button = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.Bitmap(u'../maps/pics/save_s.png',
              wx.BITMAP_TYPE_PNG),
              id=wxID_PANEL1SAVE_SOURCE_BUTTON,
              label=gl('DS7', -1, self.lang),
              name=u'save_source_button', parent=self.panel_top, pos=wx.Point(550, 200),
              size=wx.Size(160, 40), style=0)
        self.save_source_button.Bind(wx.EVT_BUTTON,
              self.OnSave_source_buttonButton,
              id=wxID_PANEL1SAVE_SOURCE_BUTTON)
       

        self.nucl_no = len(self.sel_nucl_l)
        self.seg_no = int(self.seg_no_combo.GetValue())

        self.sizer_tot = wx.BoxSizer(wx.VERTICAL)
        self.sizer_tot.Add(self.panel_top, 0, wx.BOTTOM, 20)
        self.SetSizer(self.sizer_tot)

        self.first_run = True
        self.grid_width = 1000

        #vytvorim po startu prvni tridu zdrojoveho clenu
        self.segments = [] #segmentu
        #pripravime pole na odkladani hodnot pri zmene rozmeru tabulky
        self.data_segm = numpy.zeros((4,50))
        #self.data_nucl = numpy.zeros((132,50))

        self.create_segment_grid()
        self.create_nuclide_grid()

        self.source_term = hin00dat_class.Source_term()



    def OnAdd_buttonButton(self, event):
        #GetChecked vrci tuple intu
        #GetCheckedString vraci tuple stringu na tech pozicich
        #pri zmene nacteme soucasna data a po zmene velikosti tabulky je vratime

        selections = self.all_nucl_listBox.GetCheckedStrings()
        for sel in selections:
            if (not sel in self.sel_nucl_l):
                self.sel_nucl_l.append(sel)

        #deselect all items in nucl_listbox
        for i in self.all_nucl_listBox.GetChecked():
            self.all_nucl_listBox.Check(i, check=False)
            
        #self.sel_nucl_l.sort()
        #rovnani nuklidu podle atomovyho cisla pred vykresleni do GUI
        self.sel_nucl_l = self.sort_nuclides(self.sel_nucl_l)
        self.sel_nucl_listBox.Set(self.sel_nucl_l)
        print "Selected nuclides: ", self.sel_nucl_l


        self.create_segment_grid()
        self.create_nuclide_grid()

        event.Skip()

    def OnRemove_buttonButton(self, event):
        selections = self.sel_nucl_listBox.GetCheckedStrings()
        for sel in selections:
            self.sel_nucl_l.remove(sel)
        
        #deselect all items in sel_nucl_listbox
        for i in self.all_nucl_listBox.GetChecked():
            self.all_nucl_listBox.Check(i, check=False)        
        
        #self.sel_nucl_l.sort()
        #rovnani nuklidu podle atomovyho cisla pred vykresleni do GUI
        self.sel_nucl_l = self.sort_nuclides(self.sel_nucl_l)
        self.sel_nucl_listBox.Set(self.sel_nucl_l)
        #deselect all items in nucl_listbox
        for i in self.sel_nucl_listBox.GetChecked():
            self.sel_nucl_listBox.Check(i, check=False)
            
        self.sel_nucl_listBox.Set(self.sel_nucl_l)
        print "Selected nuclides: ", self.sel_nucl_l

        self.create_segment_grid()
        self.create_nuclide_grid()

        event.Skip()

    def OnSeg_no_comboCombobox(self, event):
        "zmena poctu segmentu tim kombem"
        #priprava na zmenu, aby nezmizely uz hotovy data

        self.create_segment_grid()
        self.create_nuclide_grid()

        if (self.GetParent().GetParent().page5.meteo_type_combo1.GetValue() == self.GetParent().GetParent().page5.combo_choices1[0]):
            if (not self.GetParent().GetParent().page5.bottom_panel.seg_no_combo.GetValue() == self.seg_no_combo.GetValue()):
                self.GetParent().GetParent().page5.bottom_panel.seg_no_combo.SetValue(self.seg_no_combo.GetValue())
                self.GetParent().GetParent().page5.bottom_panel.On_seg_count_Combobox(0)

    def OnLoad_source_buttonButton(self, event):
        self.OnOpen_source_term(self.MacWin)
        event.Skip()

    def OnSave_source_buttonButton(self, event):
        st = self.map_GUI_to_source_term_object()
        self.OnSave_source_term(st)
        event.Skip()

    def create_nucl_list(self):
        "nacte ze souboru seznam nuklidu"
        self.sel_nucl_l = []
        self.nucl_list = ["H3",      "BE7",     "C14",     "F18",     "NA22",    "NA24",    
        "CL38",    "AR41",    "K42",     "CR51",    "MN54",    "MN56",    
        "FE55",    "FE59",    "CO58",    "CO60",   "NI63",    "CU64",    
        "ZN65",    "AS76",    "KR85M",   "KR85",    "KR87",    "KR88",    
        "KR89",    "KR90",    "RB86",    "RB88",    "RB89",   "SR89",    
        "SR90",    "SR91",    "SR92",    "Y88",     "Y90",     "Y91M",    
        "Y91",     "ZR95",    "ZR97",    "NB95",    "NB97",    "MO99",    
        "TC99M",   "TC99",    "RU103",   "RU105",   "RU106",   "RH105",   
        "RH106M",  "AG110M",  "SB122",   "SB124",   "SB125",   "SB127",   
        "SB129",   "TE125M",  "TE127M",  "TE127",   "TE129M",  "TE129",   
        "TE131M",  "TE131",   "TE132",   "TE133M",  "TE133",   "TE134",   
        "I129O",   "I129",    "I129A",   "I131O",   "I131",    "I131A",   
        "I132O",   "I132",    "I132A",  "I133O",   "I133",    "I133A",   
        "I134O",   "I134",   "I134A",   "I135O",   "I135",    "I135A",   
        "XE131M",  "XE133M",  "XE133",   "XE135M",  "XE135",   "XE137",   
        "XE138",   "CS134",   "CS136",   "CS137",   "CS138",   "BA139",   
        "BA140",   "LA140",   "CE139",   "CE141",   "CE143",   "CE144",   
        "PR143",   "PR144",   "ND147",   "PM147",   "SM153",   "EU154",   
        "W187",    "U235",    "U238",    "NP239",   "PU238",   "PU239",   
        "PU240",   "PU241",   "AM241",   "CM242",   "CM244",   "HTO",     
        "H3O",     "CO2",     "CO"]
        #self.nucl_list.sort()


    def sort_nuclides(self, keys): #keys je pole nuklidu....
        """sorts nuclides according to their atomic number"""
        nuc = [""] * 123 #number of nuclides is 123

        for nucl in keys:
            if (nucl.strip() in self.nucl_list):
                index = self.nucl_list.index(nucl.strip())
                nuc[index] = nucl

        nuclides_sort = []

        for nucl in nuc:
            if nucl != "":
                nuclides_sort.append(nucl)

        return nuclides_sort

    def create_nuclide_grid(self):
        "tabulka s nuklidama"
     
        print "nucl no %d, nucl_no_old %d" % ( self.nucl_no, self.nucl_no_old)
        self.seg_no = int(self.seg_no_combo.GetValue())
        self.nucl_no = len(self.sel_nucl_l)

        dict = {}
        if (not self.first_run):
            for j in range(self.nucl_no_old):
                nucl_data = numpy.zeros(self.seg_no_old)
                for i in range(self.seg_no_old):
                    nucl_data[i] = float(self.nucl_grid.GetCellValue(j,i))

                nucl_name = self.nucl_grid.GetRowLabelValue(j)
                dict[nucl_name] = nucl_data

            self.sizer_tot.Remove(self.nucl_grid)
            self.sizer_tot.Remove(self.table_label_1)
            self.table_label_1.Destroy()
            self.nucl_grid.Destroy()

        #dela se az jako druha tabulka, musi to bejt tady
        self.first_run = False

        self.nucl_grid = wx.grid.Grid(self, wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize)#(grid_width, 200))
        self.nucl_grid.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.OnChangeNuclGrid)
        self.nucl_grid.CreateGrid(self.nucl_no, self.seg_no)
        self.nucl_grid.SetRowLabelSize(180)
        self.nucl_grid.SetDefaultColSize(60)
        #self.nucl_grid.SetDefaultColSize((self.grid_width-180)/self.seg_no, True)

        for i in range(self.seg_no):
            for j in range(self.nucl_no):
                self.nucl_grid.SetCellValue(j,i,"%3.2E" % 0.0)
                self.nucl_grid.SetCellAlignment(j,i,wx.ALIGN_RIGHT,wx.ALIGN_CENTRE)

        for i in range(self.seg_no):
            self.nucl_grid.SetColLabelValue(i, "%d" % (i+1))
        for i in range(self.nucl_no):
            self.nucl_grid.SetRowLabelValue(i, self.sel_nucl_l[i])

        #na zaver prihodim do sizeru a udelam layout
        self.table_label_1 = wx.StaticText(self, -1, label=gl('DS13', -1, self.lang), pos=wx.DefaultPosition, size=wx.DefaultSize)
        self.table_label_1.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Tahoma'))
        self.sizer_tot.Add(self.table_label_1, 0, wx.EXPAND|wx.BOTTOM, 10)
        self.staticText2.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False,
              u'Tahoma'))
        self.sizer_tot.Add(self.nucl_grid, 1, wx.EXPAND)

        #do resizovane tabulky vratime puvodni data
        if (not self.first_run):
            for j in range(self.nucl_no):
                rl = self.nucl_grid.GetRowLabelValue(j)
                if (dict.has_key(rl)):
                    data = dict[rl]
                    for i in range(min(self.seg_no_old, self.seg_no)):
                        self.nucl_grid.SetCellValue(j, i, "%3.2E" % data[i])
        self.Layout()

    def create_segment_grid(self):
        "tabulka se segmentama"
        self.seg_no_old = self.seg_no #tadyto se dala prvni, takze zde se uklada puvodni pocet segmentu
        self.nucl_no_old = self.nucl_no #tadyto se dala prvni, takze zde se uklada puvodni pocet segmentu a nuklidu
        self.seg_no = int(self.seg_no_combo.GetValue())
        self.nucl_no = len(self.sel_nucl_l)
        
        #pri zmene nacteme soucasna data a po zmene velikosti tabulky je vratime
        if (not self.first_run):
            self.data_segm = numpy.zeros((4,50))
            for i in range(self.seg_no_old):
                for j in range(4):
                    cell_data = float(self.segm_grid.GetCellValue(j,i))
                    self.data_segm[j, i] = cell_data

            self.sizer_tot.Remove(self.segm_grid)
            self.sizer_tot.Remove(self.table_label_2)
            self.table_label_2.Destroy()
            self.segm_grid.Destroy()


        self.segm_grid = wx.grid.Grid(self, wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize)#(grid_width, 200))
        self.segm_grid.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.OnChangeSegmGrid)

        wx.grid.EVT_GRID_CELL_CHANGE
        self.segm_grid.CreateGrid(4, self.seg_no)
        self.segm_grid.SetRowLabelSize(180)
        self.segm_grid.SetDefaultColSize(60)#(self.grid_width-180)/self.seg_no, True)

        
        for i in range(self.seg_no):
            for j in range(4):
                self.segm_grid.SetCellAlignment(j,i,wx.ALIGN_RIGHT,wx.ALIGN_CENTRE)

        for i in range(self.seg_no):
            self.segm_grid.SetColLabelValue(i, "%d" % (i+1))
            self.segm_grid.SetCellValue(0, i, "%5.4f" % 1.0)
            self.segm_grid.SetCellValue(1, i, "%2.1f" % 0.0)
            self.segm_grid.SetCellValue(2, i, "%2.1f" % 0.0)
            self.segm_grid.SetCellValue(3, i, "%3.2f" % 100.0)

        seg_grid_lab = [gl('DS9', -1, self.lang), gl('DS10', -1, self.lang), gl('DS11', -1, self.lang), gl('DS12', -1, self.lang)] # "tepeln� vydatnost [kW]", "vertik�ln� rychlost [m/s]", "v�ka zdroje �niku [m]"]

        for i in range(4):
            self.segm_grid.SetRowLabelValue(i, seg_grid_lab[i])

        #na zaver prihodim do sizeru a udelam layout
        self.table_label_2 = wx.StaticText(self, -1, label=gl('DS8', -1, self.lang), pos=wx.DefaultPosition, size=wx.DefaultSize)
        self.table_label_2.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Tahoma'))
        self.sizer_tot.Add(self.table_label_2, 0, wx.EXPAND|wx.BOTTOM, 10)
        self.sizer_tot.Add(self.segm_grid, 0, wx.EXPAND|wx.BOTTOM, 20)

        #vratime data co tam uz byly do tabulky segmentu
        if (not self.first_run):
            for i in range(min(self.seg_no, self.seg_no_old)):
                self.segm_grid.SetCellValue(0,i,"%5.4f" % self.data_segm[0, i])
                self.segm_grid.SetCellValue(1,i,"%2.1f" % self.data_segm[1, i])
                self.segm_grid.SetCellValue(2,i,"%2.1f" % self.data_segm[2, i])
                self.segm_grid.SetCellValue(3,i,"%4.1f" % self.data_segm[3, i])

        self.Layout()



    def OnOpen_source_term(self,  MacWin, f_name=''):
        """ Opens a file with a source term"""
        self.impl_path = ""
        self.s = ""
        
        if (f_name):
            self.impl_path = f_name
            handle = open(self.impl_path,"r")
            self.s = handle.readlines()
            handle.close()
        else:
            dlg = wx.FileDialog(self, gl('DS14', -1, self.lang), ".", "", "*.svn", wx.FD_OPEN)
            dlg.CentreOnScreen()
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetFilename()
                dirname = dlg.GetDirectory()
                #f = open(os.path.join(self.dirname, self.filename), 'r')
                self.impl_path = os.path.join(dirname, filename)
                handle = open(self.impl_path,"r")
                self.s = handle.readlines()
                handle.close()
            dlg.Destroy()

        if (len(self.s) > 0): #je zadano jmeno nejakeho soubory
            "ahoj"
            #vytvorim novy objekt typu Source_term a naplnim ho
            line = re.sub('^[ ]+', '', self.s[3])
            doby = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin]))
            line = re.sub('^[ ]+', '', self.s[6])
            tep_kap = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin]))
            line = re.sub('^[ ]+', '', self.s[9])
            rychlosti = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin]))
            line = re.sub('^[ ]+', '', self.s[12])
            vysky = map(float, re.split('[\r\n\t ]+',line[0:len(line)-MacWin]))
            seg_no = len(doby)
            print "Pocet segmentu: ", seg_no
            nucl_no = len(self.s)-15
            
            nuc_seg_tab = numpy.zeros((nucl_no, seg_no))
            counter = 0
            nucl_names = []
            #ted pekne po segmentech roztridim nuklidy
            for line in self.s[15:]:
                nucl_names.append(line[:12].rstrip())
                line0 = re.sub('^[ ]+', '', line[12:])
                nuc_seg_tab[counter, :] = map(float, re.split('[\r\n\t ]+',line0[0:len(line0)-MacWin]))[:]
                print nucl_names[counter], nuc_seg_tab[counter, :]
                counter += 1

            segmenty = []
            for i in range(seg_no):
                nuclides = {}
                for j in range(nucl_no):
                    nucl = hin00dat_class.Nuclide(nucl_names[j], nuc_seg_tab[j, i])
                    nuclides[nucl.name] = nucl.magnitude

                segment = hin00dat_class.Segment(i, doby[i], tep_kap[i], rychlosti[i], vysky[i], nuclides)
                segmenty.append(segment)


            text1 = self.s[0][0:len(self.s[0])-self.MacWin]
            text2 = self.s[1][0:len(self.s[1])-self.MacWin]
            source_term = hin00dat_class.Source_term(text1, text2, segmenty)

            self.map_source_term_object_to_GUI(source_term)

    def OnSave_source_term(self, st, fname=''):
        """ Opens a file with a source term"""
        
        self.impl_path = ""
        if (fname):
            self.impl_path = fname
        else:
            dlg = wx.FileDialog(self, gl('DS14', -1, self.lang), ".", "", "*.svn", wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT)
            dlg.CentreOnScreen()

            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetFilename()
                dirname = dlg.GetDirectory()
                self.impl_path = os.path.join(dirname, filename)

            dlg.Destroy()

        #je vybrana nejaka cesta
        if (len(self.impl_path) > 0):
            try:
                handle = open(self.impl_path,"w+")
                out = self.make_source_term_write_string(st)
                print out
                handle.write(out)
                handle.close()
            except IOError:
                print "Nelze otevrit soubor pro zapis zdrojoveho clenu"


    def OnChangeNuclGrid(self, event):
        "zmena udaju v tabulce nuklidu"
        #self.create_source_term()

    def OnChangeSegmGrid(self, event):
        "zmena udaju v tabulce semgnetu"
        #self.create_source_term()



    def map_GUI_to_source_term_object(self):
        "zkolektuje data ze vsech kolonek a vytvori relevantni source term objekt"
        self.seg_no = int(self.seg_no_combo.GetValue())
        self.nucl_no = len(self.sel_nucl_l)

        segments = [] #segmentu
        

        for i in range(self.seg_no):
            nuclides = {} #nuklidy pro urcitey segment
            seg_no = i #cislo segmentu
            doba = float(self.segm_grid.GetCellValue(0,i)) #trvani segmentu
            teplo = float(self.segm_grid.GetCellValue(1,i)) #tepelna vydanost segmetnu
            rychlost = float(self.segm_grid.GetCellValue(2,i)) #vetiklani rychlost segmentu
            vyska = float(self.segm_grid.GetCellValue(3,i)) #vyska uniku segmentu
            segment = hin00dat_class.Segment(seg_no, doba, teplo, rychlost, vyska)

            for j in range(self.nucl_no):
                "projede vsechny nuklidy v tom segmentu"
                nuclide = hin00dat_class.Nuclide(self.nucl_grid.GetRowLabelValue(j), float(self.nucl_grid.GetCellValue(j, i)) )
                nuclides[nuclide.name] = nuclide.magnitude

            segment.nuclides = nuclides
            segments.append(segment)

        #print segments
        source_term = hin00dat_class.Source_term(self.textCtrl1.GetValue(),
                                                 self.textCtrl2.GetValue(),
                                                 segments)

        return source_term

    def map_source_term_object_to_GUI(self, st): #st = source term
        """obsah objektu typu source term mirroruje do GUI pro segmenty a nuklidy"""
        self.textCtrl1.SetValue(unicode(st.text1, errors='ignore'))
        self.textCtrl2.SetValue(unicode(st.text2, errors='ignore'))

        #nasvatime combo box
        sc = len(st.segmenty)
        self.sel_nucl_l = []

        nucl_list = st.get_list_of_nuclides()

        for nuklid in nucl_list:
            self.sel_nucl_l.append(nuklid)

        self.seg_no_combo.SetValue("%d" % int(sc))
        self.sel_nucl_listBox.Set(self.sel_nucl_l)
        
        self.create_segment_grid()
        self.create_nuclide_grid()

        seg_counter = 0

        for segment in st.segmenty:
            self.segm_grid.SetCellValue(0, seg_counter, "%5.4f" % segment.doba_uniku)
            self.segm_grid.SetCellValue(1, seg_counter, "%2.1f" % segment.tepelna_vydatnost)
            self.segm_grid.SetCellValue(2, seg_counter, "%2.1f" % segment.vertikalni_rychlost)
            self.segm_grid.SetCellValue(3, seg_counter, "%4.1f" % segment.vyska_zdroje)
            nucl_counter = 0

            for nuclid in nucl_list:
                self.nucl_grid.SetCellValue(nucl_counter, seg_counter, "%3.2E" % segment.nuclides[nuclid])
                nucl_counter += 1

            seg_counter += 1

    def make_source_term_write_string(self, st):
        """udelam obsah souboru *.svn se zdrojovym clenem a vrati ho pro zapis"""
        s = ""

        s += st.text1 + "\n"
        s += st.text2 + "\n"
        s += "Trvani fazi uniku (hodiny)\n"
        for segment in st.segmenty:
            s += files_tools.make_string("%5.4f" % segment.doba_uniku, 10, 1)
        s += "\n\n"
        s += "Energie uniku ve fazich (kW)\n"
        for segment in st.segmenty:
            s += files_tools.make_string("%2.1f" % segment.tepelna_vydatnost, 10, 1)
        s += "\n\n"
        s += "Vertikalni rychlost vytoku (m/s)\n"
        for segment in st.segmenty:
            s += files_tools.make_string("%2.1f" % segment.vertikalni_rychlost, 10, 1)
        s += "\n\n"
        s += "Vyska uniku ve fazich (m/s)\n"
        for segment in st.segmenty:
            s += files_tools.make_string("%4.1f" % segment.vyska_zdroje, 10, 1)
        s += "\n\n"

        s += "Nuklidy a unikle aktivity: [Bq]"

        dict = st.get_nuclide_dictionary()

        nuclide_list = st.get_list_of_nuclides()

        for nuclide in nuclide_list:
            list = dict[nuclide]
            s += "\n"+files_tools.make_string(nuclide, 11, 0)
            for data in list:
                s += files_tools.make_string("%3.2E" % data, 10, 1)

        return s
            





        
    












        