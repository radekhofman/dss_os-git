'''
Created on Aug 25, 2013

'''
import admin_tools as at
import cPickle
import locking
import time
import pymongo
import bson
from bson.objectid import ObjectId
from tools import mo2ma, ma2mo

import logging

# getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']
db_nucl_groups = at.dbs['db_nucl_groups']

def get_list_of_groups(username):
    """
    Returns list of user nuclides groups.
    """
    logging.debug("Getting list of nuclides groups")
    groups = db_nucl_groups.find({"username": username})
    return groups

def create_group(group):
    """
    Creates new nuclides group.
    """
    logging.debug("Creating nuclides group.")
    result = {"id" : "-1" , "message" : "Undefined message."}
    try:
        db_nucl_groups.insert(group)
        logging.debug("Group was stored as : '%s'" % (group))
        result["id"] = group["_id"]
        result["message"] = "Group was stored."
    except Exception, e:
        logging.error("Unexpected error when storing nuclides group '%s' into db.", group)
        logging.exception(e)
        result['id'] = '-1'
        result['message'] = "Error when storing nuclided group."
    finally:
        return result


def delete_group(group_id):
    """
    Deletes new nuclides group.
    """
    logging.debug("Deleting nuclides group.")
    db_nucl_groups.remove({"_id": ObjectId(group_id)})
