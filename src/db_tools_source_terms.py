# -*- encoding: UTF-8 -*-
import admin_tools as at
import cPickle
import locking
import time
import pymongo
import bson
from tools import mo2ma, ma2mo
#getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']


def insert_source_term(st):
    """
    inserts source term into collection
    """
    st["activities"] = ma2mo(st["activities"])
    st["segments_properties"] = ma2mo(st["segments_properties"])
    
    if st.has_key("H"):
        st["H"] = ma2mo(st["H"])
        
    db_source_terms.insert(st)
    print "source term inserted!"

def get_list_of_source_terms(username):
    """
    return list of workers
    """
    source_terms = db_source_terms.find({"username": username})
    
    return source_terms

    
def get_source_terms_count(username):
    """
    returns count of registered workers
    """
    return get_list_of_source_terms(username).count()


def save_source_term(st):
    """
    saves source term of a user into source terms collection
    """
    st["activities"] = ma2mo(st["activities"])
    st["rel_heights"] = ma2mo(st["rel_heights"])
    
    res = ""
    
    try:
        db_source_terms.insert(st)
        res = "Source term successfully saved!"
        print res
    except Exception, e:
        print "Error inserting source term", e
        res = "Source term not inserted", e
    finally:
        return res

def get_source_term_of_id(_id):
    """
    return source term DB entry of given _id
    """
    ret = db_source_terms.find_one({"_id": bson.objectid.ObjectId(_id)})
    
    #only these encoded entries of source term dictionary are transformed
    ret["activities"] = mo2ma(ret["activities"])
    ret["rel_heights"] = mo2ma(ret["rel_heights"])
    
    return ret
    

def delete_source_term(_id):
    """
    deletes source_term of a given _id from collection
    """
    #st = db_source_terms.find_one({"_id": _id})
    print "deleting source term..."
    db_source_terms.remove({"_id": bson.objectid.ObjectId(_id)})
    
    