import matplotlib
#matplotlib.use('GTK') 
matplotlib.use('Agg')
import math
import numpy
import pylab
from math import *
from pylab import *
import Image
import matplotlib.pyplot as plt
from matplotlib.pyplot import *
from matplotlib import cm
import os
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import Image


#prechozi verze upravena pro vykreslovani okynek
def polar_rose_centre_plotter(radius, parallels, theta, data0, isoN, tit, parallels2, theta2, logar_flag, area_calc, points_to_plot, names, saveFigFlag, fileName, showFlag, ownLevelsFlag, own_levels):
    #radius - polomer kruhu kolem stredu polarni site - polomer od zdroje

    
    last_index = 0 #index of the last concentric circle in the polar network in the direction from the centre
    last_index1 = 0

    zero = numpy.zeros(0)
    parallels2_0 = concatenate((zero, parallels2))
    for i in range(41):
        if (radius*math.sqrt(2.0) > parallels2_0[i] and radius*math.sqrt(2.0) <= parallels2_0[i+1]):
            last_index = i+1
            break
    if (last_index == 0):
        last_index = len(parallels2_0)-1

    R = array(parallels[:last_index+1])
    r,t = pylab.meshgrid(R, theta)
    rmax = max(R)*math.sqrt(2)
    if (rmax > 100.):
        rmax = 100.
    print R
    #print r.shape

    data1 = numpy.zeros((81, 42))

    for i in range(61):
        data1[i][:] = data0[20+i][:]
        #print "i: %d    20+i: %d" % (i, 20+i)
    for i in range(21):
        data1[60+i][:] = data0[i][:]
        #print "60+i: %d    i: %d" % (60+i, i)

    data = numpy.zeros((81, 42))
    for i in range(81):
        data[i][:] = data1[80-i][:]

    X = r*cos(t)
    Y = r*sin(t)
    Z = data[:,:last_index+1]

    for i in range(41):
        if (rmax > parallels2_0[i] and rmax <= parallels2_0[i+1]):
            last_index1 = i+1
            break

    if (last_index == 0):
        last_index = 41

    if (last_index1 == 0):
        last_index1 = 41


    fig = plt.figure(figsize=(15,15), dpi=400)
    ax = fig.add_axes([0., 0., 1., 1.])
    #ax = axes([0,0,1,1], frameon=False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_frame_on(False)
    #ax.set_aspect(1.)
    #ax.set_frame_on(True) #turn on/off rectangular frame
    #ax.set_xticks(numpy.arange(-100, 120, 20)) #  axis ticks.
    #ax.set_yticks(numpy.arange(-100, 120, 20)) # draw a circle around the edge of the plot.

    #print data0
    #print data0.shape
    #print data0[0,:]
    print "Data max: %E " % (Z.max())
    print "Data min: %E " % (Z.min())

    alphaVal = 1.0
    

    theta_plot = pylab.linspace(0., 2.*pylab.pi , 81.) #na kruhy
    theta_plot2 = pylab.linspace(0., 2.*pylab.pi , 17.) #ma radialy
    theta_plot3 = pylab.linspace(0., 2.*pylab.pi , 41.) #ma radialy
    
    #kam az se pocita
    havar_rad = 100.
    lw0 = 0.5
    lw01 = 0.05
    
    ax.plot(havar_rad*cos(theta_plot),havar_rad*sin(theta_plot),'r--',  alpha=1.0, aa='True', linewidth=7)

    ax.plot(rmax*cos(theta_plot),rmax*sin(theta_plot),'k:',  alpha=alphaVal, aa='True', linewidth=lw0)
    for radius1 in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]: #parallels[:last_index1]:
        ax.plot(radius1*cos(theta_plot),radius1*sin(theta_plot),'k-',  alpha=alphaVal, aa='True', linewidth=lw0)

    for radius1 in range(1, 10): #parallels[:]:
        ax.plot(radius1*cos(theta_plot),radius1*sin(theta_plot),'k-',  alpha=alphaVal, aa='True', linewidth=lw01)
        
    for radius1 in range(10, 100, 5): #parallels[:]:
        ax.plot(radius1*cos(theta_plot),radius1*sin(theta_plot),'k-',  alpha=alphaVal, aa='True', linewidth=lw01)        

    x_data = numpy.zeros(2)
    y_data = numpy.zeros(2)
    
    rmax = parallels2[last_index1]
    r_1 = 10.
    for angle in theta_plot2[:]:
        x_data[0] = r_1*cos(angle)
        y_data[0] = r_1*sin(angle)
        x_data[1] = rmax*cos(angle)
        y_data[1] = rmax*sin(angle)
        a = matplotlib.lines.Line2D(x_data, y_data, color='k', linestyle='-', alpha=alphaVal, linewidth=lw0, aa='True',)
        ax.add_line(a)

    r_2 = 1.0
    for angle in theta_plot[:-1]:
        x_data[0] = r_2*cos(angle)
        y_data[0] = r_2*sin(angle)
        x_data[1] = r_1*cos(angle)
        y_data[1] = r_1*sin(angle)
        a = matplotlib.lines.Line2D(x_data, y_data, color='k', linestyle='-', alpha=alphaVal, linewidth=lw01, aa='True',)
        ax.add_line(a)
    
    pocetRaduOdMaxima = isoN #number of isoplets
    logMax = math.log(Z.max(), 10.0)
    radMaxima = math.floor(logMax)


    levels = logspace(radMaxima - pocetRaduOdMaxima, radMaxima+1, num = pocetRaduOdMaxima+2)



    cont_alpha = 1.0

    cs1 = ax.contourf(X, Y, pylab.ma.masked_where(Z == 0, Z), levels, cmap=cm.jet, norm=matplotlib.colors.LogNorm(), )
    #cs2 = ax.contour(X, Y, pylab.ma.masked_where(Z == 0, Z), cs1.levels, colors = 'k', hold='on', linewidths = (lw0,),)
   
    #cb = colorbar(cs1,  shrink=0.7)
    #cb.set_ticks(levels)
    #cb.set_ticklabels([("%3.2E" % tick) for tick in levels])
        

    ax.set_xlim(-1.*radius, radius)
    ax.set_ylim(-1.*radius, radius)
    ax.set_aspect(1.)
    

    print fileName
    #for PNG must be set hing DPI
    #savefig(fileName, transparent=True, dpi=300)
    #for SVG not
    #SAVING SVG with no borders
    savefig(fileName, transparent=True)
    
    #now we save colorbar... could be done better:)
    fig = plt.figure(figsize=(1.3, 6))
    
    #this should turn off all axes and frame box
    ax1 = fig.add_axes([0.0, 0.0, 0.000001, 1.0])
    ax1.set_frame_on(False)
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    ax1.set_xticks([])
    ax1.set_yticks([])
    cax = axes([0.0, 0.1, 0.15, 0.8])
    cb = colorbar(cs1, orientation='vertical', cax=cax)
    cb.set_ticks(levels)
    cb.set_ticklabels([("%2.1E" % tick) for tick in levels[:-1]])
    c_fname = fileName[:-4]+"_colorbar.svg"
    plt.savefig(c_fname)
    
    """
    im = Image.open(c_fname)
    crop_rectangle = (112, 230, 913, 310)
    cropped_im = im.crop(crop_rectangle)

    cropped_im.save(c_fname)
    """                     


