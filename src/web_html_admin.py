# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools_users

##########################################################################################################################
### ADMIN BODY
##########################################################################################################################


def head():
    """
    geenrates JS for viewing of task results on google map in modal window
    """
    
    #<script type="text/javascript" src="http://geoxml3.googlecode.com/svn/branches/polys/geoxml3.js"></script>
    ret = """
          <script type="text/javascript" src="/scripts/app/admin.js"></script>    
    """ 
    
    return ret

def get_list_of_users():
    """
    renders body of users page
    """
    
    ret = db_tools_users.get_list_of_users()
    
    return ret