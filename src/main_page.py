#!/usr/bin/env python
# -*- encoding: UTF-8 -*-
'''
Module providing application main pages.

'''
import cherrypy
import os
import copy
import web_services as ws
from mako.lookup import TemplateLookup
from auth import require, member_of, check_credentials
import config as cf
import sys
import web_html #navibar a footer
import web_html_main 
import account_page
import task_page_puff
import results_page
import contact_page
import workers_page
import tools_page
import db_tools
import nuclides_page

SESSION_KEY = cf.SESSION_KEY
HTML_TEMP_DIR = cf.HTML_TEMP_DIR
SCRIPTS_DIR = cf.SCRIPTS_DIR

#config same for all the parts
cp_conf_common = cf.cp_conf_common

lookup = TemplateLookup(directories=[cf.HTML_TEMP_DIR])

class DSSAdminGUI:
    # all methods in this controller (and subcontrollers) is
    # open only to members of the admin group
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [member_of('admin')] #extension of basic cofig of some special items
    _cp_config = cp_conf

    @cherrypy.expose
    def index(self): #index of admin area for administrator
        return """Admin aaarea"""

from cherrypy import _cperror
class MainApp:
    #main class with login form to admin and user sections
    _cp_config = cp_conf_common

    @cherrypy.expose
    def index(self):
        try:
            return self.render_main_page()
        except Exception, e:
            return """
            <html>
                <head>
                    <title>DSS Openshift</title>
                </head>
                <body>
                    <h1>Error</h1>
                    <p>
                    %s
                    </p>
                </body>
            </html>
            """ % (_cperror.format_exc())

    @cherrypy.expose
    def login(self, username=None, password=None): #index of admin area for administrator

        error_msg, group = check_credentials(username, password) #group = usergroup
            
        if error_msg and (username != None or password != None):
            #remove
            if (cherrypy.session.has_key(SESSION_KEY)):
                print "nic", cherrypy.session[SESSION_KEY]
            return self.render_login_page(error_msg)
        
        elif error_msg and username == None and password == None:
            #the first start of the page, we don't displaye error message
            return self.render_login_page()

        elif error_msg == None:
            #session and internal cherrypy varialbel login initialized with the username - if none, user is logged
            cherrypy.session[SESSION_KEY] = cherrypy.request.login = username
            self.on_login(username)
            raise cherrypy.HTTPRedirect("/account")
            
            #if (group == "admin"):
            #    raise cherrypy.HTTPRedirect("/admin")
            #elif (group == "user"):
            #    raise cherrypy.HTTPRedirect("/app")

    @cherrypy.expose
    def logout(self):
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        sess[SESSION_KEY] = None
        if username:
            cherrypy.request.login = None
            #self.on_logout(username)
        raise cherrypy.HTTPRedirect("/login")
    
    def on_login(self, username):
        """
        performs some operations associated with user login, i.e. updates last user login
        """
        db_tools.update_last_login(username)
    
    def render_main_page(self):
        #navibar with buttons
        head = web_html.head()
        nav = web_html.navibar(active=1) #renders navibar according to login state
        bod = web_html_main.main_body()
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod, bottom_scripts="",footer=fot)
        
    def render_login_page(self, msg=None):
        #navibar with buttons
        head = ""
        nav = web_html.navibar(active=2) #renders navibar according to login state
        bod = web_html_main.login_form(msg)
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
