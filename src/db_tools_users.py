# -*- encoding: UTF-8 -*-
from pymongo import Connection
import simplejson as sj
from bson import json_util
import sys
import db_tools
import os
import data.config
import uuid

#connection to mongo
conn = Connection(data.config.MONGODB_DB_URL)

#getting a databased
db = conn["testpy"]

#getting collections.. #db.collection_names() prints the current collections
#instantiation collections
db_users = db['users']
db_tasks = db['db_tasks']


def get_list_of_users():
    """
    returns return list of users sorted by username
    """
    users = db_users.find().sort([("username",1)])
    print users
    return users

def get_users_count():
    """
    returns count of registered users
    """
    return get_list_of_users().count()

def get_user(username):
    """
    returns user data by username
    """
    user = db_users.find_one({"username": username})
    if user:
        print "getting user..."
        user = sj.dumps(user, default=json_util.default)
    return user

def create_guest_user():
    """
    Creates new guest/dummy user.
    """
    username = "guest_%s" %(str(uuid.uuid4()))
    db_tools.insert_user(
                   username = username,
                   password = (str(uuid.uuid4())),
                   email = "guest@guest.com",
                   user_type = "guest",
                   first_name = "Guest",
                   middle_name = "",
                   surname =  "",
                   institute = "",
                   street1 = "",
                   street2 = "",
                   city = "",
                   zip = "",
                   state = "",
                   country = "",
                   )
    return username

def find_user(username):
    return db_users.find_one({"username": username})

def delete_expired_guest_users(expiration_in_seconds=60*60):
    """
    Finds all guest users.
    """
    import calendar
    import time
    current_time = calendar.timegm(time.gmtime())
    db_users.remove({"user_type": "guest", "created": {"$lt": current_time - expiration_in_seconds}})


def save_user(user):
    """
    saves user data by username
    """
    username = user["username"]
    if username:
        print "saving user..."
        dbuser = db_users.find_one({"username": username})
        _id = dbuser["_id"]
        db_users.update( { "_id": _id },
                 { "$set": { "first_name"   : user["first_name"],
                             "middle_names" : user["middle_names"],
                             "surname"      : user["surname"],
                             "email"        : user["email"],
                             "status"       : user["status"],
                             "user_type"    : user["user_type"],
                             "institute"    : user["institute"],
                             "street1"      : user["street1"],
                             "street2"      : user["street2"],
                             "city"         : user["city"],
                             "state"        : user["state"],
                             "country"      : user["country"]
                           }
                  })

def is_admin(username):
    """
    if user type is admin returns true else false
    """
    is_admin = False
    user = db_users.find_one({"username": username})
    if (user and user["user_type"] == "admin"):
        is_admin = True
    return is_admin

def is_guest(user):
    """
    Returns Trues in case that passed user is guest user.
    """
    return user and user['user_type'] and user['user_type'] == "guest"

def is_guest_username(username):
    """
    Returns Trues in case that passed user is guest user.
    """
    user = db_users.find_one({"username": username})
    return user and user['user_type'] and user['user_type'] == "guest"

def delete_user(username):
    """
    deletes user with given username from collection
    """
    user = db_users.find_one({"username": username})
    print "deleting user..."
    _id = user["_id"]
    db_users.remove({"_id": _id})

def disable_user(username):
    """
    disables user with given username in collection
    """
    user = db_users.find_one({"username": username})
    print "disabling user..."
    _id = user["_id"]
    db_users.update({"_id": _id}, {"$set": { "state": 0 }})

def enable_user(username):
    """
    enables user with given username in collection
    """
    user = db_users.find_one({"username": username})
    print "enabling user..."
    _id = user["_id"]
    db_users.update({"_id": _id}, {"$set": { "state": 1 }})

def make_admin(username):
    """
    set user type to admin by given username in collection
    """
    user = db_users.find_one({"username": username})
    print "making admin user..."
    _id = user["_id"]
    db_users.update({"_id": _id}, {"$set": { "user_type": "admin" }})