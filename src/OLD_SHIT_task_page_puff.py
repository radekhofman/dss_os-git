# -*- encoding: UTF-8 -*-
import cherrypy
import copy
import config as cf
import web_html #comtains methods for web HTML rendering
import web_html_task #tasks - celection of task type
import web_html_task_disp #task type duspersion
#import web_html_task_assim #task type assimilation
from mako.lookup import TemplateLookup
import simplejson as sj
import numpy
from db_tools import ma2mo, mo2ma #ndarray serialization to string - can be inserted into mongo
import db_tools as dt
import tools
import validators
import db_tools_source_terms as dtst
import db_tools_meteo as dtm
import db_models
import cherrypy
import time


lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common
SESSION_KEY = cf.SESSION_KEY


class Task:
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    @cherrypy.expose
    def index(self): #index of main DSS GUI for common user
        return self.render_page()
    
    
    def render_page(self):
        #navibar with buttons
        head = ""
        nav = web_html.navibar(active=3) #renders navibar according to login state
        bod = web_html_task.task_body().decode("windows-1252").encode("utf8")
        fot = web_html.get_footer()
                
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod, footer=fot)

    @cherrypy.expose
    def create(self, **kwargs): #task type: 0 - 2...
        kwargs["username"] = cherrypy.session[SESSION_KEY]
        
        nav = web_html.navibar(active=3) #renders navibar according to login state
        head = web_html_task_disp.head()
        bod = web_html_task_disp.task_create_body_0(**kwargs)
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod, footer=fot)
    
    
    @cherrypy.expose
    def update_nucl_and_segment_tables(self, **kwargs):   
        """
        return html editable table with source term and segments setting for selected nuclides and number of segments
        # in kwargs are dozens of checkboxes belonging to particular nuclides
        
        ['segments_count', 
         'segments_count_old', 
         'nuclides_count_old', 
         'preset_value_check',
         'preset_value', 
         'nuclides_old[]', 
         'segments_properties[]', 
         'nuclides_modal_checked[]',
         'activities'] - activities of nuclides already entered in table
        """
        cherrypy.response.headers['Content-Type'] = 'application/json'

        keys = kwargs.keys()
        for key in keys:
            print key, kwargs[key]

        #these are present always        
        segments_count = int(kwargs["segments_count"])
        segments_count_old = int(kwargs["segments_count_old"])
        preset_value_check = kwargs["preset_value_check"] #use preset value or existing values where applicable?
        preset_value  = kwargs["preset_value"] #preset value
        nuclides_count_old = int(kwargs["nuclides_count_old"])
        
        nuclides_old_sort = []
        nuclides_sort = []
        
        activities = None
        activities_old = None
        segments_properties = None
        segments_properties_old = None
                
        if (kwargs.has_key("segments_properties_old[]")): #segments_count_old > 0):
            segments_properties_old = numpy.reshape(kwargs["segments_properties_old[]"], (4, segments_count_old))
                    
        if (kwargs.has_key("nuclides_old[]")): #nuclides_count_old > 0):
            nuclides_old_sort = tools.sort_nuclides(kwargs["nuclides_old[]"])
            #if (kwargs.has_key["activities_old"]): #must be fulfilled
            activities_old = numpy.reshape(kwargs["activities_old[]"], (nuclides_count_old, segments_count_old))            
            print activities_old
            
            
        if (kwargs.has_key("nuclides_modal_checked[]")):
            checked_nuclides = kwargs["nuclides_modal_checked[]"]
            if (not isinstance(checked_nuclides, list)):
                checked_nuclides = [checked_nuclides,]
                
            nuclides_sort = tools.sort_nuclides(checked_nuclides)
         
        nuclides_count = len(nuclides_sort)
           
        if (segments_count*nuclides_count > 0):
            activities = numpy.chararray((nuclides_count, segments_count), itemsize=20) 
            activities[:] = preset_value #we insert it as a string, we don't need to validate it
        
            #filling in activities values
            if (preset_value_check == "false" and segments_count_old*nuclides_count_old > 0): #we want back values for all entries where applicable
                #preset values stays only in fields previously not included in the ST table
                segments_properties = numpy.chararray((4, segments_count), itemsize=20) 
                for j in range(segments_count):
                    if j < segments_count_old:
                        segments_properties[0, j] = segments_properties_old[0, j]
                        segments_properties[1, j] = segments_properties_old[1, j]
                        segments_properties[2, j] = segments_properties_old[2, j]
                        segments_properties[3, j] = segments_properties_old[3, j]
                    else: 
                        segments_properties[0, j] = "%3.2f" % 1.0
                        segments_properties[1, j] = "%3.2E" % 0.0
                        segments_properties[2, j] = "%3.2E" % 0.0
                        segments_properties[3, j] = "%3.2E" % 50.0
                
                for i in range(nuclides_count):
                    nucl = nuclides_sort[i]
                    for j in range(segments_count):
                        if (nucl in nuclides_old_sort and j < segments_count_old): #is old value applicable??
                            nuc_old_index = nuclides_old_sort.index(nucl)
                            activities[i,j] = activities_old[nuc_old_index, j]
            
        html_tables = web_html_task_disp.get_source_and_segment_tables(segments_count=segments_count, nuclides=nuclides_sort, activities=activities, segments_properties=segments_properties)
        
        """
        keys = kwargs.keys()
        
        segments_count = int(kwargs["segments_count"])

        nuclides = []
        
        for key in keys:
            if not key in ["segments_count", "preset"]: #it is nuclide
                nuclides.append(key)
                
        act = None
        
        if kwargs.has_key("preset_value_check"): #"preset_value_check" checkbox is "on" - return key on, we want to use preset values
            try: #we dont validate preset value
                act = numpy.ones((len(nuclides), segments_count)) * float(kwargs["preset"]) 
            except:
                print "wrong preset value, not float"
        
        if kwargs.has_key("activities"):
            act = kwargs["activities"]
        #default values of activites in the source table
       
        
        nuclides_sort = tools.sort_nuclides(nuclides)

        html_tables = web_html_task_disp.get_source_and_segment_tables(segments_count=segments_count, nuclides=nuclides_sort, activities=act)
        return sj.dumps({"source_table": html_tables})
        """
        return sj.dumps({"source_table": html_tables})

    @cherrypy.expose
    def update_meteo_table(self, **kwargs):
        """
        return adjusted meteotable
        previous values do not stay unchanged during resize 
        """   
        #values = {"hours_count": int(kwargs["hours_count"])}
        
        meteo_table = web_html_task_disp.generate_meteo_table(hours_count=int(kwargs["hours_count"]))
        return meteo_table
    
    @cherrypy.expose
    def insert_disp_task(self, **kwargs):
        """
        processes dispersion task form and if succeeds, inserts the task into queue and redirects to results page 
        """
    
        #1. we gather data from forms
        segments_properties_orig, segments_count = web_html_task_disp.parse_segment_args(kwargs)  #returns numpy.chararray  (3, segm_count, 2) 0 - data, 1 - validation - 0:OK, 1:ERROR
        nuclides, activities_orig = web_html_task_disp.parse_nuclides_args(kwargs) 
        meteo_orig, hours_count = web_html_task_disp.parse_meteo_args(kwargs)   
           
        title1 = kwargs["text1"] #we dont validate
        lat_orig = kwargs["lat"]
        lon_orig = kwargs["lon"]
        #H_orig = kwargs["height"] #release height
        disp_formula = kwargs["disp_f"] #we dont validate
        inversion_orig = kwargs["inversion"] #inversion layer height 0 - no inversion
        calm = int(kwargs["calmRadios"]) #modification on calm conditions @we dont validate
        t_ref_orig = kwargs["ref_time"]
        
                     
        #2. we validate data
        #construction of validator objects
        segments_properties_validator = validators.Validator(segments_properties_orig)
        activities_validator = validators.Validator(activities_orig)
        meteo_validator = validators.Validator(meteo_orig)
        lat_validator = validators.Validator(lat_orig)
        lon_validator = validators.Validator(lon_orig)
        #H_validator = validators.Validator(H_orig)
        inversion_validator = validators.Validator(inversion_orig)
        t_ref_validator = validators.Validator(t_ref_orig)
        
        #validation
        segments_properties_validator.validate_numeric_matrix(float)
        activities_validator.validate_numeric_matrix(float)
        meteo_validator.validate_meteo()
        lat_validator.validate_numeric(float)
        lon_validator.validate_numeric(float)
        #H_validator.validate_numeric(float)
        inversion_validator.validate_numeric(float)
        t_ref_validator.validate_numeric(float) 
        
        msg = "" 
        num_err_msg = "Error during parsing of entered values, please check red-bordered inputs and try again."
              
        isValid = True
        if (not segments_properties_validator.is_valid()):
            isValid = False
            msg = num_err_msg
        if (not activities_validator.is_valid()):
            isValid = False  
            msg = num_err_msg   
        if (not meteo_validator.is_valid()):
            isValid = False  
            msg = num_err_msg               
        if (not lat_validator.is_valid()):    
            isValid = False
            msg = num_err_msg
        if (not lon_validator.is_valid()):    
            isValid = False
            msg = num_err_msg
        #if (not H_validator.is_valid()):    
        #    isValid = False
        #    msg = num_err_msg
        if (not inversion_validator.is_valid()):    
            isValid = False
            msg = num_err_msg
        if (not t_ref_validator.is_valid()):    
            isValid = False
            msg = num_err_msg
        
        #additional conditions:
        #validation of reference time vs. total release time
        isValid_add = True
        if (isValid): #numeric values must be valid since we test them on additional conditions
            total_time = segments_properties_validator.conversion[0,:].sum() #tital time of propagation in hours
            if (total_time > t_ref_validator.conversion / 3600.):
                isValid_add = False
                msg = "Total release time %3.2f hours must be smaller than reference time %3.2f hours" % (total_time, t_ref_validator.conversion/3600.)
                t_ref_validator.validator_result = 1
            else:
                t_ref_validator.validator_result = 0
             
            MAX_NUCL_LIMIT = 25    
                
            if (len(nuclides) > MAX_NUCL_LIMIT):
                isValid_add = False
                msg = "Selected %d nuclides, this must not be greater than %d, please remove some nuclides." % (len(nuclides), MAX_NUCL_LIMIT)
                                    
        #3. we submit task if OK, otherwise wirte error message and return forms
        if (isValid and isValid_add): #entered data are valid, we can submit task
            username = cherrypy.session.get(cf.SESSION_KEY)
 
            """
            if (kwargs.has_key("cb_save_st")):
                #user wants to save source term into db 
                dtst.insert_source_term(db_models.source_term_model(**{"username": username, #username
                                                                     "activities": ma2mo(activities_validator.conversion),
                                                                     "segment_properties": ma2mo(segments_properties_validator.conversion),
                                                                     "segments_count": segments_count,
                                                                     "nuclides": nuclides,
                                                                     "description": "ST "+title1,
                                                                     }))
            """
            dt.create_task(username = username,
                   usergroup_of_task_owner = dt.get_usergroup_of_user(username),        
                   description = title1,
                   task_type = 0, #task type: 0 - dispersion HARP, 1 - .....                     
                   task_input = { "meteo": ma2mo(meteo_validator.conversion),
                                  "activities": ma2mo(activities_validator.conversion),
                                  "segments_properties": ma2mo(segments_properties_validator.conversion),
                                  "nuclides": nuclides,
                                  "inversion": inversion_validator.conversion,
                                  "disp_formula": disp_formula,
                                  #"H": H_validator.conversion,
                                  "lat": lat_validator.conversion,
                                  "lon": lon_validator.conversion,
                                  "on_calm_modification": int(calm),
                                  "t_ref": t_ref_validator.conversion
                                  #"duration": duration 
                                  },
                   task_output = None )
                               
            raise cherrypy.HTTPRedirect("/results")
        
        else: #entered data are not VALID!! 
            
            
            params = {"experiment_desc": "",
                      #"date": "3",#date,
                      "lat": lat_validator.orig_input,
                      "lat_validator_result":  lat_validator.validator_result,
                      "lon": lon_validator.orig_input,
                      "lon_validator_result":  lon_validator.validator_result, 
                      #"H": H_validator.orig_input,
                      #"H_validator_result":  H_validator.validator_result, 
                      "disp_formula": disp_formula, 
                      "calm": calm, 
                      "inversion": inversion_validator.orig_input,
                      "inversion_validator_result":  inversion_validator.validator_result,                       
                      "t_ref": t_ref_validator.orig_input,
                      "t_ref_validator_result":  t_ref_validator.validator_result, 
                       }
             
            print  activities_validator.orig_input
            #we insert into the form data exactly as were entered by the user, si the user can correct it
            return self.create(**{"msg": msg,
                                  
                                  "params": params,

                                  "segments_count": segments_count,
                                  "nuclides": nuclides,
                                  "hours_count": hours_count,
                                  "segments_properties": segments_properties_validator.orig_input,
                                  "segments_properties_validator_result": segments_properties_validator.validator_result,
                                  "activities": activities_validator.orig_input,
                                  "activities_validator_result": activities_validator.validator_result,
                                  "meteo": meteo_validator.orig_input,
                                  "meteo_validator_result": meteo_validator.validator_result
                                  }) #prints error message due to validation procedure     
##############    
    @cherrypy.expose
    def duplicate_and_edit_task(self, **kwargs):
        """
        duplicate taks and opens it for edit for new modified simulation run and discards its previous outputs
        """
        username = cherrypy.session.get(cf.SESSION_KEY)
        
        id = int(kwargs["task"])
        #obtaining the original task
        task = dt.get_task_of_given_id(username, id)
        #extraction of setting, source term and meteo
        #print task
        
        """
        DATA STRUCTURE:
        dt.create_task(username = username,
               description = title1,
               task_type = "dispersion",                     
               task_input = { "meteo": ma2mo(meteo),
                              "activities": ma2mo(activities),
                              "segments_properties": ma2mo(segments_properties),
                              "nuclides": nuclides,
                              "inversion": inversion,
                              "disp_formula": disp_formula,
                              "lat": float(lat),
                              "lon": float(lon),
                              "on_calm_modification": int(calm)
                              #"duration": duration 
                              },
               task_output = None )
        
        PARAMS TAB1
        params = {"experiment_desc": "",
                  #"date": "3",#date,
                  "lat": 49.1817, #default location is NPP Temelin
                  "lon": 14.3754,
                  "disp_form_0": "selected", #default dispersion formula
                  "disp_form_1": "",
                  "calm_modif_yes": "checked", #default is modification of calm conditions ON
                  "calm_modif_no": "",
                  "inversion": 0, #default is without inversion
                  "t_ref": 172800 #default is 48hours = 172800 seconds
                   }        
        """
        
        #now we have to create a dictionary containing all values of keys for editation of a task
        #mandatory for tab 0

        
        meteo = mo2ma(task["task_input"]["meteo"])
        
        values = {
        "params": {"experiment_desc": task["description"],
                #"date": "3",#date,
                "lat": task["task_input"]["lat"], #49.1817, #default location is NPP Temelin
                "lon": task["task_input"]["lon"], #14.3754,
                #"H": task["task_input"]["H"], #100, #default relesae height
                "disp_formula": task["task_input"]["disp_formula"] , 
                "calm": task["task_input"]["on_calm_modification"], 
                "inversion": task["task_input"]["inversion"], #default is without inversion
                "t_ref": task["task_input"]["t_ref"] #default is 48hours = 172800 seconds 
                },       

        #mandatory for tab 1
        "segments_count": mo2ma(task["task_input"]["activities"]).shape[1],
        "nuclides": task["task_input"]["nuclides"],
        "activities": mo2ma(task["task_input"]["activities"]),
        "segments_properties": mo2ma(task["task_input"]["segments_properties"]),
        #mandatory for tab2
        "hours_count": meteo.shape[0],
        "meteo": meteo
        }
        
        print "rendering page with:"
        print values
        #now we have to open new task web page with predeffined values ...
        return self.create(**values)
        #raise cherrypy.HTTPRedirect("/results")
        
    @cherrypy.expose
    def load_st(self, **kwargs):
        """
        reutnr HTML table of a selected source term from DB
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        st = dtst.get_source_term_of_id(kwargs["_id"])
        st_table = web_html_task_disp.get_source_and_segment_tables(**st)
        nucl_table = web_html_task_disp.generate_nucl_table(**st)
        segments_combo = web_html_task_disp.generate_segment_combo(**st)
        tmpl = lookup.get_template("new_task_modal_nucl_table.html")
        nucl_modal = tmpl.render(nucl_table=nucl_table, segments_combo=segments_combo)
        
        return sj.dumps({"st_table": st_table, "nucl_modal": nucl_modal}) #we return multiple values, we must return JSON
        
    @cherrypy.expose
    def delete_st(self, **kwargs):
        """
        deletes source term of a given _id from database
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        d = {}
        
        try:
            dtst.delete_source_term(kwargs["_id"])
            msg = "OK, deleted"            
        except Exception, e:
            msg = "Error, not deleted", e
        finally:
            print msg
            d = {"msg": msg}
            return sj.dumps(d)       
            
    @cherrypy.expose
    def save_st(self, **kwargs):    
        """
        obtains a source term from JS and stores it in DB
        """ 
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        try:
            username = cherrypy.session.get(cf.SESSION_KEY) 
            kwargs["username"] = username  
            
            #gathering data
            nuclides_count = int(kwargs["nuclides_count"])
            segments_count = int(kwargs["segments_count"])
            activities_orig = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["activities[]"]), (nuclides_count, segments_count)) )
            segments_properties_orig = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["segments_properties[]"]), (4, segments_count)) )
            nuclides = kwargs["nuclides[]"]
            if (not isinstance(nuclides, list)): #just one nuclide, we must transform to list
                nuclides = [nuclides,] 
            description = kwargs["description"]
            
            #creation of validation
            segments_properties_validator = validators.Validator(segments_properties_orig)
            activities_validator = validators.Validator(activities_orig)
            
            #validation
            segments_properties_validator.validate_numeric_matrix(float)
            activities_validator.validate_numeric_matrix(float)
            
            #decision
            isValid = True
            if (not segments_properties_validator.is_valid()):
                isValid = False
            if (not activities_validator.is_valid()):
                isValid = False 
                
            if isValid: #OK, data are, valid, wil be entered into DB...
                
                st = {"username": username,
                      "activities": activities_validator.conversion,
                      "segments_properties": segments_properties_validator.conversion,
                      "nuclides_count": nuclides_count,
                      "segments_count": segments_count,
                      "nuclides": nuclides,
                      "description": description,
                      "modified": time.time()
                       }  
                
                table = web_html_task_disp.get_source_and_segment_tables(**st) #must be here, inside insertion method is converted into pickel string!! - mutable
                msg = dtst.save_source_term(st) 
                
                #we must return table, because in case of validatino error we want to remove colord borders
                return sj.dumps({"msg": msg, "table": table, "status": 0}) #we return OK status
                             
            else: #data not valid, we return HTML table with data and highlihting of errors   
                st = {"segments_count": segments_count,
                      "nuclides": nuclides,
                      "segments_properties": segments_properties_validator.orig_input,
                      "segments_properties_validator_result": segments_properties_validator.validator_result,
                      "activities": activities_validator.orig_input,
                      "activities_validator_result": activities_validator.validator_result,
                      } 
                      
                table = web_html_task_disp.get_source_and_segment_tables(**st)
                msg = "Save failed, please check entered data and try again."
                return sj.dumps({"msg": msg, "table": table, "status": 1}) #we return 1 status - error during validation, must be corrected
        except Exception, e:
            print e
            return sj.dumps({"msg": "Error:(", "status": 1})

    def array_to_chararray(self, array):
        """
        converts numpy array to chararray
        """
        m = len(array)
        n = len(array[0])
        
        ret = numpy.chararray((m, n), itemsize=20)
        
        for i in range(m):
            for j in range(n):
                ret[i,j] = array[i][j]
                
        return ret
    
    
    @cherrypy.expose    
    def update_st_load_form(self):
        """
        return HTML table with list of saved tasks
        """
        username = cherrypy.session.get(cf.SESSION_KEY) 
        st_table = web_html_task_disp.get_source_term_table(username)
        return st_table
        
        
    @cherrypy.expose
    def load_meteo(self, **kwargs):
        """
        reutnr HTML table of a selected meteo from DB
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        meteo = dtm.get_meteo_of_id(kwargs["_id"])
        meteo_table = web_html_task_disp.generate_meteo_table(**meteo) #hours_count, meteo=None, meteo_validator_result=None)
        
        return sj.dumps({"meteo_table": meteo_table}) #we return multiple values, we must return JSON

    @cherrypy.expose
    def delete_meteo(self, **kwargs):
        """
        deletes meteo of a given _id from database
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        d = {}
        
        try:
            dtm.delete_meteo(kwargs["_id"])
            msg = "OK, deleted"            
        except Exception, e:
            msg = "Error, not deleted", e
        finally:
            print msg
            d = {"msg": msg}
            return sj.dumps(d)        
        
    @cherrypy.expose
    def save_meteo(self, **kwargs):  
        """"
        saves meteo to DB
        """     
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON     
        
    
        username = cherrypy.session.get(cf.SESSION_KEY) 
        #kwargs["username"] = username  
        
        #gathering data
        meteo_hours_count = int(kwargs["meteo_hours_count"])
        meteo_orig = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["meteo[]"]), (meteo_hours_count, 4)) )
        description = kwargs["description"]
        
        print meteo_orig
        #creation of validation
        meteo_validator = validators.Validator(meteo_orig)
        
        #validation
        meteo_validator.validate_meteo()
        
        #decision
        isValid = True
        if (not meteo_validator.is_valid()):
            isValid = False
            
        if isValid: #OK, data are, valid, wil be entered into DB...       
            meteo = {"username": username, #username
                     "meteo": meteo_validator.conversion, #numpy.array: must be pickeld before insertion to DB
                     "hours_count": meteo_hours_count,
                     "description": description,
                     "modified": time.time()
                     }   
            
            meteo_table = web_html_task_disp.generate_meteo_table(**meteo) #must be here, inside insertion method is converted into pickel string!! - mutable
            msg = dtm.save_meteo(meteo) 
            
            #we must return table, because in case of validatino error we want to remove colord borders
            return sj.dumps({"msg": msg, "meteo_table": meteo_table, "status": 0}) #we return OK status    

        else: #data not valid, we return HTML table with data and highlihting of errors   
            print "Not valid"
            meteo = {"meteo": meteo_validator.orig_input, #numpy.array: must be pickeld before insertion to DB
                     "meteo_validator_result": meteo_validator.validator_result,
                     "hours_count": meteo_hours_count,
                     }  
                  
            meteo_table = web_html_task_disp.generate_meteo_table(**meteo)
            msg = "Save failed, please check entered data and try again."
            return sj.dumps({"msg": msg, "meteo_table": meteo_table, "status": 1}) #we return 1 status - error during validation, must be corrected
        """
        except Exception, e:
            print "Error", e
            return sj.dumps({"msg": "Error:(", "status": 1})   
        """
    @cherrypy.expose    
    def update_meteo_load_form(self):
        """
        return HTML table with list of saved tasks
        """
        username = cherrypy.session.get(cf.SESSION_KEY) 
        meteo_table = web_html_task_disp.get_saved_meteo_table(username)
        return meteo_table