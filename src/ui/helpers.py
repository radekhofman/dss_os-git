# -*- encoding: UTF-8 -*-
'''
Provides helper UI functions intended to be used in templates.
'''
import cherrypy
import db_tools_users

def is_guest_user():
    user_name = cherrypy.request.login
    return db_tools_users.is_guest_username(user_name)
