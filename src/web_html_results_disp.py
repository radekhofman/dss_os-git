import db_tools as dt
import tools
import web_html_results
import time
import cPickle
import numpy

###constants
ages = ["<1 year", "1-2 years", "2-8 years", "8-12 years",  "12-18 years", "adults"]
organs = ["Wholebody - effective dose", "Gonads", "Red bone marrow", "Lungs", "Thyroid", "Colon"]
times = ["7 days", "30 days", "90 days", "1 year", "5 years", "50 years"]
    
def get_dipersion_task_details(task, task_status): #whole dictionary of task
    """
    return html of tabs with dispersion task details
    """    
    task_input = None #something could happen...
    output = None
    task_id = None
    
    if task != None:
        task_id = task["task_id"]
        task_input = task["task_input"]
        output = task["task_output"]
        
    tab1 = get_tab1(task) #task infor - worker info
    tab2 = get_tab2(task) #task input
    tab3 = get_tab3(task_id, task_input, output, task_status) #taks output
    
    ret = """
      <div class="tabbable" style="margin-bottom: 18px;">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab1" data-toggle="tab">Task information</a></li>
          <li><a href="#tab2" data-toggle="tab">Task initialization</a></li>
          <li><a href="#tab3" data-toggle="tab">Task results</a></li>
        </ul>
        <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
          <div class="tab-pane active" id="tab1">
            %s
          </div>
          <div class="tab-pane" id="tab2">
            %s
          </div>
          <div class="tab-pane" id="tab3">
            %s
          </div>
        </div>
      </div> <!-- /tabbable -->    
    """ % (tab1, tab2, tab3)
    
    #modal window for 2-D viewing of results
    #tune the firt line in order to centre modal window on the screen
    ret += """      
      <div id="myModal2D" class="modal hide fade" style="width: 930px; margin: -60px 0 0 -460px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center>
                <div id="map_label"></div>
          </center>
             
        </div>
            
            <div class="modal-body" style="max-height: 560px;">
               
                  
                   <div class="row">
                   <div class="span10">
                       <div id="map_canvas" style="height: 550px; border: 1px solid gray;">google map</div>
                   </div> <!-- / span10 -->
                   <div id="map_colorbar" class="span2" style="width: 110px; margin-left: 10px;">
                        
                   </div> <!-- / width span2-->
                   </div>
                  
            </div> <!-- / modal body -->         
      </div>    <!-- / My modal 2D-->  
    """
            
    return ret


def get_tab1(task):
    """
    generated content for the first tab i task details - task info
    
         "username": kwargs["username"], #author of the task
         "description": kwargs["description"], #task description
         "task_id": kwargs["task_id"],
         "task_type": kwargs["task_type"],
         "task_input": kwargs["task_input"],
         "task_output": kwargs["task_output"],
         "task_priority": 0,
         "created": time.time(), #we insert epoch - it can be converted into anything time.strftime("%d %b %Y %H:%M:%S UTC", time.gmtime()),
         "start_of_computation": None, #time when the task computation started
         "end_of_computation": None, #time of computation finish
         "computation_time": None, #time spend on task computation
         "task_status": 0, #task status: 0 - free - queeing, not solved, 1 - under solution, 2 - solved and closed, 3 - error occured
         "worker_ip": None, #ip address of the associated worker
         "worker_machine_name": None,
         "worker_id": None,
         "worker_username": None #username of the worker    
    """
    accor1 = """
    <table class="table table-bordered table-striped">
    <thead>
        <tr><th colspan="2">General information</th></tr>
    </thead>
    <tbody>
        <tr><th>Username</th><td>%s</td></tr>
        <tr><th>Task created</th><td>%s</td></tr>
        <tr><th>Task status</th><td>%s</td></tr>
        <tr><th>Description</th><td>%s</td></tr>
        <tr><th>Task ID</th><td>%s</td></tr>
        <tr><th>Task type</th><td>%s</td></tr>
        <tr><th>Task priority</th><td>%s</td></tr>
    </tbody> 
    </table>
    """ % (task["username"],
           tools.convert_epoch_to_time(task["created"]),
           web_html_results.get_status_bagde(task["task_status"]),
           task["description"],
           task["task_id"],
           web_html_results.get_task_type_badge(task["task_type"]),
           task["task_priority"])
    
    if (task["task_status"] in [1,2,3]):
        t1 = "N/A"
        t2 = "N/A"
        t3 = "N/A"
        
        if (task["task_status"] == 2):
            t1 = tools.convert_epoch_to_time(task["start_of_computation"])
            t2 = tools.convert_epoch_to_time(task["end_of_computation"])
            t3 = "%10.2f sec" % task["computation_time"]  
               
        accor2 = """  
        <table class="table table-bordered table-striped">     
        <thead>
            <tr><th colspan="2">Properties of assigned worker</th></tr>
        </thead>    
        <tbody>
            <tr><th>Worker's maschine name</th><td>%s</td></tr>
            <tr><th>Worker's IP address</th><td>%s</td></tr>
            <tr><th>Worker's ID</th><td>%s</td></tr>
            <tr><th>Worker's username</th><td>%s</td></tr>
            <tr><th>Start of computation</th><td>%s</td></tr>
            <tr><th>End of computation</th><td>%s</td></tr>
            <tr><th>Spent time</th><td>%s</td></tr>
        </tbody>
        </table>
        """ %  (task["worker_data"]["worker_machine_name"],
               task["worker_data"]["worker_ip"][0],
               task["worker_data"]["worker_id"],
               task["worker_data"]["worker_username"],
               t1,
               t2,
               t3
               )
    else:
        accor2 = ""
        
        
    ret = """
        <div class="accordion" id="accordion_general">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_general" href="#collapseOne_general">
                  Task information
                </a>
              </div>
              <div id="collapseOne_general" class="accordion-body collapse in">
                <div class="accordion-inner">
                  %s
                </div>
              </div>
            </div>          
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_general" href="#collapseTwo_general">
                  Worker information
                </a>
              </div>
              <div id="collapseTwo_general" class="accordion-body collapse">
                <div class="accordion-inner">
                  %s
                </div>
              </div>
            </div>    
        </div>
        
    """ % (accor1, accor2)

    return ret

 

####################################################################################################################################
####### TAB2
####################################################################################################################################
def get_tab2(task):
    """
    accordion with review of task inputs
    """
    task_input = task["task_input"]
    help = get_tab1_help()
    scenario = get_scenario_summary(task)
    source = get_source_summary(task_input)
    meteo = get_meteo_summary(task_input)
    
    ret = """
      <div class="row">
        <div class="span12 columns">   
          <div class="accordion" id="accordion_input">
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_input" href="#collapseOne_input">
                  Release scenario
                </a>
              </div>
              <div id="collapseOne_input" class="accordion-body collapse in">
                <div class="accordion-inner">
                  %s
                </div>
              </div>
            </div>          
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_input" href="#collapseTwo_input">
                  Source term
                </a>
              </div>
              <div id="collapseTwo_input" class="accordion-body collapse">
                <div class="accordion-inner">
                  %s
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_input" href="#collapseThree_input">
                  Meteorological data
                </a>
              </div>
              <div id="collapseThree_input" class="accordion-body collapse">
                <div class="accordion-inner">
                  %s
                </div>
              </div>
            </div>
          </div>   
          
      </div> <!-- /span11 --> 
     </div> <!-- /row -->       
    
    """ % (scenario, source, meteo)    
    
    return ret

def get_scenario_summary(task):
    """
    summary of dispersion task scenario
    
    what is in a task DB entry:

    {u'username': u'panda', 
    u'task_id': 7, 
    u'task_type': u'dispersion', 
    u'description': u'', 
    u'task_output': None, 
    u'worker_ip': None, 
    u'start_of_computation': None, 
    u'computation_time': None, 
    u'task_input': {u'activities': u"cnumpy.core.multiarray\n_reconstruct\np1\n(cnumpy\nndarray\np2\n(I0\ntS'b'\ntRp3\n(I1\n(I1\nI1\ntcnumpy\ndtype\np4\n(S'f8'\nI0\nI1\ntRp5\n(I3\nS'<'\nNNNI-1\nI-1\nI0\ntbI00\nS'\\x00\\x00@\\xe5\\x9c0\\xa2B'\ntb.",
     u'inversion': u'0', 
     u'segments_properties': u"cnumpy.core.multiarray\n_reconstruct\np1\n(cnumpy\nndarray\np2\n(I0\ntS'b'\ntRp3\n(I1\n(I3\nI1\ntcnumpy\ndtype\np4\n(S'f8'\nI0\nI1\ntRp5\n(I3\nS'<'\nNNNI-1\nI-1\nI0\ntbI00\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00'\ntb.", 
     u'disp_formula': u'KFK/Julich',
     u'H': u'100', 
     u'on_calm_modification': 1, 
     u'lon': 14.3754, 
     u'meteo': u"cnumpy.core.multiarray\n_reconstruct\np1\n(cnumpy\nndarray\np2\n(I0\ntS'b'\ntRp3\n(I1\n(I24\nI4\ntcnumpy\ndtype\np4\n(S'f8'\nI0\nI1\ntRp5\n(I3\nS'<'\nNNNI-1\nI-1\nI0\ntbI00\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00'\ntb.", 
     u'lat': 49.1817, 
     u'nuclides': [u'CS137'],
     u't_ref': 17200}, 
    u'created': 1354915806.394433, 
    u'task_priority': 0, 
    u'end_of_computation': None, 
    u'task_status': 0,
    u'_id': ObjectId('50c25fdeb652170ba3968efa'), 
    u'worker_username': None}    
    
    """
    #table_header = "<th>Parameter</th><th>Value</th>" 
  
    ret = """

            <table class="table table-bordered table-striped">
             
             <tbody>     
    """ #% table_header
    
    nuclides = ""
    if "nuclides" in task["task_input"]:
        for nuc in task["task_input"]["nuclides"][:-1]:
            nuclides += "%s, " % tools.mnl(nuc)
        nuclides += "%s" % tools.mnl(task["task_input"]["nuclides"][-1])
    
    
    #ret += "<tr><td>Task type:</td><td>%s</td></tr>" % task_type
    #ret += "<tr><td>Description:</td><td>%s</td></tr>" % description
    #ret += "<tr><td><b>Number of segments:</b></td><td>%d segments</td></tr>" % dt.mo2ma(task["task_input"]["segments_properties"]).shape[1]
    ret += "<tr><td><b>Released nuclides:</b></td><td>%s</td></tr>" % nuclides
    ret += "<tr><td><b>Source coordinates:</b></td><td>Lat: %f deg Lon: %f deg</td></tr>" % (task["task_input"]["lat"], task["task_input"]["lon"])
    #ret += "<tr><td><b>Height of release:</b></td><td>%5.1f m</td></tr>" % float(task["task_input"]["H"])
    #ret += "<tr><td><b>Dispersion formula:</b></td><td>%s</td></tr>" % task["task_input"]["disp_formula"]
 
    ret += """
             </tbody>
            </table> 
    """
        
    return ret
def get_source_summary(task_input):
    """
    summary of dispersion task source term
    """    
    print task_input.keys()
    
    nuclides = task_input["nuclides"]
    activities = dt.mo2ma(task_input["activities"])
    
    nucl_count = activities.shape[0]
    seg_count = activities.shape[1]
    
    table_header = "<th>No.</th><th>Nuclide</th>"
    
    for i in range(seg_count):
        table_header += "<th>Seg. %d</th>" % (i+1)
    
    ret = """
            <div style="overflow: scroll;">
            <table class="table table-bordered table-striped">
             <thead>
               <tr> 
                %s
               </tr>
             </thead>
             <tbody>     
    """ % table_header

    for i in range(len(nuclides)):
        ret += "<tr><th>%d</th><th>%s</th>" % (i+1, tools.mnl(nuclides[i]))
        for j in range(seg_count):
            ret += "<td>%3.2E</td>" % (float(activities[i, j]))         
        ret += "</tr>"      
    
    ret += """
             </tbody>
            </table> 
            </div>
    """
    return ret

def get_meteo_summary(task_input):
    """
    summary of dispersion task meteo
    """
    meteo = dt.mo2ma(task_input["meteo"])
    
    table_header = "<th>Hour</th><th>Wind direction [deg]</th><th>Wind speed [m/s]</th><th>Stab. category</th><th>Precipitation [mm/h]</th>"
    ret = """
            <table class="table table-bordered table-striped">
            <thead>
              <tr>
                %s
              </tr>
            </thead>
            <tbody>       
    """ % table_header 
    
    for i in range(meteo.shape[0]):
        ret += """
              <tr>
              <td>%d</td>
              <td>%5.2f</td>
              <td>%3.2f</td>
              <td>%s</td>
              <td>%3.2f</td>
              </tr>
        """ % (i+1, float(meteo[i,0]), float(meteo[i,1]), meteo[i,2], float(meteo[i,3]))
    
    ret += """
            </tbody>
           </table>  
    """   
    
    return ret
####################################################################################################################################
####### TAB3
####################################################################################################################################
######DATA TYPES - seialization for mongodb
def ma2mo(matrix): #matrix - numpy nd.array to mongo, matrix is serialized using cPickle
    return cPickle.dumps(matrix)

def mo2ma(mongo_obj): #mongo to numpy nd.array
    return cPickle.loads(str(mongo_obj))

def get_tab3(task_id, task_input, output, task_status):
    """
    accordion with outputs of task
    """
    
    if (int(task_status) == 2):
        help = get_tab2_help()
         
        lon = task_input["lon"] #deg
        lat = task_input["lat"] #deg
        desc = "<sup>41</sup>Ar - Time integrated dose [Sv]"
                
        ret = """
          <div class="row">
            <div class="span12 columns">   
              <table class="table table-bordered table-striped">
              <tr>
                  <td>%s</td><td>%s</td>
              </tr>
              </table>          
          </div> <!-- /span9 --> 
         </div> <!-- /row -->       
        
        """ % (desc, get_action_button(task_id, lat, lon, desc))
    else:
        ret = "<h3>Task computation not finished yet.</h3>"
    
    return ret



#################################################################################################################################################
### ACCORDIONS
#################################################################################################################################################



def get_table_header():
    ret = """       
              <tr>
                <th>No.</th>
                <th>Nuclide</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Actions</th>
              </tr>    
    """
    return ret    

def get_table_end():
    ret = """
            </tbody>
           </table> 
    """
    
    return ret

def get_table_body(task_id, ident, quantity, unit, keys, input):
    """
    table body for given identifier, ie. #DEP
    """
    ret = ""
    index = 1

    for key in keys:
        if key.startswith(ident):
            nucl = key[5:].strip()
            ret += """
            <tr>
            <th>%d</th>
            <th>%s</th>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            </tr>
            """ % (index, tools.mnl(nucl), quantity, unit, get_action_button(task_id, key, input["lat"], input["lon"], tools.mnl(nucl)+": "+quantity+" ["+unit+"]"))
            index += 1
    
    return ret

def get_doses_early_accordion(task_id, input, output):
    """
    inner of accordion with early phase outputs
    """
    ret = ""
 
    ret += """<table class="table table-bordered table-striped">"""
    ret += "<thead>"   
    ret += """<tr><th colspan="6">External irradiation from the cloud</th></tr>"""
    ret += get_table_early_header()
    ret += "</thead>"
    ret += "<tbody>"    
    ret += get_doses_early_table_body(task_id, "#DCM", "External irradiation from the cloud", "Sv", output.keys(), input) #return table body for specified identificator
    #ret += get_table_end()
    ret += "</tbody>"
    
    ret += "<thead>"
    ret += """<tr><th colspan="6">External irradiation from deposition</th></tr>"""
    ret += get_table_early_header()
    ret += "</thead>"
    ret += "<tbody>"    
    ret += get_doses_early_table_body(task_id, "#DCD", "External irradiation from deposition", "Sv", output.keys(), input) 
    #ret += get_table_end()
    ret += "</tbody>"
    
    ret += "<thead>"
    ret += """<tr><th colspan="6">External irradiation from inhalation</th></tr>"""
    ret += get_table_early_header()
    ret += "</thead>"
    ret += "<tbody>"    
    ret += get_doses_early_table_body(task_id, "#DCH", "External irradiation from inhalation", "Sv", output.keys(), input) 
    #ret += get_table_end()
    ret += "</tbody>"
    
    ret += "<thead>"
    ret += """<tr><th colspan="6">Total committed doses (cloud + depo + inhalation)</th></tr>"""
    ret += get_table_early_header()
    ret += "</thead>"
    ret += "<tbody>"    
    ret += get_doses_early_table_body(task_id, "#DCT", "Total committed doses", "Sv", output.keys(), input) 
    #ret += get_table_end()
    ret += "</tbody>"
    ret += "</table>"    
      
    return ret

def get_doses_early_table_body(task_id, ident, quantity, unit, keys, input):
    """
    table body for given identifier, ie. #DEP
    """
    ret = ""
    index = 1

    for key in keys:
        if key.startswith(ident):
            print ident
            age = ages[int(key[8])-1]
            organ = organs[int(key[9])-1]
            ret += """
            <tr>
            <td>%d</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            </tr>
            """ % (index, quantity, age, organ, unit, get_action_button(task_id, key, input["lat"], input["lon"], quantity+", "+age+", "+organ+" ["+unit+"]"))
            index += 1
    
    return ret

def get_table_early_header():
    ret = """
              <tr>
                <th>No.</th>
                <th>Quantity</th>
                <th>Age group</th>
                <th>Organ</th>
                <th>Unit</th>
                <th>Actions</th>
              </tr>
    """
    return ret 

def get_doses_late_accordion(task_id, input, output):
    """
    inner of accordion with late phase
    """
    ret = ""
    ret += """<table class="table table-bordered table-striped">"""
    ret += "<thead>"
    ret += """<tr><th colspan="7">Long-term external irradiation from deposition</th></tr>"""
    ret += get_table_late_header()
    ret += "</thead>"
    ret += "<tbody>" 
    ret += get_doses_late_table_body(task_id, "#DDD", "External irradiation from deposition", "Sv", output.keys(), input) #return table body for specified identificator
    ret += "</tbody>"
    
    ret += "<thead>"
    ret += """<tr><th colspan="7">Internal irradiation due to long-term resuspension</th></tr>"""
    ret += get_table_late_header()
    ret += "</thead>"
    ret += "<tbody>" 
    ret += get_doses_late_table_body(task_id, "#DDR", "Internal irradiation due to resuspension", "Sv", output.keys(), input) 
    ret += "</tbody>"
    ret += "</table>"
    
    
    #ret += "<thead>"
    #ret += """<tr><th colspan="7">Total committed doses (from early and late phases)</th></tr>"""
    #ret += get_table_late_header()
    #ret += "</thead>"
    #ret += "<tbody>" 
    #ret += get_doses_late_table_body(task_id, "#DDT", "Total committed doses (from early and late phases)", "Sv", output.keys(), input) 
    #ret += "</tbody>"
    #ret += "</table>"

    return ret    

def get_table_late_header():
    ret = """
              <tr>
                <th>No.</th>
                <th>Quantity</th>
                <th>Age group</th>
                <th>Organ</th>
                <th>Time</th>
                <th>Unit</th>
                <th>Actions</th>
              </tr>     
    """
    return ret     

def get_doses_late_table_body(task_id, ident, quantity, unit, keys, input):
    """
    table body for given identifier, ie. #DEP
    """
    ret = ""
    index = 1
    
    for key in keys:
        if key.startswith(ident):
            print ident
            age = ages[int(key[9])-1]
            organ = organs[int(key[10])-1]
            time = times[int(key[11])-1]
            ret += """
            <tr>
            <td>%d</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>            
            <td>%s</td>
            </tr>
            """ % (index, quantity, age, organ, time, unit, get_action_button(task_id, key, input["lat"], input["lon"], quantity+", "+age+", "+organ+", "+time+" ["+unit+"]"))
            index += 1
    
    return ret

def get_action_button(task_id, lat, lon, desc):
    """
    generates html of action button for viewing results of task entries
    """
    
    ret = """
            <div class="btn-group">
              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a onclick="prepare_overlay(%d, %f, %f, '%s'); $('#myModal2D').modal('show'); return false;">Show on map</a></li>   
                <li><a href="/results/prepare_data_to_download?task_id=%s">Download data</a></li>        
              </ul>
            </div><!-- /btn-group -->    
    """ % (task_id, lat, lon, desc, task_id) #we must send it without leading "#" whne using GET
    
    return ret
    
#################################################################################################################################################
###HELP TEXTS
#################################################################################################################################################
def get_tab1_help():
    """
    return html with tab3 help
    """
    
    ret = """
          <h3>tab1</h3>
          <p>Get base styles and flexible support for collapsible components like accordions and navigation.</p>
          <a href="assets/js/bootstrap-collapse.js" target="_blank" class="btn">Download file</a>
          <p class="muted"><strong>*</strong> Requires the Transitions plugin to be included.</p>    
    """
    
    return ret
    
def get_tab2_help():
    """
    return html with tab3 help
    """
    
    ret = """
          <h3>tab2</h3>
          <p>Get base styles and flexible support for collapsible components like accordions and navigation.</p>
          <a href="assets/js/bootstrap-collapse.js" target="_blank" class="btn">Download file</a>
          <p class="muted"><strong>*</strong> Requires the Transitions plugin to be included.</p>    
    """
    
    return ret

  
    