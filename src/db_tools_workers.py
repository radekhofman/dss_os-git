# -*- encoding: UTF-8 -*-
import admin_tools as at
import cPickle
import locking
import time
import pymongo

#getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']

WORKER_MAX_LOG_LINES = 200 #if workers log exceeds this number of lines, old log lines are replaced with new ones

def append_log(log, new_log):
    """
    appends new log entries from the worker to the log with respect to limits given by WORKER_MAX_LOG_LINES
    """
    difference = WORKER_MAX_LOG_LINES - (len(log) + len(new_log))
    if difference < 0:
        return log[difference:] + new_log       
    else:
        return log + new_log#we extend the current log with the new lines

def update_worker_status(worker_dict):
    """
    inserts worker into DB, otherwis updates just subset of worker's entry, like log, status, system resources of the machine, etc. 
    """
    #lock = locking.Lock() #MUTEX
    #maibe drastically slows down:(
    #try:         
    #    lock.acquire()
    
    worker = db_workers.find_one({"worker_username": worker_dict["worker_username"]})
    #print "nalezeny worker:", worker
    
    if (worker == None):
        #the worker with its username is not in BD, the first query...
        worker_dict["worker_last_ping"] = time.time() #initialization of last ping
        db_workers.insert(worker_dict)
    else:
        #modification of the worker with new data
        #mozna nejaky porovnavani jejich vlastnosti, kdyz budou dva se stejnym jmenem...
        worker["worker_ping_interval"] = time.time() - worker["worker_last_ping"]
        worker["worker_last_ping"] = time.time()
        worker["worker_log"] = append_log(worker["worker_log"], worker_dict["worker_log"])
        worker["worker_resources"] = worker_dict["worker_resources"]
        worker["worker_state"] = worker_dict["worker_state"]
        worker["worker_task_percentage"] = worker_dict["worker_task_percentage"]
        db_workers.save(worker)
    
    #finally:
    #    lock.release()


def get_list_of_workers():
    """
    return list of workers
    """
    workers = db_workers.find()
    
    return workers

def get_workers_slice(first=0, size=10):
    """
    returns all workerss registered in DB
    """
    #we return slice
    workers = get_list_of_workers()
    wc = workers.count()
    last = first + size
    if first < 0:
        first = 0
    
    if last > wc:
        last = wc
    
    return workers[first:last]
    
def get_workers_count():
    """
    returns count of registered workers
    """
    return get_list_of_workers().count()

def delete_worker(worker_username):
    """
    deletes worker with given worker_username from collection
    """
    worker = db_workers.find_one({"worker_username": worker_username})
    print "deleting worker..."
    _id = worker["_id"]
    db_workers.remove({"_id": _id})
    
    