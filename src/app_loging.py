#!/usr/bin/python
# -*- coding: UTF-8 -*-

import logging.handlers
import os
import sys


# Set up a specific logger with our desired output level
app_logger = logging.getLogger('SE-APP-LOG')
app_logger.setLevel(logging.DEBUG)

console_handler = logging.StreamHandler()

# Set logger formating
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)

app_logger.addHandler(console_handler)
