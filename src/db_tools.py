# -*- encoding: UTF-8 -*-
import admin_tools as at
import db_models
import cPickle
import locking
import time
import pymongo

def get_datetime():
    return time.strftime("%d %b %Y %H:%M:%S UTC", time.gmtime())


#getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']
db_nucl_groups = at.dbs['db_nucl_groups']

######GENERAL METHODS
#clears all collections in the DB
def clear_mongo(colls='all'): #names of collections to clear, e.g. 'db_users'

    if colls == 'all':
        colls = db.collection_names()

    for name in colls:
        db[name].remove()

def clear_workers():
    db["db_workers"].remove()
#######USERS
def insert_user(**kwargs): #argumemnts are parsed from a dictionary
    #this keys must be entered by admin othertwise insertion of user is aborted
    compulsory_keys = ["username", "password", "email", "user_type"]
    valid = True
    for key in compulsory_keys:
        if (key not in kwargs.keys()):
            valid = False

    if valid:
        #is username unique?
        if (db_users.find_one({"username": kwargs["username"]}) == None):

            d = db_models.user_DB_model(kwargs)
            db_users.insert(d)
            print "User %s successfully inserted!" % kwargs["username"]
            return True
        else:
            print db_users.find_one({"username": kwargs["username"]})
            print "User %s not created, username already exists!" % kwargs["username"]
            return False

    else:
        print "Not enought data for new user insertion"
        return False

def update_last_login(username):
    """
    updates users last login time for future login
    """
    user = db_users.find_one({"username" : username})
    user["last_login2"] = user["last_login"]  
    user["last_login"] = get_datetime()
    
    db_users.save(user)


def get_last_login(username):
    """
    returns las tlogin of the user as a formatted string, in UTC
    """
    user = db_users.find_one({"username" : username})
    
    if user["last_login2"] == None:
        return "first login"
    else:
        return user["last_login2"]


########tasks    
def get_tasks_of_user_slice(username, first=0, size=10):
    """
    returns all tasks belonging to given user
    """
    #we return slice
    tasks = get_tasks_of_user(username)
    tc = tasks.count()
    last = first + size
    if first < 0:
        first = 0
    
    if last > tc:
        last = tc
    
    return tasks[first:last]

def get_tasks_of_user(username):
    """
    returns all tasks belonging to given user
    """
    #we return slice
    tasks = db_tasks.find({"username": username}).sort("created", -1)
    
    return tasks

def get_tasks_count_of_user(username):
    """
    gets number of tasks of user
    """    
    return db_tasks.find({"username": username}).count()

def get_user(username):
    """
    returns dict with user details of partiuclar user
    """
    return db_users.find_one({"username": username})
 
def get_usergroup_of_user(username):
    """
    returns usergroup of particulate user
    """
    return db_users.find_one({"username": username})["user_group"]
    
    
def get_new_task_id():
    #vrati id pro novy task, task_id is integer incrementted by one
    d = db_tasks.find()
    
    d = db_service.find_one()
    
    if d:
        id = d["last_taks_id"]
        d["last_taks_id"] = id +1
        db_service.save(d)
        return id
        
    else: #the first id
        db_service.insert({"last_taks_id" : 1})
        return 0
        
    

def create_task(**kwargs): #argumemnts are parsed from a dictionary
    #this keys must be entered by admin othertwise insertion of user is aborted
    lock = locking.Lock()
    try: #MUTEX
        lock.acquire()    
        compulsory_keys = ["username", "description", "task_type", "task_input", "task_output"]
        valid = True
        for key in compulsory_keys:
            if (key not in kwargs.keys()):
                valid = False
    
        if valid: #the request for new task is valid
            #obtaining new task_id - integer
            #db_tasks.create_index([("task_id", pymongo.ASCENDING)]) 
            kwargs["task_id"] = get_new_task_id()
            #creating db netry
            d = db_models.task_DB_model(kwargs)
            #inserting entry...
            db_tasks.insert(d)
            db_tasks.ensure_index("created", background=True) 
            db_tasks.ensure_index("username", background=True)
            
            return True
        else:
            print "Not enought data for new task creation"
            return False
    finally:
        lock.release()        

#gets a free task from queue, sets its status to "under solution" and assignes it to a worker
def get_free_task(worker_dict):
    """
    finds a task with status 0 - free task and stores inaformation about worker who took it
    """
    #we have to acquire lock on this part of code
    lock = locking.Lock()
    try: #MUTEX
        lock.acquire()
        task_type = worker_dict["worker_type"]
        free_task = db_tasks.find_one({"task_type": task_type, 
                                       "task_status": 0, 
                                       "usergroup_of_task_owner": worker_dict["worker_dedicated"]
                                       })
        if free_task:
            #returns a free task if there are tasks in teh queue
            free_task["task_status"] = 1
            #we update worker info about assigned worker before we pass task to the worker=>
            #we do not need to upgrade it after the task is done:)
            free_task["start_of_computation"] = time.time() #epoch of computation start
            free_task["worker_data"] = worker_dict  
             
            db_tasks.save(free_task)
            #musi se save, ne update viz dale, to cely se to prepise tou jedinou hodnotou - db_tasks.update(free_task, {"task_status": 1})
            return free_task
        else:
            #currently, there are no free tasks
            return None
    finally:
        lock.release()


def update_finished_task(task): #called from a worked with outputs, which are stored into DB for the user
    lock = locking.Lock()
    try: #MUTEX        
        lock.acquire()

        """
        out_mg = [] #conversion of outputs for mongodb
        for out in outputs:
            out_mg.append(cPickle.dumps(out))

        task["task_output"] = out_mg
        task["task_status"] = 2
        """
        
        #we obtain already modified task... so we just save its new state
        db_tasks.save(task) #, {"task_output": out_mg, "task_status": 2}) #status set to "solved and closed"
        return 1

    finally:
        lock.release()

def delete_task(task_id):
    "deletes task with given ID from collection"
    task = db_tasks.find_one({"task_id": task_id})
    print "deleting task:"
    _id = task["_id"]
    db_tasks.remove({"_id": _id})
     
    """
    #db_tasks.remove(task)
    print task
    print task["task_id"], task_id
    db_tasks.remove(task) #, safe=True)
    print "after remove:"
    task = db_tasks.find_one({"task_id": task_id})
    print task["task_id"]
    """

def get_task_of_given_id(username, task_id):
    """
    returns task of provided id
    """
    return db_tasks.find_one({"task_id": task_id, "username": username})      

def get_output_of_task(task_id, ident):
    """
    for given dispersion task task return demanded output identified with identificator
    """
    task = db_tasks.find_one({"task_id": task_id})
    if task != None:
        return mo2ma(task["output"][ident])
    else:
        return None


######DATA TYPES - seialization for mongodb
def ma2mo(matrix): #matrix - numpy nd.array to mongo, matrix is serialized using cPickle
    return cPickle.dumps(matrix)

def mo2ma(mongo_obj): #mongo to numpy nd.array
    return cPickle.loads(str(mongo_obj))



