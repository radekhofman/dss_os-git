# -*- encoding: UTF-8 -*-
import cherrypy
import copy
import config as cf
import web_html #comtains methods for web HTML rendering
import web_html_results
from mako.lookup import TemplateLookup
import db_tools
import os
import google_map_tools
from hashlib import md5
from cherrypy.lib.static import serve_file
import results.Plotters
import numpy

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common
SESSION_KEY = cf.SESSION_KEY

class Results:
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    def __init__(self, url):
        self.base_url = url if url.startswith("http://") else "http://%s" % url
    
    @cherrypy.expose
    def index(self, first=0, detail=None, num=None, task_status=None): #index of main DSS GUI for common user
        return self.render_page(first, detail, num, task_status)
    
    @cherrypy.expose
    def render_page(self, first, detail, num, task_status): #num= number of selected task, and his status...
        #navibar with buttons
        head = web_html_results.head()
        nav = web_html.navibar(active=4) #renders navibar according to login state
        bod = web_html_results.results_body(int(first), detail, num, task_status)
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
    
    
    @cherrypy.expose
    def generate_task_table(self, first, size):
        """
        return html of user task table
        """
        username = cherrypy.session[SESSION_KEY]
        ret = web_html_results.generate_users_task_table(username, int(first), int(size))
        return ret
    
    @cherrypy.expose
    def del_task(self, first, task):
        "deletes selected task and redirects back to task list"
        db_tools.delete_task(int(task))
        raise cherrypy.HTTPRedirect("/results?first=%d" % int(first))
    
    @cherrypy.expose
    #post method for preparation of the overlay
    def prepare_overlay_disp(self, task_id, ident):
        """
        is called by javasctiprt from rsults page
        prepares KML overlay of particular output for google map, saves it o overlay directory and returns its path
        """    
        try:
            #identificataor of the figure
            #if doesnot exist, we create new figure, saves resources in repetitive viewing of results
            task_id = int(task_id) #post returns unicode..
            
            fig_ident = "fig_disp_%d_%s" % (task_id, ident)
    
    
            username = cherrypy.session[SESSION_KEY]
    
            task = db_tools.get_task_of_given_id(username, task_id)
            
            data = db_tools.mo2ma(task["task_output"]["dose"])
            
            #create and save figure
            img_savepath = os.path.join(cf.TEMP_DIR, fig_ident+".svg")
            
            ###########
            task_input = task["task_input"]
            Dx = task_input["output_domain_size_x"] #km
            Dy = task_input["output_domain_size_y"] #km
            Ds = task_input["output_grid_step"] #km
            
            #google_map_tools.plot_rose_and_save(data, img_savepath)
            
            
            print "Got", numpy.sum(data[0, :, :], axis=0).shape
            print "Expected:", (int(Dx/Ds), int(Dy/Ds))
            
            data_sum = numpy.reshape(numpy.sum(data[0, :, :], axis=0), (int(Dx/Ds), int(Dy/Ds))) 
            results.Plotters.contourAStanice(data_sum, img_savepath, [], [], [], Dx, Dy, Ds, "", 6)
            #create KML
            lat = task["task_input"]["lat"]
            lon = task["task_input"]["lon"]
            kml_savepath = os.path.join(cf.TEMP_DIR, fig_ident+".kml")
            google_map_tools.create_KML_file(lat, lon, self.base_url+"/static/overlays/"+fig_ident+".svg", kml_savepath)
            
            return self.base_url+"/static/overlays/"+fig_ident
        except Exception as e:
            print "Exception %s " % e
            import logging
            logging.exception(e)
            return "??"
 
    @cherrypy.expose
    def prepare_data_to_download(self, task_id, ident): 
        """
        is called by javascript and opens window with data file
        """
        #cherrypy.response.headers['Content-Type'] = 'py/x-download'  
        
        username = cherrypy.session[SESSION_KEY]
        filename = username+"_"+task_id+"_"+ident+"_"+md5(username+task_id+ident).hexdigest()+".txt"
        
        ident = "#"+ident #using GET was send without leading "#"
        task_id = int(task_id)
        
        task = db_tools.get_task_of_given_id(username, task_id)
        #data is a sparse matrix, must be converted to full matrix before plotting    
        data = db_tools.mo2ma(task["task_output"][ident]).todense()
        
        s = [ident+"  "+"80x42 matrix"+"\n"]
        for i in range(80):
            line = ""
            for j in range(42):
                line += "  %3.2E" % data[i,j]
            line += "\n"
            s.append(line)
        
        file_path = os.path.join(cf.DATAFILES_DIR, filename)
        f = open(file_path, "w+")
        f.writelines(s)
        f.close()
        
        return serve_file(file_path, "py/x-download", "attachment")

        
        
        
