# -*- encoding: UTF-8 -*-
from pymongo import Connection
import sys
import os
import data.config

#connection to mongo
#getting connection
#conn = Connection()
#conn = Connection("mongodb://admin:M8AVjrY8-Lk4@127.9.9.2:27017/")
conn = Connection(data.config.MONGODB_DB_URL)

#getting a databases
db = conn["testpy"]

#getting collections.. #db.collection_names() prints the current collections
#instantiation collections
db_users = db['users']
db_tasks = db['tasks']
db_service = db['service']
db_workers = db['workers']
db_source_terms = db['source_terms']
db_meteo = db['meteo']
db_receptors = db['receptors']
db_nucl_groups = db['nucl_groups']

dbs = {"db": db,
       "db_users": db_users,
       "db_tasks": db_tasks,
       "db_service": db_service,
       "db_workers": db_workers,
       "db_source_terms": db_source_terms,
       "db_meteo": db_meteo,
       "db_receptors": db_receptors,
       "db_nucl_groups": db_nucl_groups
       }





##################methods for admin
def print_users():
    users = db_users.find()
    print "===================================="
    print "Users registered to dss.utia.cas.cz:"
    print "------------------------------------"
    for user in users:
        username = user["username"]
        print "name:", username, "type:", user["user_type"], "group:", user["user_group"], "login:", user["last_login2"]
    print "===================================="


def print_tasks(username=""):
    tasks = []
    if username != "":
        tasks = db_tasks.find({"username": username})
    else:
        tasks = db_tasks.find()
    print "===================================="
    print "Tasks created in dss.utia.cas.cz: %d" % tasks.count()
    print "------------------------------------"
    for task in tasks:
        #print task
        print task["task_id"], task["username"], task["task_status"], task["description"]
        #print "name:", task["username"], "desc:", task["description"], "ID:", task["task_id"], "status:", task["task_status"]
    print "===================================="

def print_services():
    serv = db_service.find()

    for s in serv:
        #print task
        print s
        #print "name:", task["username"], "desc:", task["description"], "ID:", task["task_id"], "status:", task["task_status"]

def print_workers():
    workers = db_workers.find()
    for worker in workers:
        print "========================="
        print worker["worker_username"], "size=", sys.getsizeof(worker), "bytes"

        keys = worker.keys()
        for key in keys:

            print "size of ", key, "=",sys.getsizeof(worker[key]), "bytes"

        log = worker["worker_log"]
        for log_line in log:
            print log_line
        #print worker["worker_log"]


def drop_tasks():
    """
    drops task collection
    """
    db_tasks.drop()