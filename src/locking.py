import os
import fcntl
import openshift.variables as os_vars

DEFAULT_FN = "lock.panda"

class Lock:

    def __init__(self, filename=None):
        # Get passed filename or use the default one
        self.filename = filename if filename else self.__get_default_lockfile()
        # This will create it if it does not exist already
        self.handle = open(self.filename, 'w')

    # Bitwise OR fcntl.LOCK_NB if you need a non-blocking lock
    def acquire(self):
        fcntl.flock(self.handle, fcntl.LOCK_EX)

    def release(self):
        fcntl.flock(self.handle, fcntl.LOCK_UN)

    def __del__(self):
        self.handle.close()

    def __get_default_lockfile(self):
        """
        Returns default lock file in respect to  possible openshift deployment.
        """
        lock_filename = DEFAULT_FN
        if os_vars.OPENSHIFT_TMP_DIR:
            lock_filename = os.path.join(os_vars.OPENSHIFT_TMP_DIR, DEFAULT_FN)
        return lock_filename

