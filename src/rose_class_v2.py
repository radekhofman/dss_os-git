#import scipy.io
import numpy
import re
import copy
import plotters_42
import math
#import covariance_tools

#tadyta trida je specielne na praci s podmnozinama polarni site
#pokud mozno s libovolnejma, ale v krajnim pripade aspon s podmnozinou kolem sredu do 13 od zdroje, v ramci havarijni zony JETE

#UZ NEJSOU SUBINDEXES, ale romzery se automaticky nactou z dictionary s maticema

class Rose_class_v2:
    
    ENSEMBLE = 0. #ENSEMBKE jako obdelnik - ve sloupcich jsou vektory vsech stavovych bodu
    ##r, t - odkud kam to bude z puvodniho 0-79 a 0-34
    def __init__(self, r1, r2, t1, t2):

        self.r1 = r1#scipy.io.loadmat(path_to_ensemble)["r1"] #sub_indexes[0]
        self.r2 = r2#scipy.io.loadmat(path_to_ensemble)["r2"] #sub_indexes[1]
        self.t1 = t1#scipy.io.loadmat(path_to_ensemble)["t1"] #sub_indexes[2]
        self.t2 = t2#scipy.io.loadmat(path_to_ensemble)["t2"] #sub_indexes[3]
        
        print "Loading rose of dimensions [%d - %d] x [%d - %d]" % (self.r1, self.r2, self.t1, self.t2)

        r1 = self.r1
        t1 = self.t1
        r2 = self.r2 + 1
        t2 = self.t2 + 1

        #definition of parallerls - stredy policek, kde se to homogenizuje
        self.parallels_FULL = [  # 0
                          0.1, #-6
                          0.2, #-5
                          0.3, #-4
                          0.5, #-3
                          0.7, #-2
                          1.0, #-1
                          1.4, # 0
                          1.9, # 1
                          2.5, # 2
                          3.2, # 3
                          4.0, # 4
                          4.5, # 5
                          5.5, # 6
                          6.5, # 7
                          7.5, # 8
                          8.5, # 9
                          9.5, #10
                         10.5, #11
                         11.5, #12
                         13.0, #13
                         15.0, #14
                         17.0, #15
                         19.0, #16
                         21.0, #17
                         23.0, #18
                         25.0, #19
                         27.0, #20
                         29.0, #21
                         32.5, #22
                         37.5, #23
                         42.5, #24
                         47.5, #25
                         52.5, #26
                         57.5, #27
                         62.5, #28
                         67.5, #29
                         72.5, #30
                         77.5, #31
                         82.5, #32
                         87.5, #33
                         92.5, #34
                         97.5] #35

        self.parallels = copy.deepcopy(self.parallels_FULL[t1:t2])
        #druha sada radialnich vzdalenosti zobrazujici hranici policek
        self.parallels2_FULL = [  # 0
                          0.15, #-6
                          0.25, #-5
                          0.4, #-4
                          0.6, #-3
                          0.85, #-2
                          1.2, #-1
                          1.65, # 0
                          2.2, # 1
                          2.85, # 2
                          3.6, # 3
                          4.25, # 4
                          5.0, # 5
                          6.0, # 6
                          7.0, # 7
                          8.0, # 8
                          9.0, # 9
                         10.0, #10
                         11.0, #11
                         12.0, #12
                         14.0, #13
                         16.0, #14
                         18.0, #15
                         20.0, #16
                         22.0, #17
                         24.0, #18
                         26.0, #19
                         28.0, #20
                         30.0, #21
                         35.0, #22
                         40.0, #23
                         45.0, #24
                         50.0, #25
                         55.0, #26
                         60.0, #27
                         65.0, #28
                         70.0, #29
                         75.0, #30
                         80.0, #31
                         85.0, #32
                         90.0, #33
                         95.0, #34
                        100.0] #35

        self.parallels2 = copy.deepcopy(self.parallels2_FULL[t1:t2])
        self.theta_FULL = numpy.linspace(0., (2.*numpy.pi)  , 81.) #- (2.*numpy.pi/80.)
        self.theta2_FULL = numpy.linspace(2.25*(2*numpy.pi/360.), 2.*numpy.pi + 2.25*(2*numpy.pi/360.) , 81.)
        self.theta_plot = numpy.linspace(0., (2.*numpy.pi)  , 81.) #- (2.*numpy.pi/80.)
        self.theta2_plot = numpy.linspace(2.25*(2*numpy.pi/360.), 2.*numpy.pi + 2.25*(2*numpy.pi/360.) , 81.)
        self.theta = copy.deepcopy(self.theta_FULL[r1:r2])
        self.theta2 = copy.deepcopy(self.theta2_FULL[r1:r2])

        #print "Initial shapes:"
        #print "parallels   %d " % len(self.parallels)
        #print "parallels 2 %d " % len(self.parallels2)
        #print "theta       %d " % len(self.theta)
        #print "theta 2     %d " % len(self.theta2)
        
        self.r_span = r2 - r1 
        self.t_span = t2 - t1 
        self.tot_nod = self.r_span * self.t_span
        
        #jen tak nejak ruzice, kdyby bylo potreba nacist...
        self.title = ""
        self.data = numpy.zeros((self.r_span, self.t_span))
       
        print "Number of nodes: %d * %d = %d" % (self.r_span, self.t_span, self.tot_nod)
        self.X_mesh, self.Y_mesh = self.get_meshgrid()

        #self.cov_fact = covariance_tools.Covariance_factory(self.X_mesh, self.Y_mesh, self.r1, self.r2, self.t1, self.t2)

        #self.load_prior(path_to_ensemble)

    def loadRose(self, path, skipLines):
        #opens a file and loads data
        #path - path to a file
        #skipLines - int, how many lines should be skipped before loading the data
        print "Reading data from %s" % path
        handle = open(path,"r")
        s = handle.readlines()
        print "Loaded %d lines " % (len(s))
        self.title = s[0]
        data = numpy.zeros((81, 42))
        for i in range(skipLines, 80+skipLines):
            line = re.sub('^[ ]+', '', s[i])
            x = map(float, re.split('[\r\n\t ]+', line[0:len(line)-1]))
            data[i-skipLines][0:42] = x[1:43]
        line = re.sub('^[ ]+', '', s[skipLines])
        x = map(float, re.split('[\r\n\t ]+', line[0:len(line)-1]))
        data[80][0:42] = x[1:43]

        #pak si z te ruzice corpnu jen potrebnou velikost co chci
        self.data = data[self.r1:self.r2+1, self.t1:self.t2+1]
        handle.close()

    """
    def load_prior(self, path_to_ensemble):
        "nacte ze souboru prior mean a covariance"
        #nacteni z DICTIONARY, kam sem MEAN a COV nagenerovanou z 5k ruzic ulozil
        dict = {}
        scipy.io.loadmat(path_to_ensemble, dict)
        self.ENSEMBLE = dict["ensemble"]
        
        print "(Sub)ENSEMBLE dimension [%d, %d]" % (self.ENSEMBLE.shape[0], self.ENSEMBLE.shape[1])
    """    

    def get_meshgrid(self):
        """udelam meshgrid ze zadaneho vyrezu"""
        R = self.parallels
        theta = self.theta

        r,t = numpy.meshgrid(R, theta)

        X = r*numpy.cos(t)
        Y = r*numpy.sin(t)

        return X, Y
    
    def load_receptors(self, X, Y, names):
        self.X_meas = X
        self.Y_meas = Y
        self.names_meas = names

    def vizualize_covariance_slice(self, radius, distance, M, saveFigFlag, fileName, showFlag):
        ###radisu, distance = SOURADNICE V TOM VYREZU!!!!!!!!!!! souradnice polarni site, jakej bod se bere-relativne k vyrezu
        """zobrazi rez kovariancni matici a reshpauje ho pred tim do tvaru r2-r1+1 a t2-t1+1"""
        ###x1, y1 - nene, je to rozmer te subkovariance
        x1 = self.r_span
        y1 = self.t_span
        row = self.t_span*radius + distance
        slice = numpy.reshape(copy.deepcopy(M[row][:]), (x1, y1), order='C')
        #slice = numpy.concatenate((slice0, numpy.zeros((1,self.r_span)) ))

        print "Covariance slice"
        print slice.shape

        print "Min slice: %E;" % (slice.min())
        print "Max slice: %E;" % (slice.max())

        isoN = 8
        a = row// self.t_span + self.r1
        b = row % self.t_span + self.t1
        tit = "Slice %d;  [%d; %d]" % (row, a, b)

        #nastaveni flagu pro vykreslovani
        logar_scale_flag = True#True   #chceme logaritmickou stupnici
        area_calc        = False  #nechceme pocitat plochu

        M = slice#numpy.ma.masked_where(slice<=0, slice)
        points_to_plot = []
        x = self.parallels[b]*math.cos(-1*(a*4.5/360.*2.*math.pi) + math.pi/2.)
        y = self.parallels[b]*math.sin(-1*(a*4.5/360.*2.*math.pi) + math.pi/2.)
        #point = [x, y]
        #ten bod, kde se uakzuje jakej to je rez te kovariancni matice
        points_to_plot= [[x*1000], [y*1000]]#.append(point)

        dist = self.parallels2_FULL[self.t2]
        M_FULL = numpy.zeros((81, 35))
        print M_FULL[self.r1:self.r2+1, self.t1:self.t2+1].shape
        print M.shape
        M_FULL[self.r1:self.r2+1, self.t1:self.t2+1] = M[:, :]
        print "vzdalensot %f " % dist
        plotters_42.polar_rose_centre_plotter(dist, self.parallels_FULL, self.theta_plot, M_FULL, isoN, tit, self.parallels2_FULL, self.theta2_plot, logar_scale_flag, area_calc, points_to_plot, [], saveFigFlag, fileName, showFlag)
        #X, Y = self.get_meshgrid()
        #plotters.covariance_plotter(M, X, Y, tit)

    def vizualize_col_vec(self, col_vec, rad, receptor_flag, names_flag, saveFigFlag, fileName, showFlag, ownLevelsFlag, levels, title, rady):
        "preshapuje sloupcovy vektor na matici ruzice a zobrazi"
        print "Min M: %E;" % (col_vec.min())
        print "Max M: %E;" % (col_vec.max())

        isoN = rady
        radius = rad
        if (rad == 0):
            radius = self.parallels2_FULL[self.t2]
        tit = title

        #M = numpy.zeros((self.r_span+1, self.t_span))
        #A = mean[0, :].copy().reshape(1, self.t_span)
        #M = numpy.concatenate((mean, A))

        dist = self.parallels2_FULL[self.t2]
        M_FULL = numpy.zeros((81, 42))
        print M_FULL[self.r1:self.r2+1, self.t1:self.t2+1].shape
        print col_vec.shape
        #A = mean[0, :].copy().reshape(1, self.t_span)
        #M_FULL = numpy.concatenate((mean, A))
        M_FULL[self.r1:self.r2+1, self.t1:self.t2+1] = numpy.reshape(col_vec, (self.r_span, self.t_span))#M[:, :]
        #print "vzdalensot %f " % dist

        #nastaveni flagu pro vykreslovani
        logar_scale_flag = True#True   #chceme logaritmickou stupnici
        area_calc        = False  #nechceme pocitat plochu

        receptors = []
        if (receptor_flag == True):
            receptors = [self.X_meas, self.Y_meas]
        names = []
        if (names_flag == True):
            names = self.names_meas
        plotters_42.polar_rose_centre_plotter(radius, self.parallels_FULL, self.theta_plot, M_FULL, isoN, tit, self.parallels2_FULL, self.theta2_plot, logar_scale_flag, area_calc, receptors, names, saveFigFlag, fileName, showFlag, ownLevelsFlag, levels)

"""
    def load_prior(self, path):
        "nacte ze souboru prior mean a covariance"
        #nacteni z DICTIONARY, kam sem MEAN a COV nagenerovanou z 5k ruzic ulozil
        #vycucnu si jen podmnozinu bodu polarni site co zrovna potrebuju, abych se s tim netahal
        dict = {}
        scipy.io.loadmat(path, dict)
        x0 = dict["mean"]
        P0 = dict["covariance"]

        #ted provedu vyber pomnoziny bodu polarni site co chci
        self.x0 = x0[self.r1:self.r2+1, self.t1:self.t2+1]
        print "(Sub)mean dimension [%d, %d]" % (self.x0.shape[0], self.x0.shape[1])

        self.P0 = self.cov_fact.select_subcovariance(P0, self.r1, self.t1, self.r2, self.t2)
        print "(Sub)covariance dimension [%d, %d]" % (self.P0.shape[0], self.P0.shape[1])



    def load_background_covariance(self, path):
        "nacte prior covarinaci pro cas t=0 JEN PRO POTREBY PREULOZENI DO FORMATU *.mat !!!!"
        print "Reading PRIOR COVARIANCE data from %s" % path
        handle = open(path, "r")
        s = handle.readlines()
        print "Loaded %d lines " % (len(s))
        print self.P0.shape
        for i in range(0, len(s)):
            line = re.sub('^[ ]+', '', s[i])
            x = map(float, re.split('[\r\n\t ]+', line[0:len(line)-2]))
            self.P0[i][0:2800] = x[0:2800]
        print "Loading of PRIOR COVARIANCE finished"
        print self.P0.shape

    def load_background_mean(self, path):
        "nacte prior mean pro cas t=0 JEN PRO POTREBY PREULOZENI DO FORMATU *.mat !!!!"
        print "Reading PRIOR MEAN data from %s" % path
        handle = open(path, "r")
        s = handle.readlines()
        print "Loaded %d lines " % (len(s))
        print self.x0.shape
        for i in range(0, len(s)):
            line = re.sub('^[ ]+', '', s[i])
            x = map(float, re.split('[\r\n\t ]+', line[0:len(line)-2]))
            self.x0[i][0:35] = x[0:35]
        print "Loading of PRIOR MEAN finished"
        print self.x0.shape
"""



