import LatLongUTMconversion
import rose_class_v2


def create_KML_file(lat, lon, imgpath, kmlpath):
    """
    creates KML overlay for google maps
    """
    n, s, e, w = get_coordinates_of_overlay(lat, lon)
    
    s = """<?xml version="1.0" encoding="UTF-8"?>
           <kml xmlns="http://www.opengis.net/kml/2.2">
           <GroundOverlay>
           <name>davka</name>
          <Icon>
            <href>%s</href>
          </Icon>
          <altitude>0.0</altitude>
          <altitudeMode>clampToGround</altitudeMode>
          <LatLonBox>
            <north>%f</north>
            <south>%f</south>
            <east>%f</east>
            <west>%f</west>
            <rotation>0.0</rotation>
          </LatLonBox>
        </GroundOverlay>
        </kml>    
    """ % (imgpath, n, s, e, w)
    
    f = open(kmlpath, "w+")
    f.write(s)
    f.close()
    
    
def get_coordinates_of_overlay(lat, lon, radius=100):
    """
    -lat, lon - coordinates of the centre of the reelase
    -radius in km
    -calculates soordinates of edges of overlay according to radius and coordinates of the centre
    """
    ReferenceEllipsoid = 23 #WGS-84
    #UTM coordinates of the source
    
    (zone, c_easting, c_northing) = LatLongUTMconversion.LLtoUTM(ReferenceEllipsoid, lat, lon)
    
    x0 = c_easting - radius * 1000. #* math.sqrt(2)
    y0 = c_northing + radius * 1000. #* math.sqrt(2)
    
    x1 = c_easting + radius * 1000. #* math.sqrt(2)
    y1 = c_northing - radius * 1000. #* math.sqrt(2)
    
    n, w = LatLongUTMconversion.UTMtoLL(23, y0, x0, zone)
    s, e = LatLongUTMconversion.UTMtoLL(23, y1, x1, zone)
    
    return n, s, e, w


def plot_rose_and_save(data, savepath):
    """
    plots polar rose and saves it on given path
    data of shape 80x42, saves svg with the figure to overlays directory
    """
    
    
    
    r1 = 0
    r2 = 79
    t1 = 0
    t2 = 41
    radius = 100
    orders = 6
    rose = rose_class_v2.Rose_class_v2(r1, r2, t1, t2)
    print "google tools",savepath
    rose.vizualize_col_vec(data, radius, False, False, True, savepath, False, False, None, None, orders)

    
    
    
