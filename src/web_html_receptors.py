# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import db_tools_source_terms as dtst
import db_tools_receptors as dtr
import tools
from mako.lookup import TemplateLookup
import config as cf
import numpy
import types

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
status_labels = ["border-color: green; border-width: 2px;" ,  "border-color: red; border-width: 2px;"]
##########################################################################################################################
### TASK BODY
##########################################################################################################################




###LOAD METEO DATA
def get_saved_receptors_table(username):
    """
    returns HTML table with receptors data stored in database for a particular user identified with username
    """
    receptors = dtr.get_list_of_receptors(username)
    
    ret = """
    <p>
    <div style="overflow: scroll;">
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th valign="middle">Description</th>
            <th>Modified</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>        
    """
    index = 1
    length_limit = 300
    for receptor in receptors:
        dsc = receptor["description"]
        if (len(dsc) > length_limit):
            dsc = dsc[:length_limit] + "..."
            
        modif = receptor["modified"]
        _id = receptor["_id"]
        action = get_load_receptors_actions(_id)
        ret += """
        <tr>
        <td>%d</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        </tr>
        """ % (index, dsc, tools.convert_epoch_to_time(modif), action)
        index += 1
   
    ret += """
    </tbody>
    </table>
    </div>
    """    
      
    return ret


def get_load_receptors_actions(_id):
    """
    returns HTML of load meteo action button
    """
    ret = """
    <button class="btn btn-primary btn-small dropdown-toggle" data-toggle="dropdown" onclick="$('#load_receptors_modal').modal('hide'); load_receptors('%s');">Load</span></button>
    <button class="btn btn-danger btn-small dropdown-toggle" data-toggle="dropdown" onclick="$('#load_receptors_modal').modal('hide'); delete_receptors('%s');">Delete</span></button>
    """ % (_id, _id)
    
    return ret