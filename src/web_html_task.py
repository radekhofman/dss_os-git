# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools

from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])

##########################################################################################################################
### TASK BODY
##########################################################################################################################

def task_body():
    """
    Returns content of the 'New Task' page.
    """
    return lookup.get_template("new_task.html").render()

