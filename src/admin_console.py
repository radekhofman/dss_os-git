import db_tools as dt
import admin_tools
import db_tools_workers as dtw
import sys

if __name__ == "__main__":
    panda = 0
    while (panda == 0):
        print "Welcome to admin console!\n"
        print "available actions:"
        print "1: add user"
        print "2: clear mongodb"
        print "3: print users"
        print "4: print tasks"
        print "5: drop task collection"
        print "6: print db_service" 
        print "7: print db_workers" 
        print "8: delete db_workers"
        print "9: delete worker" 
        print "99: exit"         
        
        a = raw_input("choose action:")
        
        if (a == "1"):
            "add user"
            
            print "\nAdd user: \n"
            username= raw_input("Username: ") 
            password= raw_input("Password: ") 
            email= raw_input("E-mail: ")
            user_type = raw_input("User type (user, admin): "), 
            user_group = raw_input("User group: "),
            if (user_group == ""):
                user_group = None
            first_name = raw_input("First name: ") 
            middle_name = raw_input("Middle names: ") 
            surname =  raw_input("Last name: ") 
            institute =  raw_input("Institute: ") 
            street1 =  raw_input("Street 1: ") 
            street2 =  raw_input("Street 2: ") 
            city =  raw_input("City: ") 
            zip =  raw_input("Zip: ") 
            state = raw_input("State: ") 
            country =  raw_input("Country: ")                       
            #default group is user     
               
            dt.insert_user(username=username, 
                   password=password, 
                   email=email,
                   user_type = user_type,
                   first_name = first_name,
                   middle_name = middle_name,
                   surname =  surname,
                   institute = institute,
                   street1 = street1,
                   street2 = street2,
                   city = city,
                   zip = zip,
                   state = state,
                   country =  country,                      
                   ) #default group is user
            
            
        if (a == "2"):
            print "\nClear MongoDB: \n"
            b = raw_input("Are you sure? [Y/N]: ")
            if (b == "Y"):
                dt.clear_mongo()    
                print "MongoDB empty!"
                
        if (a == "3"):
            admin_tools.print_users()  
                
        if (a == "4"):
            print "Available users:"
            admin_tools.print_users()
            username = raw_input("Enter username: ")
            admin_tools.print_tasks(username)              
                
        if (a == "5"):
            print "\nDrop task collection: \n"
            b = raw_input("Are you sure? [Y/N]: ")
            if (b == "Y"):
                admin_tools.drop_tasks()    
                print "MongoDB Tasks empty!"   
                admin_tools.print_tasks()         

        if (a == "6"):
            admin_tools.print_services()    
            
        if (a == "7"):
            admin_tools.print_workers()                

        if (a == "8"):
            print "\nClear Workers?: \n"
            b = raw_input("Are you sure? [Y/N]: ")
            if (b == "Y"):
                dt.clear_workers()  
                print "Workers DB empty!"
  
        if (a == "9"):
            workers = dtw.get_list_of_workers()
            for worker in workers:
                print worker["worker_username"]
            wun = raw_input("name of worker to delete: ")
            dtw.delete_worker(wun)   
                       
        if (a == "99"):
            panda = 1                 
                    