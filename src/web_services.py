# -*- encoding: UTF-8 -*-
import cPickle
import db_tools as dt
import db_tools_workers as dtw
import simplejson as sj
import cherrypy
import config as cf
import copy
import auth

#standard config of all exposed classes in my app
cp_conf_common = cf.cp_conf_common

class WebServices:
    """
    class impleenting web services of the app
    - prividing of tasks to workers and harvesting workesrs results into DB

    """
    cp_conf = copy.deepcopy(cp_conf_common)
    #cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf

    ###MAPPING OF DB TOOLS TO WEB SERVICES PROVIDED BY THE SERVER
    @cherrypy.expose
    def get_task(self, username, md5_pass, worker_dict):
        """
        connector for interaction of workers with the server - fetches new tasks
          
         d = {"username": kwargs["username"], #author of the task
         "usergroup_of_task_owner": kwargs["usergroup_of_task_owner"],
         "description": kwargs["description"], #task description
         "task_id": kwargs["task_id"],
         "task_type": kwargs["task_type"], #task type, 0 - dispersion HARP, 1 - .....
         "task_input": kwargs["task_input"],
         "task_output": kwargs["task_output"],
         "task_priority": 0,
         "created": time.time(), #we insert epoch - it can be converted into anything time.strftime("%d %b %Y %H:%M:%S UTC", time.gmtime()),
         "task_status": 0, #task status: 0 - free - queeing, not solved, 1 - under solution, 2 - solved and closed, 3 - error occured

         #data related to task computation
         "start_of_computation": None, #time when the task computation started
         "end_of_computation": None, #time of computation finish
         "computation_time": None, #time spend on task computation
         
         #data from worker
         "worker_data": None #whole worker dictionarye worker
         "task_log": [], #array of lines
         "task_percentage": 0, #float 0. - 100.
         }        
        """
        
        cherrypy.response.headers['Content-Type'] = 'application/json'
        auth_res, user_type =  auth.check_credentials_md5(username, md5_pass)
        if auth_res == None: #OK, no error message
            worker_dict_pckl = str(sj.loads(worker_dict)["worker_dict_pckl"]) #must be string, NOT unicode.. JSON return unicode ???
            worker_dict = cPickle.loads(worker_dict_pckl)
            task = dt.get_free_task(worker_dict) ##worker_info = [name, [aliases], ip, worker_username, worker_id]
            #print task
            return sj.dumps({"task_pckl": cPickle.dumps(task) }) #we return a free task, if present
        else:
            print "Web service returns nothing, authorization error"
            return sj.dumps({"task_pckl": cPickle.dumps(None) }) # we dont return anything
            #todo: modification of error message due to user login error


    @cherrypy.expose
    def update_worker_status(self, username, md5_pass, worker_dict):
        """
        updating status of a worker in workers' DB, if not in DB,  update_worker_status(worker_dict) inserts it              
        """
        cherrypy.response.headers['Content-Type'] = 'application/json'
        auth_res, user_type =  auth.check_credentials_md5(username, md5_pass)
        
        if (auth_res == None): #OK, no error message
            worker_dict_pckl = str(sj.loads(worker_dict)["worker_dict_pckl"]) #must be string, NOT unicode.. JSON return unicode ???
            worker_d = cPickle.loads(worker_dict_pckl)
            dtw.update_worker_status(worker_d)    
        else:
            print "Error worker status update - authorization error" 
        
        
    @cherrypy.expose
    def update_task_status(self, ): 
        """
        updating status of a task in tasks' DB
        updates percentage and/or task's log
        """
        pass


    @cherrypy.expose
    def insert_results(self, username, md5_pass, task_json):
        #updating of the done task and import of results from worker
        cherrypy.response.headers['Content-Type'] = 'application/json'
        auth_res, user_type =  auth.check_credentials_md5(username, md5_pass)
  
        if auth_res == None: #OK, no error message
            task_json = sj.loads(task_json)
            task_pckl = str(task_json["task_pckl"]) #must be string, NOT unicode.. JSON returns unicode ???
            task = cPickle.loads(task_pckl)
            
            try:
                res = dt.update_finished_task(task) #res contains 1 if succeeded, must be polished...
                #return res #returning message on the task update...
                return sj.dumps({"res": 1})
            
            except Exception, err:
                print "ERROR:", err
                return None
                
        else:
            print "\n\n\n\n\n\5555 hoven"
            return None # we dont return anything
            #todo: modification of error message due to user login error      
        


    #needed: method for updating worker's log which is displayed to user with notifications on progress of the task





