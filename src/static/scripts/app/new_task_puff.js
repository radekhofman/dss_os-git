// wait for the DOM to be loaded

var map1;

$(document).ready(function() {
	brython();	
	//creates some sample meteotable
	update_meteo_table(); 
	//brython init
});

$(window).load(function() {
	$('#myModalGMap').on('shown', function(e) {
		show_map();
		google.maps.event.trigger(map_source, 'resize');
	});

});

function wrap_error_msg(msg) {
	//creates red div around error message
	var error_msg = "<div class='alert alert-error'><center><h4>" + msg + "</h4></center></div>"
	return error_msg
}

function validate_source_term() {
	//validates source term - time step, relase length and puff sampling step
	$("#st_error_msg").html("");
	//clr the previous error message

	var validation_result = true;

	//gathering of data and conversion to minutes
	var simulation_length = parseFloat($('#simulation_length').val()) * 60.;
	//oring in hours get current release length
	var number_of_puffs = parseFloat($('#nucl_puff_form [name=duration_of_release]').val());
	//orig. in hours
	var inner_time_step = parseFloat($('#nucl_puff_form [name=inner_time_step]').val());
	//min
	var puff_sampling_step = parseFloat($('#nucl_puff_form [name=puff_sampling_step]').val());
	//min

	//alert(simulation_length+" dr"+duration_of_release+" is "+inner_time_step+" pss"+puff_sampling_step)

	var error_msg = "";

	/*
	if (simulation_length <= duration_of_release) {
		validation_result = false;
		error_msg += "Duration of release must not be higher than simulation length.<br>";
	}
	*/
	var duration_of_release = number_of_puffs * puff_sampling_step; //JUST HERE!!! / puff_sampling_step;
	if (number_of_puffs !== parseInt(number_of_puffs)) {
		validation_result = false;
		error_msg += "Puff sampling step must be commensurate to duration of release<br>";
	}
	var number_of_substeps = puff_sampling_step / inner_time_step;
	if (number_of_substeps !== parseInt(number_of_substeps)) {
		validation_result = false;
		error_msg += "Integration time step must be commensurate to puff sampling step.<br>";
	}
	if (inner_time_step > puff_sampling_step) {
		validation_result = false;
		error_msg += "Integration time step must not be higher than puff sampling step.<br>";
	}
	if (puff_sampling_step > duration_of_release) {
		validation_result = false;
		error_msg += "Puff sampling step must not be higher than duration of release.<br>";
	}

	//successful validation
	if (validation_result) {

		var log_data = [[]];
		var nuclides = ["AR41"];
		preset = 1.0E+10//tentative value

		for (var i = 0; i < number_of_puffs; i++) {
			log_data[0].push(log10(preset));
		}

		log("gacrt");
		draw_chart(log_data[0], puff_sampling_step);
		//JS
		log("tabel")
		create_st_table(nuclides, log_data);
		//Brython, we have argon only

		testSubmitButton();

		$('#myModalNuclide').modal('hide');
		//close modal dialog
	} else {
		//not valid
		$("#st_error_msg").html(wrap_error_msg(error_msg));
	}
}

var chart;
// = new Highcharts.Chart(options);

function draw_chart(data, puff_sampling_step) {

	var options = {

		chart : {
			renderTo : 'source_chart',
			animation : false
			//zoomType: 'x'
		},

		title : {
			text : 'Source term'
		},

		xAxis : {
			//categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},

		yAxis : {
			min : 0.0,
			title : {

				text : "Puffs' activities [Bq]"
			},
			labels : {

				formatter : function() {
					return '1.0E+' + this.value;
				}
			}
		},

		xAxis : {
			title : {

				text : "Time [min]"
			},
			labels : {

				formatter : function() {
					return puff_sampling_step * this.value;
					//in minutes
				}
			}
		},

		tooltip : {
			formatter : function() {
				return 'Activity in puff <b>' + this.x + '</b> is <b>' + Math.pow(10, this.y).toExponential(2) + 'Bq</b> of Ar-41';
			},
			yDecimals : 2,

		},

		plotOptions : {
			series : {
				cursor : 'ns-resize',
				point : {
					events : {
						/*
						 drag: function(e) {
						 // Returning false stops the drag and drops. Example:

						 //if (e.newY > 300) {
						 //    this.y = 300;
						 //    return false;
						 //}

						 $('#drag').html(
						 'Dragging <b>' + this.series.name + '</b>, <b>' +
						 this.category + '</b> to <b>' +
						 Highcharts.numberFormat(e.newY, 2) + '</b>'
						 );
						 },
						 */
						drop : function() {
							log("drop");
							try {
								var new_data = chart.series[0].data;
								var new_data_y = [];
								for (var i = 0; i < new_data.length; i++) {
									new_data_y.push(new_data[i].y);
								}
								//alert(new_data_y);

								create_st_table(["AR41"], [new_data_y]);
								log("drop OK");
							} catch(err) {
								log("drop error");

							}

						},
						/*
						 function() {
						 $('#drop').html(
						 'In <b>' + this.series.name + '</b>, <b>' +
						 this.category + '</b> was set to <b>' +
						 Highcharts.numberFormat(this.y, 2) + '</b>'
						 );
						 }
						 */
					}
				},
				stickyTracking : false
			},
			column : {
				stacking : 'normal'
			}
		},

		series : [{
			//data: [0, 10, 12, 15, 12, 11, 10, 10, 10, 10, 10, 10],
			//draggableX: true,
			draggableY : true,
			dragMinY : 0.0,
			dragMaxY : 25.0,
			//type: 'column', //column chart
			minPointLength : 2,
			name : "AR41"
		}]

	}//end of otpions
	options.series[0].data = data;
	//options.plotOptions.series.drop = create_st_table(["AR41"], [chart.series[0].data]);
	chart = new Highcharts.Chart(options);

}

//how to use localStorage
//localStorage.setItem("key", "value");
//var value = localStorage.getItem("key");

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function validate_and_update_source_graph() {
	data = validate_source_table(ev)
	if (data) {//we got some data, not false
		chart.series[0].setData(data);
		//update_source_graph(data);
	}
}

function log10(x) {
	return Math.log(x) / Math.log(10.);
}

function x10(x) {
	return Math.pow(10, x);
}

function collect_release_heights() {
	//collects release heights and returns it as a list
	var segments_count = $('#segments_count').val();
	//puffs count
	var rel_heights = [];

	for (var i = 0; i < segments_count; i++) {
		rel_heights.push(parseFloat($('#v_' + str(i)).val()));
	}

	log('found following release heights:');
	log(rel_heights);
	return rel_heights;
}

function collect_nuclide_activities() {
	//collects nuclides activities and returns it as a list
	var segments_count = $('#segments_count').val();
	var nuclides_count = $('#nuclides_count').val();

	activities = []

	//up to now we have just one nuclide, so we do not need 2D array
	for (var j = 0; j < segments_count; j++) {
		activities.push(parseFloat($('#n_AR41_' + str(j)).val()));
	}
	log('found following release activities:');
	log(activities);
	return activities;
}

function validate_source_table(ev) {
	var msg = "";
	//var inputs = [];
	var data = [[]];
	var log_data = [[]];
	$('div#source_table input[type=text]').each(function() {//validation of all fields every time
		//inputs.push($(this).attr('id'));
		val = $(this).attr('value');
		id = $(this).attr('id').split("_");

		if (isNumber(val)) {
			if (val > 0) {
				$(this).attr('style', "width: 65px");
				log(id);
				if (id[0] == "n")//yes it is a nuclide, not speed etc.
				{
					data[0].push(parseFloat(val));
					log_data[0].push(log10(parseFloat(val)));
				}

			} else {
				var msg_head = "";
				(id[0] == "n") ? msg_head += "Nuclide " + mnl(id[1]) + ", puff " + str(parseInt(id[2]) + 1) : msg_head += "Release height" + ", puff " + str(parseInt(id[1]) + 1);
				msg += msg_head + ": Value must be pozitive!<br>";
				$(this).attr('style', "border-color: red; border-width: 2px; width: 65px");
			}
		} else {
			var msg_head = "";
			(id[0] == "n") ? msg_head += "Nuclide " + mnl(id[1]) + ", puff " + str(parseInt(id[2]) + 1) : msg_head += "Release height" + ", puff " + str(parseInt(id[1]) + 1);
			msg += msg_head + ": Not valid floating point value!<br>";
			$(this).attr('style', "border-color: red; border-width: 2px; width: 65px");
		}

	});

	//finally, we print possible error messages
	if (msg !== "") {
		$('#st_error_msg_main').html(wrap_error_msg(msg));
		return false;
	} else {
		$('#st_error_msg_main').html("");
		//update of table?
		log("returning..")
		log(log_data);
		return log_data;
		//data for plotting are returned
	}
}

function validate_and_update_source_graph(ev) {
	//updates source graph on change in inputs
	var log_data = validate_source_table(ev);
	if (log_data) {//inputs are already validated
		log("updating data");
		chart.series[0].setData(log_data[0]);
		log("graph updated");
	}
}

function testSubmitButton() {
	//tests whether there is a puff configuration, if so, the submit button for the whole task is enabled, othervise not
	log("Validating configuration...");
	if ($('#nuclides_count').val() != "0" && $('#meteo_hours_count').val() != "0") {
		//submit button
		log("Configration VALID!");
		$('#submit_button').html("<button type='button' class='btn btn-success btn-large' onclick='submit_puff_task();'>Submit task</button>");
	} else {
		//disabled button
		log("Configuration NOT VALID!");
		$('#submit_button').html("<button type='button' class='btn btn-success btn-large disabled'>Submit task</button>");
	}
}

function submit_puff_task() {
	//collects both source term and meteo and also basic parameters and submits th task to the server
	var meteo = collect_meteo_data_from_form();
	var meteo_hours_count = $('#meteo_hours_count').val();
	var activities = collect_nuclide_activities();
	//AR41 only, so it is a 1D list
	var segments_count = $('#segments_count').val();
	var rel_heights = collect_release_heights();
	//release heights
	var task_desc = $('#task_desc').val();//task description
	var lat = $('#lat').val();
	var lon = $('#lon').val();

	var output_grid_step = $('#output_grid_step').val();
	var output_domain_size_x = $('#output_domain_size_x').val();
	var output_domain_size_y = $('#output_domain_size_y').val();

	var simulation_length = $('#simulation_length').val();
	var number_of_puffs = parseFloat($('#nucl_puff_form [name=duration_of_release]').val());
	var puff_sampling_step = parseFloat($('#puff_sampling_step').val());
	var duration_of_release = number_of_puffs * puff_sampling_step / 60.; //JUST//$('#duration_of_release').val()
	var inner_time_step = $('#inner_time_step').val();

	var postdata = {
		"meteo" : meteo,
		"meteo_hours_count" : meteo_hours_count,
		"activities" : activities,
		"segments_count" : segments_count,
		"rel_heights" : rel_heights,
		"description" : task_desc,
		"lat" : lat,
		"lon" : lon,
		"simulation_length" : simulation_length,
		"output_grid_step" : output_grid_step,
		"output_size_y" : output_domain_size_y,
		"output_size_x" : output_domain_size_x,
		"duration_of_release" : duration_of_release,
		"inner_time_step" : inner_time_step,
		"puff_sampling_step" : puff_sampling_step
	};

	$.ajax({
		url : '/task/insert_puff_task',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			//after successful taks insertion we do redirect to task list
			window.location = "/results";
		},
	});

	
}

function load_source_term(_id) {
	//loads selected source term and places it into table on tab2
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/load_source_term',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {

			var log_data = [[]];
			var sgments_count = data["segments_count"];
			var activities = data["activities"];
			var nuclides = ['AR41'];
			var puff_sampling_step = data["puff_sampling_step"];

			for (var i = 0; i < sgments_count; i++) {
				log_data[0].push(log10(activities[i]));
			}

			//draw chart
			draw_chart(log_data[0], puff_sampling_step);
			//draw table
			create_st_table(nuclides, log_data);

		},
	});

}

function save_source_term() {
	//collects data from meteo table and saves to DB
	var st_desc = $('#st_desc').val();
	var activities = collect_nuclide_activities();
	//AR41 only, so it is a 1D list
	var segments_count = $('#segments_count').val();
	var rel_heights = collect_release_heights();
	//release heights

	var simulation_length = $('#simulation_length').val()
	var duration_of_release = $('#duration_of_release').val()
	var inner_time_step = $('#inner_time_step').val()
	var puff_sampling_step = $('#puff_sampling_step').val()

	var postdata = {
		"segments_count" : segments_count,
		"activities" : activities,
		"rel_heights" : rel_heights,
		"description" : st_desc,
		"simulation_length" : simulation_length,
		"duration_of_release" : duration_of_release,
		"inner_time_step" : inner_time_step,
		"puff_sampling_step" : puff_sampling_step
	};

	$.ajax({
		url : '/task/save_source_term',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			var status = data["status"];

			if (status == 0)//OK, source term successfully saved, we should update saved source terms table
			{
				//update_load_st_modal();
				$("#modal_load_st_div").html(data["st_table"]);
			}

			$("#msg").html(data["msg"]);

		},
	});
}

////////// LOAD & SAVE meteo
//on save button in Comfigure meteo dialog
function update_meteo_table() {

	var hours_count =  12; //JUST FOR NOW$('#meteo_modal_hours_count').val()
	var postdata = {
		"hours_count" : hours_count
	};

	$.ajax({
		url : '/task/update_meteo_table',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			$("#meteo_table").html(data["meteo_table"]);
			testSubmitButton();
		},
	})

}

function load_meteo(_id) {
	//loads selected source term and places it into table on tab2
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/load_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			//log(data["meteo_table"]);
			$("#meteo_table").html(data["meteo_table"]);
			testSubmitButton();
		},
	});

}

function delete_meteo(_id) {
	//deletes meteo of a given ID from meteo collection
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/delete_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			update_load_meteo_modal();
			$("#msg").html(data["msg"]);
		},
	});

}

function collect_meteo_data_from_form() {
	//collects data from meteo table and returns it as a list
	var meteo_hours_count = $('#meteo_hours_count').val();
	var meteo = [];
	//will be 2D array after reshape
	var labels = ["meteo_wd_", "meteo_ws_", "meteo_stab_", "meteo_prec_"];


	for (var i = 1; i <= meteo_hours_count; i++) {
		for (var j = 0; j < 4; j++)//we have 4 columns
		{
			meteo.push($("#disp_task [name=" + labels[j] + i + "]").val());
		}
	}

	return meteo;

}

function save_meteo() {
	//collects data from meteo table and saves to DB
	var meteo_hours_count = $('#meteo_hours_count').val();
	var meteo = collect_meteo_data_from_form();
	var description = $("#save_meteo_form [name=meteo_desc]").val();

	var simulation_length = $('#simulation_length').val()
	var duration_of_release = $('#duration_of_release').val()
	var inner_time_step = $('#inner_time_step').val()
	var puff_sampling_step = $('#puff_sampling_step').val()

	var postdata = {
		"meteo_hours_count" : meteo_hours_count,
		"meteo" : meteo,
		"description" : description,
		"simulation_length" : simulation_length,
		"duration_of_release" : duration_of_release,
		"inner_time_step" : inner_time_step,
		"puff_sampling_step" : puff_sampling_step
	};

	$.ajax({
		url : '/task/save_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			var status = data["status"];
			if (status == 0)//OK, source term successfully saved, we should update saved source terms table
			{
				update_load_meteo_modal();
			}
			$('#meteo_table').html(data["meteo_table"]);
			$("#msg").html(data["msg"]);
			testSubmitButton();
		},
	});
}

function update_load_meteo_modal() {
	//updates load st table otherwise newly saved task will not be shown
	$.ajax({
		url : '/task/update_meteo_load_form',
		type : 'post',
		//data: postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		//dataType: 'json', //we expect JSON
		success : function(data) {
			$("#modal_load_meteo_div").html(data);
		},
	});
}

/////////LOAD & SAVE source term

function delete_source_term(_id) {
	//deletes source term of a given ID from source term collection
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/delete_source_term',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			//update_load_st_modal();
			$("#modal_load_st_div").html(data["st_table"]);
			$("#msg").html(data["msg"]);
		},
	});
}

//map with location of the release source
var map_source;
function show_map() {
	var myLatlng = new google.maps.LatLng(document.getElementById('lat').value, document.getElementById('lon').value);
	var myOptions = {
		zoom : 10,
		center : myLatlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map_source = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	marker = new google.maps.Marker({
		map : map_source,
		position : myLatlng,
		title : 'source of release'
	});

	marker['infowin'] = new google.maps.InfoWindow({
		content : '<div>This is a source in ' + myLatlng + '</div>'
	});

	google.maps.event.addListener(marker, 'click', function() {
		this['infowin'].open(map_source, this);
	});

	google.maps.event.trigger(map_source, 'resize');

	return false;
}
