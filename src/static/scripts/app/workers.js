
  //refreshing of task table 
  $(document).ready(function () {
      get_workers_table();
  });

  function get_workers_table() {
      var first = $("#hidden_first").val();
      var size  = $("#hidden_size").val();
      
      var postdata = {"first": first,
                      "size": size};
	  

	  $.ajax( {
	    url: '/workers/generate_workers_table',
	    type: 'post',
	    data: postdata, //postdata,
	    headers: { 
	    	"cache-control": "no-cache"  //for object property name, use quoted notation shown in second
	    },
	    //dataType: 'json',
	    success: function( data )
	    {
	        $("#workers_table").html(data);
	    },
      } );
	                      
      //$.post('/results/generate_task_table', postdata, 
      //   function(data) {  //callback function
      //             $("#task_table").html(data);           
      //   });                                  
     
      setTimeout(function(){get_workers_table()}, 5000); //setTimeout(function(){get_workers_table(first, size)}, 5000);             
  } 
   

              
