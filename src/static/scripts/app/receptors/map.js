var map = {};
//google map canvas object will be here
map.canvas = null;
//google map canvas options will be here
map.options = null;
//data model - groups and meteres will be stored here
map.data = [];
//center of the map will be stored here
map.mapCenter = null;
//all google map markers actually displayed on the will be here
map.markers = null;
//google map info window
map.infoWindow = null;

map.selectedGroupId = null;

map.lastMeterId = null;

map.editable_markers = new Boolean(); //true/false

map.init = function(editable_markers) {


	//will be user allowd to remove markers from model window with marker info click?
	map.editable_markers = editable_markers //new Boolean(); true/false
	
	//var myLatlng = new google.maps.LatLng(document.getElementById('lat').value, document.getElementById('lon').value);
	var myLatlng = new google.maps.LatLng(49.1817, 14.3754);
	 
	map.mapCenter = myLatlng; //new google.maps.LatLng(49.180778, 14.376168);

	map.options = {
		zoom : 12,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		center : map.mapCenter
	};

	map.canvas = new google.maps.Map(document.getElementById('map-canvas'), map.options);
	
	/*
	map.data = [{
		'group' : 'Group 1',
		'meters' : [{'id' : '0', 'name' : 'Meter 1', 'value' : '10000',	'timestamp' : 123, 'lat' : 49.205654, 'lng' : 14.329262	}, 
					{'id' : '1', 'name' : 'Meter 2', 'value' : '20000',	'timestamp' : 456, 'lat' : 49.192534, 'lng' : 14.305315 }, 
					{'id' : '2', 'name' : 'Meter 3', 'value' : '30000',	'timestamp' : 789, 'lat' : 49.169303, 'lng' : 14.311152 },
					{'id' : '3', 'name' : 'Meter 4', 'value' : '40000',	'timestamp' : 789, 'lat' : 49.159303, 'lng' : 14.301152 },
					{'id' : '4', 'name' : 'Meter 5', 'value' : '50000',	'timestamp' : 789, 'lat' : 49.189303, 'lng' : 14.311152 },
					{'id' : '5', 'name' : 'Meter 6', 'value' : '60000',	'timestamp' : 789, 'lat' : 49.219303, 'lng' : 14.301152 }]
		}, {
		'group' : 'Group 2',
		'meters' : [{'id' : '6', 'name' : 'Meter 1', 'value' : '10000',	'timestamp' : 123, 'lat' : 49.215654, 'lng' : 14.329262 }, 
					{'id' : '7', 'name' : 'Meter 2', 'value' : '20000',	'timestamp' : 456, 'lat' : 49.172534, 'lng' : 14.305315 }]
		}];
	*/
	map.markers = [];
	
	if(map.data) {
		map.lastMeterId = map.getLastMeterId();
		map.lastMeterId++;
	} else {
		map.lastMeterId = 0;
	}
	
	map.infoWindow = new google.maps.InfoWindow();
	
	groupTable.init('group');
	meterTable.init('meter');	
		
	map.dropMarkers();
	
	return false;
}

//clear all markers from the map
map.clearMarkers = function() {
	if (map.markers.length > 0) {
		for (var i=0; i < map.markers.length; i++) {
			map.markers[i].setMap(null);
		}
		map.markers = [];
	}

	return false;
}

//drop meters markers from map.data object on the map
map.dropMarkers = function() {
	//remove all markers which are already displayed on the map canvas
	map.clearMarkers();
	for (var j=0; j < map.data[0].meters.length; j++) {
			  		var groupId = 0;
					var meter = map.data[0].meters[j];
					var latlng = new google.maps.LatLng(meter.lat, meter.lng);
					map.addMarker(latlng, meter, groupId);
					console.log('pridavam neco');
				}	
	/*
	//get groups which are checked
	var checkedGroups = groupTable.getCheckedGroupsIdsInTable();
	//continue only if some group is checked
	if(checkedGroups.length > 0){
		//itterate through checked groups
		for (var i=0; i < checkedGroups.length; i++) {
		  if(map.data[checkedGroups[i]].meters) {		  			 
			  //continue only if group contains some meters
			  if(map.data[checkedGroups[i]].meters.length > 0){
			  	//itterate through meteres in group
			  	for (var j=0; j < map.data[checkedGroups[i]].meters.length; j++) {
			  		var groupId = checkedGroups[i];
					var meter = map.data[checkedGroups[i]].meters[j];
					var latlng = new google.maps.LatLng(meter.lat, meter.lng);
					map.addMarker(latlng, meter, groupId);
				}
			  }
		  }
		}
	
	}
	*/
	console.log('markers dropped');
	return false;
}

//loads map.data
map.loadData = function(data) {
    if (typeof data === 'object') {
        map.data = data;
        map.init();
        console.log('map.data loaded');
    }
    return false;
}

//add marker to the map - takes a LatLng object and meter
map.addMarker = function(latlng, meter, groupId) {
	var marker;
	if (latlng != null && latlng != NaN && latlng != undefined) {
		//if more than 15 groups colors reset
		var color = rainbow(15, groupId);
		marker = new StyledMarker({			
			styleIcon : new StyledIcon(StyledIconTypes.MARKER,{color: color}),
			position : latlng,
			draggable : false,
			animation : google.maps.Animation.DROP,
			map : map.canvas
		
		});
	}

	if (meter != null && meter != NaN && meter != undefined) {
		var timestamp = new Date(meter.timestamp);
		
		var mapModalContent = '';
		
		if (map.editable_markers) {
			mapModalContent = '<h4>Update meter</h4>'
							+ '<p><label>Name </label><input type="text" value="' + meter.name + '" id="info-window-meter-name"></p>' 
							//+ '<p><label>Value </label><input type="text" value="' + meter.value + '" id="info-window-meter-value"></p>'
							+ '<p><button class="btn btn btn-danger" onClick="map.removeMeter(' + groupId + ',' + meter.id + '); return false">Delete</button> ' 
							+ '<button class="btn btn-primary" onClick="map.updateMeter(' + groupId + ',' + meter.id + '); return false">Save</button>' 
							+ '</p>';
		}
		else {
			mapModalContent = '<h4>Receptor info</h4>'
							+ '<p><label>Name: </label>' + meter.name + '</p>'; 
		}

		(function(marker, meter) {
			// Attaching a click event to the current marker
			google.maps.event.addListener(marker, 'click', function(e) {
				map.infoWindow.setContent(mapModalContent);
				map.infoWindow.open(map.canvas, marker);
			});
			//save position of marker after dragndrop action
			google.maps.event.addListener(marker, 'dragend', function(e) {
				if (map.editable_markers) {
					mapModalContent = '<h4>Update meter</h4>'
									+ '<p><label>Name </label><input type="text" value="' + meter.name + '" id="info-window-meter-name"></p>' 
									//+ '<p><label>Value </label><input type="text" value="' + meter.value + '" id="info-window-meter-value"></p>'
									+ '<p><button class="btn btn btn-danger" onClick="map.removeMeter(' + groupId + ',' + meter.id + '); return false">Delete</button> ' 
									+ '<button class="btn btn-primary" onClick="map.updateMeter(' + groupId + ',' + meter.id + '); return false">Save</button>' 
									+ '</p>';
				}
				else {
					mapModalContent = '<h4>Receptor info</h4>'
									+ '<p><label>Name: </label>' + meter.name + '</p>'; 
				}
				meter.lat = marker.position.lat();
				meter.lng = marker.position.lng();
				map.infoWindow.setContent(mapModalContent);
			});
		})(marker, meter);
	}
	map.markers.push(marker);

	return false;
}

//adds meter to the map.data object
map.addMeter = function(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng) {

			map.data[0].meters.push({
				id : mId,
				name : mName,
				value : mValue,
				timestamp : mTimestamp,
				lat : mLat,
				lng : mLng
			});
			map.lastMeterId++;
			console.log('added meter to map.data: ' + mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);			


	return false;
}

map.updateMeter = function(groupId, meterId) {
	if(map.data[groupId] != undefined && map.data[groupId].meters) {
		$.each(map.data[groupId].meters, function() {
		    if (this.id == meterId) {
		        this.value = 0.0; //$('#info-window-meter-value').val();
		        this.name = $('#info-window-meter-name').val();
		        console.log('meter updated', meterId);
				map.dropMarkers();
				meterTable.loadMeterTable();
		    }
	    });
	}
}


//remove meter by meter.id
map.removeMeter = function(groupId, meterId) {	
	if(meterId >= 0 && groupId >= 0){
		for(var i=0; i < map.data[groupId].meters.length; i++) {
		  	var meter = map.data[groupId].meters[i];
			if(meter.id == meterId) {
				map.data[groupId].meters.splice(i, 1);
				console.log('meter deleted', meterId);
				map.dropMarkers();
				meterTable.loadMeterTable();
			}
		};		
	}
}

// triggers info window of the marker by given number
map.popMarker = function(num) {
	if (num >= 0 && num != null && num != NaN && num != undefined) {
		var marker = map.markers[num];
		google.maps.event.trigger(marker, 'click');
	}

	return false;
}


//adds meter group to map.data object and empty list of meters

map.addMeterGroup = function(name) {
	if (name != '' && $.trim(name).length > 0) {
		//trim group name
		name = $.trim(name);
		//add group to object
		map.data.push({
			'group' : name,
			'meters' : []
		});		
		if(map.data.length > 0){
			//add group to select box
			$('.meter-groups-modal').append('<option groupId='+(map.data.length-1)+'>' + name + '</li>');
			//add group to group table
			groupTable.addGroupToTable(map.data.length-1);
		}
		
		console.log('added group to map.data: ' + name);
	}
	return false;
}

//returns list of meters by groupId
//if not found empty list returned
map.getMetersInGroup = function(groupId) {
	var meters = [];
	if(groupId >= 0){
		var group = map.data[groupId];
		if(group && group.meters){
			if(group.meters.length > 0){
				for(var i in group.meters){
					meters.push(group.meters[i]);
				}
			}			
		}
	}
	
	return meters;
}

//returns list of group names
map.getMeterGroups = function() {
	var groups = [];
	for (var i in map.data) {
		groups.push(map.data[i].group);
	}
	
	console.log('map.getMeterGroups: ' + groups);
	
	return groups;
}

//returns last meter.id from all meters from all groups
map.getLastMeterId = function() {
	var lastMeterId = 0;
	if(map.data && map.data[0].meters.length){
		var lastGroup = 0
		if(map.data[lastGroup] && map.data[lastGroup].meters && map.data[lastGroup].meters.length >=0){
			var lastMeter = map.data[lastGroup].meters.length - 1;
			lastMeterId = map.data[lastGroup].meters[lastMeter].id;
		}
	}
	
	return lastMeterId;
}


//returns count of all meters
map.getMetersCount = function() {
	var metersCount = 0;
	if(map.data && map.data.length >= 0){
		for (var i=0; i < map.data.length; i++) {
		  	if(map.data[i].meters && map.data[i].meters.length >=0){
				for (var j=0; j < map.data[i].meters.length; j++) {
				  	metersCount++;
				}
		  	}
		}
	}
	
	return metersCount;
}

/*
$(document).ready(function() {
	var map1 = Object.create(map);
	map1.init();
	
	$('#add-group').click(function() {
		var meterGroup = $('#meter-group').val();
		map.addMeterGroup(meterGroup);
		$('#meter-group').val('');		
	});
	
	$('#info-window-meter-timestamp').datetimepicker();
});
*/
