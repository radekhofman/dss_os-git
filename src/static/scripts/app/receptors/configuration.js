configuration = {};

configuration.validateSaveConfigurationModal = function() {
	var isValid = true;	
	var configurationName = $("#configuration-name").val();
	
	if(map.data.length == 0) {
		isValid = false;
		$('#save-configuration-validation-result').html('<p>No data to save.</p>');
	}
	
	if(configurationName.length == 0) {
		isValid = false;		
		$('#save-configuration-validation-result').html('<p>Provide configuration name.</p>');
	}
	
	console.log('save configuration validation result', isValid);
	return isValid;
}


/**
 * Registers action to 'Save configuration' form submit.
 */
/*
configuration.registerSaveConfigurationAction = function() {
	$('#save-configuration').click();
}
*/
function save_rec_configuration() {
			var data = JSON.stringify(map.data[0]);
			var description = $.trim($("#configuration-name").val());
			$.ajax({
			  type: 'POST',
			  url: '/tools/save_receptors',
			  data: {
			  	description : description,
			  	data : data }
			}).done(function() {
				$('#save-configuration-validation-result').html('<p>Configuration saved.</p>');
				setTimeout(function(){ 
					$('#save-configuration-modal').modal('hide');
				}, 500 );
				console.log('configuration data saved', data);
				alert('done');
				
			}).fail(function(){
				$('#save-configuration-validation-result').html('<p>Error when saving configuration.</p>');
				setTimeout(function(){ 
					$('#save-configuration-modal').modal('hide');
				}, 500 );				
			});

	}


function load_rec_configuration() {
			var conf_name = "kuku" 
			$.ajax({
			  type: 'POST',
			  url: '/tools/load_receptors',
			  data: {
			  		name : conf_name
			  },
			  dataType: 'json', //we expect JSON
			  success: function( data ) {
			  		$('#load-configuration-modal').modal('hide');
			        alert("taaak ted");   
			        map.data = data["map_data"]
			    },	
			});		
	}


/**
 * Registers action to 'Load configuration' form submit.
 */
/*
configuration.registerLoadConfigurationAction = function() {
	$('#load-configuration').click(function() {
			var conf_name = "kuku" 
			$.ajax({
			  type: 'POST',
			  url: '/services/loadconfiguration',
			  data: {
			  		name : conf_name
			  },
			  dataType: 'json', //we expect JSON
			  success: function( data ) {
			  		$('#load-configuration-modal').modal('hide');
			        alert("taaak ted");   
			        map.data = data["map_data"]
			    },	
			});		
	});
}
*/


