mapModal = {};

mapModal.mapCenter = null;

mapModal.map = null;

mapModal.marker = null;

mapModal.mapOptions = {};

mapModal.init = function() {

	mapModal.mapCenter = new google.maps.LatLng(49.180778, 14.376168);

	mapModal.mapOptions = {
		zoom : 12,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		center : mapModal.mapCenter
	};

	mapModal.map = new google.maps.Map(document.getElementById('map-modal-canvas'), mapModal.mapOptions);

	mapModal.dropMarker();	

	return false;
}

//drop initial marker of the meter on the map
mapModal.dropMarker = function() {

	mapModal.marker = new google.maps.Marker({
		position : mapModal.mapCenter,
		map : mapModal.map,
		draggable : true,
		animation : google.maps.Animation.DROP
	});

	//change position of the marker when clicked in the map
	mapModal.setMarkerPositionOnClick(mapModal.marker);

	return false;
}

mapModal.getMarkerPosition = function(marker) {
	if (marker != null && marker != NaN && marker != undefined) {
		return marker.getPosition();
	} else {
		return false;
	}
}

mapModal.setMarkerPositionOnClick = function(marker) {
	if (marker != null && marker != NaN && marker != undefined) {
		google.maps.event.addListener(mapModal.map, 'click', function(event) {
			var clickPos = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
			mapModal.marker.setPosition(clickPos);
		});
	} else {
		return false;
	}
}

mapModal.getMarkerLat = function(marker) {
	var lat;
	if (marker != null && marker != NaN && marker != undefined) {
		lat = marker.position.lat();
		return lat;
	} else {
		return false;
	}
}

mapModal.getMarkerLng = function(marker) {
	var lng;
	if(marker != null && marker != NaN && marker != undefined) {
		lng = marker.position.lng();
		return lng;
	} else {
		return false;
	}
}

mapModal.setGroupsSelectBox = function() {
	if(map.data.length >= 0){
		for (var i=0; i < map.data.length; i++) {
			$('.meter-groups-modal').append('<option groupId='+ i +'>' + map.data[i].group + '</li>');
		};
	}
}

mapModalForm = {};

mapModalForm.picker = null;

mapModalForm.getInputValue = function(inputId) {
	var value;
	if ($(inputId).val() != '') {
		value = $(inputId).val();
		return value;
	} else {
		$(inputId).css({
			'border-color' : '#b94a48'
		});
		return value;
	}
}

mapModalForm.getSelectedGroup = function() {	
	var groupName = '';	  	
	groupName = $('.meter-groups-modal option:selected').val();
	
	return groupName;
}

//returns id of group selected in select box
mapModalForm.getSelectedGroupId = function() {
	var grouId = '';	  	
	groupId = $('.meter-groups-modal option:selected').attr('groupId');
	
	return groupId;
}

//validate add meter form
mapModalForm.validate = function() {
	var isValid = false;
	if ((mapModalForm.getInputValue('#meter-name') != '') && (mapModalForm.getInputValue('#meter-value') != '')) {
		isValid = true;
	}
	
	return isValid;
}


function save_rec_conf() {
	
		var mId = map.lastMeterId;
		var mName = $("#meter-name").val(); //mapModalForm1.getInputValue('#meter-name');		
		var mLat = mapModal.getMarkerLat(mapModal.marker);
		var mLng = mapModal.getMarkerLng(mapModal.marker);
		var mGroup = 'RMN receptors';
		var mTimestamp = 'N/A';
		var mValue = 0.0;
		map.addMeter(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);
		map.dropMarkers();
		meterTable.loadMeterTable();

		//map.addMeter(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);
		/*
		var mapModalForm1 = Object.create(mapModalForm);
		alert('panda2');
		if (mapModalForm1.validate() == true) {
			var mGroup = mapModalForm1.getSelectedGroup();
			var mId = map.lastMeterId;
			var mName = mapModalForm1.getInputValue('#meter-name');
			var mValue = mapModalForm1.getInputValue('#meter-value');
			var mTimestamp = mapModalForm.picker.getTime();
			var mLat = mapModal.getMarkerLat(mapModal.marker);
			var mLng = mapModal.getMarkerLng(mapModal.marker);
			alert('panda3');
			map.addMeter(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);
			alert('panda4');
			map.dropMarkers();
			alert('panda5');
			meterTable.loadMeterTable();
			alert('panda6');
			$('#map-modal').modal('hide');
		}
		return false;
		*/
	}

$(document).ready(function() {
	
	/*			
	$('#meter-timestamp').datetimepicker({
		format : 'dd/MM/yyyy hh:mm:ss',
		language : 'en'
	});
	*/
	
	/*
	$('#meter-timestamp').on('show', function(e) {
	  	if(mapModalForm.picker == null){
	  		mapModalForm.picker = e.date;
	  	} 
	});
	*/
	/*
	$('#meter-timestamp').on('changeDate', function(e) {
	  	mapModalForm.picker = e.date;
	});
    */
   
	$('#map-modal').on('shown', function() {
		var mapModal1 = Object.create(mapModal);
		mapModal1.init();
		
		
		map1 = Object.create(map);
		map1.init(true);		
				
	});

	/*
	$('#save-meter').click(function() {
		var mapModalForm1 = Object.create(mapModalForm);
		if (mapModalForm1.validate() == true) {
			var mGroup = mapModalForm1.getSelectedGroup();
			var mId = map.lastMeterId;
			var mName = mapModalForm1.getInputValue('#meter-name');
			var mValue = mapModalForm1.getInputValue('#meter-value');
			var mTimestamp = mapModalForm.picker.getTime();
			var mLat = mapModal.getMarkerLat(mapModal.marker);
			var mLng = mapModal.getMarkerLng(mapModal.marker);
			map.addMeter(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);
			map.dropMarkers();
			meterTable.loadMeterTable();
			$('#map-modal').modal('hide');
		}
	});
	*/
	
	mapModal.setGroupsSelectBox();
	
	
});
