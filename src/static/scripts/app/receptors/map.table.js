var mapTable = {};

//how much items per page to show
mapTable.showPerPage = null;

//amount of tr elements inside tbody
mapTable.items = null;

//number of pages
mapTable.pages = null;

//current page displayed
mapTable.currPage = null;

//last row nubmer
mapTable.lastRowNum = null;

//init table, expect table ('meter' or 'group')

mapTable.init = function(table) {

	if(table == 'meter'){
		meterTable.lastRowNum = 0;
		meterTable.loadMeterTable();
	}
	
	if(table == 'group'){		
		groupTable.lastRowNum = 0;
		groupTable.loadGroupTable();
	}

	return false;
}

mapTable.initPagination = function(table) {
	$(table).prev().remove();
	$(table).each(function() {
	    var currentPage = 0;
	    var numPerPage = 5;
	    var $table = $(this);
	    $table.bind('repaginate', function() {
	        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
	    });
	    $table.trigger('repaginate');
	    var numRows = $table.find('tbody tr').length;
	    var numPages = Math.ceil(numRows / numPerPage);
	    var $pagination = $('<div class="pagination-container pagination-centered"></div>');
	    $pagination.insertBefore($table);
	    var $pager = $('<ul></ul>');
	    $pagination.append($pager);
	    for (var page = 0; page < numPages; page++) {
	        $('<li class="page-number"></li>').text(page + 1).bind('click', {
	            newPage: page
	        }, function(event) {
	            currentPage = event.data['newPage'];
	            $table.trigger('repaginate');
	            $(this).addClass('active-page').siblings().removeClass('active-page');
	        }).appendTo($pager).addClass('clickable');
	    }    
	});
	
	return false;
}

//create new class metersTable which inherrit mapTable
var meterTable = Object.create(mapTable);
//add meter to the table (appends new row to tbody)
meterTable.addMeterToTable = function(mGroup, mId, mName, mValue, mTimestamp, mLat, mLng) {
	$('#meter-table').append('<tr><td>' + (meterTable.lastRowNum + 1) + '</td><td><a href="#" onclick="map.popMarker(' + meterTable.lastRowNum + ')">' + mName + '</a></td><td>' + mGroup + '</td></tr>');
	meterTable.lastRowNum++;
	meterTable.initPagination('#meter-table');

	console.log('added meter to table', mGroup, mId, mName, mValue, mTimestamp, mLat, mLng);
	return false;
}

// load or reload meters table #meter-table rows
// loads only meters in groups which are checked
meterTable.loadMeterTable = function() {
	meterTable.lastRowNum = 0;
	//remove all rows from table
	$('#meter-table > tbody').html('');
	//continues only if some groups in map.data

		//get ids of checked groups


	if(map.data[0].meters != undefined) {	
		//continue only if group contain some meters
		if(map.data[0].meters.length > 0) {
			//itterate through meters in group
			for (var meterId = 0; meterId < map.data[0].meters.length; meterId++) {				
				var meter = map.data[0].meters[meterId];
				var mGroup = map.data[0].group;
				var timestamp = "0"; //new Date(meter.timestamp).toISOString();
				meterTable.addMeterToTable(mGroup, meter.id, meter.name, meter.value, timestamp, meter.lat, meter.lng);
				console.log('meter table loaded');
			}
		}
	}

		

	
	return false;
}



//create new class groupsTable which inherrit mapTable
var groupTable = Object.create(mapTable);

groupTable.selectedGroups = null;

//method adds new row to #group-table
groupTable.addGroupToTable = function(groupId) {
	groupId = 0;
	/*
	if(map.data.length > 0) {
		var metersSum = map.data[groupId].meters.length;
		var name = map.data[groupId].group;
		
	    $('#group-table').append('<tr><td><input type="checkbox" checked="yes" groupId="' + groupTable.lastRowNum + '" class="group-checkbox" onClick="map.dropMarkers(); meterTable.loadMeterTable();" /></td><td>' + (groupTable.lastRowNum + 1) + '</td><td><a href="#">' + name + '</a></td><td>' + metersSum + '</td></tr>');
	    groupTable.lastRowNum++;
	    groupTable.initPagination('#group-table');
	    groupTable.items++;
	    
	    console.log('added group to table', name);
	}
	*/
    return false;
}

//itterate through groups in map.data object and add group as a row to the #group-table table
groupTable.loadGroupTable = function() {
	for (var i in map.data) {
		groupTable.addGroupToTable(i);
	}
	
	return false;
}

//returns list of group ids which are checked
groupTable.getCheckedGroupsIdsInTable = function() {
	var groups = [];
	
	if(groupTable.items > 0){
		$('.group-checkbox').each(function() {
			if($(this).prop('checked') == true){
				groups.push($(this).attr('groupId'));
			}						
		});		
	}
	
	return groups;
}
