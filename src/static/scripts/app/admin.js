function delete_user(username) {
    //loads selected source term and places it into table on tab2
      var postdata = {"username": username};
      
      $.ajax( {
        url: '/admin/delete_user',
        type: 'post',
        data: postdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {
            $("#msg").html(data["msg"]);
            location.reload();
        },
        error: function () { $("#msg").html("Unexpected error when deleting user!"); }
      } );           
}

function disable_user(username) {
    //loads selected source term and places it into table on tab2
      var postdata = {"username": username};
      
      $.ajax( {
        url: '/admin/disable_user',
        type: 'post',
        data: postdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {
            $("#msg").html(data["msg"]);
            location.reload();
        },
        error: function () { $("#msg").html("Unexpected error when disabling user!"); }
      } );           
}

function enable_user(username) {
    //loads selected source term and places it into table on tab2
      var postdata = {"username": username};
      
      $.ajax( {
        url: '/admin/enable_user',
        type: 'post',
        data: postdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {
            $("#msg").html(data["msg"]);
            location.reload();
        },
        error: function () { $("#msg").html("Unexpected error when enabling user!"); },
      } );           
}

function make_admin(username) {
    //loads selected source term and places it into table on tab2
      var postdata = {"username": username};
      
      $.ajax( {
        url: '/admin/make_admin',
        type: 'post',
        data: postdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {
            $("#msg").html(data["msg"]);
            location.reload();
        },
        error: function () { $("#msg").html("Unexpected error when making user admin!"); },
      } );           
}

function get_user(username) {
      //loads user data to form
      var getdata = {"username": username};
      
      $.ajax( {
        url: '/admin/get_user',
        type: 'get',
        data: getdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {   
            var user = jQuery.parseJSON( data["user"] );

            $("#username").html(user.username);
            $("#first_name").val(user.first_name);
            $("#middle_names").val(user.middle_names);
            $("#surname").val(user.surname);
            $("#email").val(user.email);
            $("#status").val(user.status);
            $("#user_type").val(user.user_type);
            $("#institute").val(user.institute);
            $("#street1").val(user.street1);
            $("#street2").val(user.street2);
            $("#city").val(user.city);
            $("#state").val(user.state);
            $("#country").val(user.country);
        },
        error: function () { $("#msg").html("Unexpected error when getting user data"); },
      } );
}

function save_user() {
      var postdata = {
        "username"      : $("#username").text(),
        "first_name"    : $("#first_name").val(),
        "middle_names"  : $("#middle_names").val(),
        "surname"       : $("#surname").val(),
        "email"         : $("#email").val(),
        "status"        : $("#status").val(),
        "user_type"     : $("#user_type").val(),
        "institute"     : $("#institute").val(),
        "street1"       : $("#street1").val(),
        "street2"       : $("#street2").val(),
        "city"          : $("#city").val(),
        "state"         : $("#state").val(),
        "country"       : $("#country").val()    
      };
      
      $.ajax( {
        url: '/admin/save_user',
        type: 'post',
        data: postdata,
        headers: { 
            "cache-control": "no-cache"  //for object property name, use quoted notation shown in second
        },
        dataType: 'json', //we expect JSON
        success: function( data )
        {
            $("#msg").html(data["msg"]);
            console.log(data["msg"]);
            location.reload();
        },
        error: function () { $("#msg").html("Unexpected error when saving user!"); }
      } );
}