#from local_storage import storage #local storage
import time
import random
from html import *
import math	

#insertion of combo for selction of number of points
def create_st_table(nuclides, data):
	#firstly, we gather parameters of the release
	#nuclides = array of nuclide names
	#data.shape = len(nuclides) x puff_number
	print(nuclides)
	print(data)
	
	

	#table
	t0 = TABLE(Class="table table-bordered table-striped")
	tbody1 = TBODY()
	thead1 = THEAD()
	
	puff_count = len(data[0])
	
	#header of the table
	h1 = TR(); td = TH(); h1 <= td;
	#first column
		
	for i in range(puff_count):
		td = TH("Puff %d" % (i+1))
		h1 <= td
	
	thead1 <= h1
	
	#puff properties
	h1 = TR(); td = TH("Release height"); h1 <= td; #first column
	for i in range(puff_count):
		a = INPUT(Type='text', Id='v_%d' % i, value='50.0', style = {'width': '65px'})
		a.onblur=validate_and_update_source_graph
		td = TD()
		td <= a
		h1 <= td
    
	tbody1 <= h1
	
	#second header with puff numbers and activities
	thead2 = THEAD()
	h1 = TR(); td = TD(); h1 <= td #first column
	for i in range(puff_count):
		td = TH("Puff %d" % (i+1))
		h1 <= td
	
	thead2 <= h1		
		
	tbody2 = TBODY()
	for i in range(len(nuclides)):
		h1 = TR(); td = TH(mnl(nuclides[i])); h1 <= td; #first column			
		for j in range(len(data[i])):
			td = TD()
			ide = 'n_%s_%d' % (nuclides[i], j)
			a = INPUT(Type='text', Id=ide, value='%3.2E' % (10.**float(data[i][j])), style = {'width': '65px'}, name="st_table_input")
			#storage[ide] = '%3.2E' % float(data[i][j]) #storing current value
			a.onblur=validate_and_update_source_graph #in new_task_puff.js
			td <= a
			h1 <= td
			
		tbody2 <= h1
		
	t0 <= thead1
	t0 <= tbody1
	t0 <= thead2
	t0 <= tbody2	
	doc['source_table'].html = "" #clear the div
	doc['source_table'] <= t0
	
	#insertion of segments (puffs) count
	a = INPUT(Type='hidden', Id="segments_count", value='%d' % puff_count, name="st_table_input")
	doc['source_table'] <= a
	#insertion of nuclides count
	a = INPUT(Type='hidden', Id="nuclides_count", value='%d' % len(nuclides), name="st_table_input")
	doc['source_table'] <= a		
		

def mnl(nuclide):
    """
    wrapper for make_nuclide_label(nuclide): with a short name
    """
    ret = make_nuclide_label(nuclide)
    return ret
    
def make_nuclide_label(nuclide):
    """
    from nuclide format in HARP, e.g. CS137, should make ^{137}Cs:)
    """

    text = []
    digit = ""
    for i in range(len(nuclide)):
        s = nuclide[i]
        if not s.isdigit():
            text.append(s.lower())
        else:
            digit = nuclide[i:].lower()
            break #break of for cycle
    """    
    for s in nuclide:
        print s
        if s.isdigit():
            digit += s
        else:
            text.append(s.lower()) 
    """             
    text[0] = text[0].upper()
                
    return "<sup>%s</sup>%s" % (digit, "".join(text))
   
def get_nucl_list():
    """
    generates html table with nuclides
    """
    nucl_list = ["H3",      "BE7",     "C14",     "F18",     "NA22",    "NA24",    
                "CL38",    "AR41",    "K42",     "CR51",    "MN54",    "MN56",    
                "FE55",    "FE59",    "CO58",    "CO60",   "NI63",    "CU64",    
                "ZN65",    "AS76",    "KR85M",   "KR85",    "KR87",    "KR88",    
                "KR89",    "KR90",    "RB86",    "RB88",    "RB89",   "SR89",    
                "SR90",    "SR91",    "SR92",    "Y88",     "Y90",     "Y91M",    
                "Y91",     "ZR95",    "ZR97",    "NB95",    "NB97",    "MO99",    
                "TC99M",   "TC99",    "RU103",   "RU105",   "RU106",   "RH105",   
                "RH106M",  "AG110M",  "SB122",   "SB124",   "SB125",   "SB127",   
                "SB129",   "TE125M",  "TE127M",  "TE127",   "TE129M",  "TE129",   
                "TE131M",  "TE131",   "TE132",   "TE133M",  "TE133",   "TE134",   
                "I129O",   "I129",    "I129A",   "I131O",   "I131",    "I131A",   
                "I132O",   "I132",    "I132A",  "I133O",   "I133",    "I133A",   
                "I134O",   "I134",   "I134A",   "I135O",   "I135",    "I135A",   
                "XE131M",  "XE133M",  "XE133",   "XE135M",  "XE135",   "XE137",   
                "XE138",   "CS134",   "CS136",   "CS137",   "CS138",   "BA139",   
                "BA140",   "LA140",   "CE139",   "CE141",   "CE143",   "CE144",   
                "PR143",   "PR144",   "ND147",   "PM147",   "SM153",   "EU154",   
                "W187",    "U235",    "U238",    "NP239",   "PU238",   "PU239",   
                "PU240",   "PU241",   "AM241",   "CM242",   "CM244",   "HTO",     
                "H3O",     "CO2",     "CO"] #total 123 nuclides
    
    return nucl_list
	
