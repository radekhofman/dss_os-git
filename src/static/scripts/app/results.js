  var map = null;
  var geoxml = null;
   
  //refreshing of task table 
  $(document).ready(function () {
      get_task_table();
  });

  $(document).on("click", "a.fileDownloadSimpleRichExperience", function () {
      $.fileDownload($(this).attr('href'), {
          preparingMessageHtml: "We are preparing your report, please wait...",
          failMessageHtml: "There was a problem generating your report, please try again."
      });
      return false; //this is critical to stop the click event which will trigger a normal file download!
  });

  function get_task_table() {
      var first = $("#hidden_first").val();
      var size  = $("#hidden_size").val();
      
      var postdata = {"first": first,
                      "size": size};

	  $.ajax( {
	    url: '/results/generate_task_table',
	    type: 'post',
	    data: postdata,
	    headers: { 
	    	"cache-control": "no-cache"  //for object property name, use quoted notation shown in second
	    },
	    //dataType: 'json',
	    success: function( data )
	    {
	        $("#task_table").html(data);
	    },
      } );
	                      
      //$.post('/results/generate_task_table', postdata, 
      //   function(data) {  //callback function
      //             $("#task_table").html(data);           
      //   });                                  
     
      setTimeout(function(){get_task_table()}, 5000);             
  } 
   
  $(window).load(function(){
           $('#myModal2D').on('shown', function (e) {
            google.maps.event.trigger(map, 'resize');
            
           });
  }); 
  

  function show_map0(lat, lon) {
        var myLatlng = new google.maps.LatLng(lat, lon);
        var myOptions = {
           zoom: 10,
           center: myLatlng,
           mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);                       
        google.maps.event.trigger(map, 'resize'); 
        //return false;      
  }
    

  function prepare_overlay(task_id, lat, lon, desc) {
      //calls method for rendering overlay (KML) and colorbar and places both into modal window
      //which is shown afterwards
      
      //sending request on generatin of KML overlay  
      var postdata = {"task_id": task_id,
                      "ident": "Ar-41"};
                      
      $.post('/results/prepare_overlay_disp', postdata, 
         function(data) {  //callback function
           //alert('will show overlay: http://localhost:8080/overlays/'+data);
           //the methods automatically waith till the overaly and colorbar are finished, cho cho cho:)
           geoXml = new geoXML3.parser({map: map, singleInfoWindow: true});
           geoXml.parse(data+'.kml');   
           var src_lnk = data+'_colorbar.svg'      
           $("#map_colorbar").html('<center><img height="550px" src='+src_lnk+'></center>')           
         });
    
      $("#map_label").html('<center><h3>'+desc+'</h3></center>')

      show_map0(lat, lon);
      
      
      //google.maps.event.trigger(map, 'resize');
      //var myLatlng = new google.maps.LatLng(lat, lon);
      //map.setCenter(myLatlng);
      //map.setZoom(current_zoom);                       
  }
              

           
  function save_map_and_legend_to_image() {
  	   //saves map canvas with overlay and colorbar to image using js library
  	   var oCanvas = document.getElementById("map_and_legend_canvas"); 
  	   Canvas2Image.saveAsPNG(oCanvas);  // will prompt the user to save the image as PNG.
  	   return false;
  	
  }

