// wait for the DOM to be loaded
$(document).ready(function() {
	//var options = {
	//target:        '#source_table',   // target element(s) to be updated with server response
	//url : '/task/update_nucl_table',
	//beforeSubmit:  showRequest,  // pre-submit callback
	//success:       showResponse  // post-submit callback

	// other available options:
	//url:       url         // override for form's 'action' attribute
	//type:      type        // 'get' or 'post', override for form's 'method' attribute
	//dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
	//clearForm: true        // clear all form fields after successful submit
	//resetForm: true        // reset the form after successful submit

	// $.ajax options can be used here too, for example:
	//timeout:   3000
	//};
	bind_methods();

});

function bind_methods() {

	// bind 'nuclide form' and provide a simple callback function
	//$('#nuclForm').ajaxForm({//target: '#source_table',
	//                         success: updateNuclidesAndSegmentsTable,
	//                         });
	// bind 'meteo form' and provide a simple callback function
	$('#meteoForm').ajaxForm({//target: '#source_table',
		success : updateMeteoTable,
	});

	testSubmitButton();

}

function load_source_term(_id) {
	//loads selected source term and places it into table on tab2
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/load_st_json',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			//$("#segment_and_source_tables").html(data["st_table"]);
			$("#modal_nucl_table_div").html(data["nucl_modal"]);
			sourceTermNDMTModule.updateModel($.parseJSON(data["data"]));
			bind_methods();
		},
	});

}

function delete_source_term(_id) {
	//deletes source term of a given ID from source term collection
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/delete_st',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			update_load_st_modal();
			$("#msg").html(data["msg"]);
		},
	});
}

function updateNuclidesAndSegmentsTable(responseText, statusText, xhr, $form) {
	$('#segment_and_source_tables').html(responseText);
	//now se test if nuclides are selected
	testSubmitButton();
}

function trySubmitSrcTermCfg() {
	var canSubmit = false;
	// Check if at least one nuclide is selected
	var isAnyNuclideSelected = function() {
		var checked = false;
		$("#nuclTable").find("input[type=checkbox]").each(function() {
			if (this.checked) {
				checked = true;
			};
		});
		return checked;
	};
	
	// Run validation
	if (!isAnyNuclideSelected()) {
		var valError = "<div id='noNuclSelectedValMsg' class='alert alert-error'><center><h4>At least one nuclide should be selected.</h4></center></div>";
		$("#modalNuclideValMsgs").append($(valError));
		canSubmit = false;
	}else{
		$("#noNuclSelectedValMsg").remove();
		update_source_term_table(); 
		$('#myModalNuclide').modal('hide');
		canSubmit = true;
	}
	return canSubmit;
}

function update_source_term_table() {
	// Save configuration to data model
	sourceTermNDMTModule.saveConfig();
	// Render table with new data
	sourceTermNDMTModule.renderTable();
	//Update charts
	SourceTermCharts.loadCharts(sourceTermNDMTModule.data);
}

function testSubmitButton() {
	//validates whether there is a valid source term, if so, the submit button for the whole task is enabled, othervise not
	//alert($('#nuclides_count').val());

	if ($('#nuclides_count').val() != "0" && $('#meteo_hours_count').val() != "0") {
		//submit button
		$('#submit_button').html("<button type='submit' class='btn btn-success btn-large'>Submit task</button>");
	} else {
		//disabled button
		$('#submit_button').html("<button type='button' class='btn btn-success btn-large disabled'>Submit task</button>");
	}
}

function gather_modal_nuclides_list() {
	//return list of nuclides actually checked in modal window
	var nuclides_modal_count = 123;
	//we must test 123 nuclides in modal table whether they are selected or not
	var nuclides_modal_selected = [];
	for (var i = 0; i < nuclides_modal_count; i++) {
		if ($("#nucl_cb_" + i).is(':checked'))//testing if checkbox is checked
		{
			nuclides_modal_selected.push($("#nucl_cb_" + i).attr('name'));
		}
	}

	return nuclides_modal_selected
}

function gather_nuclides_list() {
	//return list of nuclides actually included in source term table
	var nuclides_count = $('#nuclides_count').val();
	var nuclides = [];
	for (var i = 0; i < nuclides_count; i++) {
		nuclides.push($("#disp_task [name=nuclname_" + i + "]").val());
	}

	return nuclides
}

function gather_activities_data() {
	var nuclides_count = $('#nuclides_count').val();
	var segments_count = $('#segments_count').val();
	var nuclides = [];
	//gather_nuclides_list();
	var activities = [];
	for (var i = 0; i < nuclides_count; i++) {
		nuclides.push($("#disp_task [name=nuclname_" + i + "]").val());
		for (var j = 0; j < segments_count; j++) {
			activities.push($("#disp_task [name=nucl_" + nuclides[i] + "_" + j + "]").val());
			//appnd to one array, must be reshaped into proper shape!
		}
	}

	return activities

}

function gather_segment_properties() {
	var segment_properties = new Array();
	//equivalent to = []
	var labels = ["seg_dur_", "seg_heat_", "seg_speed_", "seg_height_"];
	var segments_count = $('#segments_count').val();

	for (var i = 0; i < 4; i++) {
		for (var j = 0; j < segments_count; j++) {
			segment_properties.push($("#disp_task [name=" + labels[i] + j + "]").val());
		}
	}

	return segment_properties

}

function save_source_term() {
	//collects data from source term table and saves to DB
	var nuclides_count = $('#nuclides_count').val();
	var segments_count = $('#segments_count').val();
	var segment_properties = gather_segment_properties();
	var nuclides = gather_nuclides_list();
	var activities = gather_activities_data();
	var description = $("#save_st_form [name=st_desc]").val();

	var postdata = {
		"nuclides_count" : nuclides_count,
		"segments_count" : segments_count,
		"segments_properties" : segment_properties,
		"nuclides" : nuclides,
		"activities" : activities,
		"description" : description
	};

	//alert(postdata["nuclides"]);

	$.ajax({
		url : '/task/save_st',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			var status = data["status"];
			if (status == 0)//OK, source term successfully saved, we should update saved source terms table
			{
				update_load_st_modal();
			}
			$('#segment_and_source_tables').html(data["table"]);
			$("#msg").html(data["msg"]);
		},
	});

}

function update_load_st_modal() {
	//updates load st table otherwise newly saved task will not be shown
	$.ajax({
		url : '/task/update_st_load_form',
		type : 'post',
		//data: postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		//dataType: 'json', //we expect JSON
		success : function(data) {
			$("#modal_load_st_div").html(data);
		},
	});
}

function load_meteo(_id) {
	//loads selected source term and places it into table on tab2
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/load_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			$("#meteo_table").html(data["meteo_table"]);
			bind_methods();
		},
	});

}

function save_meteo() {
	//collects data from source term table and saves to DB
	var meteo_hours_count = $('#meteo_hours_count').val();
	var meteo = [];
	//will be 2D array after reshape
	var labels = ["meteo_wd_", "meteo_ws_", "meteo_stab_", "meteo_prec_"];

	for (var i = 1; i <= meteo_hours_count; i++) {
		for (var j = 0; j < 4; j++)//we have 4 columns
		{
			meteo.push($("#disp_task [name=" + labels[j] + i + "]").val());
		}
	}

	var description = $("#save_meteo_form [name=meteo_desc]").val();

	var postdata = {
		"meteo_hours_count" : meteo_hours_count,
		"meteo" : meteo,
		"description" : description
	};

	//alert(postdata["nuclides"]);

	$.ajax({
		url : '/task/save_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {

			var status = data["status"];
			if (status == 0)//OK, source term successfully saved, we should update saved source terms table
			{
				update_load_meteo_modal();
			}
			$('#meteo_table').html(data["meteo_table"]);
			$("#msg").html(data["msg"]);
		},
	});
}

function update_load_meteo_modal() {
	//updates load st table otherwise newly saved task will not be shown
	$.ajax({
		url : '/task/update_meteo_load_form',
		type : 'post',
		//data: postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		//dataType: 'json', //we expect JSON
		success : function(data) {
			$("#modal_load_meteo_div").html(data);
		},
	});
}

function delete_meteo(_id) {
	//deletes source term of a given ID from source term collection
	var postdata = {
		"_id" : _id
	};

	$.ajax({
		url : '/task/delete_meteo',
		type : 'post',
		data : postdata,
		headers : {
			"cache-control" : "no-cache" //for object property name, use quoted notation shown in second
		},
		dataType : 'json', //we expect JSON
		success : function(data) {
			update_load_meteo_modal();
			$("#msg").html(data["msg"]);
		},
	});
}

function updateMeteoTable(responseText, statusText, xhr, $form) {
	//alert(responseText);
	$('#meteo_table').html(responseText);
	testSubmitButton();
}

//after page load : automatic resize of map
$(window).load(function() {
	$('#myModalGMap').on('shown', function(e) {
		show_map();
		google.maps.event.trigger(map, 'resize');

	});

});

//map with location of the release source
var map;
function show_map() {
	var myLatlng = new google.maps.LatLng(document.getElementById('lat').value, document.getElementById('lon').value);
	var myOptions = {
		zoom : 10,
		center : myLatlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

	marker = new google.maps.Marker({
		map : map,
		position : myLatlng,
		title : 'source of release'
	});

	marker['infowin'] = new google.maps.InfoWindow({
		content : '<div>This is a source in ' + myLatlng + '</div>'
	});

	google.maps.event.addListener(marker, 'click', function() {
		this['infowin'].open(map, this);
	});

	google.maps.event.trigger(map, 'resize');
	return false;

}

