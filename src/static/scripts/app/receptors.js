// wait for the DOM to be loaded 

function load_receptors(_id) {
	//loads selected source term and places it into table on tab2
	  var postdata = {"_id": _id};
	  
	  $.ajax( {
	    url: '/tools/load_receptors',
	    type: 'post',
	    data: postdata,
	    headers: { 
	    	"cache-control": "no-cache"  //for object property name, use quoted notation shown in second
	    },
	    dataType: 'json', //we expect JSON
	    success: function( data )
	    {
			
			map1 = Object.create(map);
			map1.init(true);
			
			receptor_data = data["receptor_data"];
			
			map.data[0] = JSON.parse(receptor_data);
			map.dropMarkers();
			meterTable.loadMeterTable();
	    },
      } );
	      	
}

function delete_receptors(_id) {
	//deletes source term of a given ID from source term collection
	 var postdata = {"_id": _id};

	  $.ajax( {
	    url: '/tools/delete_receptors',
	    type: 'post',
	    data: postdata,
	    headers: { 
	    	"cache-control": "no-cache"  //for object property name, use quoted notation shown in second
	    },
	    //dataType: 'json', //we expect JSON
	    success: function( data )
	    {
			update_receptors_load_modal();
			//$("#msg").html(data["msg"]);
	    },
      } );	 	
}


function save_receptors() {
			var data = JSON.stringify(map.data[0]);
			var description = $.trim($("#configuration-name").val());
			$.ajax({
			  type: 'POST',
			  url: '/tools/save_receptors',
			  data: {
			  	description : description,
			  	data : data }
			}).done(function() {
				$('#save-configuration-validation-result').html('<p>Configuration saved.</p>');
				setTimeout(function(){ 
					$('#save-configuration-modal').modal('hide');
				}, 500 );
				console.log('configuration data saved', data);
				update_receptors_load_modal();
				
			}).fail(function(){
				$('#save-configuration-validation-result').html('<p>Error when saving configuration.</p>');
				setTimeout(function(){ 
					$('#save-configuration-modal').modal('hide');
				}, 500 );				
			});
			
			
	
}

function update_receptors_load_modal() {
	//updates load st table otherwise newly saved task will not be shown
		$.ajax( {
		    url: '/tools/update_receptors_load_form',
		    type: 'post',
		    //data: postdata,
		    headers: { 
		    	"cache-control": "no-cache"  //for object property name, use quoted notation shown in second
		    },
		    //dataType: 'json', //we expect JSON
		    success: function( data )
		    {
		        $("#load_receptors_list").html(data);
		    },
	      } );	
}


$(document).ready(function() {
	update_receptors_load_modal();
	map.addMeterGroup("RMN receptors");
});
