# -*- encoding: UTF-8 -*
import numpy
import copy

class Validator(object):
    """
    class of vlaidator object    
    """
    
    def __init__(self, orig_input):
        """
        constructor - recives numpy.chararray ORIG_INPUT of shape (m,n) with user STRING inputs as were entered into forms 
        and contains:
         - object with original inputs of shape (m,n) of type string, (ORIG_INPUT)
         - their conversion into DESIRED_TYPE of shape (m,n) (IF POSSIBLE), (CONVERSION)
         - validation matrix of shape (m,n) of type int: 0 - OK, 1 - validation error, e.g. type error etc. (VALIDATION)
         - optional field of shape (m,n) of particular error messages in case of error
         
        """
        
        
        self.orig_input = orig_input
        #self.conversion = None  
        #self.validator_result = None
        #self.optional_error_messages... TO BE IMPLEMENTED
    
    def is_valid(self):
        if numpy.array(self.validator_result).sum() == 0:
            return True
        else:
            return False
        
    def validate_numeric_matrix(self, desired_type): #int, float
        #validation of multidimensional numerical data
        self.orig_shape = self.orig_input.shape 
        self.conversion = numpy.zeros(self.orig_shape)   
        self.validator_result = numpy.ones(self.orig_shape, dtype=numpy.int) 
        self.recursive_validation_of_narray(self.orig_input, desired_type, indices=[])

    def validate_numeric(self, desired_type, a=0, b=numpy.inf):  #int, float
        #validation of numbers
        self.conversion = 0.
        self.validator_result = 1
        try:
            self.conversion = map(desired_type, (self.orig_input,))[0] #type validation
            self.validator_result = 0 + self.validate_interval(self.conversion, a, b) #range validation
        except:
            self.validator_result = 1

    def validate_meteo(self,  a=0, b=numpy.inf):
        """
        ad-hoc validator for 4-column matrix, where 3rd column is string [a,b,c,d,e,f], floats otherwise
        """
        self.orig_shape = self.orig_input.shape
        self.conversion = numpy.zeros(self.orig_shape) 
        self.validator_result = numpy.ones(self.orig_shape, dtype=numpy.int) 
        
        for i in range(self.orig_shape[0]): #number of hours in meteoforecast
            for j in range(4): #columns
                if j == 2: #text column
                    psc = self.orig_input[i,j].strip().upper() 
                    if psc in ["A", "B", "C", "D", "E", "F"]:
                        self.conversion[i,j] = ["A", "B", "C", "D", "E", "F"].index(psc)
                        self.validator_result[i,j] = 0
                    else:
                        self.validator_result[i,j] = 1
                else: #float columns
                    try:
                        self.conversion[i,j] = map(float, (self.orig_input[i,j],))[0] #type validation
                        self.validator_result[i,j] = 0 + self.validate_interval(self.conversion[i,j], a, b) #range validation
                    except Exception, e:
                        self.validator_result[i,j] = 1
                    
                  
    def recursive_validation_of_narray(self, narray, desired_type, indices=[], a=0, b=numpy.inf):
        """ 
        recurrent validation of all elements of n-array of an arbitrary shape, one dimension by one...:)
        [a, b] - validation interval, the value must fit into the intevral, closed set
        """
        if type(narray) != str:
            for i in range(narray.shape[0]):
                row = narray[i]         
                indices_new = copy.deepcopy(indices)
                indices_new.append(i)
                self.recursive_validation_of_narray(row, desired_type, indices_new)
        else:
            try:
                self.conversion[tuple(indices)] = map(desired_type, (narray,))[0]              
                self.validator_result[tuple(indices)] = 0 + self.validate_interval(self.conversion[tuple(indices)] , a, b)
            except Exception, e:
                self.validator_result[tuple(indices)] = 1


    def validate_interval(self, x, a, b):
        """
        [a, b] - validation interval, the value must fit into the intevral, closed set
        """
        if x >= a and x <= b:
            return 0
        else:
            return 1
                        
    def ecfs(self, shape):
        """
        calculates elements count from shape of n-array
        """     
        return numpy.array(shape).prod()   
                   

def main():
    ###EXAMPLE USAGE:
    orig = numpy.random.normal(0,1,(3,3))

    a = numpy.chararray((3,3), itemsize=12)
    for i in range(3):
        for j in range(3):
            a[i,j] = str(orig[i,j])
    
    a[1,2] = "abc"            
    
    ###a simulates text data entered by user
    
    #we eant validate data against float type
    desired_type = float
    
    #it is easy...
    a_validator = Validator(a)
    a_validator.validate_numeric(float)
    
    print "Original user input"
    print a_validator.orig_input
    print "Validation result"
    print a_validator.validator_result   
       
    
if __name__ == "__main__":
    main()
    

    

    