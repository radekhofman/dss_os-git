'''
Created on Jan 7, 2014

@author: krablak
'''
import os

# Mongo connection URL with switch for Openshift deployment
MONGODB_DB_URL = os.environ.get('OPENSHIFT_MONGODB_DB_URL') if os.environ.get('OPENSHIFT_MONGODB_DB_URL') else 'mongodb://localhost:27017/'
