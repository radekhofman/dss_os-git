'''
Created on Sep 24, 2013


'''

import simplejson as sj
import cherrypy
from mako.lookup import TemplateLookup
import copy
import config as cf
from auth import require, member_of, check_credentials
import web_html_admin
import web_html #comtains methods for web HTML rendering
import db_tools_users

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common
SESSION_KEY = cf.SESSION_KEY

class Admin:
    
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [member_of('admin')] #extension of basic cofig of some special items
    _cp_config = cp_conf

    @cherrypy.expose
    def index(self): #index of main DSS GUI for common user
        return self.render_page()
    
    @cherrypy.expose
    def render_page(self):
        #navibar with buttons
        ab = 4
        if (cherrypy.session.get(cf.SESSION_KEY) != None):
            ab = 7
        print cherrypy.session.get(cf.SESSION_KEY)     
        head = web_html_admin.head()
        nav = web_html.navibar(active=ab) #renders navibar according to login state
        tmpl = lookup.get_template("admin.html")
        users = web_html_admin.get_list_of_users()
        bod = tmpl.render(users=users)

        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
    
    @cherrypy.expose
    def delete_user(self, username):
        """
        deletes user of a given username from database
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}
        if username:
            try:
                db_tools_users.delete_user(username)
                msg = "OK, user %s deleted " % username
            except Exception, e:
                msg = "Error, user not deleted", e
            finally:
                print msg
                d = {"msg": msg}
                return sj.dumps(d)
    
    @cherrypy.expose
    def disable_user(self, username):
        """
        disables user term of a given username in database
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}
        if username:
            try:
                db_tools_users.disable_user(username)
                msg = "OK, user %s disabled " % username
            except Exception, e:
                msg = "Error, user not disabled", e
            finally:
                print msg
                d = {"msg": msg}
                return sj.dumps(d)
    
    @cherrypy.expose
    def enable_user(self, username):
        """
        enables user term of a given username in database
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}
        if username:
            try:
                db_tools_users.enable_user(username)
                msg = "OK, user %s enabled " % username
            except Exception, e:
                msg = "Error, user not enabled", e
            finally:
                print msg
                d = {"msg": msg}
                return sj.dumps(d)
        
    @cherrypy.expose
    def make_admin(self, username):
        """
        set user type to admin by given username in database
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}
        if username:
            try:
                db_tools_users.make_admin(username)
                msg = "OK, user %s is admin " % username         
            except Exception, e:
                msg = "Error, user not set as admin", e
            finally:
                print msg
                d = {"msg": msg}
                return sj.dumps(d)

    @cherrypy.expose
    def get_user(self, username):
        """
        returns user data in JSON by username 
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}

        if username:
            try:
                user = db_tools_users.get_user(username)
                if user:
                    msg = "OK, received %s data " % username
                else:
                    msg = "User with %s username does not exist" % username     
            except Exception, e:
                msg = "Error, cannot receive user data", e
            finally:
                d = {"msg": msg,
                     "user": user}
                return sj.dumps(d)
    
    @cherrypy.expose
    def save_user(self, **kwargs):
        """
        saves user to DB
        """
        cherrypy.response.headers['Content-Type'] = 'py/json' #we return multiple values, we must return JSON
        d = {}

        try:
            db_tools_users.save_user(user = { "username"        : kwargs["username"],
                                              "first_name"      : kwargs["first_name"],
                                              "middle_names"    : kwargs["middle_names"],
                                              "surname"         : kwargs["surname"],                                        
                                              "email"           : kwargs["email"],
                                              "status"          : kwargs["status"],
                                              "user_type"       : kwargs["user_type"],
                                              "institute"       : kwargs["institute"],
                                              "street1"         : kwargs["street1"],
                                              "street2"         : kwargs["street2"],
                                              "city"            : kwargs["city"],
                                              "state"           : kwargs["state"],
                                              "country"         : kwargs["country"]                                                 
                                            })
            msg = "User %s saved successfully" % kwargs["username"]   
        except Exception, e:
            msg = "Error, cannot save user data", e
        finally:
            d = {"msg": msg}
            return sj.dumps(d)