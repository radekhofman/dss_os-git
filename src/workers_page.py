import cherrypy
import copy
import config as cf
import web_html
import web_html_worker #comtains methods for web HTML rendering

from mako.lookup import TemplateLookup
#import simplejson as sj
#import numpy
#from db_tools import ma2mo, mo2ma #ndarray serialization to string - can be inserted into mongo
#import db_tools as dt
import db_tools_workers as dtw
#import tools
from caching import *

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common

#because of caching to save server's CPU load... this table is sam for all client at a time 
@Cache
def generate_wt(first, size):   
    #return HTML table with list of workers
    workers = dtw.get_workers_slice(first=int(first), size=int(size))
    return web_html_worker.worker_table(workers, first, size)
 
generate_wt.maxAge = 5 #seconds this saves DB load etc.
Cache.startCollection()

class Workers(object):
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    #workers_page is open to all users
    #cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    @cherrypy.expose
    def index(self): 
        return self.render_page()
    
    
    def render_page(self):
        #navibar with buttons
        head = web_html_worker.head()
        
        ab = 3
        if (cherrypy.session.get(cf.SESSION_KEY) != None):
            ab = 5
        
        nav = web_html.navibar(active=ab) #renders navibar according to login state
        bod = web_html_worker.get_body().decode("windows-1252").encode("utf8")
        fot = web_html.get_footer()
                
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
    

    @cherrypy.expose
    def generate_workers_table(self, first, size): #same as in the case of task table.. firt worker to display and the number of displayed workers
        #calls methods for table generation
        return generate_wt(first, size)
    
    


        
        
        
        
        
        

