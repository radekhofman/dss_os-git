# -*- encoding: UTF-8 -*-
import cherrypy

from app_loging import app_logger as logging

import db_tools_users

SESSION_KEY = '_cp_username'

class GuestUserPage:
    """
    Provides handlers related to Guest user features.
    """

    @cherrypy.expose
    def login(self):
        """
        POST method for creating guest user and login as created guest user.
        """
        logging.debug("Login as guest user was requested.")
        # Create guest user
        guest_user_name = self.__create_guest()
        # Do fake login as guest user
        if self.__login_as_guest(guest_user_name):
            # Redirect to 'New Task' page
            raise cherrypy.HTTPRedirect("/task/")
        else:
            logging.error("Guest login failed.")
            # Guest login fail - redirect to root page
            raise cherrypy.HTTPRedirect("/")

    def __create_guest(self):
        guest_user_name = db_tools_users.create_guest_user()
        logging.debug("Guest user %s created." % (guest_user_name))
        return guest_user_name

    def __login_as_guest(self, guest_user_name):
        logging.debug("Logging as guest user %s" % (guest_user_name))
        if guest_user_name:
            cherrypy.session[SESSION_KEY] = cherrypy.request.login = guest_user_name
            return True
        else:
            logging.error("No user was passed for login.")
            return False
