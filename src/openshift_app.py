# -*- encoding: UTF-8 -*-
'''
Startup file for Openshift deployment.
During the build phase should be renamed to 'application'

'''

import sys
sys.stdout = sys.stderr

import os
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()

import atexit

import cherrypy
import config as cf
import account_page
import task_page_puff
import results_page
import contact_page
import workers_page
import tools_page
import nuclides_page
import main_page
import guest_user_page
import web_services as ws

import schedulers.guest_deleting

import openshift.variables as os_vars
from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_DIR])

cherrypy.config.update({'environment': 'embedded'})

if cherrypy.__version__.startswith('3.0') and cherrypy.engine.state == 0:
    cherrypy.engine.start(blocking=False)
    atexit.register(cherrypy.engine.stop)


# Application initialization
application = cherrypy.Application(main_page.MainApp(), script_name=None, config=cf.global_conf_openshift)

# Basic cherrypy configuration and initialization
cherrypy.tree.mount(application.root, '/', config=cf.config)
cherrypy.config.update(
                       {
                        'tools.sessions.on' : True,
                        'tools.auth.on': True,
                        'tools.sessions.storage_type' : "file",
                        'tools.sessions.storage_path' : os_vars.OPENSHIFT_TMP_DIR,
                        'tools.sessions.timeout' : 60
                        }
                       )
cherrypy.config.update(cf.config)

# Dispatch configuration
application.root.account = account_page.Account()  # Account
application.root.task = task_page_puff.Task()  # Simulation
application.root.results = results_page.Results(os_vars.OPENSHIFT_GEAR_DNS) #DA
application.root.nuclides = nuclides_page.Nuclides()  # Nuclides managment
application.root.workers = workers_page.Workers()  # workers
application.root.contact = contact_page.Contact()  # contact
application.root.tools = tools_page.Tools()  # contact
application.root.guest = guest_user_page.GuestUserPage() # Guest user login handler
application.root.services = ws.WebServices()

# Start guest user deleting job
schedulers.guest_deleting.start_guest_deleting_job()

