#coding=utf-8
"""
tools for insertion and export of meteo.wea into the MongoDB

current source term DB model:

    d = {"username": kwargs["username"], #username
         "meteo": kwargs["meteo"], #numpy.array: must be pickeld before insertion to DB
         "hours_count": kwargs["hours_count"],
         "description": kwargs["description"],
         "modified": time.time()
         }
"""
import files_tools
import os
import numpy
import time
import admin_tools
import db_tools_meteo as dtm
import re

def load_meteo(path, f_name, username):

    handle = open(path+os.sep+f_name,"r")
    s = handle.readlines()
    handle.close()

    print f_name
    if (len(s) > 0): #je zadano jmeno nejakeho soubory
        #vytvorim novy objekt typu Source_term a naplnim ho

        decription=unicode(s[0], errors='ignore')
        meteo_hours_count = len(s)-1
        
        stabs = ["A", "B", "C", "D", "E", "F"]
        meteo = numpy.zeros((meteo_hours_count, 4))

        for i in range(1,len(s)): 
            line = s[i].split()   
                   
            meteo[i-1][0] = line[2]#float(s[i][17:25])
            meteo[i-1][1] = line[3]#float(s[i][25:33])
            meteo[i-1][2] = stabs.index(line[4])
            meteo[i-1][3] = line[5]#float(s[i][37:45])
            
        print meteo
        
        d = {"username": username, #username
             "meteo": meteo, #numpy.array: must be pickeld before insertion to DB
             "hours_count": meteo_hours_count,
             "description": decription,
             "modified": time.time()
             }
        
        return d
    


def save_data_to_meteo_wea_file( meteo, fname=''):

    #st = map_GUI_to_source_term_object()
    impl_path = ""
    if (fname):
        impl_path = fname


    #je vybrana nejaka cesta
    if (len(impl_path) > 0):
        try:
            handle = open(impl_path,"w+")
            out = make_meteo_write_string(meteo)
            print out
            handle.write(out)
            handle.close()
        except IOError:
            print "Nelze otevrit soubor %s pro zapis zdrojoveho clenu" % impl_path


def make_meteo_write_string( meteo):
    s = ""

    s += meteo.title + "\n"
    for i in range(meteo):
        s += files_tools.make_string("%3.2f" % meteo.data[i,0], 8, 1)
        s += files_tools.make_string("%4.2f" % meteo.data[i,1], 8, 1)
        s += files_tools.make_string("%5.2f" % meteo.data[i,2], 8, 1)
        s += files_tools.make_string("%3.2f" % meteo.data[i,3], 8, 1)
        s += files_tools.make_string(meteo.get_cat_stab(i), 4, 1)
        s += files_tools.make_string("%2.2f" % meteo.data[i,5], 8, 1)
        s += "\n"

    return s

if __name__ == "__main__":
    
    path_to_meteo = "meteo_wea"+os.sep+"batch_3"
    # f_name = "ETE_vystreleni regul- svazku_2005.svn"
    # load_source_term(path_to_sourceterms, f_name)
    
    files_all = os.listdir(path_to_meteo)
    wea_files = []
    
    print "Insertion of source terms into DB"
    print "Following source terms will be inserted:"
    for file in files_all:
        if file.endswith("wea") and not file.startswith("."): #the dot is because of .svn dir:)
            print file
            wea_files.append(file)
            
    print "Available users:"
    admin_tools.print_users()
    username = raw_input("Enter username: ")
    
    for file in wea_files:
        meteo = load_meteo(path_to_meteo, file, username=username)
        dtm.save_meteo(meteo)
           
    