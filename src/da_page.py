# -*- encoding: UTF-8 -*-
import cherrypy
import copy
import config as cf
import web_html #comtains methods for web HTML rendering
#import auth
#from mako.template import Template
from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common

class Assimilation:
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    @cherrypy.expose
    def index(self): #index of main DSS GUI for common user
        return self.render_page()
    
    @cherrypy.expose
    def render_page(self):
        #navibar with buttons
        nav = web_html.navibar(active=4) #renders navibar according to login state
        bod = web_html.account_body()
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
