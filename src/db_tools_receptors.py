# -*- encoding: UTF-8 -*-
import admin_tools as at
import cPickle
import locking
import time
import pymongo
import bson
from tools import mo2ma, ma2mo
#getting collections..
db = at.dbs['db']
db_users = at.dbs['db_users']
db_tasks = at.dbs['db_tasks']
db_service = at.dbs['db_service']
db_workers = at.dbs['db_workers']
db_source_terms = at.dbs['db_source_terms']
db_meteo = at.dbs['db_meteo']
db_receptors = at.dbs['db_receptors']


def get_list_of_receptors(username):
    """
    return list of receptor configurations
    """
    receptors = db_receptors.find({"username": username})
    
    return receptors

    
def get_receptors_count(username):
    """
    returns count of receptor configurations of a user
    """
    return get_list_of_receptors(username).count()


def save_receptors(receptors):
    """
    saves receptor configuration
    """
        
    res = ""
    
    try:
        db_receptors.insert(receptors)
        res = "Receptors successfully saved!"
    except Exception, e:
        print "Error inserting receptors", e
        res = "Receptors not inserted", e
    finally:
        return res

def get_receptors_of_id(_id):
    """
    return receptors configuration DB entry of given _id
    """
    ret = db_receptors.find_one({"_id": bson.objectid.ObjectId(_id)})  
    return ret
    

def delete_receptors(_id):
    """
    deletes receptors of a given _id from collection
    """
    #st = db_source_terms.find_one({"_id": _id})
    print "deleting receptors..."
    db_receptors.remove({"_id": bson.objectid.ObjectId(_id)})
    
    