# -*- encoding: UTF-8 -*-
import db_tools as dt
import admin_tools as at
from db_tools import ma2mo #ndarray serialization to string - can be inserted into mongo
import time
import numpy

if __name__ == "__main__":
    #dt.clear_mongo()

    #some testing users
    dt.insert_user(username="petrpecha", password="pepepe", email="pecha@utia.cas.cz", user_type="user" )
    dt.insert_user(username="emilka", password="pepepe", email="email@email.cz", user_type="user" )
    dt.insert_user(username="tester1", password="tester1", email="email@email.cz", user_type="user" )
    
    #printing of reuslts
    at.print_users()
    at.print_tasks()
    
