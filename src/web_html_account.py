# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import db_tools_users

def get_key(dic, key):
    """
    returs value of a demanded key if present or None
    """
    if dic.has_key(key):
        if dic[key] != None:
            return dic[key]
        else:
            return ""
    else:
        return ""

##########################################################################################################################
### ACCOUNT BODY
##########################################################################################################################

def account_body():
    """
    renders body of account page
    """
    username = cherrypy.session.get(cf.SESSION_KEY)
    
    found_user = db_tools_users.find_user(username)
    ui_username = username
    if found_user:
        ui_username = "%s %s" % (found_user['first_name'],found_user['surname'])

    ll = db_tools.get_last_login(username)
    user_details = get_user_details_table(username)

    ret = """
            <div class="marketing">
              <h1>Welcome %s!</h1>
            <p class="marketing-byline">Last login: %s</p>

            <hr>
            <div class="page-header">
                <h1>Account details <small>Registration details and contact information</small></h1>
            </div>
            %s
    """ % (ui_username, ll, user_details)


    return ret

def get_user_details_table(username):
    """
    returns HTML table with user details ??? possibility of editation?
    """
    user = db_tools.get_user(username)

    ret = """
      <div class="row-fluid">
       <div class="span7">
        <table class="table table-bordered table-striped">
         <tbody>
    """

    keys = user.keys()

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Username", get_key(user, "username") if not db_tools_users.is_guest(user) else "guest")

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Last login", get_key(user, "last_login2"))

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Name", get_key(user, "first_name") + " " + get_key(user, "middle_name") + " " + get_key(user, "surname"))

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Institute", get_key(user, "institute"))

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Address", get_key(user, "street1") + "<br>" + get_key(user, "street2") + "<br>" + get_key(user, "city") + "<br>" + get_key(user, "country"))

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("E-mail", get_key(user, "email"))

    ret += """
          <tr>
            <td><b>%s</b></td><td>%s</td>
          </tr>
    """ % ("Account created", get_key(user, "created"))

    ret += """
         </tbody>
        </table>
       </div>
      </div>
    """
    return ret