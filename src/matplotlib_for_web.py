from matplotlib.pyplot import *
from matplotlib import pyplot as plt

"""
ax = axes([0,0,1,1], frameon=False)
ax.plot(range(10))
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.set_aspect('equal')
show()
"""

fig = plt.figure(figsize=(20,20))
ax = fig.add_axes([0., 0., 1., 1.])
ax.plot(range(10))
show()