# -*- encoding: UTF-8 -*-
import cherrypy
import copy
import config as cf
import web_html #comtains methods for web HTML rendering
import web_html_task #tasks - celection of task type
import web_html_task_disp #task type HARP dispersion
import web_html_puff #task puff duspersion
import web_html_HARP_asim #task puff assimilation

from mako.lookup import TemplateLookup
import simplejson as sj
import numpy
from db_tools import ma2mo, mo2ma #ndarray serialization to string - can be inserted into mongo
import db_tools as dt
import tools
import validators
import db_tools_source_terms as dtst
import db_tools_meteo as dtm
import db_models
import cherrypy
import time
import web_html_receptors_editor

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common
SESSION_KEY = cf.SESSION_KEY


class Task:
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    @cherrypy.expose
    def index(self): #index of main DSS GUI for common user
        #navibar with buttons
        head = ""
        nav = web_html.navibar(active=3) #renders navibar according to login state
        bod = web_html_task.task_body().decode("windows-1252").encode("utf8")
        fot = web_html.get_footer()
                
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod,bottom_scripts="", footer=fot)
    

    @cherrypy.expose
    def create(self, **kwargs): #task type: 0 - 2...
        kwargs["username"] = cherrypy.session[SESSION_KEY]
        
        nav = web_html.navibar(active=3) #renders navibar according to login state
        task_type = int(kwargs.get("task_type", 0)) #in the case of alder HARP simulations, task_type not stored in DB
        # JS bottom scripts
        bottom_scripts = ""
        
        if task_type == 0: #harp simulation
            head = web_html_task_disp.head()
            bod = web_html_task_disp.task_create_body_0(**kwargs)
        elif task_type == 1: #harp assimilation
            head = web_html_HARP_asim.head()
            bod = web_html_HARP_asim.task_create_body_0(**kwargs)            
        elif task_type == 2: #puff simulation
            head = web_html_puff.head()
            bod = web_html_puff.task_create_body_0(**kwargs)
            bottom_scripts = web_html_puff.bottom_scripts()
        elif task_type == 9: #puff simulation
            head = web_html_receptors_editor.head()
            bod = web_html_receptors_editor.task_create_body_0(**kwargs)
        
        fot = web_html.get_footer()
        
        tmpl = lookup.get_template("basic.html")
        #rendering of stuff to template... ooooou jeeee
        return tmpl.render(head=head, navibar=nav, main_body=bod, bottom_scripts=bottom_scripts, footer=fot)
    
    
   
    @cherrypy.expose
    def update_meteo_table(self, **kwargs):
        """
        return adjusted meteotable
        previous values do not stay unchanged during resize 
        """   
        cherrypy.response.headers['Content-Type'] = 'application/json'
                
        meteo_table = web_html_task_disp.generate_meteo_table(hours_count=int(kwargs["hours_count"]))
        return sj.dumps({"meteo_table": meteo_table})
 
 
    @cherrypy.expose
    def insert_puff_task(self, **kwargs):
        """
        processes new puff tasks and if succeeds, inserts the task into queue and redirects to results page 
        we do not use forms any more, everything is send via post like in the 21st centrury
        """ 
        print "Inserting new puff task!"
        isValid = True
        
        
        try:
        
            #server side validation will be here
            #bla bla 
            #validy 
            #validy
            #
                     
            meteo_hours_count = int(kwargs["meteo_hours_count"])
            meteo = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["meteo[]"]), (meteo_hours_count, 4)) )
            segments_count = int(kwargs["segments_count"])
            activities = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["activities[]"]), (segments_count, 1)) )        
            rel_heights = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["rel_heights[]"]), (segments_count, 1)) )
            desc = kwargs["description"]
            
            lat = float(kwargs["lat"])
            lon = float(kwargs["lon"])        

            
            simulation_length = float(kwargs["simulation_length"])
            output_grid_step = float(kwargs["output_grid_step"])
            output_domain_size_x = float(kwargs["output_size_x"])
            output_domain_size_y = float(kwargs["output_size_y"])                                             
            duration_of_release = float(kwargs["duration_of_release"])
            inner_time_step = float(kwargs["inner_time_step"])
            puff_sampling_step = float(kwargs["puff_sampling_step"])
            print "Task valid"   
        
        except:
            print "task not valid"
            isValid = False
        
        
        if (isValid): #entered data are valid, we can submit task
            print "Task valid, inserting... %s" % desc
            username = cherrypy.session.get(cf.SESSION_KEY)
            
            dt.create_task(username = username,
                   usergroup_of_task_owner = dt.get_usergroup_of_user(username),        
                   description = desc,
                   task_type = 2, #task type: 0 - dispersion HARP, 1 - .....                     
                   task_input = { "meteo": ma2mo(meteo),
                                  "nuclides": ["Ar41"],
                                  "activities": ma2mo(activities),
                                  "rel_heights": ma2mo(rel_heights),
                                  "description": desc,
                                  "lat": lat,
                                  "lon": lon,
                                  "output_grid_step": output_grid_step,
                                  "output_domain_size_x": output_domain_size_x,
                                  "output_domain_size_y": output_domain_size_y,
                                  "simulation_length": simulation_length,
                                  "duration_of_release": duration_of_release,
                                  "inner_time_step": inner_time_step,
                                  "puff_sampling_step": puff_sampling_step 
                                  },
                   task_output = None) 
              
    
        
    @cherrypy.expose
    def load_source_term(self, **kwargs):
        """
        reutnr HTML table of a selected source term from DB
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        st = dtst.get_source_term_of_id(kwargs["_id"])
        print st
        rel_heights = map(float, st["rel_heights"][:,0])
        activities = map(float, st["activities"][:,0])
        segments_count = st["segments_count"]
        puff_sampling_step = st["puff_sampling_step"]
        return sj.dumps({"rel_heihts": rel_heights, 
                         "activities": activities,
                         "segments_count": segments_count,
                         "puff_sampling_step": puff_sampling_step})
        
    @cherrypy.expose
    def load_st_json(self, **kwargs):
        """
        Returns loaded source term data in pure json form.
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        #Prepare configuration modal dialog
        st = dtst.get_source_term_of_id(kwargs["_id"])
        nucl_table = web_html_task_disp.generate_nucl_table(**st)
        segments_combo = web_html_task_disp.generate_segment_combo(**st)
        tmpl = lookup.get_template("new_task_modal_nucl_table.html")
        nucl_modal = tmpl.render(nucl_table=nucl_table, segments_combo=segments_combo)
        
        # Prepare JSONable data model
        src_term_data = web_html_task_disp.get_source_and_segment_json(**st)
        
        return sj.dumps({"data" : src_term_data,"nucl_modal": nucl_modal})
        
    @cherrypy.expose
    def delete_source_term(self, **kwargs):
        """
        deletes source term of a given _id from database
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        d = {}
        
        try:
            dtst.delete_source_term(kwargs["_id"])
            msg = "OK, deleted"            
        except Exception, e:
            msg = "Error, not deleted", e
        finally:
            print msg
            st_table = self.update_st_load_form()
            d = {"msg": msg, "st_table": st_table}
            return sj.dumps(d)       
            
    @cherrypy.expose
    def save_source_term(self, **kwargs):    
        """
        saving a source term from JS and stores it in DB
        """ 
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
    
        username = cherrypy.session.get(cf.SESSION_KEY) 
        isValid = True
                    
        try:
            #gathering data
            #TO DO srver-side validy validy...    
            segments_count = int(kwargs["segments_count"])
            activities = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["activities[]"]), (segments_count, 1)) )
            rel_heights = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["rel_heights[]"]), (segments_count, 1)) )
            description = kwargs["description"]
            simulation_length = kwargs["simulation_length"]
            duration_of_release = kwargs["duration_of_release"]
            inner_time_step = kwargs["inner_time_step"]
            puff_sampling_step = kwargs["puff_sampling_step"]             
            
            print "Saving source term:"
            print segments_count
            print activities
            print rel_heights
            print description
        except:
            isValid = False
                
        if isValid: #OK, data are, valid, wil be entered into DB...
            print "Source term valid."
            st = {"username": username,
                  "segments_count": segments_count,
                  "activities": activities,
                  "rel_heights": rel_heights,
                  "description": description,
                  "modified": time.time(),
                  "simulation_length" : simulation_length,
                  "duration_of_release" : duration_of_release,
                  "inner_time_step" : inner_time_step,
                  "puff_sampling_step" : puff_sampling_step                      
                 }  
            
            msg = dtst.save_source_term(st) 
            st_table = self.update_st_load_form()
            #we must return table, because in case of validatino error we want to remove colord borders
            return sj.dumps({"msg": msg, "st_table": st_table, "status": 0}) #we return OK status
                         
        else: #data not valid, status 1 returned 
            msg = "There were errors in the source term!"
            print "Source term not valid"  
            print msg
            return sj.dumps({"msg": msg, "status": 1}) #we return 1 status - error during validation, must be corrected
    
    def array_to_chararray(self, array):
        """
        converts numpy array to chararray
        """
        m = len(array)
        n = len(array[0])
        
        ret = numpy.chararray((m, n), itemsize=20)
        
        for i in range(m):
            for j in range(n):
                ret[i,j] = array[i][j]
                
        return ret
    
    
    @cherrypy.expose    
    def update_st_load_form(self):
        """
        return HTML table with list of saved tasks
        """
        username = cherrypy.session.get(cf.SESSION_KEY) 
        st_table = web_html_task_disp.get_source_term_table(username)
        return st_table
        
        
    @cherrypy.expose
    def load_meteo(self, **kwargs):
        """
        reutnr HTML table of a selected meteo from DB
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        
        meteo = dtm.get_meteo_of_id(kwargs["_id"])
        meteo_table = web_html_task_disp.generate_meteo_table(**meteo) #hours_count, meteo=None, meteo_validator_result=None)
        
        return sj.dumps({"meteo_table": meteo_table}) #we return multiple values, we must return JSON

    @cherrypy.expose
    def delete_meteo(self, **kwargs):
        """
        deletes meteo of a given _id from database
        """
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON
        d = {}
        
        try:
            dtm.delete_meteo(kwargs["_id"])
            msg = "OK, deleted"            
        except Exception, e:
            msg = "Error, not deleted", e
        finally:
            print msg
            d = {"msg": msg}
            return sj.dumps(d)        
        
    @cherrypy.expose
    def save_meteo(self, **kwargs):  
        """"
        saves meteo to DB
        """     
        cherrypy.response.headers['Content-Type'] = 'application/json' #we return multiple values, we must return JSON     
        
    
        username = cherrypy.session.get(cf.SESSION_KEY) 
        #kwargs["username"] = username  
        
        #gathering data
        meteo_hours_count = int(kwargs["meteo_hours_count"])
        meteo_orig = self.array_to_chararray( numpy.reshape(numpy.array(kwargs["meteo[]"]), (meteo_hours_count, 4)) )
        description = kwargs["description"]
        
        print meteo_orig
        #creation of validation
        meteo_validator = validators.Validator(meteo_orig)
        
        #validation
        meteo_validator.validate_meteo()
        
        #decision
        isValid = True
        if (not meteo_validator.is_valid()):
            isValid = False
            
        if isValid: #OK, data are, valid, wil be entered into DB...       
            meteo = {"username": username, #username
                     "meteo": meteo_validator.conversion, #numpy.array: must be pickeld before insertion to DB
                     "hours_count": meteo_hours_count,
                     "description": description,
                     "modified": time.time()
                     }   
            
            meteo_table = web_html_task_disp.generate_meteo_table(**meteo) #must be here, inside insertion method is converted into pickel string!! - mutable
            msg = dtm.save_meteo(meteo) 
            
            #we must return table, because in case of validatino error we want to remove colord borders
            return sj.dumps({"msg": msg, "meteo_table": meteo_table, "status": 0}) #we return OK status    

        else: #data not valid, we return HTML table with data and highlihting of errors   
            print "Not valid"
            meteo = {"meteo": meteo_validator.orig_input, #numpy.array: must be pickeld before insertion to DB
                     "meteo_validator_result": meteo_validator.validator_result,
                     "hours_count": meteo_hours_count,
                     }  
                  
            meteo_table = web_html_task_disp.generate_meteo_table(**meteo)
            msg = "Save failed, please check entered data and try again."
            return sj.dumps({"msg": msg, "meteo_table": meteo_table, "status": 1}) #we return 1 status - error during validation, must be corrected
        """
        except Exception, e:
            print "Error", e
            return sj.dumps({"msg": "Error:(", "status": 1})   
        """
    @cherrypy.expose    
    def update_meteo_load_form(self):
        """
        return HTML table with list of saved tasks
        """
        username = cherrypy.session.get(cf.SESSION_KEY) 
        meteo_table = web_html_task_disp.get_saved_meteo_table(username)
        return meteo_table