# -*- encoding: UTF-8 -*-
from hashlib import md5
import time

def get_kwarg(kwargs, key):
    """
    returs value of a demanded key if present or None otherwise
    """
    if kwargs.has_key(key):
        return kwargs[key]
    else:
        return None

def user_DB_model(kwargs):
    """
    db model of a single entry to db_users collection
    user id is incremented
    """

    #md5 hash od password
    md5_pass = md5(kwargs["password"]).hexdigest()

    #username is user ID
    #state: 0 - non-active, 1 - active, maybe some other...
    
    d = {"username": kwargs["username"],
         "md5_pass": md5_pass,
         "first_name": get_kwarg(kwargs, "first_name"),
         "middle_names": get_kwarg(kwargs, "middle_names"),
         "surname": get_kwarg(kwargs, "surname"),
         "institute": get_kwarg(kwargs, "institute"),
         "street1": get_kwarg(kwargs, "street1"),
         "street2": get_kwarg(kwargs, "street2"),
         "city": get_kwarg(kwargs, "city"),
         "state": get_kwarg(kwargs, "state"),
         "country": get_kwarg(kwargs, "country"), 
         "email": kwargs["email"],
         "state": 1, #state - active user...
         "user_group": get_kwarg(kwargs, "user_group"), #user group - for rezervation of workers etc...
         "user_type": kwargs["user_type"], #user type - for adrministration of access rights
         "created":  time.time(), #we use epoch - can be converted into arbitrary time... time.strftime("%d %b %Y %H:%M:%S UTC", time.gmtime()),
         "last_login": None, #current last login, next login will be stored the current time a this will be the next... moved to last_login2
         "last_login2": "First login", #true last login       
         }
         
    return d


def task_DB_model(kwargs):
    """
    db model of a signle entry to the task queue
    the queue is consumed by workers - processes ran on computation machines
    """

    d = {"username": kwargs["username"], #author of the task
         "usergroup_of_task_owner": kwargs["usergroup_of_task_owner"],
         "description": kwargs["description"], #task description
         "task_id": kwargs["task_id"],
         "task_type": kwargs["task_type"], #task type, 0 - dispersion HARP, 10 - PUFF MODEL TAKS - added during openshift campaign:)
         "task_input": kwargs["task_input"],
         "task_output": kwargs["task_output"],
         "task_priority": 0,
         "created": time.time(), #we insert epoch - it can be converted into anything time.strftime("%d %b %Y %H:%M:%S UTC", time.gmtime()),
         "task_status": 0, #task status: 0 - free - queeing, not solved, 1 - under solution, 2 - solved and closed, 3 - error occured

         #data related to task computation
         "start_of_computation": None, #time when the task computation started
         "end_of_computation": None, #time of computation finish
         "computation_time": None, #time spend on task computation

         #data from worker
         "worker_data": None, #whole worker dictionary
         "task_log": [], #array of lines
         "task_percentage": 0 #float 0. - 100.
        }

    return d

#METHOD NOT USED - IS INDUCED task_page, ON ITS SOURCE TERM SAVE IT CREATES THIS DICTIONARY
def source_term_db_model(**kwargs):
    """
    db model of a source term
    """
    
    d = {"username": kwargs["username"], #username
         "activities": kwargs["activities"], #numpy.array: must be pickeld before insertion to DB
         "segment_properties": kwargs["segment_properties"], #numpy.array: must be pickeld before insertion to DB
         "segments_count": kwargs["segments_count"],
         "nuclides": kwargs["nuclides"],
         "description": kwargs["description"],
         "modified": time.time()
         }
    
    return d

def meteo_db_model(**kwargs): #METHOD NOT USED - IS INDUCED task_page, ON ITS METEO SAVE IT CREATES THIS DICTIONARY
    """
    db model of a source term
    """
    
    d = {"username": kwargs["username"], #username
         "meteo": kwargs["meteo"], #numpy.array: must be pickeld before insertion to DB
         "hours_count": kwargs["hours_count"],
         "description": kwargs["description"],
         "modified": time.time()
         }
    
    return d

#METHOD NOT USED - IS INDUCED BY THE WORKER, ON ITS START IT CREATES THIS DICTIONARY
def worker_DB_model(kwargs):
    """
    db model of a signle worker
    
    d = {"worker_username": kwargs["worker_username"],
         "worker_id": kwargs["worker_id"], #not used, unique should be worker_username identifiing particulate worker
         "worker_ip": kwargs["worker_ip"],
         "worker_machine_name": kwargs["worker_machine"], #domanin name of the workers physical machine, multiple workers can share one maschime
         "worker_creation": time.time(),
         "worker_type": kwargs["worker_type"], #type of the worker, 0 - dispersion HARP, 1 - .....
         "worker_last_ping": None, #the time of last worker's query on a free task 
         "worker_ping_interval": None,
         "worker_DEFAULT_PING_INTERVAL": kwargs["worker_default_ping_interval"], #real ping interval set in the worker
         "worker_state": 0, #state of a worker: 0 - idle, 1 - computing, 2 - ??down?? - whatever
         "worker_dedicated": None, #is worker dedicatd to a particular array of users? None if not, [user1, user2, ...] otehrwise
         ##"worker_key": kwargs["worker_key"], #key associated to a worker in order to be alble to authenticate workers identity
         #following proprties are related to platform
         "worker_architecture": kwargs["worker_architecture"], #workers platform.architecture()
         "worker_node": kwargs["worker_node"], 
         "worker_platform": kwargs["worker_platform"],
         "worker_machine": kwargs["worker_machine"],
         "worker_processor": kwargs["worker_processor"],
         "worker_system": kwargs["worker_system"],
         "worker_version": kwargs["worker_version"],
         "worker_log": [], #array of lines
         "worker_resources": {},
         "worker_task_percentage": 0
        }
    
    return d
    """
    pass

def receptors_DB_model(username, description, data):
    """
    DB model of receptors configuration entry
    """
    d = {"username": username,
         "description": description,
         "data": data,
         "modified": time.time()
         }
    
    return d
    
    







