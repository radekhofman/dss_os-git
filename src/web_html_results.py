# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import web_html_results_disp
import tools
from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
##########################################################################################################################
### RESULTS BODY
##########################################################################################################################
def head():
    """
    geenrates JS for viewing of task results on google map in modal window
    """
    
    #<script type="text/javascript" src="http://geoxml3.googlecode.com/svn/branches/polys/geoxml3.js"></script>
    ret = """
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
          <script type="text/javascript" src="/scripts/thirdparty/ProjectedOverlay.js"></script>
          <script type="text/javascript" src="/scripts/thirdparty/geoxml3.js"></script>
          <script type="text/javascript" src="/scripts/app/results.js"></script>    
    """ 
    
    return ret

def results_body(first=0, detail_task_id=None, num=None, task_status=None):
    """
    renders body of account page
    """
    username = cherrypy.session.get(cf.SESSION_KEY)
    size = 5 #in task table will be always 5 tasks
    
    #tasks = generate_users_task_table(username, first, size) #we always show 10 latest tasks since the first given
    controls = generate_user_task_table_controls(username, first, size)
    label, task_details = generate_details_of_selected_task(username, detail_task_id, num, task_status)
        
    tmpl = lookup.get_template("results_base.html")
    ret = tmpl.render(first=first, size=size, controls=controls, label=label, task_details=task_details)
    
    return ret


def generate_user_task_table_controls(username, first, size):
    """
    generates buttons for viewing older/newer tasks
    """
    
    tc = db_tools.get_tasks_count_of_user(username)
    
    start = first - size
    stop = first + size
    
    dis_start = """
          <li class="previous">
            <a href="/results?first=%d">&larr; Newer</a>
          </li>
          """ % start

    dis_end = """          
          <li class="next">
            <a href="/results?first=%d">Older &rarr;</a>
          </li> 
          """ % stop    

    if (first-size < 0):
        start = 0
        dis_start = ""
    if (first+size >= tc):
        stop = tc
        dis_end = ""
                 
    ret = ""
    
    ret += """
        <ul class="pager">
          %s  
          %s
        </ul>
        """ % ( dis_start, dis_end)
        
    return ret 



def generate_users_task_table(username, first, size):
    """
    generates table summing up users current and historic tasks
    """
    #username = cherrypy.session.get(cf.SESSION_KEY)
    
    tasks = db_tools.get_tasks_of_user_slice(username, first, size=size)
    
    
    ret = """
        <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No.</th>
                <th>Date</th>
                <th>Description</th>                
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>        
            """
     #task No. is task ID!!! it is just for user !!!  
     
    index = first+1
    
    t_type = ""    
     
    #we iterate over users tasks
    for task in tasks: 
        
        status = task["task_status"]
        t_status = get_status_bagde(status)
            
        t_type = get_task_type_badge(task["task_type"])    
           
         
         #dropdown button with action for given task
        t_actions = """
            <div class="btn-group">
              <button class="btn btn-primary btn-small dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
              """
        if (status in [0,1,2,3]): #possibly ... :D in case of queued task can user inspect at least ther inputs ...
            t_actions += """
                        <li><a href="/results?first=%d&detail=%s&num=%d&task_status=%d">Task details</a></li>
                        <li class="divider"></li>
                """ % (first, task["task_id"], index, task["task_status"])
        t_actions += """<li><a href="/results/del_task?first=%d&task=%s">Delete task</a></li>
              </ul>
            </div><!-- /btn-group -->         
         """ % (first, task["task_id"])
        ret += """
              <tr>
                <td>%d</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
              </tr>
            """ % (index, tools.convert_epoch_to_time(task["created"]), task["description"], t_status, t_actions)
         
        index += 1
            
    ret += """  
            </tbody>  
        </table>
        """
    return ret

def get_task_type_badge(task_type):
    """
    return color badge expressing task status in texto form
    """
    t_type = ""
    if (task_type == "assimilation"):
        t_type = """<span class="label label-success">Assimilation</span>"""
    if (task_type == "dispersion"):
        t_type = """<span class="label label-warning">Dispersion simulation</span>"""
    return t_type   
    
def get_status_bagde(status):
    """
    returns HTML code for color bage with status accorgint ot integer expressing task status
    """
    t_status = ""
    if (status == 0):
        t_status = """<span class="label label-warning">Queuing</span> """
    elif (status == 1):
        t_status = """<span class="label label-info">Computing</span> """
    elif (status == 2):
        t_status = """<span class="label label-success">Done</span> """
    elif (status == 3):
        t_status = """<span class="label label-important">Computation failed!</span> """  
        
    return t_status
    
def generate_details_of_selected_task(username, task_id, num, task_status):
    """
    return summary on results of selected task
    """
    res_tabs = "No task selected."
    tdet = ""
    

    if (task_id != None):
        res_tabs = task_tab_results(username, int(task_id), task_status) #tabs..
    
    if (num != None):
        tdet = "of task no. %d" % int(num)
                 
    return tdet, res_tabs

def task_tab_results(username, task_id, task_status):
    """
    return tabs with summary of task inputs and results
    """
    
    task = db_tools.get_task_of_given_id(username, int(task_id))
    
    ret = web_html_results_disp.get_dipersion_task_details(task, task_status)
    
    
    return ret


