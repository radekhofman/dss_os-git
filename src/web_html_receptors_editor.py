# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import db_tools_source_terms as dtst
import db_tools_meteo as dtm
import tools
from mako.lookup import TemplateLookup
import config as cf
import numpy
import types

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])

##########################################################################################################################
### TASK BODY
##########################################################################################################################


def head():
    ret = """

    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuprwbqdqmQdJ4pCNtUEklP0xnNyb-V7Q&sensor=false"></script>

    
    <script src="/scripts/thirdparty/styledmarker.js"></script>
    <script src="/scripts/app/receptors/map.tools.js"></script>
    <script src="/scripts/app/receptors/main.js"></script>
    <script src="/scripts/app/receptors/map.js"></script>
    <script src="/scripts/app/receptors/map.modal.js"></script>
    <script src="/scripts/app/receptors/map.table.js"></script>
    <script src="/scripts/app/receptors.js"></script>

      
    """
    #<script type="text/python" src="/scripts/new_task_puff.py"></script>     
    #<script type="text/javascript" src="/scripts/app/new_task_puff.js"></script>
    
    return ret


def task_create_body_0(**values):   #HTML page with forms for runnig of new taskof type 1
    #dict values could contains all possible parameters of the release which could be loaded into forms...
    #notepad with tabs with form for initialization of a new task
    ret = ""
    
    if (values.has_key("msg")):
            ret += """<div id="msg" class="alert alert-error"><center><h3> %s </h3></center></div>""" % values["msg"]
    
    tmpl = lookup.get_template("receptors_editor.html")
    ret += tmpl.render()
    
    tmpl = lookup.get_template("addMeterModal.html")
    ret += tmpl.render()

    tmpl = lookup.get_template("loadConfigurationModal.html")
    ret += tmpl.render()
    
    tmpl = lookup.get_template("saveConfigurationModal.html")
    ret += tmpl.render()    
   
    return ret



