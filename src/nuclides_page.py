'''
Created on Aug 24, 2013

Provides AJAX interface for nuclides and nuclides groups operations.

'''

import simplejson as sj
import cherrypy
from mako.lookup import TemplateLookup
import copy
import config as cf

import web_html_nucl_groups

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
cp_conf_common = cf.cp_conf_common
SESSION_KEY = cf.SESSION_KEY

class Nuclides:
    
    # all methods in this controller (and subcontrollers) is
    # open only to registered users
    cp_conf = copy.deepcopy(cp_conf_common)
    cp_conf['auth.require'] = [] #extension of basic cofig of some special items
    _cp_config = cp_conf
    
    @cherrypy.expose
    def load_group_json(self, **kwargs):
        kwargs["username"] = cherrypy.session[SESSION_KEY]
        return web_html_nucl_groups.get_groups_json(**kwargs)
    
    @cherrypy.expose
    def create_group_json(self, **kwargs):
        kwargs["username"] = cherrypy.session[SESSION_KEY]
        return web_html_nucl_groups.create_group_json(**kwargs)
    
    @cherrypy.expose
    def delete_group_json(self, **kwargs):
        kwargs["username"] = cherrypy.session[SESSION_KEY]
        return web_html_nucl_groups.delete_group_json(**kwargs)
    
