# -*- encoding: UTF-8 -*-
import cherrypy
import config as cf
import db_tools
import db_tools_source_terms as dtst
import db_tools_meteo as dtm
import tools
from mako.lookup import TemplateLookup
import config as cf
import numpy
import types
import simplejson as sj

lookup = TemplateLookup(directories=[cf.HTML_TEMP_D])
status_labels = ["border-color: green; border-width: 2px;" ,  "border-color: red; border-width: 2px;"]
##########################################################################################################################
### TASK BODY
##########################################################################################################################


def head():
    ret = """
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="/scripts/app/new_task.js"></script>
        <script type="text/javascript" src="/scripts/common/validations.js"></script>
        <script type="text/javascript" src="/scripts/disp_task/source_term/configure_ui.js"></script>
        <script type="text/javascript" src="/scripts/disp_task/source_term/configure_model.js"></script>
    """
    return ret


def task_create_body_0(**values):   #HTML page with forms for runnig of new taskof type 1
    #dict values could contains all possible parameters of the release which could be loaded into forms...
    #notepad with tabs with form for initialization of a new task
    ret = ""
    
    if (values.has_key("msg")):
            ret += """<div id="msg" class="alert alert-error"><center><h3> %s </h3></center></div>""" % values["msg"]
    
    tab1 = release_scenario(**values)
    tab2 = source_term(**values)
    tab3 = meteo(**values)
    
    tmpl = lookup.get_template("new_task_base.html")
    ret += tmpl.render(tab1=tab1, tab2=tab2, tab3=tab3)
    
    #now we append a modal form window with configuration of nuclides 
    nucl_table = generate_nucl_table(**values)
    segments_combo = generate_segment_combo(**values)
    tmpl = lookup.get_template("new_task_modal_nucl_table.html")
    
    ret += tmpl.render(nucl_table=nucl_table, segments_combo=segments_combo)
    
    
    #now we append a modal window with map of source location
    tmpl = lookup.get_template("new_task_modal_source_location_map.html")
    ret += tmpl.render()
   
    #now we add modal windows with setting of meteotable
    tmpl = lookup.get_template("new_task_modal_meteo_settings.html")
    ret += tmpl.render()
    
    #now we add modal windows with form for loading of source terms
    
    #placeholder of the load for ajax update - otherwise saved STs woun't be shown without refresh
    st_table = get_source_term_table(values["username"])
    tmpl = lookup.get_template("new_task_modal_load_st.html")
    ret += tmpl.render(st_table=st_table)
    
    #now we add modal windows with form for saving of source terms
    tmpl = lookup.get_template("new_task_modal_save_st.html")
    ret += tmpl.render()

    #placeholder of the load for ajax update - otherwise saved meteos will not be shown without refresh
    meteo_table = get_saved_meteo_table(values["username"])#get_st_load_table(values["username"])
    tmpl = lookup.get_template("new_task_modal_load_meteo.html")
    ret += tmpl.render(meteo_table=meteo_table)
    
    #now we add modal windows with form for saving of source terms
    tmpl = lookup.get_template("new_task_modal_save_meteo.html")
    ret += tmpl.render()    
   
    return ret



################################
#### TABS
################################

def release_scenario(**values): #values - eventual values of the form fields in case of Load task case
    """
    reurn HTML for the firt tab - release scenario
    """
    mandatory_validators = ["lat_validator_result",
                            "lon_validator_result",
                            #"H_validator_result",
                            "inversion_validator_result",
                            "t_ref_validator_result"]
    
    params = None
          
    if values.has_key("params"):
        params = values["params"]
        
        calm = params["calm"]
        if (calm):
            params["calm_modif_yes"] = "checked"
            params["calm_modif_no"] = ""
        else:
            params["calm_modif_yes"] = ""
            params["calm_modif_no"] = "checked"
            
        selected_disp_formula = params["disp_formula"]

        disp_formulas = ["HOSKER (with height correction)", 
                        "HOSKER (without height correction)",
                        "KFK",
                        "HOSKER (with reflection on Hmix)", #nakej hosker
                        "KFK (with reflection on Hmix)", #KFK
                        "SCK/CEN (smooth terrain)"]        
        #disp_formulas = ["KFK/Julich",
        #                 "SCK/CEN Mol, Belgium, smooth terrain"]
        
        disp_formulas_flags = [""] * len(disp_formulas)
        disp_formulas_flags[disp_formulas.index(selected_disp_formula)] = "selected"  
        for i in range(len(disp_formulas)):
            params["disp_form_%d" % i] =  disp_formulas_flags[i]    
            
        #finally we add mandaotry validators due to wrong inputs highlighting  
        for validator in mandatory_validators:
            if not params.has_key(validator):
                params[validator] = ""
            else:
                params[validator] = status_labels[params[validator]]
                  
         
    else:
        #creation of default params for blank new task
        #date = db_tools.get_datetime()
        params = {"experiment_desc": "",
                  #"date": "3",#date,
                  "lat": 49.1817, #default location is NPP Temelin
                  "lon": 14.3754,
                  #"H": 100, #default relesae height
                  "disp_form_0": "", #default dispersion formula
                  "disp_form_1": "",
                  "disp_form_2": "",
                  "disp_form_3": "",
                  "disp_form_4": "",
                  "disp_form_5": "selected",
                  "calm_modif_yes": "checked", #default is modification of calm conditions ON
                  "calm_modif_no": "",
                  "inversion": 0, #default is without inversion
                  "t_ref": 172800 #default is 48hours = 172800 seconds
                   }
 
        for validator in mandatory_validators:
            params[validator] = "" 
 
    tmpl = lookup.get_template("new_task_tab1.html")
    ret = tmpl.render(**params)

    return ret    


def source_term(**values):
    """
    tab with configuration of segments and source term
    """

    tables = get_source_and_segment_tables(**values)   
    
    tmpl = lookup.get_template("new_task_tab2.html")
    ret = tmpl.render(segment_and_source_tables=tables)    
    
    return ret

                      
def plume_depletion():
    return ""
    
def near_standing_buildings():
    return ""


def generate_segment_combo(**values):
    """
    generates combo box for selction of number of segments, sets deafult the selected
    """
    segments = [""] * 12
    if (values.has_key("segments_count")):
        segments[values["segments_count"]-1] = "selected"
    
    s = ""
    for i in range(len(segments)):
        s += "<option %s>%s</option>\n" % (segments[i], i+1)
        
    return s
        

def generate_nucl_table_no_checkboxes(**values):
    """
    generated checkboxed nuclide table for modal configuration
    """
    nucl_list = tools.get_nucl_list()
    selected_nuclides = []
    if (values.has_key("nuclides")):
        selected_nuclides = values["nuclides"]
    
    ret = """
        <table class="table table-bordered table-striped">
        <tbody>     
    """
    
    for i in range(15): #24 full rows
        ret += """<tr>"""

        ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[i*8]))

        ret += """<td>%s</td>""" % (tools.mnl(nucl_list[i*8+1]))
      
        ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[i*8+2]))
     
        ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[i*8+3]))
      
        ret += """<td>%s</td>""" % (tools.mnl(nucl_list[i*8+4]))   
        
      
        ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[i*8+5]))
       
        ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[i*8+6]))
       
        ret += """<td>%s</td>""" % (tools.mnl(nucl_list[i*8+7]))          
        ret += """</tr>"""
            
    ret += """<tr>"""
    
    ret += """<td>%s</td>""" % (tools.mnl(nucl_list[120]))
 
    ret += """<td>%s</td>""" % ( tools.mnl(nucl_list[121]))
   
    ret += """<td>%s</td>""" % (tools.mnl(nucl_list[122]))
    ret += """<td>%s</td><td>%s</td>""" % ("", "")
    #ret += """<td class="td1">%s</td><td>%s</td>""" % ("", "")   
    ret += """</tr>"""            
                
    ret += """
        </tbody>
        </table>  
    """
    
    return ret

def generate_nucl_table(**values):
    """
    generated checkboxed nuclide table for modal configuration
    """
    nucl_list = tools.get_nucl_list()
    selected_nuclides = []
    if (values.has_key("nuclides")):
        selected_nuclides = values["nuclides"]
    
    ret = """
        <table id="nuclTable" class="table table-bordered table-striped">
        <tbody>     
    """
    
    for i in range(24): #24 full rows
        ret += """<tr>"""
        checked_flag = False
        if (nucl_list[i*5] in selected_nuclides):
            checked_flag = True
        ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % ( get_checkbox(i*5, nucl_list[i*5], checked_flag), tools.mnl(nucl_list[i*5]))
        checked_flag = False
        if (nucl_list[i*5+1] in selected_nuclides):
            checked_flag = True
        ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(i*5+1, nucl_list[i*5+1], checked_flag), tools.mnl(nucl_list[i*5+1]))
        checked_flag = False
        if (nucl_list[i*5+2] in selected_nuclides):
            checked_flag = True        
        ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(i*5+2, nucl_list[i*5+2], checked_flag), tools.mnl(nucl_list[i*5+2]))
        checked_flag = False
        if (nucl_list[i*5+3] in selected_nuclides):
            checked_flag = True        
        ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(i*5+3, nucl_list[i*5+3], checked_flag), tools.mnl(nucl_list[i*5+3]))
        checked_flag = False
        if (nucl_list[i*5+4] in selected_nuclides):
            checked_flag = True        
        ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(i*5+4, nucl_list[i*5+4], checked_flag), tools.mnl(nucl_list[i*5+4]))    
        ret += """</tr>"""
            
    ret += """<tr>"""
    checked_flag = False
    if (nucl_list[120] in selected_nuclides):
        checked_flag = True     
    ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(120, nucl_list[120], checked_flag), tools.mnl(nucl_list[120]))
    checked_flag = False
    if (nucl_list[121] in selected_nuclides):
        checked_flag = True    
    ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(121, nucl_list[121], checked_flag), "CO<sub>2</sub>") #tools.mnl(nucl_list[121]))
    checked_flag = False
    if (nucl_list[122] in selected_nuclides):
        checked_flag = True    
    ret += """<td><div style="border: 1px solid black"><table><tr><td>%s</td><td>%s</td></tr></table></div></td>""" % (get_checkbox(122, nucl_list[122], checked_flag), "CO") #tools.mnl(nucl_list[122]))
    ret += """<td>%s</td><td>%s</td>""" % ("", "")
    #ret += """<td class="td1">%s</td><td>%s</td>""" % ("", "")   
    ret += """</tr>"""            
                
    ret += """
        </tbody>
        </table>  
    """
    
    return ret

def get_checkbox(no, id, checked_flag):
    """
    returns checkbox
    """
    checked = ""
    
    if checked_flag:
        checked = "checked"
    
    ret = """
                <input type="checkbox" id="nucl_cb_%d" name="%s" %s> 
    """ % (no, id, checked)
    
    return ret

def get_source_and_segment_json(**kwargs):
    """
    Returns JSON representation of source term configuration.
    """
    nuclides = tools.get_kwarg(kwargs, "nuclides", default_value = []) 
    #sorting of nuclides according to mass number
    nuclides_sorted = tools.sort_nuclides(nuclides)
    
    #Datastructure
    data = {'segments' : [], 'releasedActivities' : []}
    
    #Generate only in case that some nuclides were found
    if (nuclides_sorted != []):
        segments_count = tools.get_kwarg(kwargs, "segments_count", default_value = 0)
        activities = tools.get_kwarg(kwargs, "activities")
        segments_properties = tools.get_kwarg(kwargs, "segments_properties")
        #Generate segments 
        for i in range(segments_count):
            #Prepare new segment definition
            segment = {}
            #Add reference of new segment into list of all segments
            data['segments'].append(segment)
            segment['number'] = i + 1
            
            def toFloat(out_format,segments_properties,order,index):
                """
                Helper function for getting float values from segment properties.
                """
                try:
                    return out_format % float(segments_properties[order, index])
                except:
                    return "%s" % segments_properties[order, index] 
            
            #Load values from segments_properties
            segment['duration'] = toFloat("%3.2f",segments_properties,0,i)
            segment['thermalPower'] = toFloat("%3.2f",segments_properties,1,i)
            segment['verticalVelocity'] = toFloat("%3.2f",segments_properties,2,i)
            segment['releaseHeight'] = toFloat("%4.1f",segments_properties,3,i)
            
        nuc_index = 0
        for nucl in nuclides_sorted:
            segmentValues = []
            activity = {'name':nucl, 'segmentValues' : segmentValues}
            data['releasedActivities'].append(activity)
            for i in range(int(segments_count)):
                segmentValue = {}
                activity['segmentValues'].append(segmentValue)
                if type(activities) != types.NoneType:
                    try:
                        segmentValue['value'] = "%3.2E" % float(activities[nuclides.index(nuclides_sorted[nuc_index]), i])
                    except:
                        segmentValue['value'] = "%s" % activities[nuclides.index(nuclides_sorted[nuc_index]), i]             
                segmentValue['name'] =  nucl  
            nuc_index += 1
    return sj.dumps(data);
        
def get_source_and_segment_tables(**kwargs):
    """
    returns html table with imputs for configuration of the source term   
    """    
    nuclides = tools.get_kwarg(kwargs, "nuclides", default_value = []) 
    segments_count = tools.get_kwarg(kwargs, "segments_count", default_value = 0)
                               
    activities = tools.get_kwarg(kwargs, "activities")
    segments_properties = tools.get_kwarg(kwargs, "segments_properties")
    activities_validator_result = tools.get_kwarg(kwargs, "activities_validator_result") 
    segments_properties_validator_result = tools.get_kwarg(kwargs, "segments_properties_validator_result")
    
    #sorting of nuclides according to mass number
    nuclides_sorted = tools.sort_nuclides(nuclides)
    
    ret = ""
    
    if (nuclides_sorted != []):  
        #we have data  
        table_header = ""
        for i in range(segments_count):
            table_header += "<th>Segment %d</th>" % (i+1)
        
        
        ret += """
            <p>
            <div style="overflow: scroll;">
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th valign="middle" colspan=2>Properties of segments</th>
                    %s
                  </tr>
                </thead>
                <tbody>    
        """ % table_header
        
        ###SEGMENT DURATION
        ret_dura = "<tr><th colspan=2>Segment duration [h]</th>"
        ret_heat = "<tr><th colspan=2>Thermal power [kW]</th>"
        ret_velo = "<tr><th colspan=2>Vertical velocity [m/s]</th>"
        ret_heig = "<tr><th colspan=2>Release height [m]</th>"
        
        #index = 0
        for i in range(segments_count):
            duration = "%3.2f" % 1.0
            heat = "%3.2E" % 0.0
            velocity = "%3.2E" % 0.0
            height = "%4.1f" % 50.
            
            if (type(segments_properties) != types.NoneType): 
                try:
                    duration = "%3.2f" % float(segments_properties[0, i])
                except:
                    duration = "%s" % segments_properties[0, i]   
                
                try:
                    heat = "%3.2E" % float(segments_properties[1, i])
                except:
                    heat = "%s" % segments_properties[1, i]               
                       
                try:
                    velocity = "%3.2E" % float(segments_properties[2, i])
                except:
                    velocity = "%s" % segments_properties[2, i]  

                try:
                    height = "%4.1f" % float(segments_properties[3, i])
                except:
                    height = "%s" % segments_properties[3, i]  
                        
            control_group_duration = ""
            control_group_heat = ""
            control_group_velocity = ""
            control_group_height = "" 
            
    
            if type(segments_properties_validator_result) != types.NoneType:
                control_group_duration = status_labels[segments_properties_validator_result[0,i]]
                control_group_heat = status_labels[segments_properties_validator_result[1,i]]
                control_group_velocity = status_labels[segments_properties_validator_result[2,i]] 
                control_group_height = status_labels[segments_properties_validator_result[3,i]]  
       
                
            ret_dura += """<td><div><input class="span1" style="width: 65px; %s" type="text" value="%s" name="seg_dur_%d"></div></td>""" % (control_group_duration, duration, i)                      
            ret_heat += """<td><div><input class="span1" style="width: 65px; %s" type="text" value="%s" name="seg_heat_%d"></div> </td>""" % (control_group_heat, heat, i)
            ret_velo += """<td><div><input class="span1" style="width: 65px; %s" type="text" value="%s" name="seg_speed_%d"></div> </td>""" % (control_group_velocity, velocity, i)
            ret_heig += """<td><div><input class="span1" style="width: 65px; %s" type="text" value="%s" name="seg_height_%d"></div> </td>""" % (control_group_height, height, i)
            
            #index += 1   
              
        ret_dura += "</tr>"
        ret_heat += "</tr>"
        ret_velo += "</tr>"
        ret_heig += "</tr>"
        
        ret += ret_dura
        ret += ret_heat
        ret += ret_velo   
        ret += ret_heig                        
       
        ret += """     
         </tbody>              
        """
        
        ret += """ 
                <thead>
                  <tr>
                    <th colspan=2>Released activities [Bq]</th>
                    %s
                  </tr>
                </thead>
                <tbody>       
        """ % table_header
        
        
        nuc_index = 0
        for nucl in nuclides_sorted:
            line = ""
            for i in range(int(segments_count)):
                activity = ""
                if type(activities) != types.NoneType:
                    try:
                        activity = "%3.2E" % float(activities[nuclides.index(nuclides_sorted[nuc_index]), i]) #we must keep order given by the original non-sorted nuclides array..
                    except:
                        activity = "%s" % activities[nuclides.index(nuclides_sorted[nuc_index]), i]  #we must keep order given by the original non-sorted nuclides array..             
                
                control_group_activity = ""
                
                if activities_validator_result != None:
                    control_group_activity = status_labels[activities_validator_result[nuclides.index(nuclides_sorted[nuc_index]), i]]  #we must keep order given by the original non-sorted nuclides array..
                    
                line += """<td><div><input class="span1" style="width: 65px; %s" type="text" value="%s" name="nucl_%s_%d"></div></td>""" % (control_group_activity, activity, nucl, i)      
    
            print tools.mnl(nucl)
            ret += "<tr><th>%d</th><th>%s</th> %s </tr>" % (nuc_index+1,tools.mnl(nucl), line) #here, nuclde as a label - transformed into ^{137}Cs format, otherwise it is IDENTIFICATOR, do not transform
            nuc_index += 1
    
        
        ret += """
         </tbody>
        </table>
        </div>
        </p> 
        """ 
    else: #nothing - new task
        ret += """
        <p>
        <h3>No source term specified.</h3>
        </p>
        <p>
        Use buttons
        </p>
        """
        
        
    ret += """
        <input type="hidden" id="segments_count" name="segments_count" value="%d"> 
        <input type="hidden" id="nuclides_count" name="nuclides_count" value="%d">    
    """ % ( int(segments_count), len(nuclides))
    
    for i in range(len(nuclides_sorted)):
        ret += """
        <input type="hidden" name="nuclname_%d" value="%s">
        """ % (i, nuclides_sorted[i]) 
    
    return ret

def meteo(**kwargs):
    """
    renders HTML of meteotable into appropriate template 
    """   
    meteotable = generate_meteo_table(**kwargs)
    
    tmpl = lookup.get_template("new_task_puff_tab3.html")
    ret = tmpl.render(meteotable=meteotable)
    
    return ret

def generate_meteo_table(**kwargs): #meteodata is a numpy ndarray with meteodata.shape=(hours, 4)
    """
    returns HTML for meteotable
    """
    print kwargs.keys()
    
    hours_count = tools.get_kwarg(kwargs, "hours_count", default_value = 0)
    meteo = tools.get_kwarg(kwargs, "meteo")
    meteo_validator_result = tools.get_kwarg(kwargs, "meteo_validator_result")    
     
    ret = "" 
                                                      
    if (hours_count > 0):
        table_header = "<th>Hour</th><th>Wind direction [deg]</th><th>Wind speed [m/s]</th><th>Stab. category</th><th>Precipitation [mm/h]</th>"
    
        ret = """
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    %s
                  </tr>
                </thead>
               </table>
            <div style="height: 400px; overflow: scroll; margin-top: 5px; margin-bottom: 5px;">
    
               <table class="table table-bordered table-striped">
                <tbody>       
        """ % table_header
        
        stabs = ["A", "B", "C", "D", "E", "F"]
        
        wdir = numpy.linspace(0.,140.,15)
        
        for i in range(hours_count):
            if (type(meteo) != types.NoneType):
                mt = meteo[i,:]
                status = ["", "", "", ""]
                if (meteo_validator_result != None): #invalid meteodata with validation matrix                
                    status[0] = status_labels[meteo_validator_result[i,0]]
                    status[1] = status_labels[meteo_validator_result[i,1]]
                    status[2] = status_labels[meteo_validator_result[i,2]]
                    status[3] = status_labels[meteo_validator_result[i,3]]
                    meteo_tuple = ( i+1, status[0], mt[0],  i+1, status[1], mt[1], i+1,  status[2], mt[2], i+1, status[3], mt[3], i+1)
                else: #valid meteodata without validation matrix
                    meteo_tuple = (i+1, "", "%4.1f" % mt[0], i+1, "", "%4.1f" % mt[1], i+1, "", stabs[int(mt[2])],  i+1, "", "%2.1f" % mt[3], i+1)
            else: #no data, we enter default values
                meteo_tuple = (i+1, "", "%4.1f" % wdir[i], i+1,"", "%4.1f" % 3.0, i+1, "", "E", i+1,  "", "%2.1f"% 0., i+1)
    
            ret += """<tr>
                        <td>%d</td>
                        <td><input style="%s" class="span1" type="text" value="%s" name="meteo_wd_%d"></td>
                        <td><input style="%s" class="span1" type="text" value="%s" name="meteo_ws_%d"></td>
                        <td><input class="span1" type="text" style="%s" value="%s" name="meteo_stab_%d"></td>
                        <td><input style="%s" class="span1" type="text" value="%s" name="meteo_prec_%d"></td>
                      </tr>
                   """ % (meteo_tuple)
        
        ret += """
         </tbody>
        </table>
        </div> <!-- scrolling div--> 
        """
    else: #nothing - new task
        ret += """
        <p>
        <h3>No meteo specified.</h3>
        </p>
        <p>
        Use buttons
        </p>
        """   
         
    ret += """
    <input type="hidden" id="meteo_hours_count" name="meteo_hours_count" value="%d">
    """ % hours_count 
    
    return ret


def parse_meteo_args(kwargs):
    """
    parses form and extracts meteorological data
    """
    
    meteo_hours_count = int(kwargs["meteo_hours_count"])
    meteo = numpy.chararray((meteo_hours_count, 4), itemsize=20)
    
    
    for i in range(meteo_hours_count):
        meteo[i, 0] = kwargs["meteo_wd_%d" % (i+1)]
        meteo[i, 1] = kwargs["meteo_ws_%d" % (i+1)]
        meteo[i, 2] = kwargs["meteo_stab_%d" % (i+1)]#["A", "B", "C", "D", "E", "F"].index(kwargs["meteo_stab_%d" % (i+1)].upper())
        meteo[i, 3] = kwargs["meteo_prec_%d" % (i+1)]
        
    return meteo, meteo_hours_count
    
def parse_nuclides_args(kwargs):
    """
    parses form and extracts source term parameters - nuclides and acitivities
    """
    segments_count = int(kwargs["segments_count"])
    nucl_count = int(kwargs["nuclides_count"])
    
    activities_validator = numpy.chararray((nucl_count, segments_count), itemsize=20)   
    #first coordinate: nuclide index
    #second coordinate: segment index
    #third coordinate: validation result 0 - OK, 1 - error 
    
    nuclides = []
    
    keys = kwargs.keys()
    for key in keys:
        if key.startswith("nucl_"): #key: e.g. nucl_Cs137_2 
            nuclide = key.split("_")[1] #nuclide
            segment = key.split("_")[2] #segment
            
            if not nuclide in nuclides:
                nuclides.append(nuclide)
            
            nindex = nuclides.index(nuclide) 
            activities_validator[nindex, segment] = kwargs[key] #we save what user has entered
                      
            
    
    return nuclides, activities_validator        

def parse_segment_args(kwargs):
    """
    parses form and extracts parameters of segments - duration, vertical speed, heat capacity
    """    
    segments_count = int(kwargs["segments_count"])
    segments_properties = numpy.chararray((4, segments_count), itemsize=20) 
       
    for i in range(segments_count):
        segments_properties[0, i] = kwargs["seg_dur_%d" % i] #extraction of data from form
        segments_properties[1, i] = kwargs["seg_heat_%d" % i]
        segments_properties[2, i] = kwargs["seg_speed_%d" % i]
        segments_properties[3, i] = kwargs["seg_height_%d" % i]
            
        
    return segments_properties, segments_count #returns numpy.chararray

def numpy_array_to_tuple(a):
    """
    recurent function for conversion of numpy.array to tuple
    """
    try:
        return tuple(numpy_array_to_tuple(i) for i in a)
    except TypeError:
        return a

#####################LOAD SOURCE TERM
def get_source_term_table(username):
    """
    returns HTML table with source terms stored in database for a particular user
    """
    source_terms = dtst.get_list_of_source_terms(username)
    
    ret = """
    <p>
    <div style="overflow: scroll;">
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th valign="middle">Description</th>
            <th>Modified</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>        
    """
    index = 1
    length_limit = 300
    for st in source_terms:
        dsc = st["description"]
        if (len(dsc) > length_limit):
            dsc = dsc[:length_limit] + "..."
            
        modif = st["modified"]
        _id = st["_id"]
        action = get_load_st_actions(_id)
        ret += """
        <tr>
        <td>%d</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        </tr>
        """ % (index, dsc, tools.convert_epoch_to_time(modif), action)
        index += 1
  
    ret += """
    </tbody>
    </table>
    </div>
    """    
      
    return ret

def get_load_st_actions(_id):
    """
    returns HTML of load source term action button
    """
    ret = """
    <button class="btn btn-primary btn-small dropdown-toggle" data-toggle="dropdown" onclick="$('#myModalLoadST').modal('hide'); load_source_term('%s');">Load</span></button>
    <button class="btn btn-danger btn-small dropdown-toggle" data-toggle="dropdown" onclick="delete_source_term('%s');">Delete</span></button>
    """ % (_id, _id)
    
    return ret


###LOAD METEO DATA
def get_saved_meteo_table(username):
    """
    returns HTML table with meteo data stored in database for a particular user identified with username
    """
    meteos = dtm.get_list_of_meteos(username)
    
    ret = """
    <p>
    <div style="overflow: scroll;">
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No.</th>
            <th valign="middle">Description</th>
            <th>Modified</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>        
    """
    index = 1
    length_limit = 300
    for meteo in meteos:
        dsc = meteo["description"]
        if (len(dsc) > length_limit):
            dsc = dsc[:length_limit] + "..."
            
        modif = meteo["modified"]
        _id = meteo["_id"]
        action = get_load_meteo_actions(_id)
        ret += """
        <tr>
        <td>%d</td>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
        </tr>
        """ % (index, dsc, tools.convert_epoch_to_time(modif), action)
        index += 1
  
    ret += """
    </tbody>
    </table>
    </div>
    """    
      
    return ret


def get_load_meteo_actions(_id):
    """
    returns HTML of load meteo action button
    """
    ret = """
    <button class="btn btn-primary btn-small dropdown-toggle" data-toggle="dropdown" onclick="$('#myModalLoadMeteo').modal('hide'); load_meteo('%s');">Load</span></button>
    <button class="btn btn-danger btn-small dropdown-toggle" data-toggle="dropdown" onclick="delete_meteo('%s');">Delete</span></button>
    """ % (_id, _id)
    
    return ret