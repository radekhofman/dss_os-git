'''
Created on Aug 25, 2013
'''
import simplejson as sj
import db_tools_nucl_groups as dbtng
import logging
import types

def get_groups_json(**values):
    """
    Returns list of nuclides groups in JSON format.
    """
    logging.debug("Getting user nuclides groups '%s'" % (values))
    #Check if required attributes were passed
    nucl_groups_json = []
    if 'username' in values:
        nucl_groups = dbtng.get_list_of_groups(values['username'])
        for group in nucl_groups:
            nucl_group_json = {}
            nucl_group_json["id"] = str(group["_id"])
            nucl_group_json["name"] = group["name"]
            nucl_group_json["username"] = group["username"]
            nucl_group_json["description"] = group["description"]
            nucl_group_json["nuclides"] = group["nuclides"] if isinstance(group["nuclides"],types.ListType) else [group["nuclides"]] 
            nucl_groups_json.append(nucl_group_json)
    return sj.dumps(nucl_groups_json)

def create_group_json(**values):
    """
    Creates new nuclides groups.
    """
    logging.debug("Creating new nuclides group with params '%s'" % (values))
    new_group = {}
    result = {"id" : "-1"}
    if 'name' in values and 'nuclides[]' in values and 'username' in values:
        new_group['name'] = values['name']
        new_group['description'] = values['description']
        new_group['nuclides'] = values['nuclides[]']
        new_group['username'] = values['username']
        store_res = dbtng.create_group(new_group)
        result = {"id":str(store_res["id"]), "message":store_res["message"]}
    else:
        logging.error("One of required parameters was not filled.");
        result["message"] = "One of required parameters was not filled."
    return sj.dumps(result)

def delete_group_json(**values):
    """
    Deletes nuclides group by id.
    """
    logging.debug("Deleting nuclides group with params '%s'" % (values))
    if "group_id" in values:
        dbtng.delete_group(values["group_id"])
    else:
        logging.error("One of required parameters was not filled."); 
    

