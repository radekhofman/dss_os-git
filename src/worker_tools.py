# -*- encoding: UTF-8 -*-
"""
tools for updateting and managins list of registered workers in DB
"""
import admin_tools as at

db_workers = at.dbs['db_workers'] #this is common for all users

def update_worker_state(kwargs):
    """
    - updates state of a worker in workers' DB
    - only one method for update of worker's entry (if the worker identified with an username already exists) and
        ... creation of a new worker entry if the worker (uniquelly identifies with its username) in not present in DB
    """
    
    worker = db_workers.find_one({"worker_username": kwargs["worker_username"]}) #we use find one since the worker_username is unique
        if worker: #not None


if __name__ == '__main__':
    pass