# DSS OpenShift Edition
##  Introduction
The purpose of this application is to demonstrate that running of complex scientific models can be conducted also from the web environment using a small home-made cluster powered by multiple OpenShift gears.

Distributed computatinal framework originally developed for [DSS](https://dss.utia.cas.cz) was modified for deployment on [OpenShift](https://openshift.com) and powers a simple [Gaussian puff model](http://en.wikipedia.org/wiki/Atmospheric_dispersion_modeling) simulating aerial propagation of a radioactive noble gas [<sup>41</sup>Ar](http://en.wikipedia.org/wiki/Argon).

The framework runs on multiple [OpenShift](https://openshift.com) gears. One gear serves CherryPy web-server providing web interface and a task manager serving a task queue. Tasks inserted into the queue by users are processed by computatinal nodes (workers) hosted on different gears.

All source codes including deployment scripts were released under [GPL 2 licence](http://www.gnu.org/licenses/gpl-2.0.html) and are available on [Bitbucket](https://bitbucket.org/radekhofman/dss_os-git). As a great by-product, a [Cherrypy application template](https://bitbucket.org/krablak/dss-cherrypy-app-template) for an easy start of CherryPy application development on OpenShift was created.

## Workspace Structure
- **src** DSS application source for development
- **worker_src** workers source for development
- **deploy** applications prepared for deployment
	- *dss* DSS deploy directory
	- *worker* workers deploy directory
- **deploy-templates/dss** template for DSS OpenShift application structure
- *Gruntfile.js* build script for Grunt.js
- *package.json* build configuraton


## Deployment With Grunt.js
Deployment scripts and itself whole build process was created just to make the deployment such easy as it's on our projects running on Google App Engine == **Hit the button to get version deployed on servers**. Thats the whole goal.

### Overview
From the highlevel point of view looks the deployment process like this:
![Deployment Overview](https://dl.dropboxusercontent.com/u/78826/dssos/build-process.png)

The heart of deployment process is the Grunt.js powered build script which works like this:

![Deploy Script Detail](https://dl.dropboxusercontent.com/u/78826/dssos/grunt-detail.png)

And according to distributed infrastructure of DSS the deployment process is able to deploy all application components to the right gear:

![Infrastructure Deployment](https://dl.dropboxusercontent.com/u/78826/dssos/infrastructure-deploy.png)

### Prerequisites
At first **only** MacOS or Linux are supported for development and deployment. You also have to set up [node.js](http://nodejs.org/) and [Grunt.js](http://gruntjs.com/) on your machine if you haven't already.

### First Deploy
Before first deploy init the Grunt.js:

	cd to/project/directory 	//The one containing Gruntfile.js
	npm install

Configure file **package.json** to point on your gear Git repository:
	
	{
	  // Name of application could by changed	
	  "name": "DSSOS",
	  // Application version should be updated in case of modification
	  "version": "0.0.1",
	  // Path to DSS application python sources
	  "src_dss_dir": "./src/",
	  // Path to template of DSS openshift application
	  "deploy_dss_template_dir": "./deploy-template/app/",
	  // Work directory for DSS deploy scripts
	  "deploy_dss_out_dir": "./deploy/",
	  // Path to git repository to which the DSS application will be deployed
	  "deploy_git_repo": "ssh://52c4786e4382ec5b16000028@testpy-krablak.rhcloud.com/~/git/testpy.git/",
	  // Path to worker sources
	  "src_worker_dir": "./worker_src/",
	  // Work directory for worker deploy scripts
	  "deploy_worker_out_dir": "./deploy/worker/",
	  // List of repositories to which the worker application will be pushed
	  "deploy_worker_git_repos": [
    	"ssh://52d8d8cd4382ec344a000082@python-krablak.rhcloud.com/~/git/python.git/"
    	],
	  // Build number updated automatically by build scripts
	  "build": 1,
	  "devDependencies": {
	  ...

Run the deploy tasks:

	grunt
	
At this moment application should be deployed on your OpenShift gear. In case of any problem during deployment use:

	grunt --verbose
	
to get more informations from scripts execution.

### Repeated Deployment
Repeated deployment could be executed by running following command:

	grunt
	
### Grunt.js Tasks
Very short description of build tasks:

- **default** - default task prepares DSS application and workers and deploys it on OpenShift gear. Increments build number in *package.json*.
- **deploy_dss** - deploys DSS on OpenShift gear. Execution do not affect build number.
- **deploy_worker** - deploys worker on OpenShift gears. Execution do not affect build number.
- **deploy_dss_prepare** - prepares DSS application for deployment into *deploy/dss* directory. Execution do not affect build number.
- **deploy_dss_to_openshift** - deploys DSS application into gear repository. Execution do not affect build number.
- **deploy_worker_prepare** - prepares worker application for deployment into *dss/worker* directory. Execution do not affect build number.
- **deploy_worker_to_openshift** - deploys worker application into configured gear repositories. Execution do not affect build number.