module.exports = function(grunt) {

	// Project plugin configuration.
	grunt.initConfig({
				pkg : grunt.file.readJSON('package.json'),

				copy : {

					// Copy DSS deploy template
					deploy_dss_template : {
						files : [ {
							dot : true,
							expand : true,
							cwd : '<%= pkg.deploy_dss_template_dir %>',
							src : [ '**/*.!(pyc)', '**/*', '.git/**/*',
									'.openshift/**/*' ],
							dest : '<%= pkg.deploy_dss_out_dir %>',
						} ]
					},

					// DSS Sources to deploy template
					deploy_dss_sources : {
						files : [ {
							dot : true,
							expand : true,
							cwd : '<%= pkg.src_dss_dir %>',
							src : [ '**/*.!(pyc)' ],
							dest : '<%= pkg.deploy_dss_out_dir %>/wsgi/',

						} ]
					},

					// Worker sources to deploy
					deploy_worker_sources : {
						files : [ {
							dot : true,
							expand : true,
							cwd : '<%= pkg.src_worker_dir %>',
							src : [ '**/*.!(pyc)', '**/*', '.openshift/**/*' ],
							dest : '<%= pkg.deploy_worker_out_dir %>',

						} ]
					},

				},

				mkdir : {
					deploy_dss_application : {
						options : {
							create : [ '<%= pkg.deploy_dss_out_dir %>' ]
						},
					},
					deploy_worker_application : {
						options : {
							create : [ '<%= pkg.deploy_worker_out_dir %>' ]
						},
					},
				},

				rename : {

					// Renames openshift_app.py to application in deploy files
					deploy_dss_application : {
						src : '<%= pkg.deploy_dss_out_dir %>/wsgi/openshift_app.py',
						dest : '<%= pkg.deploy_dss_out_dir %>/wsgi/application'
					}

				},

				clean : {

					// Clean deploy directory
					dss_deploy : {
						src : '<%= pkg.deploy_dss_out_dir %>'
					},
					worker_deploy : {
						src : '<%= pkg.deploy_worker_out_dir %>'
					},

				},

				shell : {

					// Executes shell command which will commit and push deploy
					// DSS files to OpenShift
					deploy_dss_to_openshift : {
						command : [
								'cd <%= pkg.deploy_dss_out_dir %>',
								'pwd',
								'git init',
								'git status',
								'git add .',
								'git commit . -m "Deploy <%= pkg.name %> v<%= pkg.version %> build <%= pkg.build %>"',
								'git push <%= pkg.deploy_dss_git_repo %> --force',
				                ].join(';'),
						options : {
							stdout : true
						}
					},
          // Deploys worker files to Openshift
          deploy_worker_to_openshift : {
						command : [
								'cd <%= pkg.deploy_worker_out_dir %>',
								'pwd',
								'git init',
								'git status',
								'git add .',
								'git commit . -m "Deploy <%= pkg.name %> v<%= pkg.version %> build <%= pkg.build %>"',
								(function(){
									  grunt.log.writeln("Reading worker deploy repositories from package.json deploy_worker_git_repos");
					                  var result = "";
					                  var worker_repos = require('./package.json')['deploy_worker_git_repos'];
					                  for(var i = 0; i < worker_repos.length;i++){
					                	  result += "git push " + worker_repos[i] + " --force;";
					                	  grunt.log.writeln("Deploy repository : " + worker_repos[i]);
					                  }
					                  grunt.log.writeln("Push commands : " + result);
					                  return result;
					                })(),
								].join(';'),
						options : {
							stdout : true
						}
					},
				},

			'string-replace': {

				// Replace version in worker welcome file
				replace_version_worker:{
					files: {
						'<%= pkg.deploy_worker_out_dir %>wsgi/html_templates/welcome.html': '<%= pkg.deploy_worker_out_dir %>wsgi/html_templates/welcome.html'
					},
					options : {
						replacements: [{
					        pattern: /%%app.version%%/ig,
					        replacement: 'v<%= pkg.version %> build <%= pkg.build %>'
					      }]
					}
				},

				// Replace version in worker DSS footer file
				replace_version_dss:{
					files: {
						'<%= pkg.deploy_dss_out_dir %>wsgi/html_templates/footer.html': '<%= pkg.deploy_dss_out_dir %>wsgi/html_templates/footer.html'
					},
					options : {
						replacements: [{
					        pattern: /%%app.version%%/ig,
					        replacement: 'v<%= pkg.version %> build <%= pkg.build %>'
					      }]
					}
				},
				
				// Replace version and build number in assets name placeholders
				replace_assets_version_dss:{
					files: {
						'<%= pkg.deploy_dss_out_dir %>wsgi/html_templates/basic.html': '<%= pkg.deploy_dss_out_dir %>wsgi/html_templates/basic.html',
						'<%= pkg.deploy_dss_out_dir %>wsgi/web_html_puff.py': '<%= pkg.deploy_dss_out_dir %>wsgi/web_html_puff.py'
					},
					options : {
						replacements: [{
					        pattern: /%%asset.version%%/ig,
					        replacement: '<%= pkg.version %>.<%= pkg.build %>'
					      }]
					}
				}
			},
			
			concat: {
				    options: {
				      separator: ';'
				    },
				    
				    // Concates DSS thirparty scripts
				    dss_thirdparty: {
				      src: [
				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/thirdparty/bootstrap.js', 
				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/thirdparty/bootstrap-datetimepicker.min.js',
				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/thirdparty/bootstrap-tab.js',
				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/thirdparty/bootstrap-collapse.js',
				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/thirdparty/jquery.form.js',
				            ],
				      dest: '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/dss.thirdparty.js'
				    }
			},
			
			uglify: {
			    options: {
			      mangle: false,
			      preserveComments: false,
			      compress: true,
			      report: 'min'
			    },
			    
			    // Creates minified version of all DSS scripts
			    dss_scripts: {
			      files: {
			        '<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/dss.thirdparty.min.<%= pkg.version %>.<%= pkg.build %>.js': ['<%= pkg.deploy_dss_out_dir %>wsgi/static/scripts/dss.thirdparty.js']
			      }
			    }
			  },
			  
			  usemin: {
					 html: '<%= pkg.deploy_dss_out_dir %>**/*.html'
			  },
			  
			  cssmin: {
				  // Minifies main DSS
				  dss_main_css: {
				    files: {
				      '<%= pkg.deploy_dss_out_dir %>wsgi/static/css_styles/dss.min.<%= pkg.version %>.<%= pkg.build %>.css': [
				                                          				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/css_styles/bootstrap.css', 
				                                        				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/css_styles/bootstrap.ext.css',
				                                        				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/css_styles/forms.css',
				                                        				            '<%= pkg.deploy_dss_out_dir %>wsgi/static/css_styles/main.css',
				                                        				            ]
				    }
				  }
			  }

			});

	// Internal task incrementing build number
	grunt.registerTask('increment_build', function() {
		var pkg = grunt.file.readJSON('package.json');
		pkg.build = parseInt(pkg.build) + 1;
		grunt.file.write('package.json', JSON.stringify(pkg, null, 2));
	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-mkdir');
	grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-replace');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-git');
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-rename');
	grunt.loadNpmTasks('grunt-string-replace');

	// Prepares DSS application to deploy on OpenShift in deploy directory
	grunt.registerTask('deploy_dss_prepare', [
              'clean:dss_deploy',
              'mkdir:deploy_dss_application',
			  'copy:deploy_dss_template',
              'copy:deploy_dss_sources',
      		  'rename:deploy_dss_application',
      		  'string-replace:replace_assets_version_dss',
              'string-replace:replace_version_dss',
              'concat:dss_thirdparty',
      		  'uglify:dss_scripts',
      		  'cssmin:dss_main_css',
      		  'usemin'
            ]);
	// Deploys DSS application from deploy directory to OpenShift
	grunt.registerTask('deploy_dss_to_openshift', [
            'shell:deploy_dss_to_openshift'
            ]);

	// Prepares Worker application to deploy on OpenShift in deploy directory
	grunt.registerTask('deploy_worker_prepare', [
            'clean:worker_deploy',
            'mkdir:deploy_worker_application',
            'copy:deploy_worker_sources',
            'string-replace:replace_version_worker'
            ]);
	// Deploys Worker application from deploy directory to OpenShift
	grunt.registerTask('deploy_worker_to_openshift', [
            'shell:deploy_worker_to_openshift'
            ]);
	
	// Deploys DSS to OpenShift gear
	grunt.registerTask('deploy_dss', [
            'deploy_dss_prepare',
            'deploy_dss_to_openshift',
            ]);
	
	// Deploys Workers to OpenShift gear
	grunt.registerTask('deploy_workers', [
             'deploy_worker_prepare',
	         'deploy_worker_to_openshift'
            ]);

	// Default task prepares DSS and worker applications and deploys them on OpenShift gear
	grunt.registerTask('default', [
            'increment_build',
            'deploy_dss_prepare',
            'deploy_dss_to_openshift',
            'deploy_worker_prepare',
            'deploy_worker_to_openshift'
            ]);

};
